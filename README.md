SMILE Global
============

Please address any questions to nfreedman@smilec.org

=== Building

SMILE Global was built with Eclipse.

To build with Eclipse, download the latest version of Eclipse (v4.2 has been tested).
Import the project into Eclipse (Import > Existing Projects into Workspace).

To build the project, choose Build, or Build Automatically from the Eclipse project menu.

The project is built using the Vaadin v6 framework. Any changes to the widgets used 
(located in WebContent/VAADIN/widgetsets, and WEB-INF/lib) necessitates that the widgetset be rebuilt. 
To rebuild the widgetset, download the Vaadin plugin for Eclipse: https://vaadin.com/eclipse. The plug-in
creates a gear-icon with blue text '}>' in the corner, which will rebuild the widgetset. Automatic 
widgetset re-compilation can be turned on in the project properties > Vaadin menu.

=== Dependencies 

[Database]
A database is required for SMILE. The database .sql file is located in external/SmileWeb.sql. 
Database configuration is stored in src/hibernate.cfg.xml.

SMILE also requires read/write/execute permissions for the tomcat6/webapps/uploads directory

All other dependencies should be included.

=== Running
SMILE is built to run on Tomcat v6. There are two ways to run the project. It can be run
using Eclipse, via Run on Server. A Tomcat 6 server must be configured. 

Alternatively, a .WAR file can be exported from Eclipse by right-clicking on the project in 
the Project Explorer and choosing Export -> Export .WAR file. The WAR file contains all
files needed for deployment to any tomcat 6 server.

When running on Tomcat, SMILE is available at the url:
localhost:[tomcat port]/SmileWeb
Changing the name of the .WAR file deployed changes the servlet name

For Apache proxy forwarding to mask port 8080, update httpd.cnf as follows:

<VirtualHost *:80>
  ServerName smile.stanford.edu
  ServerAlias smile


    # Proxy specific settings
    ProxyRequests Off
    ProxyPreserveHost On

    ProxyPass /SmileWeb/VAADIN/ http://localhost:8080/SmileWeb/VAADIN/
    ProxyPassReverse /SmileWeb/VAADIN/ http://localhost:8080/SmileWeb/VAADIN/
    ProxyPass /SmileWeb/APP/ http://localhost:8080/SmileWeb/APP/
    ProxyPassReverse /SmileWeb/APP/ http://localhost:8080/SmileWeb/APP/
    ProxyPass /uploads/ http://localhost:8080/uploads/
    ProxyPassReverse /uploads/ http://localhost:8080/uploads/
    ProxyPass / http://localhost:8080/SmileWeb/
    ProxyPassReverse / http://localhost:8080/SmileWeb/
    ProxyPassReverseCookiePath /SmileWeb /

</VirtualHost>
 
=== Database Configuration 
  
  Update database settings in src/hibernate.cfg.xml


=== SMTP Configuration

  In webapps/SmileWeb/WEB-INF/classes/resources/conf.properties, assign values to MAIL_HOST, FROM_MAIL and PASSWORD 
  in order to use email services such as "forgot passward."
  
  # Host whose mail services will be used 
  # (Default value : stanford smtp server) 
  MAIL_HOST=

  # Return address to appear on emails 
  # (Default value : smile@stanford.edu) 
  FROM_MAIL=
  PASSWORD=password




