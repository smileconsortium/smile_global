#Users python 3
#Usage: the mysqldump from the development SQL database had table names in lowercase, given
#that the development database was case-insensitive. The SQL table on the production database
#requires CapitalCase table names if it is case-sensitive.

#Note: may need to convert .sql file to UTF-8 first

import codecs
import re

dir_path = "/Users/Noah/Documents/Programming/Eclipse/smile_global/external/"
file_in = dir_path + "smile_global_dump.sql"
file_out = dir_path + "smile_global_dump_CaseCorrected.sql"

f = codecs.open(file_in, "r", "utf-8")
out = codecs.open(file_out, "w", "utf-8")


for line in f:
    line = re.sub("`address`", "`Address`", line)
    line = re.sub("`admin`", "`Admin`", line)
    line = re.sub("`badge`", "`Badge`", line)
    line = re.sub("`country`", "`Country`", line)
    line = re.sub("`evite`", "`Evite`", line)
    line = re.sub("`grouporganizer`", "`GroupOrganizer`", line)
    line = re.sub("`grouptable`", "`GroupTable`", line)
    line = re.sub("`message`", "`Message`", line)
    line = re.sub("`msgrecipient`", "`MsgRecipient`", line)
    line = re.sub("`organization`", "`Organization`", line)
    line = re.sub("`passwordchangerequest`", "`PasswordChangeRequest`", line)
    line = re.sub("`question`", "`Question`", line)
    line = re.sub("`questionanswer`", "`QuestionAnswer`", line)
    line = re.sub("`questioncomment`", "`QuestionComment`", line)
    line = re.sub("`questiontag`", "`QuestionTag`", line)
    line = re.sub("`quiz`", "`Quiz`", line)
    line = re.sub("`quizquestion`", "`QuizQuestion`", line)
    line = re.sub("`tag`", "`Tag`", line)
    line = re.sub("`user`", "`User`", line)
    line = re.sub("`usergroup`", "`UserGroup`", line)
    line = re.sub("`userorganization`", "`UserOrganization`", line)
    line = re.sub("`userquestioninteraction`", "`UserQuestionInteraction`", line)
    line = re.sub("`userstats`", "`UserStats`", line)
    out.write(line);
out.close()
    
