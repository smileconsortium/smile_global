use smileglobal;

TRUNCATE Badge;

INSERT INTO `Badge` VALUES
(1,'First-Place','badges/trophy.png',20,20,0,0,0,'',0,0,0),
(2,"Teacher's Pet",'badges/question.png',0,10,0,0,0,'',0,0,0),
(3,'On The Mark','badges/pen.png',0,0,0,0,0,'',0,20,0), 
(4,'Writer','badges/pad.png',15,0,0,0,0,'',0,0,0),
(5,'Creator','badges/atom.png',50,0,0,0,0,'',0,0,0),
(6,"Gettin' Around",'badges/people.png',0,0,5,0,0,'',0,0,0),
(7,'Super-star','badges/star.png',15,0,0,0,0,'',0,0,4),
(8,'Media','badges/pallete.png',0,0,0,0,1,'',0,0,0),
(9,'Historian','badges/history.png',0,0,0,0,0,'history;5',0,0,0),
(10,'Mathematician','badges/math.png',0,0,0,0,0,'math;5',0,0,0),
(11,'Scientist','badges/science.png',0,0,0,0,0,'science;5',0,0,0),
(12,'Commentator','badges/ink.png',0,0,0,15,0,'',0,0,0),
(13,'Smooth Talker','badges/bubble.png',0,0,0,25,0,'',0,0,0),
(14,'Teacher','badges/apple.png',0,0,0,0,0,'',1,0,0);

DROP TABLE if Exists QuizOption;
CREATE TABLE QuizOption (
	id int unsigned NOT NULL AUTO_INCREMENT,
	quiz_id int unsigned NOT NULL,
	quiz_option varchar(255),
	quiz_value varchar(255),
	PRIMARY KEY(id)
);

insert into QuizOption (quiz_id, quiz_option, quiz_value) 
select distinct id, 'exam', '0' from Quiz;

alter table Question add uniqueRatings int unsigned;
update Question set uniqueRatings = uniqueAnswers;