/*WARNING - Will DROP All Tables in smileglobal Database. Only use for fresh creation.*/

/*Load countryCodes.sql to complete construction of new database*/

DROP DATABASE if exists smileglobal;
CREATE DATABASE smileglobal DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

use smileglobal;

DROP TABLE if Exists User;
CREATE TABLE User (
	id int unsigned NOT NULL AUTO_INCREMENT,
	username varchar(255) NOT NULL,
	password BLOB NOT NULL,
	email varchar(255) NOT NULL,
	firstName varchar(255) NOT NULL,
	lastName varchar(255) NOT NULL,
	phone varchar(20),
	photo_url varchar (255),
	userStats_id int unsigned NOT NULL,
	address_id int unsigned NOT NULL,
	userType tinyint unsigned NOT NULL default 1,
	profilePublic int unsigned NOT NULL default 1,
	PRIMARY KEY (id)
);

DROP TABLE if exists UserStats;
CREATE TABLE UserStats (
	id int unsigned NOT NULL AUTO_INCREMENT,
	createdOn datetime,
	lastAccessed datetime,
	cumulativeSecs int unsigned default 0,
	numSessions int unsigned default 0,
	avgSessionSecs int unsigned default 0,	
	PRIMARY KEY(id)
);

DROP TABLE if exists Evite;
CREATE TABLE Evite (
	id int unsigned NOT NULL AUTO_INCREMENT,
	user_id int unsigned NOT NULL,
	group_id int unsigned NOT NULL,
	token varchar(50) NOT NULL,
	invitedOn datetime,
	PRIMARY KEY(id)
);

DROP TABLE if exists PasswordChangeRequest;
CREATE TABLE PasswordChangeRequest (
	id int unsigned NOT NULL AUTO_INCREMENT,
	user_id int unsigned NOT NULL,
	token varchar(50) NOT NULL,
	createdOn datetime,	
	PRIMARY KEY(id)
);

DROP TABLE if Exists Organization;
CREATE TABLE Organization (
	id int unsigned NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	address_id int unsigned,
	phone varchar(255),
	website varchar(255),
	orgType varchar(255),
	PRIMARY KEY(id)
);

DROP TABLE if Exists UserOrganization;
CREATE TABLE UserOrganization (
	user_id int unsigned NOT NULL,
	organization_id int unsigned NOT NULL,
	joinedOn datetime,
	PRIMARY KEY (user_id, organization_id)
);

DROP TABLE if Exists Address;
CREATE TABLE Address (
	id int unsigned NOT NULL AUTO_INCREMENT,
	street varchar(255),
	city varchar(255),
	region varchar(255),
	country_id smallint unsigned,
	postalCode varchar(25),
	PRIMARY KEY (id)
);

DROP TABLE if exists Question;
CREATE TABLE Question (
	id int unsigned NOT NULL AUTO_INCREMENT,
	user_id int unsigned NOT NULL,
	questionText text NOT NULL,
	media_url varchar(255),
	/*if these ENUM values are changed, org.smilec.smile.global.hibernate.Question.java must also be updated*/
	contentType enum('audio', 'video', 'image', 'document'), 
	isPublic boolean not null default 1,
	createdOn datetime NOT NULL,
	uniqueAnswers int unsigned,
	avgRating float (7,4),
	avgCorrect float (7,4),
	avgSecComplete float(7,4),
	tagText varchar(255) NOT NULL,
	FULLTEXT(questionText, tagText),
	FULLTEXT(tagText),
	PRIMARY KEY(id)
) ENGINE=MyISAM; /*InnoDB doesn't support fulltext index in MySQL 5.5*/

DROP TABLE if exists QuestionAnswer;
CREATE TABLE QuestionAnswer(
	id int unsigned NOT NULL AUTO_INCREMENT,
	question_id int unsigned NOT NULL,
	answerText text,
	isCorrect boolean NOT NULL DEFAULT 0,
	numResponses int unsigned NOT NULL DEFAULT 0,
	FULLTEXT(answerText),
	PRIMARY KEY (id)
) ENGINE=MyISAM; /*InnoDB doesn't support fulltext index in MySQL 5.5*/

DROP TABLE if exists UserQuestionInteraction;
CREATE TABLE UserQuestionInteraction (
	id int unsigned NOT NULL AUTO_INCREMENT,
	user_id int unsigned NOT NULL,
	question_id int unsigned NOT NULL,
	userAnswer_id int unsigned,
	createdOn datetime,
	rating tinyint unsigned default NULL,
	secComplete int unsigned default NULL,
	PRIMARY KEY (id)
);

DROP TABLE if exists QuestionComment;
CREATE TABLE QuestionComment (
	id int unsigned NOT NULL AUTO_INCREMENT,
	user_id int unsigned NOT NULL,
	question_id int unsigned NOT NULL,	
	createdOn datetime,
	commentText varchar(255) default "",
	PRIMARY KEY (id)
);

DROP TABLE if exists Tag;
CREATE TABLE Tag (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  tagText varchar(255) NOT NULL,
  FULLTEXT(tagText),
  PRIMARY KEY (id),
  UNIQUE(tagText)
) ENGINE=MyISAM;

DROP TABLE if exists QuestionTag;
CREATE TABLE QuestionTag (
  question_id INT UNSIGNED NOT NULL REFERENCES Question,
  tag_id INT UNSIGNED NOT NULL REFERENCES Tag,
  PRIMARY KEY (question_id, tag_id)
);

DROP TABLE if exists GroupTable; /*Why not just 'Group'? Group is a reserved MySQL keyword */
CREATE TABLE GroupTable (
	id int unsigned NOT NULL AUTO_INCREMENT,
	organization_id int unsigned,
	name varchar(255) NOT NULL,
	passcode varchar(50) NOT NULL,
	organizer_id int unsigned NOT NULL,
	createdOn datetime,
	PRIMARY KEY(id)
);

DROP TABLE if exists UserGroup;
CREATE TABLE UserGroup (
	user_id int unsigned NOT NULL,
	group_id int unsigned NOT NULL,
	PRIMARY KEY(user_id, group_id)
);

DROP TABLE if exists GroupOrganizer;
CREATE TABLE GroupOrganizer (
	user_id int unsigned NOT NULL,
	group_id int unsigned NOT NULL,
	PRIMARY KEY(user_id, group_id)
);

DROP TABLE if exists Quiz;
CREATE TABLE Quiz (
	id int unsigned NOT NULL AUTO_INCREMENT,
	creator_id int unsigned NOT NULL,
	group_id int unsigned NOT NULL,
	name varchar(255),
	isPublic boolean DEFAULT 1,
	/*any changes to enum requires that org.smilec.smile.global.hibernate.Quiz.java also be updated*/
	status ENUM('create', 'solve', 'show_result', 'closed', 'async', 'syncTimed'),
	startTime datetime DEFAULT null,
	createMinutes int unsigned DEFAULT null,
	solveMinutes int unsigned DEFAULT null,
	showFeedback boolean DEFAULT 0, /*determines whether user can see if they got an answer correct or not*/
	createdOn datetime,
	PRIMARY KEY(id)
);

DROP TABLE if exists QuizQuestion;
CREATE TABLE QuizQuestion (
	quiz_id int unsigned NOT NULL,
	question_id int unsigned NOT NULL,
	PRIMARY KEY(quiz_id, question_id)
);

/*Country State data*/ 
DROP TABLE if Exists Country;
CREATE TABLE Country (
	id smallint unsigned NOT NULL,
	name varchar(255) NOT NULL,
	iso2 varchar(2),
	iso3 varchar(3),
	PRIMARY KEY (id)
);
Insert into Country (name, id) VALUES ("Afghanistan",2),("Albania",4),("Algeria",5),("Andorra",7),("Angola",8),("Antigua and Barbuda",11),("Argentina",12),("Armenia",13),("Australia",15),("Austria",16),("Azerbaijan",17),("Bahamas",18),("Bahrain",21),("Bangladesh",22),("Barbados",23),("Belarus",24),("Belgium",25),("Belize",26),("Benin",27),("Bermuda",28),("Bhutan",29),("Bolivia",30),("Bonaire",700),("Bosnia and Herzegovina",31),("Botswana",32),("Brazil",34),("Brunei Darussalam",36),("Bulgaria",37),("Burkina Faso",38),("Burundi",39),("Cambodia",40),("Cameroon",41),("Canada",42),("Cape Verde",43),("Cayman Islands",44),("Central African Republic",45),("Chad",46),("Chile",47),("China",48),("Colombia",51),("Comoros",52),("Congo",53),("Congo, The Democratic Republic Of The",54),("Costa Rica",56),("Croatia",58),("Cuba",59),("Cyprus",60),("Czech Republic",61),("Côte D\Ivoire",57),("Denmark",62),("Djibouti",63),("Dominica",64),("Dominican Republic",65),("Ecuador",66),("Egypt",67),("El Salvador",68),("Equatorial Guinea",69),("Eritrea",70),("Estonia",71),("Ethiopia",73),("Fiji",76),("Finland",77),("France",78),("French Southern Territories",82),("Gabon",83),("Gambia",84),("Georgia",1),("Germany",85),("Ghana",86),("Greece",88),("Greenland",89),("Grenada",90),("Guatemala",93),("Guinea",95),("Guinea-Bissau",96),("Guyana",97),("Haiti",98),("Honduras",101),("Hungary",103),("Iceland",104),("India",105),("Indonesia",106),("Iran, Islamic Republic Of",107),("Iraq",108),("Ireland",109),("Israel",111),("Italy",112),("Jamaica",113),("Japan",114),("Jordan",116),("Kazakhstan",117),("Kenya",118),("Kiribati",119),("Korea, Democratic People's Republic Of",121),("Korea, Republic of",120),("Kuwait",122),("Kyrgyzstan",123),("Lao People's Democratic Republic",124),("Latvia",125),("Lebanon",126),("Lesotho",127),("Liberia",128),("Libya",129),("Liechtenstein",130),("Lithuania",131),("Luxembourg",132),("Macao",133),("Macedonia, the Former Yugoslav Republic Of",134),("Madagascar",135),("Malawi",136),("Malaysia",137),("Maldives",138),("Mali",139),("Malta",140),("Marshall Islands",141),("Mauritania",143),("Mauritius",144),("Mexico",146),("Micronesia, Federated States Of",147),("Moldova, Republic of",148),("Monaco",149),("Mongolia",150),("Montenegro",151),("Morocco",153),("Mozambique",154),("Myanmar",155),("Namibia",156),("Nauru",157),("Nepal",158),("Netherlands",159),("New Zealand",162),("Nicaragua",163),("Niger",164),("Nigeria",165),("Norway",169),("Oman",170),("Pakistan",171),("Palau",172),("Palestinian Territory",173),("Panama",175),("Papua New Guinea",176),("Paraguay",177),("Peru",178),("Philippines",180),("Poland",183),("Portugal",184),("Qatar",186),("Romania",189),("Russian Federation",190),("Rwanda",191),("Saint Helena",192),("Saint Kitts And Nevis",193),("Saint Vincent And The Grenadines",195),("Samoa",196),("San Marino",197),("Sao Tome and Principe",198),("Saudi Arabia",199),("Senegal",200),("Serbia",201),("Seychelles",202),("Sierra Leone",203),("Singapore",205),("Slovakia",206),("Slovenia",207),("Solomon Islands",208),("Somalia",209),("South Africa",210),("South Sudan",703),("Spain",212),("Sri Lanka",213),("Sudan",214),("Suriname",215),("Swaziland",217),("Sweden",218),("Switzerland",219),("Syrian Arab Republic",220),("Taiwan",221),("Tajikistan",222),("Tanzania",223),("Thailand",224),("Timor-Leste",226),("Togo",227),("Tonga",229),("Trinidad and Tobago",230),("Tunisia",231),("Turkey",232),("Turkmenistan",233),("Tuvalu",235),("Uganda",236),("Ukraine",237),("United Arab Emirates",238),("United Kingdom",239),("United States",240),("United States Minor Outlying Islands",241),("Uruguay",242),("Uzbekistan",243),("Vanuatu",244),("Venezuela, Bolivarian Republic of",245),("Viet Nam",247),("Western Sahara",251),("Yemen",252),("Zambia",255),("Zimbabwe",256);


DROP TABLE if Exists Admin;
CREATE TABLE Admin (
	id int(11) unsigned NOT NULL AUTO_INCREMENT,
	username varchar(255) NOT NULL,
	password BLOB NOT NULL,
	email varchar(255) DEFAULT "",
	firstName varchar(255) DEFAULT "",
	lastName varchar(255) DEFAULT "",
	phone varchar(20) DEFAULT "",
	PRIMARY KEY (id),
    UNIQUE (username)
);

DROP TABLE if exists Badge;
CREATE TABLE Badge (
	id int unsigned NOT NULL AUTO_INCREMENT,
	title varchar(255) DEFAULT "",
	image varchar (255) DEFAULT "",
	questionCreated int unsigned NOT NULL default 0,
	questionAnswered int unsigned NOT NULL default 0,
	sessions int unsigned NOT NULL default 0,
	comments int unsigned NOT NULL default 0,
	qMedia int unsigned NOT NULL default 0,
	createdTag varchar(255) DEFAULT "", /*in format tagName;tagCount, as in 'science;5'*/
	groupCreated int unsigned NOT NULL default 0,
	correctAnswer int unsigned NOT NULL default 0,
	avgRatingQ int unsigned NOT NULL default 0,
	PRIMARY KEY(id)
);

INSERT into Badge (title, questionCreated, questionAnswered, sessions, comments, createdTag, qMedia, groupCreated, correctAnswer, avgRatingQ, image) VALUES
("Scientist", 0, 0, 0, 0, 'science;5', 0, 0, 0, 0, 'trophies/beaker.png'),
("Author", 0, 0, 0, 40, '', 0, 0, 0, 0, 'trophies/books.png'),
("Mathematician", 0, 0, 0, 0, 'math;10', 0, 0, 0, 0, 'trophies/calculator.png'),
("Math Genius", 0, 0, 0, 0, 'math;50', 0, 0, 0, 0, 'trophies/calculator2.png'),
("Photographer", 0, 0, 0, 0, '', 20, 0, 0, 0, 'trophies/camera.png'),
("Historian", 0, 0, 0, 0, 'history;10', 0, 0, 0, 0, 'trophies/castle.png'),
("King of Correct", 0, 0, 0, 0, '', 0, 0, 1000, 0, 'trophies/crown.png'),
("Media Pro", 0, 0, 0, 0, '', 100, 0, 0, 0, 'trophies/film.png'),
("Commentator", 0, 0, 0, 15, '', 0, 0, 0, 0, 'trophies/megaphone.png'),
("Group Painter", 0, 0, 0, 0, '', 0, 4, 0, 0, 'trophies/paint.png'),
("Question Beginner", 25, 0, 0, 0, '', 0, 0, 0, 0, 'trophies/q-ball-blue.png'),
("Question Expert", 125, 0, 0, 0, '', 0, 0, 0, 0, 'trophies/q-ball-green.png'),
("Question Guru", 250, 0, 0, 0, '', 0, 0, 0, 0, 'trophies/q-ball-orange.png'),
("Answerer Beginner", 0, 50, 0, 0, '', 0, 0, 0, 0, 'trophies/q-marker-blue.png'),
("Answerer Expert", 0, 250, 0, 0, '', 0, 0, 0, 0, 'trophies/q-marker-green.png'),
("Answerer Guru", 0, 750, 0, 0, '', 0, 0, 0, 0, 'trophies/q-marker-orange.png'),
("Session Star", 0, 0, 15, 0, '', 0, 0, 0, 0, 'trophies/ribbon-gold.png'),
("Group Builder", 0, 0, 0, 0, '', 0, 12, 0, 0, 'trophies/tools.png'),
("First Place", 0, 0, 0, 0, '', 0, 0, 100, 0, 'trophies/trophy-gold.png'),
("Super-Star", 10, 0, 0, 0, '', 0, 0, 0, 4.5, 'trophies/trophy-stars.png');
	
DROP TABLE if exists Message;
CREATE TABLE Message (
	id int unsigned NOT NULL AUTO_INCREMENT,
	sender_id int unsigned NOT NULL,
	subject varchar(255) NOT NULL,
	message BLOB NOT NULL,
	sentOn datetime NOT NULL,
	replyTo int unsigned DEFAULT 0, /*the id of the message that this message is replying to if reply*/
	messageType int unsigned DEFAULT 0, /*0 = message, 1 = evite*/
	metaData varchar(255) DEFAULT NULL,
	senderDeleted boolean default 0,
	PRIMARY KEY(id)
);

DROP TABLE if exists MsgRecipient;
CREATE TABLE MsgRecipient (
	mr_id int unsigned NOT NULL AUTO_INCREMENT,
	msg_id int unsigned NOT NULL,
	recipient_id int unsigned NOT NULL,
	recipientDeleted boolean default 0,
	isRead boolean DEFAULT 0,
	PRIMARY KEY(mr_id)
);

/*Type Lists*/ /*
DROP TABLE if Exists UserType;
CREATE TABLE UserType (
	id int unsigned NOT NULL AUTO_INCREMENT,
	type varchar(255) NOT NULL,
	visible boolean DEFAULT 0,
	PRIMARY KEY (id)
);
INSERT into UserType (type, visible) VALUES 
	('Student',1),
	('Teacher',1),
	('Corporate',1),
	('Non-Profit',1),
	('Other',1);

DROP TABLE if Exists UserSubType;
CREATE TABLE UserSubType (
	id int unsigned NOT NULL AUTO_INCREMENT,
	type_id int unsigned NOT NULL,
	subtype varchar(255) NOT NULL,
	visible boolean DEFAULT 0,
	PRIMARY KEY (id)
);
INSERT into UserSubType (type_id, subtype, visible) VALUES 
	(1,'University',1),
	(1,'High School',1),
	(1,'Middle School',1),
	(1,'Elementary School',1),
	(2,'University',1),
	(2,'High School',1),
	(2,'Middle School',1),
	(2,'Elementary School',1);

DROP TABLE if Exists OrganizationType;
CREATE TABLE OrganizationType (
	id int unsigned NOT NULL AUTO_INCREMENT,
	type varchar(255) NOT NULL,
	visible boolean DEFAULT 0,
	PRIMARY KEY (id)
);
INSERT into OrganizationType (type, visible) VALUES 
	('School',1),
	('University',1),
	('Business',1),
	('Non-Profit',1),
	('Governmental',1),
	('Other',1);

DROP TABLE if Exists OrgSubType;
CREATE TABLE OrgSubType (
	id int unsigned NOT NULL AUTO_INCREMENT,
	type_id int unsigned NOT NULL,
	subtype varchar(255) NOT NULL,
	visible boolean DEFAULT 0,
	PRIMARY KEY (id)
);
INSERT into OrgSubType (type_id, subtype, visible) VALUES 
	(1,'High School',1),
	(1,'Middle School',1),
	(1,'Elementary School',1);

*/
