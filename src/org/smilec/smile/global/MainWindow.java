package org.smilec.smile.global;

import org.smilec.smile.global.working.CustomComponentSmartScroll;

import com.vaadin.terminal.URIHandler;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.UriFragmentUtility.FragmentChangedEvent;
import com.vaadin.ui.UriFragmentUtility.FragmentChangedListener;
import com.vaadin.ui.Window;

public class MainWindow extends Window implements URIHandler, FragmentChangedListener {
	private SmileApplication app = null;

	public MainWindow(SmileApplication app) {
		this.app = app;

		//this.showNotification("Welcome to SMILE!", "SMILE is an innovative tool that allows users to learn, exchange, and test their knowledge by creating, sharing, and rating questions and answers. Click to get started!", Notification.TYPE_ERROR_MESSAGE);

		/*addListener(new Window.ResizeListener() {
			@Override
			public void windowResized(ResizeEvent e) {
				resized(e);
			}
		});*///manage resizing through js layer
	}

	//overload setContent for views that implement customComponentSmartScroll. 
	//Every time content is changed, call external resize.js function to set minimum window size
	public void setContent(CustomComponent newContent) {
		super.setContent(newContent);
	}

	/*public void initUrifu() { //initialize URI fragment utility
		getCurrentMainLayout().addComponent(urifu);
		urifu.addListener(this);
	}*/

	@Override
	public void fragmentChanged(FragmentChangedEvent source) {
		String fragmentString = source.getUriFragmentUtility().getFragment();
		if (fragmentString != null) {
			//System.out.println("URI Fragment: " + fragmentString);
		}
	}

	/*private void resized(ResizeEvent e) {
		//this.showNotification(getWindow().getWidth() + " x " + getWindow().getHeight(), null);
		System.out.println("MainWindow: " + getWindow().getWidth() + " x " + getWindow().getHeight());
		//Resize height of main vertical layout to ensure scroll bar shows up when needed, but that content can still expand to bottom of screen
		getCurrentMainLayout().setHeight(
				Utils.greaterOfTwo(getWindow().getHeight() - 5, getCurrentView().getMinPanelHeight())
						+ "px");
		getCurrentView().setHeight(
				Utils.greaterOfTwo(getWindow().getHeight() - 5, getCurrentView().getMinPanelHeight())
						+ "px");
	}*/

	private CustomComponentSmartScroll getCurrentView() {
		return (CustomComponentSmartScroll) getContent();
	}

	/*public void setUriFragment(String frag) {
		urifu.setFragment(frag, true);
	}

	public String getUriFragment() {
		return urifu.getFragment();
	}

	public UriFragmentUtility getUrifu() {
		return urifu;
	}*/
}
