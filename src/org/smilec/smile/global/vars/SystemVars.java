package org.smilec.smile.global.vars;

public class SystemVars {

	public static final int minAnswers = 2; //minimum number of answers allowed
	public static final int defaultAnswers = 4; //minimum number of answers allowed
	public static final int maxAnswers = 6; //max answers allowed on a question
	public static final double maxRating = 5; //maxrating
	public static final double minRating = 1; //minrating
	public static final double ratingIncrement = 0.5;
}
