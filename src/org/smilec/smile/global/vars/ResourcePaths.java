package org.smilec.smile.global.vars;

import com.vaadin.terminal.Resource;
import com.vaadin.terminal.ThemeResource;

public class ResourcePaths {
	public static final String httpFolder = "";

	public static final Resource redError = new ThemeResource("assets/images/redError.png");
	public static final Resource greenCheck = new ThemeResource("assets/images/greenCheck.png");
	public static final Resource errorNotificationIcon =
			new ThemeResource("icons/errorNotification.png");
	public static final Resource user0TempImage = new ThemeResource("assets/images/users/user0.png");
	public static final Resource user1TempImage = new ThemeResource("assets/images/users/user1.png");
	//Rounded bg enabled and disabled
	//enabled
	public static final Resource roundedBgTL0 = new ThemeResource("assets/images/roundedBg_topLeft.png");
	public static final Resource roundedBgTM0 = new ThemeResource(
			"assets/images/roundedBg_topMiddle.png");
	public static final Resource roundedBgTR0 =
			new ThemeResource("assets/images/roundedBg_topRight.png");
	public static final Resource roundedBgM0 = new ThemeResource("assets/images/roundedBg_middle.png");
	public static final Resource roundedBgBL0 = new ThemeResource(
			"assets/images/roundedBg_bottomLeft.png");
	public static final Resource roundedBgBM0 = new ThemeResource(
			"assets/images/roundedBg_bottomMiddle.png");
	public static final Resource roundedBgBR0 = new ThemeResource(
			"assets/images/roundedBg_bottomRight.png");
	//disabled
	public static final Resource roundedBgTL1 =
			new ThemeResource("assets/images/roundedBg_topLeft1.png");
	public static final Resource roundedBgTM1 = new ThemeResource(
			"assets/images/roundedBg_topMiddle1.png");
	public static final Resource roundedBgTR1 = new ThemeResource(
			"assets/images/roundedBg_topRight1.png");
	public static final Resource roundedBgM1 = new ThemeResource("assets/images/roundedBg_middle1.png");
	public static final Resource roundedBgBL1 = new ThemeResource(
			"assets/images/roundedBg_bottomLeft1.png");
	public static final Resource roundedBgBM1 = new ThemeResource(
			"assets/images/roundedBg_bottomMiddle1.png");
	public static final Resource roundedBgBR1 = new ThemeResource(
			"assets/images/roundedBg_bottomRight1.png");
	//blue - for answered questions
	public static final Resource roundedBgTL_blue = new ThemeResource(
			"assets/images/roundedBg_topLeft_blue.png");
	public static final Resource roundedBgTM_blue = new ThemeResource(
			"assets/images/roundedBg_topMiddle_blue.png");
	public static final Resource roundedBgTR_blue = new ThemeResource(
			"assets/images/roundedBg_topRight_blue.png");
	public static final Resource roundedBgM_blue = new ThemeResource(
			"assets/images/roundedBg_middle_blue.png");
	public static final Resource roundedBgBL_blue = new ThemeResource(
			"assets/images/roundedBg_bottomLeft_blue.png");
	public static final Resource roundedBgBM_blue = new ThemeResource(
			"assets/images/roundedBg_bottomMiddle_blue.png");
	public static final Resource roundedBgBR_blue = new ThemeResource(
			"assets/images/roundedBg_bottomRight_blue.png");
	//red - for incorrectly answered questions
	public static final Resource roundedBgTL_red = new ThemeResource(
			"assets/images/roundedBg_topLeft_red.png");
	public static final Resource roundedBgTM_red = new ThemeResource(
			"assets/images/roundedBg_topMiddle_red.png");
	public static final Resource roundedBgTR_red = new ThemeResource(
			"assets/images/roundedBg_topRight_red.png");
	public static final Resource roundedBgM_red = new ThemeResource(
			"assets/images/roundedBg_middle_red.png");
	public static final Resource roundedBgBL_red = new ThemeResource(
			"assets/images/roundedBg_bottomLeft_red.png");
	public static final Resource roundedBgBM_red = new ThemeResource(
			"assets/images/roundedBg_bottomMiddle_red.png");
	public static final Resource roundedBgBR_red = new ThemeResource(
			"assets/images/roundedBg_bottomRight_red.png");
	//green - for correctly answered questions
	public static final Resource roundedBgTL_green = new ThemeResource(
			"assets/images/roundedBg_topLeft_green.png");
	public static final Resource roundedBgTM_green = new ThemeResource(
			"assets/images/roundedBg_topMiddle_green.png");
	public static final Resource roundedBgTR_green = new ThemeResource(
			"assets/images/roundedBg_topRight_green.png");
	public static final Resource roundedBgM_green = new ThemeResource(
			"assets/images/roundedBg_middle_green.png");
	public static final Resource roundedBgBL_green = new ThemeResource(
			"assets/images/roundedBg_bottomLeft_green.png");
	public static final Resource roundedBgBM_green = new ThemeResource(
			"assets/images/roundedBg_bottomMiddle_green.png");
	public static final Resource roundedBgBR_green = new ThemeResource(
			"assets/images/roundedBg_bottomRight_green.png");
	//topButtons
	public static final ThemeResource homeIcon = new ThemeResource("icons/homeIcon.png");
	public static final ThemeResource publicIcon = new ThemeResource("icons/publicIcon.png");
	public static final ThemeResource groupsIcon = new ThemeResource("icons/smileIcon.png");
	public static final ThemeResource profileIcon = new ThemeResource("icons/profileIcon.png");
	public static final ThemeResource messageIcon = new ThemeResource("icons/messageIcon.png");
}
