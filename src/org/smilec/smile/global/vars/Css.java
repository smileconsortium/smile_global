package org.smilec.smile.global.vars;

public class Css {
	public static final String userName = "usernameText";
	public static final String userName_disabled = "usernameText_disabled";
	public static final String defaultRed = "defaultRed";
	public static final String noBorders = "noBorders";
	public static final String smallFont = "smallFont";
	public static final String tableNoResize = "noResize";
	public static final String ratingStarLarge = "large";
	public static String ratingStarYellow = "userAnswer";
	public static String red = "red";

	public static String styleUsername(String username) {
		return "<strong><font color='#167CDA'>" + username + "</font></strong>";
	}

	public static String highlightRed(String username) {
		return "<strong><font color='#167CDA'>" + username + "</font></strong>";
	}
}
