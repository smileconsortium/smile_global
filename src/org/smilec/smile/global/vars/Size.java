package org.smilec.smile.global.vars;

public class Size {
	public static final int avatarWidth = 40;
	public static final int avatarHeight = 40;
	public static final int maxCharsQuestionText = 10000;
	public static final int maxCharsQuestionAnswerText = 255;
	public static final int maxCharsCommentText = 180;
	public static final int questionDisplayHeight = 65;
	public static final int maxCharsGroupName = 100;
	public static final int maxCharsQuizName = 100;
	public static final int maxMsgSubject = 255;
	public static final int maxMsg = 5000;
}
