package org.smilec.smile.global.media;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.Question.ContentTypeEnum;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.FileResource;
import com.vaadin.terminal.Sizeable;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Video;

//shared methods for media manipulation, uploading images for questions, displaying question images, etc.

public class MediaManager {
	//constants
	//mediaSelect options group options text / values
	private static final int videoHeight = 400;

	//image will be resized to fit within bounds
	private static final int maxImgWidth = 480;
	private static final int maxImgHeight = 600;

	//file path vars
	private String media_url;
	private String question_url;
	private String mediaUser_url;
	private String user_url;
	private String tmp, tmpImage;

	public MediaManager() {
		SmileApplication smile = AppData.getApplication();

		media_url = smile.media_url;
		question_url = smile.question_url;
		user_url = smile.user_url;
		mediaUser_url = smile.mediaUser_url;
		tmp = "uploadedMedia";
		tmpImage = "tmp_uploadMedia";

		/*
		System.out.println("media url: " + media_url);
		System.out.println("question url: " + question_url);
		System.out.println("user url: " + user_url);
		*/
	}

	public String getMedia_url() {
		return media_url;
	}

	public String getQuestion_url() {
		return question_url;
	}

	public String getMediaUser_url() {
		return mediaUser_url;
	}

	public String getUser_url() {
		return user_url;
	}

	public String getTmp() {
		return tmp;
	}

	public String getTmpImage() {
		return tmpImage;
	}

	public static void
			setImageSize(BufferedImage img, int targetWidth, int targetHeight, Embedded image) {
		int sourceWidth = img.getWidth();
		int sourceHeight = img.getHeight();
		int requiredWidth, requiredHeight;
		float targetRatio = (float) targetWidth / (float) targetHeight;
		float sourceRatio = (float) sourceWidth / (float) sourceHeight;
		/*System.out.println("sourceWidth: "+sourceWidth);		  
		System.out.println("sourceHeight: "+sourceHeight);
		System.out.println("targetWidth: "+targetWidth);		  
		System.out.println("targetHeight: "+targetHeight);
		System.out.println("targetRatio: "+targetRatio);		  
		System.out.println("sourceRatio: "+sourceRatio);*/

		if (sourceRatio >= targetRatio) { // source is wider than target in proportion
			requiredWidth = targetWidth;
			requiredHeight = (int) (requiredWidth / sourceRatio);
		} else { // source is higher than target in proportion
			requiredHeight = targetHeight;
			requiredWidth = (int) (requiredHeight * sourceRatio);
		}
		//System.out.println("requiredWidth: "+requiredWidth);		  
		//System.out.println("requiredHeight: "+requiredHeight);
		image.setWidth(requiredWidth, Sizeable.UNITS_PIXELS);
		image.setHeight(requiredHeight, Sizeable.UNITS_PIXELS);
	}

	public boolean saveEmbeddedQuestionMedia(Question question, String youtube) {
		if (youtube != null && youtube.trim().length() > 0) {
			//youtube = youtube.replace("watch?v=", "v/");
			int idx1 = youtube.indexOf("v=");
			String str = youtube.substring(idx1);
			int idx2 = str.indexOf("&");
			if (idx2 != -1) {
				str = str.substring(2, idx2);
			} else {
				str = str.substring(2);
			}

			youtube = "http://www.youtube.com/v/" + str;

			question.setMedia_url(youtube);
			question.setContentType(ContentTypeEnum.document);
			return true;
		} else
			return false;
	}

	public boolean saveUploadedQuestionMedia(Question question, File file, String originalUploadName) {
		//set's the media url of a Question, but does not update or save that Question
		//with Hibernate
		if (file == null || question == null) {
			return false;
		}
		String filename = file.getName();
		String filename_lower = filename.toLowerCase();
		if (filename != null && filename.length() > 0) {
			String old_name = question.getMedia_url();

			if (old_name != null && old_name.length() > 0) {
				//delete old user upload file
				//file is identified by taking extension from the filename stored in Question table
				//and by question number
				try {
					String ext = old_name.substring(old_name.lastIndexOf('.'));
					File media_file = new File(question_url + question.getId() + ext);

					if (media_file.delete()) {
						//System.out.println(media_file.getName() + " is deleted!");
					} else {
						//System.out.println("Delete operation is failed.");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				String ext = filename.substring(filename.lastIndexOf('.'));

				//reduce image size to max dimensions:
				boolean resize = false;
				BufferedImage resized = null;
				if (filename_lower.endsWith(".png") || filename_lower.endsWith(".jpg")
						|| filename_lower.endsWith(".gif")) {
					try {
						BufferedImage bimg = ImageIO.read(file);
						int width = bimg.getWidth();
						int height = bimg.getHeight();
						if (width > maxImgWidth || height > maxImgHeight) {
							resize = true;
							resized =
									Scalr.resize(bimg, Scalr.Method.QUALITY, Scalr.Mode.AUTOMATIC,
											maxImgWidth, maxImgHeight, Scalr.OP_ANTIALIAS);
						}
					} catch (Exception e) {

					}
				}

				File newfile = new File(question_url + question.getId() + ext);
				if (resize == true && resized != null) {
					saveImage(resized, newfile);
					//remove old image
					file.delete();
					file = newfile;
				} else {
					//move file from user's directory to question directory
					//and rename to question id .ext
					File dir = new File(question_url);
					if (!dir.exists())
						dir.mkdirs();
					file.renameTo(newfile);
					file = newfile;
				}
			} catch (Exception e) {
				AppData.getApplication().showNotification(AppData.getMessage("Oops"),
						AppData.getMessage("mediaSaveFail"), SmileApplication.NOTIFICATION_ERROR);
				e.printStackTrace();
			}

			question.setMedia_url(originalUploadName);

			if (filename_lower.endsWith(".png") || filename_lower.endsWith(".jpg")
					|| filename_lower.endsWith(".gif")) {
				question.setContentType(ContentTypeEnum.image);
				//System.out.println("Content type set to image");
				//System.out.println(question.getContentType());
			} else if (filename_lower.endsWith(".mp3")) {
				question.setContentType(ContentTypeEnum.audio);
			} else if (filename_lower.endsWith(".mp4")) {
				question.setContentType(ContentTypeEnum.video);
			}

			return true;
		} else
			return false;
	}

	public boolean saveNoQuestionMedia(Question question) {
		String old_name = question.getMedia_url();

		if (old_name != null && old_name.length() > 0) {
			//delete old user upload file
			//file is identified by taking extension from the filename stored in Question table
			//and by question number
			try {
				String ext = old_name.substring(old_name.lastIndexOf('.'));
				File media_file = new File(question_url + question.getId() + ext);

				if (media_file.delete()) {
					//System.out.println(media_file.getName() + " is deleted!");
				} else {
					//System.out.println("Delete operation is failed.");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		question.setContentType(ContentTypeEnum.image);
		question.setMedia_url("");
		return true;
	}

	public boolean setMedia(Panel imagePanel, Question question) {
		//set's image loaded in question into Panel imagePanel, and returns true if successful
		ContentTypeEnum type = question.getContentType();
		String name = question.getMedia_url();
		imagePanel.removeAllComponents();
		imagePanel.setCaption(null);
		if (name != null && name.trim().length() > 0) {
			if (type == ContentTypeEnum.document) // youtube video
			{
				Embedded e = new Embedded(null, new ExternalResource(name));
				e.setAlternateText(AppData.getMessage("youtubeAltText"));
				e.setMimeType("application/x-shockwave-flash");
				e.setParameter("allowFullScreen", "true");
				imagePanel.addComponent(e);
				e.setWidth("100%");
				e.setHeight(videoHeight + "px");
				//imagePanel.setCaption("Youtube video: " + name);
			} else {
				String ext = name.substring(name.lastIndexOf('.')).toLowerCase();
				File file = new File(question_url + question.getId() + ext);
				//System.out.println("file: " + file.toString());
				try {
					if (file.exists()) {
						// Display the uploaded file in the image panel.
						if (ext.equals(".png") || ext.equals(".jpg") || ext.equals(".gif")
								|| ext.equals(".jpeg")) {
							final FileResource imageResource =
									new FileResource(file, AppData.getApplication());
							Embedded image = new Embedded("", imageResource);
							image.setType(Embedded.TYPE_IMAGE);
							//BufferedImage img1 = loadImage(file.toString());
							//setImage(img1, 100, 100, image);
							image.setWidth("100%");
							image.setHeight("-1");
							imagePanel.addComponent(image);
							//imagePanel.setCaption("Uploaded image: " + question.getMedia_url());
						} else if (name.endsWith(".mp3")) {
							final Audio a = new Audio("audio");
							//a.setSources( new FileResource(file, AppData.getApplication()) );
							a.setSources(new ExternalResource(media_url + "questions/"
									+ question.getId() + ext));
							a.setWidth("100%");
							a.setHeight("-1");
							a.setAltText(AppData.getMessage("audioAltText"));
							a.setCaption(null);
							imagePanel.addComponent(a);
							//imagePanel.setCaption("Uploaded audio: " + question.getMedia_url());
						} else if (name.endsWith(".mp4")) {
							final Video v = new Video("video");
							//v.setSources( new FileResource(file, AppData.getApplication()) );
							v.setSources(new ExternalResource(media_url + "questions/"
									+ question.getId() + ext));
							v.setWidth("100%");
							//v.setHeight("-1");
							v.setHeight(videoHeight + "px");
							v.setAltText(AppData.getMessage("videoAltText"));
							v.setCaption(null);
							imagePanel.addComponent(v);
							//imagePanel.setCaption("Uploaded video: " + question.getMedia_url());
						}
					}

				} catch (Exception e) {
					AppData.getApplication().showNotification(AppData.getMessage("Error"),
							AppData.getFormattedMsg("fileOpenFail", new Object[] { file.toString() }));
					System.out.println(e.getLocalizedMessage());
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean setCreateMedia(Panel imagePanel, Question question) {
		//copied from setMedia, with custom heights
		//set's image loaded in question into Panel imagePanel, and returns true if successful
		ContentTypeEnum type = question.getContentType();
		String name = question.getMedia_url();
		//System.out.println("set media");
		imagePanel.removeAllComponents();
		imagePanel.setCaption(null);
		if (name != null && name.trim().length() > 0) {
			if (type == ContentTypeEnum.document) // youtube video
			{
				Embedded e = new Embedded(null, new ExternalResource(name));
				e.setAlternateText(AppData.getMessage("youtubeAltText"));
				e.setMimeType("application/x-shockwave-flash");
				e.setParameter("allowFullScreen", "true");
				e.setWidth("320px");
				e.setHeight("265px");

				imagePanel.addComponent(e);
				imagePanel.setCaption(AppData.getFormattedMsg("YoutubeVideo", question.getMedia_url()));
			} else {
				String ext = name.substring(name.lastIndexOf('.'));
				File file = new File(question_url + question.getId() + ext);
				//System.out.println("file: " + file.toString());
				try {
					if (file.exists()) {
						// Display the uploaded file in the image panel.
						if (name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".gif")) {
							final FileResource imageResource =
									new FileResource(file, AppData.getApplication());
							Embedded image = new Embedded("", imageResource);
							image.setType(Embedded.TYPE_IMAGE);
							BufferedImage img1 = loadImage(file.toString());
							setImageSize(img1, 100, 100, image);
							imagePanel.addComponent(image);
							imagePanel.setCaption(AppData.getFormattedMsg("UploadedImage",
									question.getMedia_url()));
						} else if (name.endsWith(".mp3")) {
							final Audio a = new Audio("audio");
							//a.setSources( new FileResource(file, AppData.getApplication()) );
							a.setSources(new ExternalResource(media_url + "questions/"
									+ question.getId() + ext));
							a.setAltText(AppData.getMessage("audioAltText"));
							a.setCaption(null);
							imagePanel.addComponent(a);
							imagePanel.setCaption(AppData.getFormattedMsg("UploadedAudio",
									question.getMedia_url()));
						} else if (name.endsWith(".mp4")) {
							final Video v = new Video("video");
							//v.setSources( new FileResource(file, AppData.getApplication()) );
							v.setSources(new ExternalResource(media_url + "questions/"
									+ question.getId() + ext));
							v.setWidth("540px");
							v.setHeight("300px");
							v.setAltText(AppData.getMessage("videoAltText"));
							v.setCaption(null);
							imagePanel.addComponent(v);
							imagePanel.setCaption("Uploaded video: " + question.getMedia_url());
						}
					}

				} catch (Exception e) {
					//AppData.getMainWindow().showNotification("Open file (" + file.toString() + ") is failed.");
					e.printStackTrace();
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	public void saveImage(BufferedImage img, String ref) {
		try {
			// String format = (ref.endsWith(".png")) ? "png" : "jpg";  
			String type = ref.toLowerCase();
			String format = "jpg";

			if (type.endsWith(".png"))
				format = "png";
			else if (type.endsWith(".gif"))
				format = "gif";

			ImageIO.write(img, format, new File(ref));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveImage(BufferedImage img, File file) throws IOException {
		// String format = (ref.endsWith(".png")) ? "png" : "jpg";  
		String type = file.getName().toLowerCase();
		String format = "jpg";

		if (type.endsWith(".png"))
			format = "png";
		else if (type.endsWith(".gif"))
			format = "gif";

		int fileLength = file.getName().length();
		String folderPath =
				file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - fileLength);
		ImageIO.write(img, format, file);
	}

	public BufferedImage loadImage(String ref) {
		BufferedImage bimg = null;
		try {

			bimg = ImageIO.read(new File(ref));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bimg;
	}
}
