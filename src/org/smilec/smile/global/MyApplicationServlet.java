package org.smilec.smile.global;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.vaadin.terminal.gwt.server.ApplicationServlet;

public class MyApplicationServlet extends ApplicationServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void writeAjaxPageHtmlHeader(BufferedWriter page, String title, String themeUri,
			HttpServletRequest request) throws IOException {
		super.writeAjaxPageHtmlHeader(page, title, themeUri, request);
		page.write("<meta name=\"viewport\" content=\"width=630, initial-scale=1.0, user-scalable=1\">");
	}
}
