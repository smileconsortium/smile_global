package org.smilec.smile.global.p.Profile;

import java.io.Serializable;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Address;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.UserDAO;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;

public class ProfileViewLogic implements Serializable, ClickListener {
	private ProfileView profileView;

	public ProfileViewLogic(ProfileView pv) {
		this.profileView = pv;
		//load avatar from user
		SmileApplication smile = AppData.getApplication();
		User user = smile.getUser();

		if (user != null) {
			updateUserInfo(user);
		}

		if (smile.getUser().getAvatar() != null) {
			profileView.getAvatarImage().setSource(smile.getUser().getAvatar());
			profileView.getAvatarImage().requestRepaint();
		}

		//click listeners
		profileView.getAvatarButton().addListener(this);
		profileView.getEditProfileButton().addListener(this);
	}

	public void updateUserInfo(User user) {
		Address address = user.getAddress();
		String str = "<br />" + address.getStreet() + "<br />" + address.getCity();
		str += (address.getRegion() != null && address.getRegion().length() > 0) ? ", " : "";
		str +=
				address.getRegion() + " " + address.getPostalCode() + "<br />"
						+ UserDAO.getCountryName(address.getCountry_id());
		profileView.getAddressLBL().setContentMode(Label.CONTENT_XHTML);

		profileView.getUsernameLBL().setValue(
				AppData.getFormattedMsg("FIELD_VALUE_ORDER",
						new Object[] { AppData.getMessage("Username"), user.getUsername() })); //See FIELD_ORDER_VALUE
		profileView.getFirstnameLBL().setValue(
				AppData.getFormattedMsg("FIELD_VALUE_ORDER",
						new Object[] { AppData.getMessage("FirstName"), user.getFirstName() }));
		profileView.getLastnameLBL().setValue(
				AppData.getFormattedMsg("FIELD_VALUE_ORDER",
						new Object[] { AppData.getMessage("LastName"), user.getLastName() }));
		profileView.getEmailLBL().setValue(
				AppData.getFormattedMsg("FIELD_VALUE_ORDER", new Object[] { AppData.getMessage("Email"),
						user.getEmail() }));
		profileView.getPhoneLBL().setValue(
				AppData.getFormattedMsg("FIELD_VALUE_ORDER", new Object[] { AppData.getMessage("Phone"),
						user.getPhone() }));
		profileView.getAddressLBL().setValue(
				AppData.getFormattedMsg("FIELD_VALUE_ORDER",
						new Object[] { AppData.getMessage("Address"), str }));
	}

	@Override
	public void buttonClick(Button.ClickEvent event) {
		if (event.getComponent() == profileView.getAvatarButton()) {
			//choose avatar
			AppData.getApplication().setUrifu("profile/avatar");
			AppData.getApplication().setContent(new AvatarSelection());
		} else if (event.getComponent() == profileView.getEditProfileButton()) {
			//choose personal
			AppData.getApplication().setUrifu("profile/personal");
			EditPersonalInfo e = new EditPersonalInfo();
			AppData.getApplication().setContent(e);
			e.setProfileViewLogic(this);
		}
	}
}
