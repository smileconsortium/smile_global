package org.smilec.smile.global.p.Groups;

import java.net.URL;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

import org.smilec.smile.global.AppData;


/**
* Simple demonstration of using the javax.mail API.
*
* Run from the command line. Please edit the implementation
* to use correct email addresses and host name.
*/
public class Emailer {
	
	Session session;
	  //Assuming you are sending email from localhost
	  //String host = "localhost";
	String host = "smtp-unencrypted.stanford.edu";
	String fromMail = "smile@stanford.edu";
	String password = "password";
	String smtp_port = "465"; 
	String ssl_factory = "javax.net.ssl.SSLSocketFactory";
  
  public Emailer()
  {
	  Properties conf = AppData.getProperties();
	  	
  	  if(conf != null)
  	  {
  		  String str="";
  		  str = conf.getProperty("MAIL_HOST"); 
  		  if(str != null && !str.equals(""))
  	  		 host = str;
  		  
  		//  System.out.println("MAIL_HOST. " + host);
  		 		
  		  str = conf.getProperty("FROM_MAIL"); 
  		  if(str != null && !str.equals(""))
  			 fromMail = str;
  		  
  		// System.out.println("FROM_MAIL. " + fromMail);
  		
  		  str = conf.getProperty("PASSWORD"); 
		  if(str != null && !str.equals(""))
			 password = str;
		  
		//  System.out.println("PASSWORD. " + password);	  		
		  
  		/*  str = conf.getProperty("SMTP_PORT"); 
		  if(str != null && !str.equals(""))
			  smtp_port = str;
		  
		  str = conf.getProperty("SSL_FACTORY"); 
		  if(str != null && !str.equals(""))
			  ssl_factory = str;*/
		  
  	  }
  	  
      // Get system properties
      Properties properties = System.getProperties();

      // Setup mail server
      
      URL url = AppData.getApplication().getURL();
	  String urlString = url.toString();
	  
	  if(!urlString.contains("stanford.edu"))
	  {  		  
		  //properties.setProperty("mail.smtp.starttls.enable", "true"); // added this line for TLS
		  properties.setProperty("mail.smtp.host", host);
		  //properties.setProperty("mail.smtp.user", fromMail);
		  //properties.setProperty("mail.smtp.password", password);		  
		  properties.setProperty("mail.smtp.port", smtp_port);
		  properties.setProperty("mail.smtp.auth", "true");
		  properties.setProperty("mail.smtp.socketFactory.port", smtp_port);  
		  properties.setProperty("mail.smtp.socketFactory.class", ssl_factory);  
		  properties.setProperty("mail.smtp.socketFactory.fallback", "false"); 
		  
		  session = Session.getDefaultInstance(properties,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(fromMail, password);
						}
					});
		  
	  }
	  else {
		  properties.setProperty("mail.smtp.host", host);
	  	  
		// Get the default Session object.
	    session = Session.getDefaultInstance(properties);
      }
  }

  public void setProperties(String host, String fromMail)
  {
	  this.host = host;
	  this.fromMail= fromMail;
  }
  
  /**
  * Send a single email.
  */
  public boolean sendEmail(String from, String to, String subject, String body)
  {
	boolean r=true;
	
    if(from !=null && !from.equals(""))
    {
    	fromMail = from;
    }
    
    MimeMessage message = new MimeMessage( session );
    
    try {
      //the "from" address may be set in code, or set in the
      //config file under "mail.from" ; here, the latter style is used
    	
      message.setFrom( new InternetAddress(fromMail) );
      
      message.addRecipient(
        Message.RecipientType.TO, new InternetAddress(to)
      );
      
      message.setSubject( subject );
      
      message.setText( body );
      
      Transport.send( message );
    }
    catch (MessagingException ex){
      System.err.println("Cannot send email. " + ex);
      r=false;
    }
    
    return r;
  }

} 

