package org.smilec.smile.global.p.Groups;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.Quiz.StatusEnum;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.vars.Css;
import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;

public class MemberGroupViewLogic implements ClickListener, Serializable {
	private static final int interfaceHeight = 145;
	private static final String jQueryHandle = ".quizLayout";

	//component vars
	private MemberGroupView memberView;
	private Layout userLayout;
	private Group group; //group for current organize group page

	//constants
	//private static int interfaceHeight = 185; //added height for area above and below jQuery object
	//private static final String GroupsJQueryHandle = ".v-csslayout-container";

	public MemberGroupViewLogic(MemberGroupView memberGroupView, Group group) {
		this.memberView = memberGroupView;
		this.group = group;
		//init
		memberGroupView.setImmediate(true);
		this.userLayout = memberGroupView.getMainLayout();
		memberGroupView.getQuizTable().addStyleName(Css.tableNoResize);
		memberGroupView.getOrganizerName().setValue(
				AppData.getFormattedMsg("groupOwner", UserDAO.getUserById(group.getOrganizer_id())
						.getUsername()));
		//set jquery css handles
		memberGroupView.getQuizCssLayout().addStyleName("quizLayout");
		//set label
		memberGroupView.getWelcomeLabel().setValue(
				"<h3><p><a href='#groups'>" + AppData.getMessage("Groups") + "</a> > " + group.getName()
						+ "</p></h3>");

		//TODO: populate tab upon tab selection
		populateQuizTable();
		//add listeners
		memberGroupView.getBackButton().addListener(this);
		memberGroupView.getRefreshButton().addListener(this);
		memberGroupView.getLeaveButton().addListener(this);
	}

	private void populateQuizTable() {
		Table quizTable = memberView.getQuizTable();
		quizTable.removeAllItems();

		quizTable.setColumnReorderingAllowed(true);
		quizTable.setColumnCollapsingAllowed(false);
		quizTable.setSelectable(false);
		quizTable.setSortDisabled(true);
		String quizColumnName = AppData.getMessage("Sessions") + " - " + group.getName();
		quizTable.addContainerProperty(quizColumnName, Label.class, null);
		quizTable.addContainerProperty(" ", Button.class, null);
		quizTable.setColumnWidth(quizColumnName, 360);
		//groupsTable.addContainerProperty("Organizer?", Boolean.class, null);
		//groupsTable.addContainerProperty("Number of Quizzes", Integer.class, null);

		User user = AppData.getApplication().getUser();
		List<Quiz> myQuizzes = GroupDAO.getQuizzes(group);
		Iterator<Quiz> i = myQuizzes.iterator();
		if (!i.hasNext()) {
			//if no quizzes
			Label label = new Label();
			label.setContentMode(label.CONTENT_XHTML);
			label.setValue(AppData.getMessage("noSessions"));
			quizTable.addItem(new Object[] { label, null }, "noQuiz");
		}
		while (i.hasNext()) {
			Quiz quiz = i.next();
			boolean isExam;
			String _isExam = quiz.getOption("exam");
			if (_isExam == null)
				isExam = false;
			else {
				isExam = Integer.parseInt(_isExam) > 0;
			}
			Label label = new Label();
			label.setContentMode(label.CONTENT_XHTML);
			Button b = new Button();
			//store instance of group on button
			b.setData(quiz);
			//add button listener
			b.addListener(this);
			b.setCaption(AppData.getMessage("Enter"));
			StatusEnum status = quiz.getStatus();
			String statusText = "";
			String quizType = "syncStatus";
			if (isExam) {
				quizType = "examStatus";
			}
			if (status.equals(StatusEnum.async)) {
				statusText = AppData.getMessage("Async");
			} else if (status.equals(StatusEnum.closed)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("NotStarted"));
			} else if (status.equals(StatusEnum.create)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("Create"));
			} else if (status.equals(StatusEnum.show_result)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("Results"));
			} else if (status.equals(StatusEnum.solve)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("Solve"));
			}
			String organizerText =
					" <span class='label-bs label-bs-warning'><i class='icon-asterisk icon-white'></i> "
							+ statusText + "</span>";
			label.setValue(quiz.getName() + organizerText);
			quizTable.addItem(new Object[] { label, b }, quiz);

			//item.getItemProperty("Group").setValue(g.getName());
			//item.getItemProperty("Organizer?").setValue(g.isOwner(user));
			//item.getItemProperty("Number of Quizzes").setValue(numQuizzes);
		}

		quizTable.requestRepaint();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button == memberView.getBackButton()) {
			AppData.getApplication().gotoGroups();
		} else if (button == memberView.getRefreshButton()) {
			User user = AppData.getApplication().getUser();
			//check if status changed
			if (GroupDAO.getGroupOrganizers(group).contains(user.getId()) || group.isOwner(user)) {
				AppData.getApplication().setUrifu(""); //first clear fragment so fragment change handler gets called
				AppData.getApplication().gotoGroupsOrganizerPage(group);
			}
			populateQuizTable();
		} else if (button == memberView.getLeaveButton()) {
			String msg = AppData.getFormattedMsg("leaveGroup?", group.getName());
			ConfirmDialog.show(AppData.getApplication().getMainWindow(),
					AppData.getMessage("LeaveGroupConfirm"), msg, AppData.getMessage("LeaveGroup"),
					AppData.getMessage("Cancel"), new ConfirmDialog.Listener() {

						@Override
						public void onClose(ConfirmDialog dialog) {
							if (dialog.isConfirmed()) {
								// Confirmed to remove user
								SmileApplication smile = AppData.getApplication();
								GroupDAO.deleteUserFromGroup(group.getId(), smile.getUser().getId());
								smile.gotoGroups();
							} else {
								// User did not confirm				                	
							}
						}
					});
		} else {

			//check if a Quiz table button
			try {
				Quiz quiz = (Quiz) button.getData();
				SmileApplication smile = AppData.getApplication();
				if (quiz != null) {
					//goto quiz page
					AppData.getApplication().gotoQuiz(group, quiz);
				}
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
			}
		}
	}
}
