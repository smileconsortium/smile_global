package org.smilec.smile.global.p.Groups;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.QuizDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.ui.BarChartData;
import org.smilec.smile.global.vars.Css;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;

public class GroupResults implements Serializable {
	public static String[] getUserResults(int quiz_id, User user) {
		//returns an array of {yourResults, classResults}, formatted as XHTML
		//to be applied to a Label

		Group group = GroupDAO.getGroupById(QuizDAO.getQuizById(quiz_id).getGroup_id());
		List<User> users = GroupDAO.getGroupUsers(group);
		//own question answering variables
		Integer numQuestionsAnswered = 0;
		Integer percentAnsweredCorrectly = null;
		Float avgSecComplete = null;
		Float avgRatingGiven = null;
		//Group average answering variables (are first summed, and then divided at end

		//Session answering variables
		//ArrayList of user id's for each var for top/lowest score, and seperate var for top/lowest group score
		ArrayList<Integer> userTopQuestionsAnswered = new ArrayList<Integer>();
		Integer topQuestionsAnswered = 0;
		ArrayList<Integer> userTopPercentAnsweredCorrectly = new ArrayList<Integer>();
		Integer topPercentAnsweredCorrectly = 0;
		ArrayList<Integer> userLowestSecComplete = new ArrayList<Integer>();
		Float lowestSecComplete = (float) 9999999;
		ArrayList<Integer> userTopAvgRatingGiven = new ArrayList<Integer>();
		Float topAvgRatingGiven = (float) 0;

		List<Object[]> quizAnswerAverages = QuizDAO.getQuizAnswerAverages(quiz_id);
		Iterator<Object[]> it = quizAnswerAverages.iterator();

		while (it.hasNext()) {
			Object[] o = it.next();
			Integer userId = (Integer) o[0];
			Integer _questionsAnswered = (Integer) o[1];
			Integer _questionsAnsweredCorrectly = (Integer) o[2];
			Integer _percentAnsweredCorrectly = null;
			if (_questionsAnswered != null && _questionsAnsweredCorrectly != null) {
				_percentAnsweredCorrectly =
						(int) Math.round((double) _questionsAnsweredCorrectly
								/ (double) _questionsAnswered * 100);
			}
			Float _avgSecComplete = (Float) o[3];
			Float _avgRatingGiven = (Float) o[4];
			//store own results

			if (userId.equals(user.getId())) {
				numQuestionsAnswered = _questionsAnswered;
				percentAnsweredCorrectly = _percentAnsweredCorrectly;
				avgSecComplete = _avgSecComplete;
				avgRatingGiven = _avgRatingGiven;
			}
			//store top questions answered
			if (_questionsAnswered != null && _questionsAnswered > topQuestionsAnswered) {
				userTopQuestionsAnswered.clear();
				userTopQuestionsAnswered.add(userId);
				topQuestionsAnswered = _questionsAnswered;
			} else if (_questionsAnswered != null && _questionsAnswered.equals(topQuestionsAnswered)) {
				userTopQuestionsAnswered.add(userId);
			}
			//store top percent answered correctly
			if (_percentAnsweredCorrectly != null
					&& _percentAnsweredCorrectly > topPercentAnsweredCorrectly) {
				userTopPercentAnsweredCorrectly.clear();
				userTopPercentAnsweredCorrectly.add(userId);
				topPercentAnsweredCorrectly = _percentAnsweredCorrectly;
			} else if (_percentAnsweredCorrectly != null
					&& _percentAnsweredCorrectly.equals(topPercentAnsweredCorrectly)) {
				userTopPercentAnsweredCorrectly.add(userId);
			}
			//store lowestSecToComplete on average
			if (_avgSecComplete != null && _avgSecComplete < lowestSecComplete) {
				userLowestSecComplete.clear();
				userLowestSecComplete.add(userId);
				lowestSecComplete = _avgSecComplete;
			} else if (_avgSecComplete != null && _avgSecComplete.equals(lowestSecComplete)) {
				userLowestSecComplete.add(userId);
			}
			//store top avg rating given
			if (_avgRatingGiven != null && _avgRatingGiven > topAvgRatingGiven) {
				userTopAvgRatingGiven.clear();
				userTopAvgRatingGiven.add(userId);
				topAvgRatingGiven = _avgRatingGiven;
			} else if (_avgRatingGiven != null && _avgRatingGiven.equals(topAvgRatingGiven)) {
				userTopAvgRatingGiven.add(userId);
			}

		}

		//Session Posting Variables
		//own question posting variables, how others did on your questions
		Integer questionsPosted = 0;
		Float correctReceived = (float) -1;
		Float ratingReceived = (float) -1;
		Float secReceived = (float) -1;

		Integer topQuestionsPosted = 0;
		ArrayList<Integer> userTopQuestionsPosted = new ArrayList<Integer>();
		Float topCorrectReceived = (float) 0;
		ArrayList<Integer> userTopCorrectReceived = new ArrayList<Integer>();
		Float lowCorrectReceived = (float) 9999999;
		ArrayList<Integer> userLowCorrectReceived = new ArrayList<Integer>();
		Float topRatingReceived = (float) 0;
		ArrayList<Integer> userTopRatingReceived = new ArrayList<Integer>();
		Float topSecReceived = (float) 0;
		ArrayList<Integer> userTopSecReceived = new ArrayList<Integer>();

		List<Object[]> postAverages = QuizDAO.getQuizPostingAverages(quiz_id);
		it = postAverages.iterator();
		while (it.hasNext()) {
			Object[] o = it.next();
			Integer userId = (Integer) o[0];
			Integer _questionsPosted = (Integer) o[1];
			Float _ratingReceived = (Float) o[2];
			Float _correctReceived = (Float) o[3];
			Float _secReceived = (Float) o[4];

			if (userId.equals(user.getId())) {
				questionsPosted = _questionsPosted;
				correctReceived = _correctReceived;
				ratingReceived = _ratingReceived;
				secReceived = _secReceived;
			}
			//top questions posted
			if (_questionsPosted != null && _questionsPosted > topQuestionsPosted) {
				topQuestionsPosted = _questionsPosted;
				userTopQuestionsPosted.clear();
				userTopQuestionsPosted.add(userId);
			} else if (_questionsPosted != null && _questionsPosted.equals(topQuestionsPosted)) {
				userTopQuestionsPosted.add(userId);
			}
			//highest correct responses received (easiest q's?)
			if (_correctReceived != null && _correctReceived > topCorrectReceived) {
				topCorrectReceived = _correctReceived;
				userTopCorrectReceived.clear();
				userTopCorrectReceived.add(userId);
			} else if (_correctReceived != null && _correctReceived.equals(topCorrectReceived)) {
				userTopCorrectReceived.add(userId);
			}
			//lowest correct responses received (hardest q's, worst q's?)
			if (_correctReceived != null && _correctReceived < lowCorrectReceived) {
				lowCorrectReceived = _correctReceived;
				userLowCorrectReceived.clear();
				userLowCorrectReceived.add(userId);
			} else if (_correctReceived != null && _correctReceived.equals(lowCorrectReceived)) {
				userLowCorrectReceived.add(userId);
			}
			//top rating received
			if (_ratingReceived != null && _ratingReceived > topRatingReceived) {
				topRatingReceived = _ratingReceived;
				userTopRatingReceived.clear();
				userTopRatingReceived.add(userId);
			} else if (_ratingReceived != null && _ratingReceived.equals(topRatingReceived)) {
				userTopRatingReceived.add(userId);
			}
			//top sec received (quickest to answer)
			if (_secReceived != null && _secReceived > topSecReceived) {
				topSecReceived = _secReceived;
				userTopSecReceived.clear();
				userTopSecReceived.add(userId);
			} else if (_secReceived != null && _secReceived.equals(topSecReceived)) {
				userTopSecReceived.add(userId);
			}

		}

		//Generate your results label
		String yourResults = "";
		if (numQuestionsAnswered == 0) {
			yourResults += "<p>" + AppData.getMessage("noQuestionsAnswered") + "</p><p></p>";
		} else {
			yourResults +=
					"<p>" + AppData.getFormattedMsg("questionsAnswered", numQuestionsAnswered)
							+ "</p><p></p>";
			if (percentAnsweredCorrectly != null) {
				yourResults +=
						"<p>"
								+ AppData.getFormattedMsg("questionsAnsweredCorrectly",
										percentAnsweredCorrectly) + "</p><p></p>";
			}
			if (avgSecComplete != null) {
				yourResults +=
						"<p>"
								+ AppData.getFormattedMsg("avgSecComplete",
										String.format("%.1f", avgSecComplete)) + "</p><p></p>";
			}
			if (avgRatingGiven != null) {
				yourResults +=
						"<p>"
								+ AppData.getFormattedMsg("avgRatingGave",
										String.format("%.1f", avgRatingGiven / 2)) + "</p><p></p>";
			}

		}
		if (questionsPosted > 0) {
			yourResults +=
					"<p>" + AppData.getFormattedMsg("questionsPosted", questionsPosted) + "</p><p></p>";
			if (correctReceived != null && correctReceived >= 0) {
				yourResults +=
						"<p>"
								+ AppData.getFormattedMsg("correctReceived",
										String.format("%.1f", correctReceived * 100)) + "</p><p></p>";
			}
			if (ratingReceived != null && ratingReceived >= 0) {
				yourResults +=
						"<p>"
								+ AppData.getFormattedMsg("ratingReceived",
										String.format("%.0f", ratingReceived / 2)) + "</p><p></p>";
			}
			if (secReceived != null && secReceived >= 0) {
				yourResults +=
						"<p>"
								+ AppData.getFormattedMsg("secReceived",
										String.format("%.0f", secReceived)) + "</p><p></p>";
			}
		} else {
			yourResults += "<p>" + AppData.getMessage("noQuestions") + "</p>";
		}

		//generate class results label
		String classResults = "";
		//class questions answered
		if (topQuestionsAnswered > 0) {
			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg("topQuestionsAnswered", new Object[] { topQuestionsAnswered,
							getUsernames(userTopQuestionsAnswered, users) });
			classResults += "</p><p></p>";
		} else {
			classResults += "<p>" + AppData.getMessage("noSessionQs") + "</p><p></p>";
		}
		if (topPercentAnsweredCorrectly > 0) {

			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg(
							"topPercentCorrect",
							new Object[] { topPercentAnsweredCorrectly,
									getUsernames(userTopPercentAnsweredCorrectly, users) });
			classResults += "</p><p></p>";
		}
		if (lowestSecComplete < 9999999) {
			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg(
							"fastestTime",
							new Object[] { String.format("%.1f", lowestSecComplete),
									getUsernames(userLowestSecComplete, users) });
			classResults += "</p><p></p>";
		}
		if (topAvgRatingGiven > 0) {

			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg(
							"topAvgRatingGiven",
							new Object[] { String.format("%.2f", topAvgRatingGiven / 2),
									getUsernames(userTopAvgRatingGiven, users) });
			classResults += "</p><p></p>";
		}
		//class questions posted
		if (topQuestionsPosted > 0) {
			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg("mostQuestions", new Object[] { topQuestionsPosted,
							getUsernames(userTopAvgRatingGiven, users) });
			classResults += "</p><p></p>";
		} else {
			classResults += "<p>" + AppData.getMessage("noPostedQs") + "</p><p></p>";
		}
		if (topCorrectReceived > 0) {
			classResults +=
					"<p>"
							+ AppData.getFormattedMsg(
									"easiestQ",
									new Object[] { getUsernames(userTopCorrectReceived, users),
											String.format("%.0f", topCorrectReceived * 100) });
			classResults += "</p><p></p>";
		}
		if (lowCorrectReceived < 9999999 && lowCorrectReceived != topCorrectReceived) {
			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg(
							"hardestQ",
							new Object[] { getUsernames(userLowCorrectReceived, users),
									String.format("%.0f", lowCorrectReceived * 100) });
			classResults += "</p><p></p>";
		}
		if (topRatingReceived > 0) {
			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg(
							"highestRated",
							new Object[] { getUsernames(userTopRatingReceived, users),
									String.format("%.1f", topRatingReceived / 2) });
			classResults += "</p><p></p>";
		}
		if (topSecReceived > 0) {
			classResults += "<p>";
			classResults +=
					AppData.getFormattedMsg(
							"highestRated",
							new Object[] { getUsernames(userTopSecReceived, users),
									String.format("%.0f", topSecReceived) });
			classResults += "</p><p></p>";
		}

		String[] results = { yourResults, classResults };
		return results;
	}

	public static void getGroupResults() {

	}

	private static String getUsernames(ArrayList<Integer> userIds, List<User> users) {
		//searches among list of users for name first, then query database for username if not in list of Users
		String userString = "";
		int maxShown = 3; //max names to show for any group
		for (int i = 0, l = userIds.size(); i < l; i++) {
			Integer userId = userIds.get(i);
			if (userId != null) {
				if (i != 0 && (i < maxShown || l - maxShown < 2)) {
					userString += ", ";
				}

				if (i < maxShown || l - maxShown < 2) {
					//first search for usernames in list of users
					String username = null;
					Iterator<User> it = users.iterator();
					while (it.hasNext()) {
						User u = it.next();
						if (u.getId().equals(userId)) {
							username = u.getUsername();
						}
					}
					if (username == null) {
						username = UserDAO.getUserById(userId).getUsername();
					}
					userString += Css.styleUsername(username);
				} else if (i == maxShown) {
					userString += AppData.getFormattedMsg("andOthers", l - i);
				}
			}
		}
		return userString;
	}

	public static BarChartData getClassQuestionChart(int quizId) {
		//returns ArrayList of chart data
		//0: questionAccuracy
		//1: questionsPosted and questionsAnswered
		//2: avg. rating given for questions, avg. rating received for questions
		//3: avg. time to answer other's q's, avg. time for other to answer your q's
		//4: num comments posted, num comments received
		ArrayList<BarChartData> charts = new ArrayList<BarChartData>();

		Group group = GroupDAO.getGroupById(QuizDAO.getQuizById(quizId).getGroup_id());
		List<User> users = GroupDAO.getGroupUsers(group);
		User user = UserDAO.getUserById(3002);

		BarChartData questionData = new BarChartData();

		questionData.title = AppData.getMessage("questionsAnswers");
		questionData.xlabel = AppData.getMessage("Questions");
		questionData.ylabel = AppData.getMessage("Student");
		questionData.legend.add(AppData.getMessage("QuestionsPosted"));
		questionData.legend.add(AppData.getMessage("QuestionsAnswered"));

		List<Object[]> quizAnswerAverages = QuizDAO.getQuizAnswerAverages(quizId);
		List<Object[]> quizPostAverages = QuizDAO.getQuizPostingAverages(quizId);

		//cycle through all users, look for matches in user id's with quizAnswerAverages
		//and quizPostAverages

		ArrayList<Number> qPostedList = new ArrayList<Number>();
		ArrayList<Number> qAnsweredList = new ArrayList<Number>();
		Iterator<User> it = users.iterator();
		while (it.hasNext()) {
			User student = it.next();
			//if not moderator
			if (student != user) {
				Integer questionsAnswered = 0;
				Integer questionsPosted = 0;

				//find match quizAnswerAverages
				Iterator<Object[]> it2 = quizAnswerAverages.iterator();
				while (it2.hasNext()) {
					Object[] o = it2.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId())) {
						questionsAnswered = (Integer) o[1];
					}
				}
				//find match quizPostedAverages
				Iterator<Object[]> it3 = quizPostAverages.iterator();
				while (it3.hasNext()) {
					Object[] o = it3.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId())) {
						questionsPosted = (Integer) o[1];
					}
				}
				questionData.name.add(student.getUsername());
				qPostedList.add(questionsPosted);
				qAnsweredList.add(questionsAnswered);
			}
		}

		ArrayList<ArrayList<Number>> qList = new ArrayList<ArrayList<Number>>();
		qList.add(qPostedList);
		qList.add(qAnsweredList);
		questionData.data = qList;

		if (users.size() < 2) {
			return null;
		}

		return questionData;
	}

	public static BarChartData getClassCorrectChart(int quizId) {
		//returns ArrayList of chart data
		//0: questionAccuracy
		//1: questionsPosted and questionsAnswered
		//2: avg. rating given for questions, avg. rating received for questions
		//3: avg. time to answer other's q's, avg. time for other to answer your q's
		//4: num comments posted, num comments received
		ArrayList<BarChartData> charts = new ArrayList<BarChartData>();

		Group group = GroupDAO.getGroupById(QuizDAO.getQuizById(quizId).getGroup_id());
		List<User> users = GroupDAO.getGroupUsers(group);
		User user = UserDAO.getUserById(3002);

		BarChartData questionData = new BarChartData();

		questionData.title = AppData.getMessage("%correctQuestionsAnswer");
		questionData.xlabel = AppData.getMessage("percAnswerCorrect");
		questionData.ylabel = AppData.getMessage("Student");
		questionData.legend.add(AppData.getMessage("percAnswerCorrect"));
		questionData.legend.add(AppData.getMessage("QuestionsAnswered"));

		List<Object[]> quizAnswerAverages = QuizDAO.getQuizAnswerAverages(quizId);

		//cycle through all users, look for matches in user id's with quizAnswerAverages
		//and quizPostAverages

		ArrayList<Number> qCorrectList = new ArrayList<Number>();
		ArrayList<Number> qAnsweredList = new ArrayList<Number>();
		Iterator<User> it = users.iterator();
		while (it.hasNext()) {
			User student = it.next();
			//if not moderator
			if (student != user) {
				Integer questionsAnswered = 0;
				Integer questionsAnsweredCorrectly = 0;
				Integer percentAnsweredCorrectly = 0;

				//find match quizAnswerAverages
				Iterator<Object[]> it2 = quizAnswerAverages.iterator();
				while (it2.hasNext()) {
					Object[] o = it2.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId())) {
						questionsAnswered = (Integer) o[1];
						questionsAnsweredCorrectly = (Integer) o[2];
						if (questionsAnswered != null && questionsAnsweredCorrectly != null) {
							percentAnsweredCorrectly =
									(int) Math.round((double) questionsAnsweredCorrectly
											/ (double) questionsAnswered * 100);
						}
					}
				}

				questionData.name.add(student.getUsername());
				qCorrectList.add(percentAnsweredCorrectly);
				qAnsweredList.add(questionsAnswered);
			}
		}

		ArrayList<ArrayList<Number>> qList = new ArrayList<ArrayList<Number>>();
		qList.add(qCorrectList);
		qList.add(qAnsweredList);
		questionData.data = qList;

		if (users.size() < 2) {
			return null;
		}

		return questionData;
	}

	public static BarChartData getClassRatingChart(int quizId) {
		//returns ArrayList of chart data
		//0: questionAccuracy
		//1: questionsPosted and questionsAnswered
		//2: avg. rating given for questions, avg. rating received for questions
		//3: avg. time to answer other's q's, avg. time for other to answer your q's
		//4: num comments posted, num comments received
		ArrayList<BarChartData> charts = new ArrayList<BarChartData>();

		Group group = GroupDAO.getGroupById(QuizDAO.getQuizById(quizId).getGroup_id());
		List<User> users = GroupDAO.getGroupUsers(group);
		User user = UserDAO.getUserById(3002);

		BarChartData ratingData = new BarChartData();

		ratingData.title = AppData.getMessage("ratingTitle");
		ratingData.xlabel = AppData.getMessage("AverageRating");
		ratingData.ylabel = AppData.getMessage("Student");
		ratingData.legend.add(AppData.getMessage("avgRatingGiven1"));
		ratingData.legend.add(AppData.getMessage("avgRatingReceived"));

		List<Object[]> quizAnswerAverages = QuizDAO.getQuizAnswerAverages(quizId);
		List<Object[]> quizPostAverages = QuizDAO.getQuizPostingAverages(quizId);

		//cycle through all users, look for matches in user id's with quizAnswerAverages
		//and quizPostAverages

		ArrayList<Number> ratingGivenList = new ArrayList<Number>();
		ArrayList<Number> ratingReceivedList = new ArrayList<Number>();
		Iterator<User> it = users.iterator();
		while (it.hasNext()) {
			User student = it.next();
			//if not moderator
			if (student != user) {
				Float ratingGiven = (float) 0;
				Float ratingReceived = (float) 0;

				//find match quizAnswerAverages
				Iterator<Object[]> it2 = quizAnswerAverages.iterator();
				while (it2.hasNext()) {
					Object[] o = it2.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId()) && o[4] != null) {
						ratingGiven = (Float) o[4] / 2;
					}
				}
				//find match quizPostedAverages
				Iterator<Object[]> it3 = quizPostAverages.iterator();
				while (it3.hasNext()) {
					Object[] o = it3.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId())) {
						if (o[2] != null) {
							ratingReceived = (Float) o[2] / 2;
						}
					}
				}

				ratingData.name.add(student.getUsername());
				ratingGivenList.add(ratingGiven);
				ratingReceivedList.add(ratingReceived);
			}
		}

		ArrayList<ArrayList<Number>> qList = new ArrayList<ArrayList<Number>>();
		qList.add(ratingGivenList);
		qList.add(ratingReceivedList);
		ratingData.data = qList;

		if (users.size() < 2) {
			return null;
		}

		return ratingData;
	}

	public static BarChartData getClassTimeChart(int quizId) {
		//returns ArrayList of chart data
		//0: questionAccuracy
		//1: questionsPosted and questionsAnswered
		//2: avg. rating given for questions, avg. rating received for questions
		//3: avg. time to answer other's q's, avg. time for other to answer your q's
		//4: num comments posted, num comments received
		ArrayList<BarChartData> charts = new ArrayList<BarChartData>();

		Group group = GroupDAO.getGroupById(QuizDAO.getQuizById(quizId).getGroup_id());
		List<User> users = GroupDAO.getGroupUsers(group);
		User user = UserDAO.getUserById(3002);

		BarChartData questionData = new BarChartData();

		questionData.title = AppData.getMessage("timeTitle");
		questionData.xlabel = AppData.getMessage("timeSecs");
		questionData.ylabel = AppData.getMessage("Student");
		questionData.legend.add(AppData.getMessage("avgTime1"));
		questionData.legend.add(AppData.getMessage("avgTime2"));

		List<Object[]> quizAnswerAverages = QuizDAO.getQuizAnswerAverages(quizId);
		List<Object[]> quizPostAverages = QuizDAO.getQuizPostingAverages(quizId);

		//cycle through all users, look for matches in user id's with quizAnswerAverages
		//and quizPostAverages

		ArrayList<Number> timeAnswerOthers = new ArrayList<Number>();
		ArrayList<Number> timeAnswerOwn = new ArrayList<Number>();
		Iterator<User> it = users.iterator();
		while (it.hasNext()) {
			User student = it.next();
			//if not moderator
			if (student != user) {
				Float timeOthers = (float) 0;
				Float timeOwn = (float) 0;

				//find match quizAnswerAverages
				Iterator<Object[]> it2 = quizAnswerAverages.iterator();
				while (it2.hasNext()) {
					Object[] o = it2.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId())) {
						timeOthers = (Float) o[3];
					}
				}
				//find match quizPostedAverages
				Iterator<Object[]> it3 = quizPostAverages.iterator();
				while (it3.hasNext()) {
					Object[] o = it3.next();
					Integer userId = (Integer) o[0];
					if (userId.equals(student.getId())) {
						timeOwn = (Float) o[4];
					}
				}
				questionData.name.add(student.getUsername());
				timeAnswerOthers.add(timeOthers);
				timeAnswerOwn.add(timeOwn);
			}
		}

		ArrayList<ArrayList<Number>> qList = new ArrayList<ArrayList<Number>>();
		qList.add(timeAnswerOthers);
		qList.add(timeAnswerOwn);
		questionData.data = qList;

		if (users.size() < 2) {
			return null;
		}

		return questionData;
	}

	/*Get's a data container of */
	public static IndexedContainer getStudentReport(List<Quiz> quizzes) {
		boolean showNonparticipants = true;
		//returns IndexedContainer with data
		//to be applied to a Label
		IndexedContainer container = new IndexedContainer();
		if (quizzes.size() > 1) {
			container.addContainerProperty("session", String.class, "");
		}
		container.addContainerProperty("username", String.class, "");
		container.addContainerProperty("name", String.class, "");
		container.addContainerProperty("questPosted", Integer.class, 0);
		container.addContainerProperty("correctRec", Integer.class, null);
		container.addContainerProperty("ratingRec", Float.class, null);
		container.addContainerProperty("timeRec", Float.class, null);
		container.addContainerProperty("questAnswered", Integer.class, 0);
		container.addContainerProperty("correctGave", Integer.class, null);
		container.addContainerProperty("ratingGave", Object.class, null);
		container.addContainerProperty("timeGave", Float.class, null);

		Iterator<Quiz> quiz_it = quizzes.iterator();
		while (quiz_it.hasNext()) {
			Quiz quiz = quiz_it.next();
			int quiz_id = quiz.getId();

			Group group = GroupDAO.getGroupById(QuizDAO.getQuizById(quiz_id).getGroup_id());
			List<User> users = new ArrayList<User>();
			HashMap<Integer, User> participants = new HashMap<Integer, User>();
			if (showNonparticipants) {
				users = GroupDAO.getGroupUsers(group);
			}

			//Session answering variables

			List<Object[]> quizAnswerAverages = QuizDAO.getQuizAnswerAverages(quiz_id);
			Iterator<Object[]> it = quizAnswerAverages.iterator();

			//store hash map of userIds and container Items to allow updating the same item
			HashMap<Integer, Item> items = new HashMap<Integer, Item>();

			while (it.hasNext()) {
				Object[] o = it.next();
				Integer userId = (Integer) o[0];
				User tempUser = UserDAO.getUserById(userId);
				if (showNonparticipants)
					participants.put(userId, tempUser);
				Integer _questionsAnswered = (Integer) o[1];
				if (_questionsAnswered == null)
					_questionsAnswered = 0;
				Integer _questionsAnsweredCorrectly = (Integer) o[2];
				Integer _percentAnsweredCorrectly = null;
				if (_questionsAnswered != null && _questionsAnsweredCorrectly != null) {
					_percentAnsweredCorrectly =
							(int) Math.round((double) _questionsAnsweredCorrectly
									/ (double) _questionsAnswered * 100);
				}
				Float _avgSecComplete = (Float) o[3];
				Float _avgRatingGiven = (Float) o[4];

				Item newItem = container.addItem(tempUser);
				items.put(userId, newItem);
				if (quizzes.size() > 1) {
					newItem.getItemProperty("session").setValue(quiz.getName());
				}
				newItem.getItemProperty("username").setValue(tempUser.getUsername());
				newItem.getItemProperty("name").setValue(
						tempUser.getLastName() + ", " + tempUser.getFirstName());
				newItem.getItemProperty("questAnswered").setValue(_questionsAnswered);
				newItem.getItemProperty("correctGave").setValue(_percentAnsweredCorrectly);
				newItem.getItemProperty("ratingGave").setValue(
						_avgRatingGiven == null ? "" : _avgRatingGiven / 2);
				newItem.getItemProperty("timeGave").setValue(_avgSecComplete);
				/*System.out.println(tempUser.getUsername() + ": " + _questionsAnswered + ", "
						+ _questionsAnsweredCorrectly + ", " + _avgSecComplete + _avgRatingGiven);*/
			}

			List<Object[]> postAverages = QuizDAO.getQuizPostingAverages(quiz_id);
			it = postAverages.iterator();

			while (it.hasNext()) {
				Object[] o = it.next();
				Integer userId = (Integer) o[0];
				Integer _questionsPosted = (Integer) (o[1] == null ? 0 : o[1]);
				if (_questionsPosted == null)
					_questionsPosted = 0;
				Float _ratingReceived = (Float) (o[2] == null ? 0f : o[2]);
				Number _correctReceived = ((Number) (o[3] == null ? 0 : o[3])).intValue() * 100;
				Float _secReceived = (Float) (o[4] == null ? 0f : o[4]);

				//retrieve item from HashMap
				Item newItem = items.get(userId);
				if (newItem == null) {
					User tempUser = UserDAO.getUserById(userId);
					newItem = container.addItem(tempUser);
					if (showNonparticipants)
						participants.put(userId, tempUser);
					if (quizzes.size() > 1) {
						newItem.getItemProperty("session").setValue(quiz.getName());
					}
					newItem.getItemProperty("username").setValue(tempUser.getUsername());
					newItem.getItemProperty("name").setValue(
							tempUser.getLastName() + ", " + tempUser.getFirstName());
				}
				newItem.getItemProperty("questPosted").setValue(_questionsPosted);
				newItem.getItemProperty("correctRec").setValue(_correctReceived);
				newItem.getItemProperty("ratingRec").setValue(_ratingReceived / 2);
				newItem.getItemProperty("timeRec").setValue(_secReceived);
			}
			if (showNonparticipants) {
				Iterator<User> user_it = users.iterator();
				while (user_it.hasNext()) {
					User u = user_it.next();
					if (!participants.containsKey(u.getId())) {
						Item newItem = container.addItem(u);
						if (quizzes.size() > 1) {
							newItem.getItemProperty("session").setValue(quiz.getName());
						}
						newItem.getItemProperty("username").setValue(u.getUsername());
						newItem.getItemProperty("name").setValue(
								u.getLastName() + ", " + u.getFirstName());
						newItem.getItemProperty("questAnswered").setValue(0);
						newItem.getItemProperty("questPosted").setValue(0);
					}
				}
			}
		}
		//add extra users, null info for users who didn't participate
		container.sort(new Object[] { "name", "username", "session" },
				new boolean[] { true, true, true });
		return container;
	}
}
