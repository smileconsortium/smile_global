package org.smilec.smile.global.p.Groups;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.Quiz.StatusEnum;
import org.smilec.smile.global.hibernate.QuizOption;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.QuestionDAO;
import org.smilec.smile.global.hibernate.logic.QuizDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.p.Public.PublicViewLogic;
import org.smilec.smile.global.ui.BarChart;
import org.smilec.smile.global.ui.BarChartData;
import org.smilec.smile.global.ui.CreateQuestionContainer;
import org.smilec.smile.global.ui.PieChart;
import org.smilec.smile.global.ui.QuestionInteractionLogic;
import org.smilec.smile.global.ui.QuestionList;
import org.smilec.smile.global.ui.QuestionListManager;
import org.smilec.smile.global.utils.Utils;
import org.smilec.smile.global.vars.Css;
import org.vaadin.addon.JFreeChartWrapper;
import org.vaadin.dialogs.ConfirmDialog;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class QuizViewLogic implements ClickListener, Serializable, SelectedTabChangeListener, QuestionListManager, CreateQuestionContainer {
	private static final int interfaceHeightCreateQuestion = 155;
	private static final String CreateQuestionJQueryHandle = ".v-csslayout-container";

	//component vars
	private QuizView quizView;
	private Layout userLayout;
	private Group group;
	private Quiz quiz;
	private List<QuizOption> options;
	private QuestionList questionDisplay;
	//vars to hold charts for moderator tab
	private JFreeChartWrapper questionsChart = null;
	private Table resultsTable = null;
	private Panel resultsPanel = null;
	private ExportPanel exportPanel = null;

	//moderator results page vars
	//graph types for comboBox on moderator results page
	public static final int GRAPH_TYPE_TOP = 0; //top graph types
	public static final int GRAPH_TYPE_QUESTION = 1; //top graph types
	public static final int GRAPH_TYPE_STUDENT = 2; //top graph types
	public static final int GRAPH_TYPE_CORRECT = 3;
	public static final int GRAPH_TYPE_S_QUESTIONS = 4; // questions by student
	public static final int GRAPH_TYPE_RATING = 5;
	public static final int GRAPH_TYPE_TIME = 6;

	private ComboBox.ValueChangeListener cbListener; //so listener can be removed
	private int currentGraphType = 1;

	private boolean alternateModerateRefresh = true; //alternates btwn true and false, refresh only attempted on moderate if true
	private User user;

	//option vars
	public boolean exam = false;

	//height
	private final static int heightNormal = 865;

	//Question Storage / state Vars
	private static int questionsPerPage = 10;
	private Boolean showOwn = true;
	private Boolean showAnswered = true;
	private int pageNum = 1;
	private int orderCode = PublicViewLogic.ORDER_CODE_DATE_ASC;
	private int previousQuerySize;
	private int previousMaxPages;

	private List<Integer> organizers;

	//Moderate Vars
	private int totalQuestions; //used to determine when to refresh
	private int totalQuestionInteractions; //used to determine when to refresh

	//Text vars
	private String createQuestionViewText = AppData.getMessage("ViewQuestion"); //ViewQuestion
	private String createQuestionEditText = AppData.getMessage("EditQuestion"); //EditQuestion

	public QuizViewLogic(QuizView quizView, Group group, Quiz quiz) {
		SmileApplication smile = AppData.getApplication();
		this.quizView = quizView;
		this.group = group;
		this.quiz = quiz;
		this.options = QuizDAO.getQuizOptions(quiz.getId());
		this.questionDisplay = quizView.getQuestionDisplay();

		user = smile.getUser();

		//set option vars
		for (int i = 0, l = options.size(); i < l; i++) {
			if (options.get(i).getQuiz_option().equals("exam")) {
				exam = Integer.parseInt(options.get(i).getQuiz_value()) > 0;
				break;
			}
		}

		//init
		//set quiz and container for create question box
		quizView.getCreateQuestionBox().setQuiz(quiz);
		quizView.getCreateQuestionBox().getLogic().setContainer(this);
		organizers = GroupDAO.getGroupOrganizers(group);
		quizView.setImmediate(true);
		this.userLayout = quizView.getMainLayout();
		questionDisplay.setQLManager(this, false);
		//set label
		String groupName = group.getName();
		quizView.getWelcomeLabel().setValue(
				"<h3><p><a href='#groups'>" + AppData.getMessage("Groups") + "</a> > <a href='#groups/"
						+ group.getId() + "'>" + groupName + "</a> > " + quiz.getName() + "</p></h3>");

		//set instance of publicViewLogic with createQuestionBox logic so that it can call 
		//update size to send JS command to browser to resize as cssLayout panel changes size
		//quizView.getCreateQuestionBox().getLogic().setQV_logic(this);

		//change visible tabs depending on whether or not group organizer
		User user = AppData.getApplication().getUser();
		TabSheet mainTabSheet = quizView.getMainTabSheet();
		if (group.isOwner(user) || organizers.contains(user.getId())) {
			initModerateTab();
			questionDisplay.setModerator(true);
			mainTabSheet.removeComponent(quizView.getSessionClosedTab());
			//add listeners
			quizView.getCloseButton().addListener(this);
			quizView.getCreateButton().addListener(this);
			quizView.getSolveButton().addListener(this);
			quizView.getShowResultsButton().addListener(this);
			quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab())
					.setCaption(AppData.getMessage("ViewQuestions")); //ViewQuestions
			if (exam) {
				quizView.getCreateButton().setVisible(false);
			}
		} else {
			mainTabSheet.removeComponent(quizView.getModerateQuiz_tab());
			initCreateQuestionsTab();
			//set available actions based on quiz
			loadQuizState(false);
			if (exam) {
				mainTabSheet.removeComponent(quizView.getCreateQuestionsTab());
			}
		}

		//build order code combo box
		ComboBox orderCB = quizView.getOrderCB();
		orderCB.removeAllItems();
		List<Order> orderItems = new ArrayList<Order>();
		//Must be in order of 0 incrementing by 1
		orderItems.add(new Order(PublicViewLogic.ORDER_CODE_DATE_DESC, AppData.getMessage("Newest")));
		orderItems.add(new Order(PublicViewLogic.ORDER_CODE_DATE_ASC, AppData.getMessage("Oldest")));
		orderItems
				.add(new Order(PublicViewLogic.ORDER_CODE_RATING_DESC, AppData.getMessage("TopRated")));
		orderItems.add(new Order(PublicViewLogic.ORDER_CODE_POPULARITY_DESC, AppData
				.getMessage("MostPopular")));
		orderItems.add(new Order(PublicViewLogic.ORDER_CODE_DIFFICULTY_ASC, AppData
				.getMessage("Easiest")));
		orderItems.add(new Order(PublicViewLogic.ORDER_CODE_DIFFICULTY_DESC, AppData
				.getMessage("Hardest")));

		BeanItemContainer<Order> beans = new BeanItemContainer<Order>(Order.class, orderItems);

		orderCB.setContainerDataSource(beans);
		orderCB.setItemCaptionPropertyId("description");
		orderCB.setNullSelectionAllowed(false);
		orderCB.setImmediate(true);

		//select orderCB based on orderCode
		Iterator it = orderCB.getItemIds().iterator();
		if (it.hasNext()) {
			Order o = (Order) it.next();
			for (int i = 0; i < orderCode; i++) {
				o = (Order) it.next();
			}
			orderCB.setValue(o);
		}

		orderCB.addListener(new ComboBox.ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				int newOrderCode = ((Order) event.getProperty().getValue()).id;
				if (newOrderCode != orderCode) {
					//if just changed orderCode, go back to page 1
					pageNum = 1;
				}
				orderCode = newOrderCode;
				gotoPage(pageNum);
			}
		});

		//TODO: populate tab upon tab selection
		//add listeners
		quizView.getBackButton().addListener(this);
		quizView.getMainTabSheet().addListener(this);
		quizView.getRefreshButton().addListener(this);
		quizView.getShowFeedbackButton().addListener(this);
		quizView.getShareButton().addListener(this);
	}

	public class Order implements Serializable {
		private int id;
		private String description;

		public Order(int id, String description) {
			this.id = id;
			this.description = description;
		}

		public int getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

		public boolean equals(Order o) {
			if (o.id == this.id)
				return true;
			return false;
		}
	}

	public void loadPage(int pageNum, int tabCode, int orderCode) {
		this.pageNum = pageNum;
		this.orderCode = orderCode;
		//tabCode 0 = create, 1 = answer
		if (tabCode == 1 && quiz.getStatus() != StatusEnum.create
				&& quiz.getStatus() != StatusEnum.closed) {
			populateAnswerQuestionList(pageNum);
			quizView.getMainTabSheet().setSelectedTab(quizView.getAnswerQuestionsTab());
		}
	}

	private void populateAnswerQuestionList(int pageNum) {
		String query = "";
		//first count how many questions are in quiz
		int querySize = QuestionDAO.countQuestions(query, user, showOwn, showAnswered, quiz);
		int maxPages = ((querySize - 1) / questionsPerPage) + 1;
		//store number of quiz questions to know when to refresh page
		previousQuerySize = querySize;
		previousMaxPages = maxPages;
		if (pageNum > maxPages) {
			pageNum = 1;
		}
		int start = (pageNum - 1) * questionsPerPage;

		List<Question> questionList =
				QuestionDAO.getQuestions(query, start, questionsPerPage, user, showOwn, showAnswered,
						orderCode, quiz);
		boolean showFeedback = quiz.getShowFeedback() == 1;
		boolean answerable = true;
		if (quiz.getStatus().equals(StatusEnum.show_result)) {
			answerable = false;
		}
		questionDisplay.setQuestionSet(questionList, pageNum, maxPages, showFeedback, answerable, exam);
		//update how many have been answered
		refreshQuestionCount();
		//set order code combo-box
		ComboBox cb = quizView.getOrderCB();
		Iterator it = cb.getItemIds().iterator();
		if (it.hasNext()) {
			Order o = (Order) it.next();
			for (int i = 0; i < orderCode; i++) {
				o = (Order) it.next();
			}
			cb.setValue(o);
		}
	}

	private void moderateRefresh() { //called when on moderate tab
		if (alternateModerateRefresh) {
			alternateModerateRefresh = false;
			//check for new questions or userquestioninteractions
			int newTotalQuestions = QuizDAO.getNumQuizQuestions(quiz.getId());
			int newTotalQuestionInteractions = QuizDAO.countQuizUserInteractions(quiz);
			if (newTotalQuestions > totalQuestions
					|| newTotalQuestionInteractions > totalQuestionInteractions) {
				updateSessionSummaryActivity();
			}
			initModerateTab();
		} else {
			alternateModerateRefresh = true;
		}
	}

	private void answerQuestionRefresh() {//called when on Answer Question tab
		//refresh questionList if number of questions increases
		String query = "";
		//TODO: optimize into single query
		int querySize = QuestionDAO.countQuestions(query, user, showOwn, showAnswered, quiz);
		//check session state

		if (querySize > previousQuerySize) { //refresh 
			previousQuerySize = querySize;
			refreshQuestionCount();

			//if maxPages changes, refresh pagination and number of questions text
			int maxPages = ((querySize - 1) / questionsPerPage) + 1;
			if (maxPages > previousMaxPages) {
				questionDisplay.buildPagination(pageNum, maxPages);
			}
		}
	}

	private void refreshQuestionCount() {
		String query = "";
		int numQuestionsNotOwn = QuizDAO.getNumQuizQuestionsNotOwn(quiz.getId(), user.getId());
		int numQuestionsAnswered = QuizDAO.getNumQuizQuestionsAnswered(quiz.getId(), user.getId());
		int numQuestionsTotal = QuizDAO.getNumQuizQuestions(quiz.getId());
		if (group.isOwner(user) || organizers.contains(user.getId())) {
			quizView.getAnsweredLabel().setValue(AppData.getFormattedMsg("xQPosted", numQuestionsTotal)); //xQPosted
		} else {
			quizView.getAnsweredLabel().setValue(
					AppData.getFormattedMsg("answeredX", new Object[] { numQuestionsAnswered,
							numQuestionsNotOwn }));
			AppData.getFormattedMsg("createdX", new Object[] { numQuestionsTotal, numQuestionsNotOwn });
			//answeredX
			//createdX
		}
	}

	private void refreshQuestionList() {
		populateAnswerQuestionList(pageNum);
		questionDisplay.reselectQuestion();
	}

	private void initModerateTab() {
		//get current quiz mode, called when state is changed by button and by moderate refresh (in case there are two organizers)
		StatusEnum quizState = quiz.getStatus();
		String stateText = "";
		//System.out.println("InitModerateTab, sessionState: " + quizState);
		if (quizState == StatusEnum.async) {
			//async mode
			//System.out.println("InitModerateTab: Async");
			//hide panels for other modes
			quizView.getSyncPanel().setVisible(false);
			quizView.getModerateInfo().setValue(AppData.getMessage("AsynchronousQuiz"));
			//show feedback for questions?
			if (quiz.getShowFeedback().equals(1)) {
				quizView.getShowFeedbackLabel().setValue(AppData.getMessage("feedbackVisible"));
				quizView.getShowFeedbackButton().setCaption(AppData.getMessage("hideFeedback"));
			} else {
				quizView.getShowFeedbackLabel().setValue(AppData.getMessage("feedbackHidden"));
				quizView.getShowFeedbackButton().setCaption(AppData.getMessage("showFeedback"));
			}
		} else { //if synchronous session
			quizView.getSyncPanel().setVisible(true);
			quizView.getAsyncPanel().setVisible(false);
		}
		//set different create modes
		if (quizState == StatusEnum.closed) {
			if (exam) {
				stateText =
						AppData.getFormattedMsg("StateX",
								Css.highlightRed(AppData.getMessage("sessionNotStart")))
								+ AppData.getMessage("quizInstruct0");
			} else {
				stateText =
						AppData.getFormattedMsg("StateX",
								Css.highlightRed(AppData.getMessage("sessionNotStart")))
								+ AppData.getMessage("pressCreate");
			}
			quizView.getCloseButton().addStyleName(Css.defaultRed);
			quizView.getCreateButton().removeStyleName(Css.defaultRed);
			quizView.getCreateButton().setEnabled(true);
			quizView.getCreateButton().removeStyleName(Css.defaultRed);
			quizView.getSolveButton().setEnabled(true);
			quizView.getSolveButton().removeStyleName(Css.defaultRed);
			quizView.getShowResultsButton().setEnabled(true);
			quizView.getShowResultsButton().removeStyleName(Css.defaultRed);
		} else if (quizState == StatusEnum.create) {
			stateText =
					AppData.getFormattedMsg("StateX", Css.highlightRed(AppData.getMessage("createMode")))
							+ AppData.getMessage("pressSolve");
			quizView.getCloseButton().removeStyleName(Css.defaultRed);
			quizView.getCreateButton().setEnabled(true);
			quizView.getCreateButton().addStyleName(Css.defaultRed);
			quizView.getSolveButton().setEnabled(true);
			quizView.getSolveButton().removeStyleName(Css.defaultRed);
			quizView.getShowResultsButton().setEnabled(true);
			quizView.getShowResultsButton().removeStyleName(Css.defaultRed);
		} else if (quizState == StatusEnum.solve) {
			stateText =
					AppData.getFormattedMsg("StateX", Css.highlightRed(AppData.getMessage("solveMode")))
							+ AppData.getMessage("pressShow");
			quizView.getCloseButton().removeStyleName(Css.defaultRed);
			quizView.getCreateButton().setEnabled(true);
			quizView.getCreateButton().removeStyleName(Css.defaultRed);
			quizView.getSolveButton().setEnabled(true);
			quizView.getSolveButton().addStyleName(Css.defaultRed);
			quizView.getShowResultsButton().setEnabled(true);
			quizView.getShowResultsButton().removeStyleName(Css.defaultRed);
		} else if (quizState == StatusEnum.show_result) {
			quizView.getCloseButton().removeStyleName(Css.defaultRed);
			stateText =
					AppData.getFormattedMsg("StateX", Css.highlightRed(AppData.getMessage("showMode")));
			quizView.getCreateButton().setEnabled(true);
			quizView.getCreateButton().removeStyleName(Css.defaultRed);
			quizView.getSolveButton().setEnabled(true);
			quizView.getSolveButton().removeStyleName(Css.defaultRed);
			quizView.getShowResultsButton().setEnabled(true);
			quizView.getShowResultsButton().addStyleName(Css.defaultRed);
		}
		quizView.getSyncLabel().setValue(stateText);
		//store num Questions and UserQuestionInteractions, then refresh only when these change
		totalQuestions = QuizDAO.getNumQuizQuestions(quiz.getId());
		totalQuestionInteractions = QuizDAO.countQuizUserInteractions(quiz);
		updateSessionSummaryActivity();
	}

	private void initCreateQuestionsTab() {
		//check if table is built

		Table qTable = quizView.getMyQuestionsTable();
		if (qTable.getContainerPropertyIds().size() == 0) {
			//load questions
			List<Question> myQs = QuizDAO.getQuizQuestionsCreated(quiz.getId(), user.getId());
			//build table
			qTable.addContainerProperty(AppData.getMessage("QuestionsCreated"), String.class, ""); //QuestionsCreated
			qTable.setSelectable(true);
			qTable.setMultiSelect(false);
			populateQuestionsCreated();
			//add listeners
			quizView.getEditQuestionButton().addListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					if (quizView.getEditQuestionButton().getCaption().equals(createQuestionEditText)) {
						createEditQuestion();
					} else {
						createViewQuestion();
					}
				}

			});
		}
		//edit question logic
	}

	@Override
	public void questionCreated() {
		populateQuestionsCreated();
	}

	@Override
	public void questionEdited() {
		populateQuestionsCreated();
	}

	@Override
	public void questionEditCancel() {

	}

	private void populateQuestionsCreated() {
		Table qTable = quizView.getMyQuestionsTable();
		//update question table
		qTable.removeAllItems();
		List<Question> myQs = QuizDAO.getQuizQuestionsCreated(quiz.getId(), user.getId());
		Iterator<Question> it = myQs.iterator();
		//only allow editing if in sync create mode
		if (quiz.getStatus().equals(StatusEnum.create) || quiz.getStatus().equals(StatusEnum.async)) {
			if (!it.hasNext()) {
				quizView.getEditQuestionButton().setEnabled(false);
			} else {
				quizView.getEditQuestionButton().setEnabled(true);
				quizView.getEditQuestionButton().setCaption(createQuestionEditText);
			}
		} else {
			quizView.getEditQuestionButton().setVisible(true);
			if (group.isOwner(user) || organizers.contains(user.getId())) {
				quizView.getEditQuestionButton().setCaption(createQuestionEditText);
			} else {
				quizView.getEditQuestionButton().setCaption(createQuestionViewText);
			}
		}
		while (it.hasNext()) {
			Question qq = it.next();
			qTable.addItem(new Object[] { qq.getQuestionText() }, qq);
		}
	}

	private void createEditQuestion() {
		Table qTable = quizView.getMyQuestionsTable();
		Question selectedQ;
		if (qTable.getValue() != null) {
			selectedQ = (Question) qTable.getValue();
			quizView.getCreateQuestionBox().getLogic().editQuestion(selectedQ);
		} else {
			//show notification
			AppData.getApplication().showNotification(AppData.getMessage("Sorry"),
					AppData.getMessage("quizMsg0"), SmileApplication.NOTIFICATION_WARNING);
		}
	}

	private void createViewQuestion() {
		Table qTable = quizView.getMyQuestionsTable();
		Question selectedQ;
		if (qTable.getValue() != null) {
			selectedQ = (Question) qTable.getValue();
			quizView.getCreateQuestionBox().getLogic().viewQuestion(selectedQ);
		} else {
			//show notification
			AppData.getApplication().showNotification(AppData.getMessage("Sorry"),
					AppData.getMessage("quizMsg1"), SmileApplication.NOTIFICATION_WARNING);
		}
	}

	private void initResultsTab() {
		if (group.isOwner(user) || organizers.contains(user.getId())) {
			drawResultsGraph(currentGraphType);
			//init question comboBox
			ComboBox cb = quizView.getModerateResultsCB();
			cb.setTextInputAllowed(false);
			if (cb.getItemIds().size() == 0) {
				//if cb is not initiated
				cb.removeAllItems();
				List<GraphType> cbItems = new ArrayList<GraphType>();
				cbItems.add(new GraphType(GRAPH_TYPE_TOP, AppData.getMessage("SessionSummary")));
				cbItems.add(new GraphType(GRAPH_TYPE_QUESTION, AppData.getMessage("QuestionTable")));
				cbItems.add(new GraphType(GRAPH_TYPE_STUDENT, AppData.getMessage("StudentTable")));
				cbItems.add(new GraphType(GRAPH_TYPE_CORRECT, AppData.getMessage("accuracyGraph"))); //accuracyGraph
				cbItems.add(new GraphType(GRAPH_TYPE_S_QUESTIONS, AppData.getMessage("questionGraph"))); //questionGraph
				cbItems.add(new GraphType(GRAPH_TYPE_RATING, AppData.getMessage("ratingsGraph"))); //ratingsGraph
				cbItems.add(new GraphType(GRAPH_TYPE_TIME, AppData.getMessage("timeGraph"))); //timeGraph
				BeanItemContainer<GraphType> beans =
						new BeanItemContainer<GraphType>(GraphType.class, cbItems);

				cb.setContainerDataSource(beans);
				cb.setItemCaptionPropertyId("description");
				cb.setNullSelectionAllowed(false);
				cb.setImmediate(true);
				cb.removeListener(cbListener);
				//select currently selected item
				Iterator it = cb.getItemIds().iterator();
				GraphType g = (GraphType) it.next();
				for (int i = 0; i < currentGraphType; i++) {
					g = (GraphType) it.next();
				}
				cb.setValue(g);
				cbListener = new ComboBox.ValueChangeListener() {

					@Override
					public void valueChange(ValueChangeEvent event) {
						int newGraphCode = ((GraphType) event.getProperty().getValue()).id;
						currentGraphType = newGraphCode;
						drawResultsGraph(currentGraphType);
					}

				};
				cb.addListener(cbListener);
			}

		} else {
			//if not group organizer or owner
			String[] results = GroupResults.getUserResults(quiz.getId(), user);
			String userResults = results[0];
			String classResults = results[1];
			quizView.getUserResultsLabel().setValue(userResults);
			quizView.getClassResultsLabel().setValue(classResults);
			quizView.getModerateResultsCB().setVisible(false);
			quizView.getExportButton().setVisible(false);
			quizView.getShareButton().setVisible(false);
		}
		//addListener for resultsRefresh
		quizView.getResultsRefreshButton().removeListener(this);
		quizView.getResultsRefreshButton().addListener(this);
		quizView.getExportButton().removeListener(this);
		quizView.getExportButton().addListener(this);
	}

	private void drawResultsGraph(int graphType) {
		if (graphType == GRAPH_TYPE_TOP) {
			//remove existing resuts components
			clearResultsComponents();
			quizView.getUserResultsHL().setVisible(true);
			String[] results = GroupResults.getUserResults(quiz.getId(), user);
			String classResults = results[1];
			quizView.getUserResultsPanel().setVisible(false);
			quizView.getClassResultsLabel().setValue(classResults);
			quizView.getResultsVL().setVisible(true);
		} else if (graphType == GRAPH_TYPE_QUESTION) {
			quizView.getUserResultsHL().setVisible(false);
			if (questionsChart != null) {
				quizView.getResultsVL().removeComponent(questionsChart);
				questionsChart = null;
			}
			clearResultsComponents();
			resultsTable = new Table();
			resultsTable.setImmediate(true);
			resultsTable.setHeight("400px");
			resultsTable.setWidth("100%");
			resultsTable.setSelectable(true);
			resultsTable.setMultiSelect(false);
			resultsTable.setColumnReorderingAllowed(true);
			resultsTable.addListener(new Property.ValueChangeListener() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1456165300462264251L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					Question q = (Question) resultsTable.getValue();
					if (q != null) {
						VerticalLayout panelVL = new VerticalLayout();
						Label qLabel = new Label();
						qLabel.setContentMode(Label.CONTENT_XHTML);
						qLabel.setValue(AppData.getFormattedMsg("questionX", q.getQuestionText()));
						panelVL.addComponent(qLabel);
						Label answerLabel = new Label();
						answerLabel.setContentMode(Label.CONTENT_XHTML);
						String answerString = "";
						ArrayList<QuestionAnswer> answers = q.getQuestionAnswerList();
						String correctText =
								" <span class='label-bs label-bs-success'><i class='icon-ok icon-white'></i> "
										+ AppData.getMessage("CorrectAnswer") + "</span>";
						//for answer choice chart
						ArrayList<String> labels = new ArrayList<String>();
						ArrayList<Number> percent = new ArrayList<Number>();
						ArrayList<String> legend = new ArrayList<String>();
						legend.add(AppData.getMessage("NumberChoices")); //NumberChoices
						for (int i = 0, l = answers.size(); i < l; i++) {
							QuestionAnswer answer = answers.get(i);
							answerString += "<p><b>(" + (i + 1) + ")</b> " + answer.getAnswerText();
							if (answer.getIsCorrect()) {
								answerString += correctText;
							}
							answerString += "</p>";
							//for chart
							labels.add("" + (i + 1));
							int numResponses = answers.get(i).getNumResponses();
							percent.add(numResponses);
						}
						answerLabel.setValue(answerString);
						HorizontalLayout answerHL = new HorizontalLayout();
						answerHL.setWidth("100%");
						answerHL.setHeight("-1");
						answerHL.addComponent(answerLabel);
						answerHL.setExpandRatio(answerLabel, 1f);
						//answer choice pie chart
						ArrayList<ArrayList<Number>> numArray = new ArrayList<ArrayList<Number>>();
						numArray.add(percent);
						BarChartData answerData =
								new BarChartData(AppData.getFormattedMsg("answeredByX",
										q.getUniqueAnswers()), "", AppData.getMessage("Answer"), legend,
										labels, numArray, 200, 160);
						BarChart answerChart = new BarChart(answerData);
						if (q.getUniqueAnswers() > 0) {
							answerHL.addComponent(answerChart.getChart());
						}
						panelVL.addComponent(answerHL);
						resultsPanel.setContent(panelVL);

						//build userQuestionInteraction panel
						Table userAnswerTable = new Table();
						userAnswerTable.setHeight("250px");
						userAnswerTable.setWidth("100%");
						userAnswerTable.addContainerProperty(AppData.getMessage("User"), String.class,
								null);
						userAnswerTable.addContainerProperty(AppData.getMessage("AnswerChoice"),
								Integer.class, null);
						userAnswerTable.addContainerProperty(AppData.getMessage("Correct"),
								String.class, null);
						userAnswerTable.addContainerProperty(AppData.getMessage("Rating"),
								Integer.class, null);
						userAnswerTable.addContainerProperty(AppData.getMessage("AnswerTimeS"),
								Integer.class, null); //AnswerTimeS

						List<UserQuestionInteraction> interactions =
								QuestionDAO.getUserQuestionInteractions(q.getId());
						Iterator<UserQuestionInteraction> it = interactions.iterator();
						//for rating distribution graph
						labels = new ArrayList<String>();
						ArrayList<Integer> ratings = new ArrayList<Integer>();
						for (int ii = 1; ii <= 5; ii++) {
							labels.add("" + ii);
							ratings.add(0);
						}
						int counter = 0;
						while (it.hasNext()) {
							UserQuestionInteraction uqi = it.next();
							String user = uqi.getUser().getUsername().toString();
							QuestionAnswer qa = uqi.getUserAnswer();
							//determine answer number
							Integer answerNum = null;
							for (int i = 0, l = answers.size(); i < l; i++) {
								if (qa.equals(answers.get(i))) {
									answerNum = i + 1;
								}
							}
							String correct =
									qa.getIsCorrect() ? AppData.getMessage("True") : AppData
											.getMessage("False");
							Integer rating = null;
							if (uqi.getRating() != null) {
								rating = uqi.getRating() / 2;
							}
							//add for graph
							if (rating != null && rating > 0) {
								ratings.set(rating - 1, ratings.get(rating - 1).intValue() + 1);
							}
							Integer time = uqi.getSecComplete();
							userAnswerTable.addItem(new Object[] { user, answerNum, correct, rating,
									time }, counter);
							counter++;
						}
						/*numArray = new ArrayList<ArrayList<Number>>();
						numArray.add(ratings);
						BarChartData data =
								new BarChartData("Rating distribution", "", "", legend, labels,
										numArray, 200, 160);
						BarChart barChart = new BarChart(data, false, true, false);*/
						PieChart ratingChart =
								new PieChart(AppData.getMessage("RatingDistribution"), labels, ratings,
										200, 160, true);

						if (q.getUniqueAnswers() > 0) {
							//build rating distribution graph
							answerHL.addComponent(ratingChart.getChart());
						}
						panelVL.addComponent(userAnswerTable);
					}
				}
			});
			resultsPanel = new Panel();
			VerticalLayout resultsPanelVL = (VerticalLayout) resultsPanel.getContent();
			resultsPanelVL.addComponent(new Label(AppData.getMessage("quizInstruct1"))); //quizInstruct1

			String questionColumnName = AppData.getMessage("Question");
			resultsTable.addContainerProperty(questionColumnName, String.class, null);
			resultsTable.setColumnWidth(questionColumnName, 285);

			resultsTable.addContainerProperty(AppData.getMessage("User"), String.class, null);
			resultsTable.addContainerProperty(AppData.getMessage("TimesAnswered"), Integer.class, null);
			resultsTable.addContainerProperty(AppData.getMessage("perCorrect"), Integer.class, null); //perCorrect
			resultsTable.addContainerProperty(AppData.getMessage("AvgRating"), Float.class, null); //AvgRating
			resultsTable.addContainerProperty(AppData.getMessage("avgTime0"), Float.class, null); //avgTime0

			List<Question> questions = QuizDAO.getQuizQuestions(quiz.getId());
			Iterator<Question> it = questions.iterator();
			while (it.hasNext()) {
				Question q = it.next();
				String qText = q.getQuestionText();
				String username = UserDAO.getUserName(q.getUserId());
				Integer answers = q.getUniqueAnswers();
				Integer avgCorrect = (int) (q.getAvgCorrect() * 100f);
				Float avgRating = q.getAvgRating() / 2;
				Float avgTime = q.getAvgSecComplete();

				resultsTable.addItem(new Object[] { qText, username, answers, avgCorrect, avgRating,
						avgTime }, q);
			}

			quizView.getResultsVL().addComponent(resultsTable);
			quizView.getResultsVL().addComponent(resultsPanel);
		} else if (graphType == GRAPH_TYPE_STUDENT) {
			quizView.getUserResultsHL().setVisible(false);
			if (questionsChart != null) {
				quizView.getResultsVL().removeComponent(questionsChart);
				questionsChart = null;
			}
			clearResultsComponents();
			resultsTable = new Table();
			resultsTable.setImmediate(true);
			resultsTable.setHeight("400px");
			resultsTable.setWidth("100%");
			resultsTable.setSelectable(true);
			resultsTable.setMultiSelect(false);
			resultsTable.setColumnReorderingAllowed(true);
			resultsTable.addListener(new Property.ValueChangeListener() {
				@Override
				public void valueChange(ValueChangeEvent event) {
					User u = (User) resultsTable.getValue();
					Item i = resultsTable.getItem(u);
					if (u != null) {
						VerticalLayout panelVL = new VerticalLayout();
						resultsPanel.setCaption(AppData.getFormattedMsg("XsessionResults", new Object[] {
								u.getFirstName(), u.getLastName(), u.getUsername() })); //XsessionResults

						Label createLabel = new Label();
						Label answerLabel = new Label();
						createLabel.addStyleName("lineHeightMid");
						answerLabel.addStyleName("lineHeightMid");

						resultsPanel.setContent(panelVL);

						createLabel.setContentMode(Label.CONTENT_XHTML);
						answerLabel.setContentMode(Label.CONTENT_XHTML);
						Integer qPosted = (Integer) i.getItemProperty("questPosted").getValue();
						if (qPosted == null)
							qPosted = 0;
						String createString = AppData.getFormattedMsg("QuestionsCreatedX", qPosted);
						//QuestionsCreatedX
						if (qPosted > 0) {
							createString +=
									"<li>"
											+ AppData.getFormattedMsg("correctRecX",
													i.getItemProperty("correctRec").toString())
											+ "</li>"
											+ "<li>"
											+ AppData.getFormattedMsg("ratingRecX",
													i.getItemProperty("ratingRec").toString())
											+ "</li>"
											+ "<li>"
											+ AppData.getFormattedMsg("timeRecX",
													i.getItemProperty("timeRec").toString()) + "</li>";
						}
						Integer qAnswered = (Integer) i.getItemProperty("questAnswered").getValue();
						String answerString =
								AppData.getFormattedMsg("QuestionsAnsweredX", +qAnswered) + "<br>"; //QuestionsAnsweredX
						if (qAnswered > 0) {
							answerString +=
									"<li>"
											+ AppData.getFormattedMsg("correctGaveX",
													i.getItemProperty("correctGave").toString())
											+ "</li>"
											+ "<li>"
											+ AppData.getFormattedMsg("ratingGaveX",
													i.getItemProperty("ratingGave").toString())
											+ "</li>"
											+ "<li>"
											+ AppData.getFormattedMsg("timeGaveX",
													i.getItemProperty("timeGave").toString()) + "</li>";
						}
						createLabel.setValue(createString);
						answerLabel.setValue(answerString);
						HorizontalLayout resultsHL = new HorizontalLayout();
						resultsHL.setMargin(true);
						resultsHL.setSpacing(true);
						resultsHL.setWidth("100%");
						resultsHL.setHeight("-1");
						resultsHL.addComponent(createLabel);
						resultsHL.addComponent(answerLabel);
						resultsHL.setExpandRatio(createLabel, 1f);
						resultsHL.setExpandRatio(answerLabel, 1f);
						panelVL.addComponent(resultsHL);
					}
				}
			});
			resultsPanel = new Panel();
			VerticalLayout resultsPanelVL = (VerticalLayout) resultsPanel.getContent();
			resultsPanelVL.addComponent(new Label(AppData.getMessage("quizInstruct2"))); //quizInstruct2

			ArrayList<Quiz> quizzes = new ArrayList<Quiz>();
			quizzes.add(quiz);
			IndexedContainer container = GroupResults.getStudentReport(quizzes);
			resultsTable.setContainerDataSource(container);
			resultsTable.setColumnHeaders(new String[] { AppData.getMessage("Username"),
					AppData.getMessage("Name"), AppData.getMessage("numQuestionsCreated"),
					AppData.getMessage("%corrQuestionCreated"), AppData.getMessage("avgRatingCreate"),
					AppData.getMessage("avgTimeCreate"), AppData.getMessage("numQuestionsAnswered"),
					AppData.getMessage("%correctQuestionsAnswer"), AppData.getMessage("avgRatingGiven"),
					AppData.getMessage("avgTimeAnswerQ") });
			quizView.getResultsVL().addComponent(resultsTable);
			quizView.getResultsVL().addComponent(resultsPanel);
		} else {
			//if BarChart
			quizView.getUserResultsHL().setVisible(false);
			//BarChartData questionsData = GroupResults.getClassQuestionChart(quiz.getId());
			BarChartData questionsData = null;
			//rebuild components
			clearResultsComponents();
			boolean integerTicks = true;
			switch (graphType) {
			case GRAPH_TYPE_CORRECT:
				questionsData = GroupResults.getClassCorrectChart(quiz.getId());
				integerTicks = false;
				break;
			case GRAPH_TYPE_S_QUESTIONS:
				questionsData = GroupResults.getClassQuestionChart(quiz.getId());
				break;
			case GRAPH_TYPE_RATING:
				questionsData = GroupResults.getClassRatingChart(quiz.getId());
				integerTicks = false;
				break;
			case GRAPH_TYPE_TIME:
				questionsData = GroupResults.getClassTimeChart(quiz.getId());
				break;
			}
			if (questionsData != null) {
				//size barChart
				int height = 400;
				int num = questionsData.name.size();
				if (num > 10) {
					height += 15 * (num - 10);
				}
				questionsData.height = height;
				questionsData.width = 800;
				BarChart questionsBar = new BarChart(questionsData, true, integerTicks, true);
				questionsChart = questionsBar.getChart();
				quizView.getResultsVL().addComponent(questionsChart);
			}
		}
	}

	public void shareQuizQuestions() {
		List<Question> questions = QuizDAO.getQuizQuestions(quiz.getId());
		Iterator<Question> it = questions.iterator();
		while (it.hasNext()) {
			Question q = it.next();
			Question nq = new Question();

			nq.setUserId(q.getUserId());
			nq.setQuestionText(q.getQuestionText());
			nq.setMedia_url(q.getMedia_url());
			nq.setIsPublic(1); // 1 public
			nq.setCreatedOn(new Date());
			nq.setUniqueAnswers(0);
			nq.setUniqueRatings(0);
			nq.setAvgCorrect(0f);
			nq.setAvgRating(0f);
			nq.setAvgSecComplete(0f);
			nq.setContentType(q.getContentType());
			nq.setTagText(q.getTagText());
			QuizDAO.insertQuestion(nq);

			Iterator<QuestionAnswer> iterator = q.getQuestionAnswerList().iterator();
			Set<QuestionAnswer> questionAnswers = new HashSet<QuestionAnswer>();

			while (iterator.hasNext()) {
				QuestionAnswer qa = iterator.next();
				QuestionAnswer new_qa = new QuestionAnswer();
				new_qa.setQuestion(qa.getQuestion());
				new_qa.setAnswerText(qa.getAnswerText());
				new_qa.setIsCorrect(qa.getIsCorrect() ? 1 : 0);
				new_qa.setNumResponses(0);
				new_qa.setQuestion(nq);
				questionAnswers.add(new_qa);
			}
			nq.setQuestionAnswers(questionAnswers);
			QuizDAO.updateQuestion(nq);

		}
		quiz.setIsPublic(1);
		QuizDAO.updateQuiz(quiz);
	}

	public void showExportResults() {
		clearResultsComponents();
		exportPanel = new ExportPanel(quiz);
		exportPanel.getCancelButton().addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				drawResultsGraph(((GraphType) quizView.getModerateResultsCB().getValue()).id);
			}

		});
		quizView.getResultsVL().addComponent(exportPanel);
	}

	public void clearResultsComponents() {
		quizView.getUserResultsHL().setVisible(false);
		if (questionsChart != null) {
			quizView.getResultsVL().removeComponent(questionsChart);
			questionsChart = null;
		}
		if (resultsTable != null) {
			quizView.getResultsVL().removeComponent(resultsTable);
		}
		if (resultsPanel != null) {
			quizView.getResultsVL().removeComponent(resultsPanel);
		}
		if (exportPanel != null) {
			quizView.getResultsVL().removeComponent(exportPanel);
			exportPanel = null;
		}
	}

	public class GraphType implements Serializable {
		private int id;
		private String description;

		public GraphType(int id, String description) {
			this.id = id;
			this.description = description;
		}

		public int getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

		public boolean equals(GraphType gt) {
			if (gt.id == this.id)
				return true;
			return false;
		}
	}

	private void updateSessionSummaryActivity() {
		//clear table
		Table activityTable = quizView.getActivityTable();
		activityTable.removeAllItems();
		activityTable.setSelectable(false);
		activityTable.addContainerProperty(AppData.getMessage("User"), String.class, "");
		activityTable.addContainerProperty(AppData.getMessage("QuestionsCreated"), String.class, "");
		activityTable.addContainerProperty(AppData.getMessage("QuestionsAnswered"), String.class, "");

		//Get Users
		List<User> users = GroupDAO.getGroupUsers(group);
		Iterator<User> i = users.iterator();
		int totalQuestionsPosted = QuizDAO.getNumQuizQuestions(quiz.getId());
		int totalQuestionsAnswered = 0;
		int usersPostedQuestions = 0;
		int usersAnsweredQuestions = 0;
		int usersAnsweringAllQuestions = 0;
		while (i.hasNext()) {
			User user = i.next();
			int questionsPosted = QuizDAO.getNumQuizQuestionsPosted(quiz.getId(), user.getId());
			if (questionsPosted > 0) {
				usersPostedQuestions++;
			}
			int questionsAnswered = QuizDAO.getNumQuizQuestionsAnswered(quiz.getId(), user.getId());
			totalQuestionsAnswered += questionsAnswered;
			if (questionsAnswered == totalQuestionsPosted) {
				usersAnsweringAllQuestions++;
			}
			activityTable.addItem(
					new Object[] { user.getUsername(), questionsPosted, questionsAnswered },
					user.getId());
		}
		String summ = "";
		if (totalQuestionsPosted > 0) {
			summ +=
					"<p>" + AppData.getFormattedMsg("totalQPosted", totalQuestionsPosted) + "</b>"
							+ "</p><p></p>"; //totalQPosted
			summ +=
					"<p>"
							+ AppData.getFormattedMsg("XYcreatedQs", new Object[] {
									usersPostedQuestions, users.size() }); //XYcreatedQs

			double difference = (double) totalQuestionsPosted / (double) users.size();
			String s = String.format("%.2f %n", difference);
			summ += "<p>" + AppData.getFormattedMsg("avgQPerMember", s) + "</p><p></p>"; //avgQPerMember
			double diff2 = (double) totalQuestionsAnswered / (double) users.size();
			summ +=
					"<p>" + AppData.getFormattedMsg("avgQAnswer", String.format("%.2f %n", diff2))
							+ "</p><p></p>";
			if (usersAnsweringAllQuestions > 0) {
				summ += "<p>" + AppData.getFormattedMsg("totalQAnswer", usersAnsweringAllQuestions); //totalQAnswer
			}
		} else {
			summ += "<p>" + AppData.getMessage("NoQs") + "</p><p></p>"; //NoQs
		}

		//summ += "</ul>";
		quizView.getSummaryLbl().setValue(summ);
		//quizView.getSummaryLbl3().setValue(s3);
		//quizView.getSummaryLbl4().setValue(s4);

		//Session summary
		//float avgCorrectRate = QuizDAO.getAverageCorrectQuestionsRate();
	}

	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {
		if (event.getTabSheet().getSelectedTab() == quizView.getAnswerQuestions_tab()) {
			populateAnswerQuestionList(pageNum);
		} else {
			questionDisplay.closeAnswerTab();
		}
		if (event.getTabSheet().getSelectedTab() == quizView.getCreateQuestionsTab()) {
			initCreateQuestionsTab();
		} else if (event.getTabSheet().getSelectedTab() == quizView.getModerateQuiz_tab()) {
			initModerateTab();
		}
		if (event.getTabSheet().getSelectedTab() == quizView.getResultsTab()) {
			initResultsTab();
		}
		//if (event.getTabSheet().getSelectedTab() == quizView.getAnswerQuestionList()) {}
	}

	@Override
	public UserQuestionInteraction submitQuestion(Question question, QuestionAnswer qa, int rating,
			String comment, Integer secComplete) {
		//check if on either create question mode or in asynchronous state
		//update quiz
		quiz = QuizDAO.getQuizById(quiz.getId());
		if (quiz.getStatus().equals(StatusEnum.closed)) {

		}
		//Submit Successful:
		User user = AppData.getApplication().getUser();
		//create ui interaction
		UserQuestionInteraction ui = new UserQuestionInteraction();
		ui.setUser(user);
		ui.setQuestion(question);
		ui.setUserAnswer(qa);
		ui.setCreatedOn(Utils.getTime());
		if (rating > 0) {
			ui.setRating(rating * 2); //rating is based on 10 to allow for future possibility of half star ratings
		}
		if (secComplete != null) {
			ui.setSecComplete(secComplete);
		}
		UserDAO.saveUserQuestionIneraction(ui);
		QuestionInteractionLogic.updateQuestionInteractionStats(ui);
		//refresh how many questions have been answered
		refreshQuestionCount();
		return ui;
	}

	@Override
	public void gotoPage(int pageNumber) {
		//System.out.println("GO TO PAGE " + pageNumber);
		//page chagnes only called from answer tab
		AppData.getApplication().gotoQuiz(group, quiz, pageNumber, QuestionList.TABCODE_ANSWER,
				orderCode);
	}

	private void loadQuizState(boolean change) {
		if (!group.isOwner(user) && !organizers.contains(user.getId())) {
			if (quiz.getStatus().equals(StatusEnum.closed)) {
				//modify createQuestionTab
				quizView.getMainTabSheet().getTab(quizView.getSessionClosedTab()).setVisible(true);
				quizView.getMainTabSheet().setSelectedTab(quizView.getSessionClosedTab());
				if (quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab()) != null) {
					quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab())
							.setEnabled(false);
				}
				quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab()).setEnabled(false);
				quizView.getMainTabSheet().getTab(quizView.getResultsTab()).setEnabled(false);
			} else if (quiz.getStatus().equals(StatusEnum.create)) {
				quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab()).setEnabled(true);
				quizView.getMainTabSheet().setSelectedTab(quizView.getCreateQuestionsTab());
				quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab()).setEnabled(false);
				quizView.getMainTabSheet().getTab(quizView.getSessionClosedTab()).setVisible(false);
				quizView.getMainTabSheet().getTab(quizView.getResultsTab()).setEnabled(false);
				if (change) {
					AppData.getApplication().showNotification(AppData.getMessage("CreateQuestions!"), //CreateQuestions!
							AppData.getMessage("quizInstruct3"), SmileApplication.NOTIFICATION_INFO);
				}
			} else if (quiz.getStatus().equals(StatusEnum.solve)) {
				populateAnswerQuestionList(pageNum);
				quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab()).setEnabled(true);
				quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab())
						.setCaption(AppData.getMessage("AnswerQuestions"));
				quizView.getMainTabSheet().setSelectedTab(quizView.getAnswerQuestions_tab());
				quizView.getMainTabSheet().getTab(quizView.getSessionClosedTab()).setVisible(false);
				if (quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab()) != null) {
					quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab())
							.setEnabled(false);
				}
				quizView.getMainTabSheet().getTab(quizView.getResultsTab()).setEnabled(false);
				if (change) {
					AppData.getApplication().showNotification(AppData.getMessage("SolveQuestions!"), //SolveQuestions!
							AppData.getMessage("answerAllQ"), SmileApplication.NOTIFICATION_INFO); //answerAllQ
				}
			} else if (quiz.getStatus().equals(StatusEnum.show_result)) {
				populateAnswerQuestionList(pageNum);
				quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab()).setEnabled(true);
				quizView.getMainTabSheet().getTab(quizView.getAnswerQuestions_tab())
						.setCaption(AppData.getMessage("ReviewQuestions"));
				quizView.getMainTabSheet().setSelectedTab(quizView.getAnswerQuestions_tab());
				quizView.getMainTabSheet().getTab(quizView.getSessionClosedTab()).setVisible(false);
				if (quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab()) != null) {
					quizView.getMainTabSheet().getTab(quizView.getCreateQuestionsTab())
							.setEnabled(false);
				}
				quizView.getMainTabSheet().getTab(quizView.getResultsTab()).setEnabled(true);
				if (change) { //if just set to show result, then reload question list
					refreshQuestionList();
					AppData.getApplication().showNotification(AppData.getMessage("ReviewQuestions"), //ReviewQuestions
							AppData.getMessage("answerQComplete"), SmileApplication.NOTIFICATION_INFO); //answerQComplete
				}
			} else if (quiz.getStatus().equals(StatusEnum.async)) {
				quizView.getMainTabSheet().removeComponent(quizView.getSessionClosedTab());
				if (quiz.getShowFeedback().equals(1)) {
					quizView.getMainTabSheet().getTab(quizView.getResultsTab()).setEnabled(true);
				} else {
					//if not showing feedback, hide results tab
					if (quizView.getMainTabSheet().getSelectedTab() == quizView.getResultsTab()) {
						quizView.getMainTabSheet().setSelectedTab(quizView.getAnswerQuestions_tab());
						AppData.getApplication().showNotification("", AppData.getMessage("quizMsg2"), //quizMsg2
								SmileApplication.NOTIFICATION_INFO);
					}
					quizView.getMainTabSheet().getTab(quizView.getResultsTab()).setEnabled(false);
				}
			}
		}
	}

	//called by refresh in QuizView, which listens to refresher from SmileApplication
	public void refresh(Refresher source) {
		//System.out.println("QUIZ REFRESH");
		//reload quiz 
		Quiz oldQuiz = quiz;
		quiz = QuizDAO.getQuizById(quiz.getId());
		//check if should show questions feedback if not a sync quiz
		if (!quiz.getStatus().equals(StatusEnum.create) && !quiz.getStatus().equals(StatusEnum.solve)
				&& !quiz.getStatus().equals(StatusEnum.show_result)) {
			if (quiz.getShowFeedback() != oldQuiz.getShowFeedback()) { //if showing feedback status changed, refreshQuestionList
				refreshQuestionList();
				loadQuizState(true);
			}
		}
		if (!quiz.getStatus().equals(oldQuiz.getStatus())) {
			//reset session state
			loadQuizState(true);
		}
		if (quizView.getMainTabSheet().getSelectedTab() == quizView.getAnswerQuestions_tab()) {
			//keep reloading questions
			answerQuestionRefresh();
		} else if (quizView.getMainTabSheet().getSelectedTab() == quizView.getModerateQuiz_tab()) {
			moderateRefresh();
		}
	}

	@Override
	public void editQuestion(Question q) {
		quizView.getCreateQuestionBox().getLogic().editQuestion(q);
		quizView.getMainTabSheet().setSelectedTab(quizView.getCreateQuestionsTab());
	}

	@Override
	public void deleteQuestion(Question q) {
		try {
			QuestionDAO.deleteQuestionComplete(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		populateAnswerQuestionList(pageNum);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button == quizView.getBackButton()) {
			AppData.getApplication().gotoGroupsMemberPage(group);
		} else if (button == quizView.getCloseButton()) {
			quiz.setStatus(StatusEnum.closed);
			QuizDAO.updateQuiz(quiz);
			initModerateTab();
		} else if (button == quizView.getCreateButton()) {
			quiz.setStatus(StatusEnum.create);
			QuizDAO.updateQuiz(quiz);
			initModerateTab();
		} else if (button == quizView.getSolveButton()) {
			quiz.setStatus(StatusEnum.solve);
			quiz.setShowFeedback(0);
			QuizDAO.updateQuiz(quiz);
			initModerateTab();
		} else if (button == quizView.getShowResultsButton()) {
			quiz.setStatus(StatusEnum.show_result);
			quiz.setShowFeedback(1);
			QuizDAO.updateQuiz(quiz);
			initModerateTab();
		} else if (button == quizView.getRefreshButton()) {
			refreshQuestionList();
		} else if (button == quizView.getShowFeedbackButton()) {
			if (quiz.getShowFeedback().equals(1)) {
				//if showing feedback, hide feedback
				quiz.setShowFeedback(0);
				QuizDAO.updateQuiz(quiz);
				initModerateTab();
			} else {
				//if hiding feedback, show feedback
				quiz.setShowFeedback(1);
				QuizDAO.updateQuiz(quiz);
				initModerateTab();
			}
		} else if (button == quizView.getResultsRefreshButton()) {
			initResultsTab();
		} else if (button == quizView.getExportButton()) {
			//show export results modal window
			showExportResults();
		} else if (button == quizView.getShareButton()) {
			// share quiz questions publicly
			if (quiz.getIsPublic() == 0) {
				ConfirmDialog.show(AppData.getApplication().getMainWindow(),
						AppData.getMessage("PleaseConfirm"), AppData.getMessage("sharePublicConfirm"),
						AppData.getMessage("ExportPublicly"), AppData.getMessage("NotYet"),
						new ConfirmDialog.Listener() {

							@Override
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									shareQuizQuestions();
									AppData.getApplication().showNotification(
											AppData.getMessage("Success"),
											AppData.getFormattedMsg("quizXShared", quiz.getName()), //quizXShared
											SmileApplication.NOTIFICATION_WARNING);
								} else {
									// User did not confirm				                	
								}
							}
						});
			} else {
				AppData.getApplication().showNotification(AppData.getMessage("Sorry"),
						AppData.getMessage("alreadyShared"), SmileApplication.NOTIFICATION_WARNING); //alreadyShared
			}
		}
	}

	@Override
	public boolean getMyQuestions() {
		//is always false, as group page is not used to display myQuestions (public page is)
		return false;
	}
}
