package org.smilec.smile.global.p.Groups;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.vars.Css;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class GroupsViewLogic implements ClickListener, Serializable {
	//component vars
	private GroupsView groupsView;
	private Layout mainLayout;
	private TextField passCodeTF; //holds reference to passCodeTF to obtain value from

	//constants
	private static int interfaceHeight = 185; //added height for area above and below jQuery object
	private static final String GroupsJQueryHandle = ".v-csslayout-container";
	private static int height = 990;

	private String SEARCH_GROUP = AppData.getMessage("GroupName");
	private String SEARCH_ORGANIZER = AppData.getMessage("OrganizerName");

	//stores organizers
	List<Integer> organizers;

	public GroupsViewLogic(GroupsView groupsView) {
		this.groupsView = groupsView;
		//init
		groupsView.setImmediate(true);
		this.mainLayout = groupsView.getMainLayout();

		populateGroupsTable();
		//add listeners
		groupsView.getCreateGroupButton().addListener(this);
		groupsView.getSearchButton().addListener(this);
		groupsView.getRefreshButton().addListener(this);
		//init search group CB
		ComboBox searchCB = groupsView.getSearchTypeCB();
		searchCB.setNullSelectionAllowed(false);
		searchCB.setTextInputAllowed(false);
		searchCB.addItem(SEARCH_GROUP);
		searchCB.addItem(SEARCH_ORGANIZER);
		searchCB.select(SEARCH_GROUP);
		//start with blank search
		searchGroups();
	}

	private void populateGroupsTable() {
		Table groupsTable = groupsView.getGroupsTable();
		groupsTable.removeAllItems();
		//set no resize of columns via Css hack
		groupsTable.addStyleName(Css.tableNoResize);

		groupsTable.setColumnReorderingAllowed(false);
		groupsTable.setColumnCollapsingAllowed(false);
		groupsTable.setSelectable(false);
		groupsTable.setSortDisabled(true);
		String groupsColumnName = AppData.getMessage("MyGroups");
		groupsTable.addContainerProperty(groupsColumnName, Label.class, null);
		groupsTable.addContainerProperty(" ", Button.class, null);
		groupsTable.setColumnWidth(groupsColumnName, 350);
		//groupsTable.addContainerProperty("Organizer?", Boolean.class, null);
		//groupsTable.addContainerProperty("Number of Quizzes", Integer.class, null);

		User user = AppData.getApplication().getUser();
		List<Group> myGroups = GroupDAO.getGroups(user);
		Iterator<Group> i = myGroups.iterator();
		while (i.hasNext()) {
			Group g = i.next();
			organizers = GroupDAO.getGroupOrganizers(g);
			int numQuizzes = GroupDAO.countQuizzes(g);
			Label label = new Label();
			label.setContentMode(label.CONTENT_XHTML);
			Button b = new Button();
			//add button listener
			b.addListener(this);
			if (g.isOwner(user)) {
				//if group owner
				b.setCaption(AppData.getMessage("Organize"));
				//store instance of group on button, also store whether is organizer or not
				b.setData(new Object[] { "myGroups", g, true });
				String organizerText =
						" <span class='label-bs label-bs-success'><i class='icon-asterisk icon-white'></i> "
								+ AppData.getMessage("owner") + "</span>";
				label.setValue(g.getName() + organizerText);
			} else if (organizers.contains(user.getId())) {
				//if group organizer
				b.setCaption(AppData.getMessage("Organize"));
				//store instance of group on button, also store whether is organizer or not
				b.setData(new Object[] { "myGroups", g, true });
				String organizerText =
						" <span class='label-bs label-bs-success'><i class='icon-asterisk icon-white'></i> "
								+ AppData.getMessage("organizer") + "</span>";
				label.setValue(g.getName() + organizerText);
			} else {
				b.setCaption(AppData.getMessage("Enter"));
				//store instance of group on button, also store whether is organizer or not
				b.setData(new Object[] { "myGroups", g, false });
				label.setValue(g.getName());
			}
			groupsTable.addItem(new Object[] { label, b }, g);

			//item.getItemProperty("Group").setValue(g.getName());
			//item.getItemProperty("Organizer?").setValue(g.isOrganizer(user));
			//item.getItemProperty("Number of Quizzes").setValue(numQuizzes);
		}

		groupsTable.requestRepaint();
	}

	private void searchGroups() {
		String searchText = (String) groupsView.getSearchGroupTF().getValue();
		List<Object[]> groups = null;
		Table gt = groupsView.getSearchGroupTable();
		gt.removeAllItems();
		gt.addContainerProperty("Group", String.class, null);
		gt.addContainerProperty("Organizer", String.class, null);
		gt.setColumnHeader("Group", AppData.getMessage("Group"));
		gt.setColumnHeader("Organizer", AppData.getMessage("Organizer"));
		gt.addContainerProperty("", Button.class, null);
		gt.setColumnWidth("Group", 270);
		gt.setColumnWidth("Organizer", 160);
		gt.setColumnWidth("", 120);
		gt.setSelectable(false);
		User user = AppData.getApplication().getUser();
		if (groupsView.getSearchTypeCB().getValue().equals(SEARCH_GROUP)) {
			groups = GroupDAO.searchNewGroupsByName(searchText, user.getId());
		} else if (groupsView.getSearchTypeCB().getValue().equals(SEARCH_ORGANIZER)) {
			groups = GroupDAO.searchNewGroupsByOrganizer(searchText, user.getId());
		}

		if (groups == null || groups.size() == 0) {
			gt.addItem(new Object[] { AppData.getMessage("noGroups"), "", null }, 0);
		} else {
			Iterator<Object[]> it = groups.iterator();
			while (it.hasNext()) {
				Object[] o = it.next();
				Group g = (Group) o[0];
				String orgUsername = (String) o[1];
				Button b = new Button();
				b.setData(new Object[] { "enterPasscode", g });
				b.setCaption(AppData.getMessage("Join"));
				b.addListener(this);
				gt.addItem(new Object[] { g.getName(), orgUsername, b }, g.getId());
			}
		}
	}

	private void showPassCodeWindow(Group selected) {
		VerticalLayout vl = new VerticalLayout();
		vl.setMargin(true);
		vl.setSpacing(true);
		Label l0 = new Label();
		l0.setValue("<p>"
				+ AppData.getFormattedMsg("groupName", selected.getName())
				+ "</p><p>"
				+ AppData.getFormattedMsg("organizerName",
						UserDAO.getUserName(selected.getOrganizer_id())) + "</p>");
		l0.setContentMode(Label.CONTENT_XHTML);
		l0.setWidth("100%");
		l0.setHeight("-1");
		Label l1 = new Label();
		l1.setWidth("100%");
		l1.setHeight("-1");
		l1.setValue(AppData.getMessage("enterPasscode"));
		TextField passCode = new TextField();
		passCode.setWidth("100%");
		passCodeTF = passCode;
		passCode.setInputPrompt(AppData.getMessage("EnterPasscode"));
		Button joinButton = new Button();
		joinButton.setCaption(AppData.getMessage("JoinGroup"));
		joinButton.setData(new Object[] { "joinGroup", selected });
		joinButton.addListener(this);
		Button cancelButton = new Button();
		cancelButton.setCaption(AppData.getMessage("Cancel"));
		cancelButton.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				cancelJoin();
			}

		});
		HorizontalLayout buttonHL = new HorizontalLayout();
		buttonHL.setWidth("100%");
		buttonHL.addComponent(cancelButton);
		buttonHL.addComponent(joinButton);
		buttonHL.setComponentAlignment(cancelButton, Alignment.BOTTOM_LEFT);
		buttonHL.setComponentAlignment(joinButton, Alignment.BOTTOM_RIGHT);
		vl.addComponent(l1);
		vl.addComponent(l0);
		vl.addComponent(passCode);
		vl.addComponent(buttonHL);
		vl.setSizeFull();
		vl.setComponentAlignment(buttonHL, Alignment.BOTTOM_RIGHT);
		//store old panel in SearchPanel data
		//groupsView.getSearchGroupTable().removeAllItems();
		groupsView.getSearchPanel().setData(groupsView.getSearchLayout());
		groupsView.getSearchPanel().setContent(vl);
	}

	private void joinGroup(Group g, String passcode) {
		if (g.getPasscode().equals(passcode)) {
			User user = AppData.getApplication().getUser();
			GroupDAO.addUserToGroup(g, user);
			AppData.getApplication().showNotification(AppData.getMessage("Success"),
					AppData.getFormattedMsg("joinGroupSuccess", g.getName()),
					SmileApplication.NOTIFICATION_SUCCESS);
			populateGroupsTable();
			//return to groups view
			Layout origLayout = (Layout) groupsView.getSearchPanel().getData();
			groupsView.getSearchPanel().setContent(origLayout);
		} else {
			AppData.getApplication().showNotification(AppData.getMessage("Sorry"),
					AppData.getMessage("incorrectPC"), SmileApplication.NOTIFICATION_ERROR);
		}
	}

	private void cancelJoin() {
		Layout origLayout = (Layout) groupsView.getSearchPanel().getData();
		groupsView.getSearchPanel().setContent(origLayout);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button == groupsView.getCreateGroupButton()) {
			AppData.getApplication().gotoCreateGroups();
		} else if (button == groupsView.getSearchButton()) {
			searchGroups();
		} else if (button == groupsView.getRefreshButton()) {
			populateGroupsTable();
		} else {
			//generated buttons store data object to identify listener type
			if (button.getData() instanceof Object[]) {
				Object[] o = (Object[]) button.getData();
				String buttonCode = (String) o[0];
				//Button on My Groups Table (either organize or enter)

				if (buttonCode.equals("myGroups")) {
					Group group = (Group) o[1];
					Boolean organizer = (Boolean) o[2];
					SmileApplication smile = AppData.getApplication();
					if (group != null) {
						if (organizer) {
							//if organizer button is pressed
							smile.gotoGroupsOrganizerPage(group);
						} else {
							//if member button is pressed
							smile.gotoGroupsMemberPage(group);
						}
					}
				} else if (buttonCode.equals("enterPasscode")) {
					Group selected = (Group) o[1];
					//check if group member
					User user = AppData.getApplication().getUser();
					if (GroupDAO.userExist(selected.getId(), user.getId())) {
						AppData.getApplication().showNotification(AppData.getMessage("Oops"),
								AppData.getMessage("alreadyMember"), SmileApplication.NOTIFICATION_INFO);

					} else {
						//if joining group, show passcode window to join
						showPassCodeWindow(selected);
					}
				} else if (buttonCode.equals("joinGroup")) {
					Group g = (Group) o[1];
					String passcode = (String) passCodeTF.getValue();
					joinGroup(g, passcode);
				}
			}
		}
	}
}
