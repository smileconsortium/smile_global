package org.smilec.smile.global.p.Groups;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Evite;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.GroupOrganizer;
import org.smilec.smile.global.hibernate.Message;
import org.smilec.smile.global.hibernate.MsgRecipient;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.Quiz.StatusEnum;
import org.smilec.smile.global.hibernate.QuizOption;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.MessageDAO;
import org.smilec.smile.global.hibernate.logic.QuizDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.vars.Css;
import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.data.Item;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;

public class OrganizeGroupViewLogic implements ClickListener, Serializable {
	private static final String groupTabJQueryHandle = ".groupTab";
	private static final int interfaceHeight = 145;
	private static final String memberTabJQueryHandle = ".memberTab";

	//component vars
	private OrganizeGroupView organizeView;
	private Layout userLayout;
	private Group group; //group for current organize group page
	private List<Integer> organizers;
	private Boolean isOwner;

	private String MEMBER;
	private String ORGANIZER;

	//constants
	//private static int interfaceHeight = 185; //added height for area above and below jQuery object
	//private static final String GroupsJQueryHandle = ".v-csslayout-container";

	public OrganizeGroupViewLogic(OrganizeGroupView organizeView, Group group) {
		this.organizeView = organizeView;
		this.group = group;

		//init
		organizeView.setImmediate(true);
		organizeView.getChangeRolePanel().setVisible(false);
		MEMBER = organizeView.MEMBER;
		ORGANIZER = organizeView.ORGANIZER;

		this.userLayout = organizeView.getMainLayout();
		organizeView.getMembersTable().addStyleName(Css.tableNoResize);
		//set jquery css handles
		organizeView.getMemberCssTab().addStyleName("memberTab");
		organizeView.getGroupTab().addStyleName("groupTab");
		//set label
		organizeView.getWelcomeLabel().setValue(
				"<h3><p><a href='#groups'>" + AppData.getMessage("Groups") + "</a> > " + group.getName()
						+ "</p></h3>");

		// populate tab upon tab selection
		populateMembersTable();
		populateQuizTable();
		updatePasscode();

		isOwner = group.isOwner(AppData.getApplication().getUser());
		//only show delete group button if an owner
		if (isOwner) {
			organizeView.getDeleteGroupButton().addListener(this);
		} else {
			organizeView.getDeleteGroupButton().setVisible(false);
		}
		//add listeners
		organizeView.getBackButton().addListener(this);
		organizeView.getCreateQuizButton().addListener(this);
		organizeView.getDeleteQuizButton().addListener(this);
		organizeView.getSendInviteButton().addListener(this);
		organizeView.getChangeButton().addListener(this);
		organizeView.getChangeRoleButton().addListener(this);
		organizeView.getRefreshButton().addListener(this);
		organizeView.getRoleChangeButton().addListener(this);
		organizeView.getRemoveButton().addListener(this);
		organizeView.getRoleCancelButton().addListener(this);
		organizeView.getMessageAllBtn().addListener(this);
		organizeView.getMessageBtn().addListener(this);
	}

	private void updatePasscode() {
		String passcode = group.getPasscode();
		if (passcode != null)
			organizeView.getPasscodeTF().setValue(passcode);
	}

	private void populateQuizTable() {
		Table quizTable = organizeView.getQuizTable();
		quizTable.removeAllItems();

		quizTable.setColumnReorderingAllowed(true);
		quizTable.setColumnCollapsingAllowed(false);
		quizTable.setSelectable(true);
		quizTable.setSortDisabled(true);
		quizTable.addStyleName(Css.tableNoResize);
		String quizColumnName = AppData.getMessage("Sessions") + " - " + group.getName();
		quizTable.addContainerProperty(quizColumnName, Label.class, null);
		quizTable.addContainerProperty(" ", Button.class, null);
		quizTable.setColumnWidth(quizColumnName, 382);
		quizTable.setColumnWidth(" ", 120);
		//groupsTable.addContainerProperty("Organizer?", Boolean.class, null);
		//groupsTable.addContainerProperty("Number of Quizzes", Integer.class, null);

		User user = AppData.getApplication().getUser();
		List<Quiz> myQuizzes = GroupDAO.getQuizzes(group);
		Iterator<Quiz> iterator = myQuizzes.iterator();
		if (!iterator.hasNext()) {
			//if no quizzes
			Label label = new Label();
			label.setContentMode(label.CONTENT_XHTML);
			label.setValue(AppData.getMessage("noSessions"));
			quizTable.addItem(new Object[] { label, null }, "noQuiz");
			organizeView.getDeleteQuizButton().setEnabled(false);
		}
		while (iterator.hasNext()) {
			String quizType;
			Quiz quiz = iterator.next();
			Label label = new Label();
			label.setContentMode(label.CONTENT_XHTML);
			Button b = new Button();
			//store instance of group on button
			b.setData(quiz);
			//add button listener
			b.addListener(this);
			b.setCaption(AppData.getMessage("Manage"));
			StatusEnum status = quiz.getStatus();
			String statusText = "";
			//check if Quiz is an exam
			List<QuizOption> options = QuizDAO.getQuizOptions(quiz.getId());
			Boolean isExam = false;
			for (int i = 0, l = options.size(); i < l; i++) {
				if (options.get(i).getQuiz_option().equals("exam")) {
					isExam = Integer.parseInt(options.get(i).getQuiz_value()) > 0;
					break;
				}
			}
			if (isExam) {
				quizType = "examStatus";
			} else {
				quizType = "syncStatus";
			}

			if (status.equals(StatusEnum.async)) {
				statusText = AppData.getMessage("Async");
			} else if (status.equals(StatusEnum.closed)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("NotStarted"));
			} else if (status.equals(StatusEnum.create)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("Create"));
			} else if (status.equals(StatusEnum.show_result)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("Results"));
			} else if (status.equals(StatusEnum.solve)) {
				statusText = AppData.getFormattedMsg(quizType, AppData.getMessage("Solve"));
			}
			String organizerText =
					" <span class='label-bs label-bs-warning'><i class='icon-asterisk icon-white'></i> "
							+ statusText + "</span>";
			label.setValue(quiz.getName() + organizerText);
			quizTable.addItem(new Object[] { label, b }, quiz);

			//item.getItemProperty("Group").setValue(g.getName());
			//item.getItemProperty("Organizer?").setValue(g.isOwner(user));
			//item.getItemProperty("Number of Quizzes").setValue(numQuizzes);
		}

		quizTable.requestRepaint();
	}

	private void refreshPage() {
		User user = AppData.getApplication().getUser();
		//check if status changed
		if (!GroupDAO.getGroupOrganizers(group).contains(user.getId()) && !group.isOwner(user)) {
			AppData.getApplication().setUrifu(""); //first clear fragment so fragment change handler gets called
			AppData.getApplication().gotoGroupsMemberPage(group);
		}
		populateMembersTable();
		populateQuizTable();
		updatePasscode();
	}

	private void populateMembersTable() {
		organizers = GroupDAO.getGroupOrganizers(group);
		Table membersTable = organizeView.getMembersTable();
		membersTable.removeAllItems();

		membersTable.setColumnReorderingAllowed(false);
		membersTable.setColumnCollapsingAllowed(false);
		membersTable.setSelectable(true);
		membersTable.setImmediate(true);
		membersTable.setSortDisabled(false);
		String quizColumnName = AppData.getMessage("GroupMembers") + " - " + group.getName();
		membersTable.addContainerProperty(" ", Embedded.class, null);
		membersTable.addContainerProperty(quizColumnName, String.class, null);
		membersTable.addContainerProperty("Role", String.class, null);
		membersTable.setColumnHeader("Role", AppData.getMessage("Role"));
		membersTable.setColumnWidth(" ", 40);
		membersTable.setColumnWidth(quizColumnName, 180);
		//groupsTable.addContainerProperty("Organizer?", Boolean.class, null);
		//groupsTable.addContainerProperty("Number of Quizzes", Integer.class, null);

		User organizer = AppData.getApplication().getUser();
		List<User> myUsers = GroupDAO.getGroupUsers(group);
		Iterator<User> i = myUsers.iterator();
		if (!i.hasNext()) {
			//if no group members
			membersTable.addItem(new Object[] { null, AppData.getMessage("noMembers") }, "noMembers");
		}
		while (i.hasNext()) {
			User user = i.next();
			String role = AppData.getMessage("Member");
			//set avatar
			Embedded e = new Embedded();
			Resource avatar = user.getAvatar();
			if (avatar == null) {
				avatar = new ThemeResource("img/component/embedded_icon.png");
			}
			e.setSource(avatar);

			if (user.getId().equals(group.getOrganizer_id())) {
				role = AppData.getMessage("Owner");
			} else if (organizers.contains(user.getId())) {
				role = AppData.getMessage("Organizer");
			}

			membersTable.addItem(new Object[] { e, user.getUsername(), role }, user);

			//item.getItemProperty("Group").setValue(g.getName());
			//item.getItemProperty("Organizer?").setValue(g.isOwner(user));
			//item.getItemProperty("Number of Quizzes").setValue(numQuizzes);
		}

		membersTable.requestRepaint();
	}

	/*
		public void updateHeight() {
			organizeView.setMinPanelHeight(organizeView.getMinPanelHeight(), GroupsJQueryHandle,
					interfaceHeight);
		}*/

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (event.getButton() == organizeView.getBackButton()) {
			AppData.getApplication().gotoGroups();
		} else if (button == organizeView.getCreateQuizButton()) {
			AppData.getApplication().gotoCreateQuiz(group);
		} else if (button == organizeView.getDeleteQuizButton()) {
			//delete quiz
			if (organizeView.getQuizTable().getValue() instanceof Quiz) {
				final Quiz quiz = (Quiz) organizeView.getQuizTable().getValue();
				if (quiz == null) {
					String msg = AppData.getMessage("noSessionSelected");
					displayMsg("DeleteSessionError", msg);
				} else {
					ConfirmDialog.show(AppData.getApplication().getMainWindow(),
							AppData.getMessage("DeleteSessionConfirmation"),
							AppData.getMessage("deleteSessionMsg"), AppData.getMessage("DeleteSession"),
							AppData.getMessage("Cancel"), new ConfirmDialog.Listener() {

								@Override
								public void onClose(ConfirmDialog dialog) {
									if (dialog.isConfirmed()) {
										// Confirmed to delete session
										try {
											QuizDAO.deleteQuizComplete(quiz.getId());
											populateQuizTable();
										} catch (Exception e) {
											e.printStackTrace();
											String msg = AppData.getMessage("sessionDeleteError");
											displayMsg(AppData.getMessage("Oops"), msg);
										}
									} else {
										// User did not confirm				                	
									}
								}
							});
				}

			}
		} else if (button == organizeView.getDeleteGroupButton()) {
			//delete group
			ConfirmDialog.show(AppData.getApplication().getMainWindow(),
					AppData.getMessage("DeleteGroupConfirmation"),
					AppData.getMessage("deleteGroupConfirm"), AppData.getMessage("DeleteGroup"),
					AppData.getMessage("Cancel"), new ConfirmDialog.Listener() {

						@Override
						public void onClose(ConfirmDialog dialog) {
							if (dialog.isConfirmed()) {
								// Confirmed to delete session
								String groupName = group.getName();
								try {
									QuizDAO.deleteGroup(group);
									AppData.getApplication().gotoGroups();
									AppData.getApplication().showNotification(
											AppData.getMessage("GroupDeleted"),
											AppData.getFormattedMsg("groupDeleteMsg", groupName),
											SmileApplication.NOTIFICATION_SUCCESS);
								} catch (Exception e) {
									e.printStackTrace();
									String msg = AppData.getMessage("grpDeleteError");
									displayMsg(AppData.getMessage("Oops"), msg);
								}
							} else {
								// User did not confirm				                	
							}
						}
					});

		} else if (button.equals(organizeView.getSendInviteButton())) {
			sendInvite((String) organizeView.getInviteMembersTextArea().getValue());
		} else if (button.equals(organizeView.getChangeButton())) {
			changePasscode((String) organizeView.getPasscodeTF().getValue());
		} else if (button.equals(organizeView.getRefreshButton())) {
			refreshPage();
		} else if (button.equals(organizeView.getRoleCancelButton())) {
			organizeView.getChangeRolePanel().setVisible(false);
			organizeView.getMembersTable().setVisible(true);
			organizeView.getChangeRoleButton().setVisible(true);
		} else if (button.equals(organizeView.getRoleChangeButton())) {
			User user = (User) organizeView.getMembersTable().getValue();
			String role = (String) organizeView.getOptionRoles().getValue();
			if (role.equals(ORGANIZER)) {
				if (!organizers.contains(user.getId())) {
					GroupOrganizer organizer = new GroupOrganizer();
					organizer.setGroup_id(group.getId());
					organizer.setUser_id(user.getId());
					GroupDAO.addGroupOrganizer(organizer);
					organizers.add(user.getId());
					Item woItem = organizeView.getMembersTable().getItem(user);
					woItem.getItemProperty("Role").setValue(AppData.getMessage("Organizer"));
				}
			} else // member
			{
				if (organizers.contains(user.getId())) {
					GroupDAO.deleteGroupOrganizer(group.getId(), user.getId());
					organizers.remove(user.getId());
					Item woItem = organizeView.getMembersTable().getItem(user);
					woItem.getItemProperty("Role").setValue(AppData.getMessage("Member"));
				}
			}

			organizeView.getChangeRolePanel().setVisible(false);
			organizeView.getMembersTable().setVisible(true);
			organizeView.getChangeRoleButton().setVisible(true);
		} else if (button.equals(organizeView.getChangeRoleButton())) {
			User user = (User) organizeView.getMembersTable().getValue();
			String msg = "";
			if (user == null) {
				msg = AppData.getMessage("noMemberSelected");
				displayMsg(AppData.getMessage("ChangeRoleError"), msg);
			} else if (user.getId().equals(group.getOrganizer_id())) {
				AppData.getFormattedMsg("changeRoleError1", user.getUsername());
				displayMsg(AppData.getMessage("ChangeRoleError"), msg); //
			} else {
				organizeView.getMemberLBL().setValue(
						AppData.getFormattedMsg("memberName", user.getUsername()));
				if (organizers.contains(user.getId()))
					organizeView.getOptionRoles().select(ORGANIZER);
				else
					organizeView.getOptionRoles().select(MEMBER);
				organizeView.getChangeRolePanel().setVisible(true);
				organizeView.getMembersTable().setVisible(false);
				organizeView.getChangeRoleButton().setVisible(false);
			}
		} else if (button.equals(organizeView.getMessageBtn())) {
			String msg;
			User user = (User) organizeView.getMembersTable().getValue();
			if (user == null) {
				msg = AppData.getMessage("messageError0");
				displayMsg(AppData.getMessage("Oops"), msg);
			} else {
				AppData.getApplication().gotoMessages(user);
			}
		} else if (button.equals(organizeView.getMessageAllBtn())) {
			AppData.getApplication().gotoMessages(group);
		} else if (button.equals(organizeView.getRemoveButton())) {
			final User user = (User) organizeView.getMembersTable().getValue();
			if (user == null) {
				String msg = AppData.getMessage("groupError0");
				displayMsg(AppData.getMessage("Oops"), msg);
			} else if (group.isOwner(user)) {
				String msg = AppData.getMessage("groupError1");
				displayMsg(AppData.getMessage("Oops"), msg);
			} else if (organizers.contains(user.getId()) && !isOwner) {
				String msg = AppData.getMessage("groupError2");
				displayMsg(AppData.getMessage("Sorry"), msg);
			} else {
				//remove
				String msg =
						AppData.getFormattedMsg("removeUser?",
								new Object[] { user.getUsername(), group.getName() });
				ConfirmDialog.show(AppData.getApplication().getMainWindow(),
						AppData.getMessage("removeUserConfirm"), msg, AppData.getMessage("Remove"),
						AppData.getMessage("Cancel"), new ConfirmDialog.Listener() {

							@Override
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to remove user
									GroupDAO.deleteUserFromGroup(group.getId(), user.getId());
									populateMembersTable();
								} else {
									// User did not confirm				                	
								}
							}
						});

			}
		} else {
			//check if a quiz table button
			try {
				Quiz quiz = (Quiz) button.getData();
				SmileApplication smile = AppData.getApplication();
				if (quiz != null) {
					smile.gotoQuiz(group, quiz);
				}
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
			}
		}
	}

	private void displayMsg(String title, String msg) {
		AppData.getApplication().showNotification(title, msg, SmileApplication.NOTIFICATION_ERROR);
	}

	private void changePasscode(String passcode) {

		String message = "";
		if (passcode.equals(group.getPasscode()))
			return;

		if (passcode == null || passcode.length() != CreateGroupView.passcodeLen) {
			message = AppData.getFormattedMsg("passcodeLengthPrompt", CreateGroupView.passcodeLen);
		}
		//check if passcode contains prohibited characters
		else if (passcode.matches(".*[/\\+\\?\\.!\\$\\|\\^%\\{\\}\\[\\]\\(\\)\\\\#]+.*")) {
			message = AppData.getMessage("passcodeBadChars");
		}

		if (message.length() > 0) {
			AppData.getApplication().showNotification(AppData.getMessage("ChangePasscodeError"),
					message, SmileApplication.NOTIFICATION_ERROR);
		} else {
			group.setPasscode(passcode);
			GroupDAO.updateGroup(group);
			AppData.getApplication().showNotification(AppData.getMessage("Success"),
					AppData.getMessage("passcodeChange"), SmileApplication.NOTIFICATION_INFO);
		}
	}

	private void sendInvite(String usersString) {
		String message = "";
		ArrayList<String> errorEntries = new ArrayList<String>();
		ArrayList<String> successUsernames = new ArrayList<String>();
		ArrayList<String> successEmails = new ArrayList<String>();
		//temp to store users in
		ArrayList<User> successUsers = new ArrayList<User>();
		Pattern pattern;
		Matcher matcher;
		String EMAIL_PATTERN =
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
						+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);

		if (usersString == "" || usersString == null) {
			message = AppData.getMessage("groupError3");
		} else {
			String[] users = usersString.split(",");
			for (int i = 0, l = users.length; i < l; i++) {
				String userString = users[i].trim();
				if (userString != "" && userString != null) {
					//first try to look up user, then send email to user
					User user = UserDAO.getUserByUserName(userString);
					if (user != null) {
						// send invite
						//For now, just add user to group automatically
						successUsernames.add(user.getUsername());
						successUsers.add(user);
					} else {
						//if user doesn't exist, check if email address is valid
						//String emailRegex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
						matcher = pattern.matcher(userString);
						//if (userString.matches(emailRegex)) {
						if (matcher.matches()) {
							// send email
							List<User> list = UserDAO.getUserByEmail(userString);
							if (list != null && list.size() > 0) {
								for (int idx = 0; idx < list.size(); idx++) {
									User u = list.get(idx);
									successUsernames.add(u.getUsername());
									successUsers.add(u);
								}
							} else
								successEmails.add(userString);
						} else {
							//if invalid email address and no username found
							errorEntries.add(userString);
						}
					}
				}
			}
		}
		//success / failure message
		if (errorEntries.size() > 0) {
			message += AppData.getMessage("groupError4");
		}
		for (int i = 0, l = errorEntries.size(); i < l; i++) {
			message += errorEntries.get(i);
			if (i < l - 1) {
				message += ", ";
			} else {
				message += ".\n";
			}
		}
		if (successUsernames.size() > 0)
			message += AppData.getMessage("inviteConfirm");
		for (int i = 0, l = successUsernames.size(); i < l; i++) {
			message += successUsernames.get(i);
			if (i < l - 1) {
				message += ", ";
			} else {
				message += ".\n";
			}
		}
		if (successEmails.size() > 0) {
			message += AppData.getMessage("inviteEmail");
		}
		for (int i = 0, l = successEmails.size(); i < l; i++) {
			message += successEmails.get(i);
			if (i < l - 1) {
				message += ", ";
			} else {
				message += ".\n";
			}
		}
		String messageTitle;
		if (errorEntries.size() > 0) {
			if (successUsernames.size() > 0 || successEmails.size() > 0) {
				messageTitle = AppData.getMessage("someInvites");
			} else {
				messageTitle = AppData.getMessage("noInvites");
			}
		} else {
			if (successUsernames.size() > 0 || successEmails.size() > 0) {
				messageTitle = AppData.getMessage("invitesSent");
			} else {
				messageTitle = AppData.getMessage("Oops");
			}
		}
		AppData.getApplication().showNotification(messageTitle, message,
				SmileApplication.NOTIFICATION_INFO);
		// send out email invites
		//for now, just add members to group
		if (successUsers.size() > 0) {
			//create Message
			Message m = new Message();
			m.setMessage(" ");
			m.setMessageType(1);
			m.setMetaData("" + group.getId());
			m.setSender_id(AppData.getApplication().getUser().getId());
			m.setSubject(AppData.getFormattedMsg("groupInvite", group.getName()));
			m.setMessage(AppData.getFormattedMsg("groupInvite1", group.getName()));
			m.setSentOn(new Date());
			m.setSenderedDeleted(0);
			MessageDAO.saveMessage(m);

			for (int i = 0, l = successUsers.size(); i < l; i++) {
				User user = successUsers.get(i);
				if (!GroupDAO.userInGroup(group.getId(), user)) {
					//GroupDAO.addUserToGroup(group, user);
					Evite e = GroupDAO.inviteUserToGroup(group.getId(), user.getId(), getEviteToken(12));
					if (e != null)
						sendEvite(user.getEmail(), e.getId(), e.getToken());
					//add recipient of message
					MsgRecipient mr = new MsgRecipient();
					mr.setRecipient_id(user.getId());
					mr.setMsg_id(m.getId());
					mr.setIsRead(false);
					mr.setRecipientDeleted(0);
					MessageDAO.saveMsgRecipient(mr);
				}
			}
		}
		for (int i = 0, l = successEmails.size(); i < l; i++) {
			Evite e = GroupDAO.inviteUserToGroup(group.getId(), 0, getEviteToken(12));
			if (e != null)
				sendEvite(successEmails.get(i), e.getId(), e.getToken());
		}
		organizeView.getInviteMembersTextArea().setValue("");
		populateMembersTable();
	}

	public static String getEviteToken(int len) {
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));

		return sb.toString();
	}

	public boolean sendEvite(String email, Integer eid, String token) {
		Emailer emailer = new Emailer();
		//java.net.URL url = AppData.getApplication().getURL();
		// host address temporarily hard-coded to smileglobal
		String url = AppData.getApplication().getHostname();
		//String s = url.getHost()+":"+url.getPort()+url.getPath(); // Port ist evtl leer

		String body = AppData.getFormattedMsg("groupInvite2", group.getName());
		body += url + "?evite=" + eid + "&t=" + token;
		body += AppData.getMessage("groupInvite3");

		return emailer.sendEmail("", email, AppData.getMessage("groupInvite4"), body);
	}
}
