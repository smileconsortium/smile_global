package org.smilec.smile.global.p.TopBar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.smilec.smile.global.AppData;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.annotations.AutoGenerated;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class TopBar extends CustomComponent implements Serializable {

	@AutoGenerated
	private HorizontalLayout mainLayout;
	@AutoGenerated
	private Refresher refresher;
	@AutoGenerated
	private Label spacer10;
	@AutoGenerated
	private GridLayout gridLayout_1;
	@AutoGenerated
	private Button forgotPasswordBtn;
	@AutoGenerated
	private Button forgotUsernameBtn;
	@AutoGenerated
	private Button loginBtn;
	@AutoGenerated
	private Label spacer200;
	@AutoGenerated
	private PasswordField passwordTF;
	@AutoGenerated
	private Label spacer100;
	@AutoGenerated
	private TextField usernameTF;
	@AutoGenerated
	private Label spacer20;
	@AutoGenerated
	private VerticalLayout rightVL;
	@AutoGenerated
	private HorizontalLayout feedbackLayout;
	@AutoGenerated
	private Label feedbackLbl;
	@AutoGenerated
	private ComboBox languageCB;
	@AutoGenerated
	private Label spacer;
	@AutoGenerated
	private VerticalLayout messagesButton;
	@AutoGenerated
	private Label messagesLabel;
	@AutoGenerated
	private HorizontalLayout horizontalLayout_2;
	@AutoGenerated
	private Label messageNum;
	@AutoGenerated
	private VerticalLayout profileButton;
	@AutoGenerated
	private Label profileLabel;
	@AutoGenerated
	private VerticalLayout groupsButton;
	@AutoGenerated
	private Label groupsLabel;
	@AutoGenerated
	private VerticalLayout publicButton;
	@AutoGenerated
	private Label publicLabel;
	@AutoGenerated
	private VerticalLayout homeButton;
	@AutoGenerated
	private Label homeLabel;
	@AutoGenerated
	private Label spacer0;
	@AutoGenerated
	private Embedded smileLogo;

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/**
	 * The constructor should first build the main layout, set the
	 * composition root and then do any custom initialization.
	 *
	 * The constructor will not be automatically regenerated by the
	 * visual editor.
	 */

	private TopBarLogic logic;
	private Command setLanguage;

	public TopBar() {
		buildMainLayout();
		localize();
		//set logic
		logic = new TopBarLogic(this);
		forgotUsernameBtn.addStyleName("smallerBtnFont");
		forgotPasswordBtn.addStyleName("smallerBtnFont");
		loginBtn.addStyleName("smallerBtnFont");
		usernameTF.addStyleName("smallerFont");
		passwordTF.addStyleName("smallerFont");
		spacer0.setValue("");
		spacer10.setValue("");
		spacer20.setValue("");
		spacer100.setValue("");
		spacer200.setValue("");
		buildLanguageBar();
		setCompositionRoot(mainLayout);
	}

	private void localize() {
		feedbackLbl
				.setValue("<a href=\"https://gse.qualtrics.com/SE/?SID=SV_3DUKnDwEyVtqvKl\" target=\"_blank\">"
						+ AppData.getMessage("feedbackSupport") + "</a>");
		usernameTF.setCaption(AppData.getMessage("Username"));
		passwordTF.setCaption(AppData.getMessage("Password"));
		loginBtn.setCaption(AppData.getMessage("Login"));
		forgotUsernameBtn.setCaption(AppData.getMessage("ForgotUsername")); //ForgotUsername
		forgotPasswordBtn.setCaption(AppData.getMessage("ForgotPassword")); //ForgotPassword
	}

	public class LanguageItem implements Serializable {
		private int id;
		private Resource icon;
		private String language;
		private String caption;

		public LanguageItem(int id, ThemeResource icon, String language) {
			this.id = id;
			this.icon = icon;
			this.language = language;
			this.caption = "";
		}

		public Resource getIcon() {
			return icon;
		}

		public int getId() {
			return id;
		}

		public String getCaption() {
			return caption;
		}

		public String getLanguage() {
			return language;
		}
	}

	private void buildLanguageBar() {
		//build language selection combo box
		//build order code ComboBox
		languageCB.removeAllItems();
		List<LanguageItem> langs = new ArrayList<LanguageItem>();
		//Must be in order of 0 incrementing by 1
		langs.add(new LanguageItem(0, new ThemeResource("icons/flags/flag_icon_us.png"), "en"));
		langs.add(new LanguageItem(1, new ThemeResource("icons/flags/flag_icon_es.png"), "es"));
		langs.add(new LanguageItem(2, new ThemeResource("icons/flags/flag_icon_fr.png"), "fr"));

		BeanItemContainer<LanguageItem> beans =
				new BeanItemContainer<LanguageItem>(LanguageItem.class, langs);

		languageCB.setContainerDataSource(beans);
		languageCB.setItemIconPropertyId("icon");
		languageCB.setItemCaptionPropertyId("caption");
		languageCB.setNullSelectionAllowed(false);
		languageCB.setImmediate(true);
		Iterator<LanguageItem> it = langs.iterator();
		LanguageItem currentLang = langs.get(0);
		while (it.hasNext()) {
			LanguageItem l = it.next();
			System.out.println(l.getLanguage());
			System.out.println(AppData.getLocale().getLanguage());
			if (l.getLanguage().equals(AppData.getLocale().getLanguage())) {
				currentLang = langs.get(l.getId());
				break;
			}
		}
		languageCB.setValue(currentLang);
		languageCB.addListener(new ComboBox.ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				String newLang = ((LanguageItem) event.getProperty().getValue()).getLanguage();
				AppData.getApplication().changeLocale(new Locale(newLang));
			}
		});
	}

	public GridLayout getLoginLayout() {
		return gridLayout_1;
	}

	public Button getLoginButton() {
		return loginBtn;
	}

	public PasswordField getPasswordField() {
		return passwordTF;
	}

	public TextField getUsernameInput() {
		return usernameTF;
	}

	public Label getFeedbackLbl() {
		return feedbackLbl;
	}

	public Button getForgotPasswordBtn() {
		return forgotPasswordBtn;
	}

	public Button getForgotUsernameBtn() {
		return forgotUsernameBtn;
	}

	public HorizontalLayout getFeedbackLO() {
		return feedbackLayout;
	}

	public HorizontalLayout getMainLayout() {
		return mainLayout;
	}

	public VerticalLayout getProfileButton() {
		return profileButton;
	}

	public VerticalLayout getGroupsButton() {
		return groupsButton;
	}

	public VerticalLayout getPublicButton() {
		return publicButton;
	}

	public VerticalLayout getHomeButton() {
		return homeButton;
	}

	public VerticalLayout getMessagesButton() {
		return messagesButton;
	}

	public Embedded getSmileLogo() {
		return smileLogo;
	}

	public VerticalLayout getRightVL() {
		return rightVL;
	}

	public TopBarLogic getLogic() {
		return logic;
	}

	public Refresher getRefresher() {
		return refresher;
	}

	public Label getHomeLabel() {
		return homeLabel;
	}

	public Label getPublicLabel() {
		return publicLabel;
	}

	public Label getGroupsLabel() {
		return groupsLabel;
	}

	public Label getProfileLabel() {
		return profileLabel;
	}

	public Label getMessagesLabel() {
		return messagesLabel;
	}

	public Label getMessageNum() {
		return messageNum;
	}

	public ComboBox getLanguageCB() {
		return languageCB;
	}

	@AutoGenerated
	private HorizontalLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new HorizontalLayout();
		mainLayout.setStyleName("topButtonShell noselect");
		mainLayout.setImmediate(false);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("60px");
		mainLayout.setMargin(false);

		// top-level component properties
		setWidth("100.0%");
		setHeight("60px");

		// smileLogo
		smileLogo = new Embedded();
		smileLogo.setStyleName("smileLogo noselect");
		smileLogo.setImmediate(false);
		smileLogo.setWidth("110px");
		smileLogo.setHeight("50px");
		smileLogo.setSource(new ThemeResource("assets/images/logo.png"));
		smileLogo.setType(1);
		smileLogo.setMimeType("image/png");
		mainLayout.addComponent(smileLogo);
		mainLayout.setComponentAlignment(smileLogo, new Alignment(48));

		// spacer0
		spacer0 = new Label();
		spacer0.setImmediate(false);
		spacer0.setWidth("30px");
		spacer0.setHeight("-1px");
		spacer0.setValue("Label");
		mainLayout.addComponent(spacer0);

		// homeButton
		homeButton = buildHomeButton();
		mainLayout.addComponent(homeButton);

		// publicButton
		publicButton = buildPublicButton();
		mainLayout.addComponent(publicButton);

		// groupsButton
		groupsButton = buildGroupsButton();
		mainLayout.addComponent(groupsButton);

		// profileButton
		profileButton = buildProfileButton();
		mainLayout.addComponent(profileButton);

		// messagesButton
		messagesButton = buildMessagesButton();
		mainLayout.addComponent(messagesButton);

		// spacer
		spacer = new Label();
		spacer.setImmediate(false);
		spacer.setWidth("100.0%");
		spacer.setHeight("-1px");
		spacer.setValue(" ");
		mainLayout.addComponent(spacer);
		mainLayout.setExpandRatio(spacer, 1.0f);

		// rightVL
		rightVL = buildRightVL();
		mainLayout.addComponent(rightVL);
		mainLayout.setComponentAlignment(rightVL, new Alignment(6));

		// gridLayout_1
		gridLayout_1 = buildGridLayout_1();
		mainLayout.addComponent(gridLayout_1);

		// spacer10
		spacer10 = new Label();
		spacer10.setImmediate(false);
		spacer10.setWidth("20px");
		spacer10.setHeight("-1px");
		spacer10.setValue("Label");
		mainLayout.addComponent(spacer10);

		// refresher
		refresher = new Refresher();
		refresher.setImmediate(false);
		refresher.setWidth("0px");
		refresher.setHeight("0px");
		mainLayout.addComponent(refresher);

		return mainLayout;
	}

	@AutoGenerated
	private VerticalLayout buildHomeButton() {
		// common part: create layout
		homeButton = new VerticalLayout();
		homeButton.setStyleName("inline-block topButton homeButton noselect");
		homeButton.setImmediate(false);
		homeButton.setWidth("70px");
		homeButton.setHeight("60px");
		homeButton.setMargin(false);

		// homeLabel
		homeLabel = new Label();
		homeLabel.setStyleName("buttonText alignCenter");
		homeLabel.setImmediate(false);
		homeLabel.setWidth("-1px");
		homeLabel.setHeight("-1px");
		homeLabel.setValue("Label");
		homeButton.addComponent(homeLabel);
		homeButton.setComponentAlignment(homeLabel, new Alignment(24));

		return homeButton;
	}

	@AutoGenerated
	private VerticalLayout buildPublicButton() {
		// common part: create layout
		publicButton = new VerticalLayout();
		publicButton.setStyleName("inline-block topButton publicButton noselect");
		publicButton.setImmediate(false);
		publicButton.setWidth("70px");
		publicButton.setHeight("60px");
		publicButton.setMargin(false);

		// publicLabel
		publicLabel = new Label();
		publicLabel.setStyleName("buttonText alignCenter");
		publicLabel.setImmediate(false);
		publicLabel.setWidth("-1px");
		publicLabel.setHeight("-1px");
		publicLabel.setValue("Label");
		publicButton.addComponent(publicLabel);
		publicButton.setComponentAlignment(publicLabel, new Alignment(24));

		return publicButton;
	}

	@AutoGenerated
	private VerticalLayout buildGroupsButton() {
		// common part: create layout
		groupsButton = new VerticalLayout();
		groupsButton.setStyleName("inline-block topButton groupsButton noselect");
		groupsButton.setImmediate(false);
		groupsButton.setWidth("70px");
		groupsButton.setHeight("60px");
		groupsButton.setMargin(false);

		// groupsLabel
		groupsLabel = new Label();
		groupsLabel.setStyleName("buttonText alignCenter");
		groupsLabel.setImmediate(false);
		groupsLabel.setWidth("-1px");
		groupsLabel.setHeight("-1px");
		groupsLabel.setValue("Label");
		groupsButton.addComponent(groupsLabel);
		groupsButton.setComponentAlignment(groupsLabel, new Alignment(24));

		return groupsButton;
	}

	@AutoGenerated
	private VerticalLayout buildProfileButton() {
		// common part: create layout
		profileButton = new VerticalLayout();
		profileButton.setStyleName("inline-block topButton profileButton noselect");
		profileButton.setImmediate(false);
		profileButton.setWidth("70px");
		profileButton.setHeight("60px");
		profileButton.setMargin(false);

		// profileLabel
		profileLabel = new Label();
		profileLabel.setStyleName("buttonText alignCenter");
		profileLabel.setImmediate(false);
		profileLabel.setWidth("-1px");
		profileLabel.setHeight("-1px");
		profileLabel.setValue("Label");
		profileButton.addComponent(profileLabel);
		profileButton.setComponentAlignment(profileLabel, new Alignment(24));

		return profileButton;
	}

	@AutoGenerated
	private VerticalLayout buildMessagesButton() {
		// common part: create layout
		messagesButton = new VerticalLayout();
		messagesButton.setStyleName("inline-block topButton messagesButton noselect");
		messagesButton.setImmediate(false);
		messagesButton.setWidth("70px");
		messagesButton.setHeight("60px");
		messagesButton.setMargin(false);

		// horizontalLayout_2
		horizontalLayout_2 = buildHorizontalLayout_2();
		messagesButton.addComponent(horizontalLayout_2);
		messagesButton.setExpandRatio(horizontalLayout_2, 1.0f);
		messagesButton.setComponentAlignment(horizontalLayout_2, new Alignment(24));

		// messagesLabel
		messagesLabel = new Label();
		messagesLabel.setStyleName("buttonText alignCenter");
		messagesLabel.setImmediate(false);
		messagesLabel.setWidth("-1px");
		messagesLabel.setHeight("18px");
		messagesLabel.setValue("Label");
		messagesButton.addComponent(messagesLabel);
		messagesButton.setComponentAlignment(messagesLabel, new Alignment(24));

		return messagesButton;
	}

	@AutoGenerated
	private HorizontalLayout buildHorizontalLayout_2() {
		// common part: create layout
		horizontalLayout_2 = new HorizontalLayout();
		horizontalLayout_2.setImmediate(false);
		horizontalLayout_2.setWidth("50px");
		horizontalLayout_2.setHeight("-1px");
		horizontalLayout_2.setMargin(false);

		// messageNum
		messageNum = new Label();
		messageNum.setStyleName("messageNum");
		messageNum.setImmediate(false);
		messageNum.setWidth("-1px");
		messageNum.setHeight("17px");
		messageNum.setValue(" ");
		messageNum.setContentMode(3);
		horizontalLayout_2.addComponent(messageNum);
		horizontalLayout_2.setComponentAlignment(messageNum, new Alignment(10));

		return horizontalLayout_2;
	}

	@AutoGenerated
	private VerticalLayout buildRightVL() {
		// common part: create layout
		rightVL = new VerticalLayout();
		rightVL.setImmediate(false);
		rightVL.setWidth("-1px");
		rightVL.setHeight("-1px");
		rightVL.setMargin(false);

		// feedbackLayout
		feedbackLayout = buildFeedbackLayout();
		rightVL.addComponent(feedbackLayout);

		return rightVL;
	}

	@AutoGenerated
	private HorizontalLayout buildFeedbackLayout() {
		// common part: create layout
		feedbackLayout = new HorizontalLayout();
		feedbackLayout.setStyleName("feedback");
		feedbackLayout.setImmediate(true);
		feedbackLayout.setWidth("183px");
		feedbackLayout.setHeight("30px");
		feedbackLayout.setMargin(false);
		feedbackLayout.setSpacing(true);

		// languageCB
		languageCB = new ComboBox();
		languageCB.setImmediate(false);
		languageCB.setWidth("60px");
		languageCB.setHeight("-1px");
		feedbackLayout.addComponent(languageCB);
		feedbackLayout.setComponentAlignment(languageCB, new Alignment(33));

		// feedbackLbl
		feedbackLbl = new Label();
		feedbackLbl.setImmediate(false);
		feedbackLbl.setWidth("-1px");
		feedbackLbl.setHeight("-1px");
		feedbackLbl
				.setValue("<a href=\"https://gse.qualtrics.com/SE/?SID=SV_3DUKnDwEyVtqvKl\" target=\"_blank\">Feedback & Support</a>");
		feedbackLbl.setContentMode(3);
		feedbackLayout.addComponent(feedbackLbl);
		feedbackLayout.setExpandRatio(feedbackLbl, 1.0f);
		feedbackLayout.setComponentAlignment(feedbackLbl, new Alignment(33));

		return feedbackLayout;
	}

	@AutoGenerated
	private GridLayout buildGridLayout_1() {
		// common part: create layout
		gridLayout_1 = new GridLayout();
		gridLayout_1.setImmediate(false);
		gridLayout_1.setWidth("-1px");
		gridLayout_1.setHeight("-1px");
		gridLayout_1.setMargin(false);
		gridLayout_1.setColumns(7);
		gridLayout_1.setRows(2);

		// spacer20
		spacer20 = new Label();
		spacer20.setImmediate(false);
		spacer20.setWidth("5px");
		spacer20.setHeight("-1px");
		spacer20.setValue("Label");
		gridLayout_1.addComponent(spacer20, 1, 0);

		// usernameTF
		usernameTF = new TextField();
		usernameTF.setCaption("Username");
		usernameTF.setImmediate(false);
		usernameTF.setWidth("140px");
		usernameTF.setHeight("20px");
		usernameTF.setSecret(false);
		gridLayout_1.addComponent(usernameTF, 2, 0);

		// spacer100
		spacer100 = new Label();
		spacer100.setImmediate(false);
		spacer100.setWidth("5px");
		spacer100.setHeight("-1px");
		spacer100.setValue("Label");
		gridLayout_1.addComponent(spacer100, 3, 0);

		// passwordTF
		passwordTF = new PasswordField();
		passwordTF.setCaption("Password");
		passwordTF.setImmediate(false);
		passwordTF.setWidth("140px");
		passwordTF.setHeight("20px");
		gridLayout_1.addComponent(passwordTF, 4, 0);

		// spacer200
		spacer200 = new Label();
		spacer200.setImmediate(false);
		spacer200.setWidth("5px");
		spacer200.setHeight("-1px");
		spacer200.setValue("Label");
		gridLayout_1.addComponent(spacer200, 5, 0);

		// loginBtn
		loginBtn = new Button();
		loginBtn.setStyleName("defaultRed");
		loginBtn.setCaption("Login");
		loginBtn.setImmediate(true);
		loginBtn.setWidth("-1px");
		loginBtn.setHeight("-1px");
		gridLayout_1.addComponent(loginBtn, 6, 0);
		gridLayout_1.setComponentAlignment(loginBtn, new Alignment(9));

		// forgotUsernameBtn
		forgotUsernameBtn = new Button();
		forgotUsernameBtn.setStyleName("link");
		forgotUsernameBtn.setCaption("Forgot Username?");
		forgotUsernameBtn.setImmediate(true);
		forgotUsernameBtn.setWidth("140px");
		forgotUsernameBtn.setHeight("20px");
		gridLayout_1.addComponent(forgotUsernameBtn, 2, 1);

		// forgotPasswordBtn
		forgotPasswordBtn = new Button();
		forgotPasswordBtn.setStyleName("link");
		forgotPasswordBtn.setCaption("Forgot Password?");
		forgotPasswordBtn.setImmediate(true);
		forgotPasswordBtn.setWidth("140px");
		forgotPasswordBtn.setHeight("20px");
		gridLayout_1.addComponent(forgotPasswordBtn, 4, 1);

		return gridLayout_1;
	}
}
