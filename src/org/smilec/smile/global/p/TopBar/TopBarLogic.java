package org.smilec.smile.global.p.TopBar;

import java.io.Serializable;
import java.util.UUID;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.PasswordChangeRequest;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.p.Groups.Emailer;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class TopBarLogic implements LayoutClickListener, ClickListener, Serializable, com.vaadin.event.MouseEvents.ClickListener {
	private TopBar topBar;
	private Embedded smileLogo;
	private VerticalLayout homeButton;
	private VerticalLayout publicButton;
	private VerticalLayout groupsButton;
	private VerticalLayout profileButton;
	private VerticalLayout messagesButton;

	public TopBarLogic(TopBar tb) {
		topBar = tb;
		smileLogo = tb.getSmileLogo();
		homeButton = tb.getHomeButton();
		publicButton = tb.getPublicButton();
		groupsButton = tb.getGroupsButton();
		profileButton = tb.getProfileButton();
		messagesButton = tb.getMessagesButton();

		topBar.getFeedbackLbl().setValue(
				"<a href='" + SmileApplication.feedbackURL + "' " + "target='_blank'>"
						+ AppData.getMessage("feedbackSupport") + "</a>"); //
		//set TopButtons
		tb.getHomeLabel().setValue(AppData.getMessage("HOME"));
		tb.getPublicLabel().setValue(AppData.getMessage("PUBLIC"));
		tb.getGroupsLabel().setValue(AppData.getMessage("GROUPS"));
		tb.getProfileLabel().setValue(AppData.getMessage("PROFILE"));
		tb.getMessagesLabel().setValue(AppData.getMessage("MESSAGES"));
		//add click listeners
		smileLogo.addListener(this);
		homeButton.addListener(this);
		publicButton.addListener(this);
		groupsButton.addListener(this);
		profileButton.addListener(this);
		messagesButton.addListener(this);
		tb.getLoginButton().addListener(this);
		tb.getForgotPasswordBtn().addListener(this);
		tb.getForgotUsernameBtn().addListener(this);

		//tb.getFeedbackLO().addListener(this);
	}

	public void setLoggedOut() {
		smileLogo.removeStyleName("active");
		homeButton.setVisible(false);
		publicButton.setVisible(false);
		groupsButton.setVisible(false);
		profileButton.setVisible(false);
		messagesButton.setVisible(false);

		topBar.getLoginLayout().setVisible(true);
		topBar.getLoginButton().setClickShortcut(KeyCode.ENTER);
		topBar.getFeedbackLO().removeComponent(topBar.getLanguageCB());
		topBar.getLoginLayout().removeComponent(topBar.getLanguageCB());
		topBar.getLoginLayout().addComponent(topBar.getLanguageCB(), 0, 0);
		topBar.getLoginLayout().setComponentAlignment(topBar.getLanguageCB(), new Alignment(9));
		topBar.getRightVL().setVisible(false);
	}

	public void setLoggedIn() {
		smileLogo.removeStyleName("active");
		homeButton.setVisible(true);
		publicButton.setVisible(true);
		groupsButton.setVisible(true);
		profileButton.setVisible(true);
		messagesButton.setVisible(true);
		topBar.getFeedbackLO().removeComponent(topBar.getLanguageCB());
		topBar.getLoginLayout().removeComponent(topBar.getLanguageCB());
		topBar.getFeedbackLO().addComponentAsFirst(topBar.getLanguageCB());
		topBar.getFeedbackLO().setComponentAlignment(topBar.getLanguageCB(), new Alignment(33));
		topBar.getRightVL().setVisible(true);
		topBar.getLoginLayout().setVisible(false);
		topBar.getLoginButton().removeClickShortcut();
	}

	@Override
	public void buttonClick(Button.ClickEvent event) {
		if (event.getButton() == topBar.getLoginButton()) {
			String username = (String) topBar.getUsernameInput().getValue();
			String password = (String) topBar.getPasswordField().getValue();
			if (UserDAO.checkUsername(username) == UserDAO.NO_USERNAME_ERROR) {
				usernameNotFound(username);
				return;
			}

			User user = UserDAO.login(username, password);
			if (user != null) {
				loginSuccess(user);
			} else {
				loginFailure(username);
			}
		} else if (event.getButton() == topBar.getForgotPasswordBtn()) {
			String username = (String) topBar.getUsernameInput().getValue();
			if (username != null && username.trim().length() > 0) {
				User user = UserDAO.getUserByUserName(username);
				if (user != null) {
					PasswordChangeRequest r = UserDAO.sendPCR(user.getId(), getRequestToken());
					if (r != null) {
						sendPCR(user.getEmail(), r.getId(), r.getToken());
						AppData.getApplication().showNotification(AppData.getMessage("PasswordChange"),
								AppData.getMessage("resetEmail"), //
								SmileApplication.NOTIFICATION_INFO);
					}
				} else
					usernameNotFound(username);
			} else {
				String caption = AppData.getMessage("Sorry");
				String description = AppData.getMessage("enterUsername"); //
				AppData.getApplication().showNotification(caption, description,
						SmileApplication.NOTIFICATION_ERROR);
			}
		} else if (event.getButton() == topBar.getForgotUsernameBtn()) {
			AppData.getApplication().setUrifu("login/forgotusername");
		}
	}

	public boolean sendPCR(String email, Integer id, String token) {
		Emailer emailer = new Emailer();
		//java.net.URL url = AppData.getApplication().getURL(); 
		//URL is temporarily hard-coded to SmileGlobal
		String url = AppData.getApplication().getHostname();
		//String s = url.getHost()+":"+url.getPort()+url.getPath(); // Port ist evtl leer

		String body = AppData.getMessage("passwordChangeRequest"); //passwordChangeRequest
		body += url + "?cid=" + id + "&w=" + token;
		body += AppData.getMessage("noReply"); //noReply

		return emailer.sendEmail("", email, AppData.getMessage("msgSmile"), body); //
	}

	private String getRequestToken() {
		//
		// Creating a random UUID (Universally unique identifier).
		//
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();

		//System.out.println("Random UUID String = " + randomUUIDString);
		//System.out.println("UUID version       = " + uuid.version());
		//System.out.println("UUID variant       = " + uuid.variant());

		return randomUUIDString;
	}

	//called if login is success
	public void loginSuccess(User user) {
		topBar.getPasswordField().setValue("");
		topBar.getUsernameInput().setValue("");
		String caption = AppData.getMessage("Welcome"); //Welcome
		String description =
				AppData.getFormattedMsg("loggedInAsFirstLast",
						new Object[] { user.getFirstName(), user.getLastName() }); //loggedInAsFirstLast
		topBar.getWindow().showNotification(caption, description, Notification.TYPE_TRAY_NOTIFICATION);
		topBar.getApplication().setUser(user);
	}

	public void usernameNotFound(String username) {
		String caption = "Sorry.";
		String description =
				AppData.getFormattedMsg("usernameNotFound", username) + " "
						+ AppData.getMessage("tryAgain"); //usernameNotFound, 

		AppData.getApplication().showNotification(caption, description,
				SmileApplication.NOTIFICATION_ERROR);
	}

	public void loginFailure(String username) {
		String caption = AppData.getMessage("Sorry");
		String description = AppData.getMessage("passwordIncorrect");
		AppData.getApplication().showNotification(caption, description,
				SmileApplication.NOTIFICATION_ERROR);
	}

	@Override
	public void layoutClick(LayoutClickEvent event) {
		SmileApplication smile = AppData.getApplication();
		Component c = event.getComponent();
		if (c == homeButton) {
			smile.gotoHome();
		} else if (c == publicButton) {
			smile.gotoPublic();
		} else if (c == groupsButton) {
			smile.gotoGroups();
		} else if (c == profileButton) {
			smile.gotoProfile();
		} else if (c == messagesButton) {
			smile.gotoMessages();
		} else if (c == topBar.getFeedbackLO()) {
			smile.loadFeedback();
		}

	}

	@Override
	public void click(ClickEvent event) {
		//Right now only for smileLogo, otherwise need to add if statement
		SmileApplication smile = AppData.getApplication();
		smile.gotoLogin();
		if (smile.isLoggedIn()) {
			smileLogo.addStyleName("active");
		}
	}

	public void selectButton(VerticalLayout vl) {
		//enable all buttons
		smileLogo.removeStyleName("active");
		homeButton.removeStyleName("v-disabled");
		publicButton.removeStyleName("v-disabled");
		groupsButton.removeStyleName("v-disabled");
		profileButton.removeStyleName("v-disabled");
		messagesButton.removeStyleName("v-disabled");
		//disable selected button
		vl.addStyleName("v-disabled");
	}

	public void deselectButtons() {
		//enable all buttons
		smileLogo.removeStyleName("active");
		homeButton.removeStyleName("v-disabled");
		publicButton.removeStyleName("v-disabled");
		groupsButton.removeStyleName("v-disabled");
		profileButton.removeStyleName("v-disabled");
		messagesButton.removeStyleName("v-disabled");
	}

	public void setMessageCount(int count) {
		Label messageNum = topBar.getMessageNum();
		messageNum.setData(count);
		if (count > 0) {
			messageNum.setValue("<span class=\"badge badge-important\">" + count + "</span>");
			if (!messageNum.isVisible())
				messageNum.setVisible(true);
		} else {
			if (topBar.isVisible()) {
				messageNum.setVisible(false);
			}
		}
	}

	public int getMessageCount() {
		if (topBar.getMessageNum().getData() != null) {
			return (Integer) topBar.getMessageNum().getData();
		} else {
			return 0;
		}
	}

}
