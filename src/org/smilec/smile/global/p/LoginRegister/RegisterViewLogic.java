package org.smilec.smile.global.p.LoginRegister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.vars.ResourcePaths;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Embedded;

public class RegisterViewLogic implements Serializable, ClickListener, ValueChangeListener {
	private RegisterView registerView;
	private RegisterInfoView registerInfoView;
	private RegexpValidator nameValidator;
	private EmailValidator emailValidator;
	private UniqueUsernameValidator usernameValidator;
	private RegexpValidator usernameCharsValidator;
	private StringLengthValidator passwordValidator;
	private ArrayList<AbstractField> validationFields;

	public RegisterViewLogic(RegisterView rv) {
		this.registerView = rv;
		registerInfoView = new RegisterInfoView();
		SmileApplication smile = (SmileApplication) registerView.getApplication();
		//if already logged in, return to login screen
		User user = null;
		try {
			user = smile.getUser();
		} catch (Throwable e) {
			System.out.println(e.getLocalizedMessage());
		}
		if (user != null) {
			smile.gotoHome();
			/*String caption = "Already logged in as <strong>" + user.getUsername() + "</strong>.";
			String description = "You must logout in order to register as a new user.";
			smile.getMainWindow().showNotification(caption, description, Notification.TYPE_WARNING_MESSAGE);
			*/
		}
		//set interaction
		registerView.getBackButton().addListener((Button.ClickListener) this);
		registerView.getNextButton().addListener((Button.ClickListener) this);

		//list of validators
		validationFields = new ArrayList<AbstractField>();
		validationFields.add(registerView.getFirstNameField());
		validationFields.add(registerView.getLastNameField());
		validationFields.add(registerView.getUsernameField());
		validationFields.add(registerView.getPasswordField());
		validationFields.add(registerView.getEmailField());
		//set field change listeners
		for (int i = 0; i < validationFields.size(); i++) {
			validationFields.get(i).addListener(this);
			//validate fields only when button is pressed
			validationFields.get(i).setImmediate(true);
		}
		//set validators
		nameValidator = new RegexpValidator("[A-Za-z -]*", AppData.getMessage("regex0")); //
		usernameCharsValidator =
				new RegexpValidator("[A-Za-z]+[A-Za-z0-9_]*", AppData.getMessage("regex1")); //
		registerView.getFirstNameField().setRequired(true);
		registerView.getFirstNameField().setRequiredError(AppData.getMessage("enterFirst")); //
		registerView.getFirstNameField().addValidator(nameValidator);
		registerView.getLastNameField().setRequired(true);
		registerView.getLastNameField().setRequiredError(AppData.getMessage("enterLast")); //
		registerView.getLastNameField().addValidator(nameValidator);
		usernameValidator = new UniqueUsernameValidator(AppData.getMessage("usernameTaken")); //
		registerView.getUsernameField().addValidator(usernameValidator);
		registerView.getUsernameField().addValidator(usernameCharsValidator);
		registerView.getUsernameField().setRequired(true);
		registerView.getUsernameField().setRequiredError(AppData.getMessage("createUN")); //	
		passwordValidator = new StringLengthValidator(AppData.getMessage("pass5")); //
		passwordValidator.setMinLength(5);
		passwordValidator.setNullAllowed(false);
		registerView.getPasswordField().addValidator(passwordValidator);
		registerView.getPasswordField().setRequired(true);
		registerView.getPasswordField().setRequiredError(AppData.getMessage("createPW")); //
		emailValidator = new EmailValidator(AppData.getMessage("emailInvalid")); //
		registerView.getEmailField().addValidator(emailValidator);
		registerView.getEmailField().setRequired(true);
		registerView.getEmailField().setRequiredError( //
				AppData.getMessage("needEmail"));

	}

	@Override
	//When Register button is clicked
			public
			void buttonClick(Button.ClickEvent event) {
		Button bb = event.getButton();
		if (bb == registerView.getNextButton()) {
			Boolean allValid = true;
			String errors = "";

			for (int i = 0; i < validationFields.size(); i++) {
				try {
					validationFields.get(i).validate();
					validationFields.get(i).setComponentError(null);
				} catch (Exception e) {
					errors += "<br>��� " + e.getMessage().toString();
					validationFields.get(i).setComponentError(new UserError(" "));
				}
				if (!validationFields.get(i).isValid()) {
					allValid = false;
				}
			}
			//check for duplicate emails
			String email = (String) registerView.getEmailField().getValue();
			email = email.trim();
			List<User> list = UserDAO.getUserByEmail(email);
			if (list.size() > 0) {
				allValid = false;
				String emailError = AppData.getMessage("emailUsed"); //
				errors += "<br>��� " + emailError;
				registerView.getEmailField().setComponentError(new UserError(emailError));
			}
			//check that confirmed pw matches pw
			if (!registerView.getPasswordField().getValue()
					.equals(registerView.getConfirmField().getValue())) {
				allValid = false;
				String confirmError = AppData.getMessage("pwMismatch"); //
				errors += "<br>��� " + confirmError;
				registerView.getConfirmField().setComponentError(new UserError(confirmError));
			}
			if (allValid == false) {
				//Set an error on button if there are any field errors
				bb.setComponentError(new UserError(AppData.getMessage("correctErrors"))); //
				addErrorLabel(errors);
			}

			try {
				if (allValid) {
					//if all fields are valid, remove any error codes, and try to create user
					bb.setComponentError(null);
					//create User POJO from field values
					User newUser = createNewUser();
					//save new user to database
					// UserDAO.createUser(newUser, (String) registerView.getPasswordField().getValue());
					//login with new user re-queried from database;
					/* registerView.getApplication().setUser(
							UserDAO.login(newUser.getUsername(), (String) registerView
									.getPasswordField().getValue()));*/

					registerInfoView.setNewUser(newUser, registerView);
					AppData.getApplication().setContent(registerInfoView);

					//set user
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
		} else if (bb == registerView.getBackButton()) {
			AppData.getApplication().gotoLogin();
		}
	}

	private void addErrorLabel(String e) {
		if (e.length() == 0) {
			//errorLayout.removeAllComponents();
			//errorLayout.requestRepaintAll();
		} else {
			AppData.getApplication().showNotification(AppData.getMessage("UhOh"),
					AppData.getMessage("correctErrors") + e, //
					SmileApplication.NOTIFICATION_ERROR);
			/*errorLayout.removeAllComponents();
			errorLabel = null;
			errorLabel = new Label();
			errorLabel.setValue(e);
			errorLabel.setIcon(ResourcePaths.redError);
			errorLabel.setDebugId("vv-errorText");
			errorLabel.setContentMode(Label.CONTENT_PREFORMATTED);
			errorLayout.addComponent(errorLabel);
			errorLayout.request RepaintAll();*/
		}
		//mainWindow.refreshSize();
	}

	private User createNewUser() {
		User user = new User();
		user.setUsername((String) registerView.getUsernameField().getValue());
		user.setFirstName((String) registerView.getFirstNameField().getValue());
		user.setLastName((String) registerView.getLastNameField().getValue());
		user.setEmail((String) registerView.getEmailField().getValue());
		user.setUserType(1);
		try {
			return user;
		} catch (RuntimeException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
		try {
			((AbstractField) event.getProperty()).validate();
			((AbstractField) event.getProperty()).setComponentError(null);
		} catch (Exception e) {
			((AbstractField) event.getProperty()).setComponentError(new UserError(" "));
		}
		//on value change, first update field to trim out leading or 
		//trailing whitespace
		event.getProperty().setValue(((String) event.getProperty().getValue()).trim());
		if (event.getProperty() == registerView.getFirstNameField()) {
			if (registerView.getFirstNameField().isValid()) {
				setStatusImage(registerView.getFirstName_emb(), true);
			} else {
				registerView.getFirstName_emb().setVisible(false);
			}
		} else if (event.getProperty() == registerView.getLastNameField()) {
			if (registerView.getLastNameField().isValid()) {
				setStatusImage(registerView.getLastName_emb(), true);
			} else {
				registerView.getLastName_emb().setVisible(false);
			}
		} else if (event.getProperty() == registerView.getUsernameField()) {
			if (registerView.getUsernameField().isValid()) {
				setStatusImage(registerView.getUsername_emb(), true);
			} else {
				registerView.getUsername_emb().setVisible(false);
			}
		} else if (event.getProperty() == registerView.getPasswordField()) {
			if (registerView.getPasswordField().isValid()) {
				setStatusImage(registerView.getPassword_emb(), true);
			} else {
				registerView.getPassword_emb().setVisible(false);
			}
		} else if (event.getProperty() == registerView.getEmailField()) {
			if (registerView.getEmailField().isValid()) {
				setStatusImage(registerView.getEmail_emb(), true);
			} else {
				registerView.getEmail_emb().setVisible(false);
			}
		}
	}

	private void setStatusImage(Embedded emb, boolean b) {
		//set image to right of field. Sets to correct check if passes, red x is it fails
		if (b) {
			emb.setSource(ResourcePaths.greenCheck);
		} else {
			emb.setSource(ResourcePaths.redError);
		}
		emb.setVisible(true);
		emb.setEnabled(true);
		emb.requestRepaint();
	}
}
