package org.smilec.smile.global.p.LoginRegister;

import com.vaadin.data.validator.AbstractStringValidator;

import org.smilec.smile.global.hibernate.logic.UserDAO;

public class UniqueUsernameValidator extends AbstractStringValidator {
	public UniqueUsernameValidator(String errorMessage) {
		super(errorMessage);
	}

	//returns true if username exists

	@Override
	protected boolean isValidString(String value) {
		if (UserDAO.checkUsername(value) == UserDAO.NO_USERNAME_ERROR)
			return true;
		return false;
	}
}
