package org.smilec.smile.global.p.LoginRegister;

import com.vaadin.data.validator.AbstractStringValidator;

import org.smilec.smile.global.hibernate.logic.UserDAO;

public class UsernameValidator extends AbstractStringValidator {
	public UsernameValidator(String errorMessage) {
		super(errorMessage);
	}

	//returns true if username exists

	@Override
	protected boolean isValidString(String value) {
		if (UserDAO.checkUsername(value) == UserDAO.USERNAME_FOUND)
			return true;
		return false;
	}
}
