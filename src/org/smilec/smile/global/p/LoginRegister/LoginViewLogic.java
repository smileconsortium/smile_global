package org.smilec.smile.global.p.LoginRegister;

import java.io.Serializable;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;

public class LoginViewLogic implements Serializable, ClickListener, LayoutClickListener {
	private LoginView loginView;

	public LoginViewLogic(LoginView lv) {
		this.loginView = lv;
		//init
		//set interaction
		loginView.getRegisterButton().addListener(this);
		loginView.getUserGuide().addListener(this);
		if (((SmileApplication) AppData.getApplication()).isLoggedIn()) {
			loginView.getLoggedOutHL().setVisible(false);
		}
	}

	@Override
	public void buttonClick(Button.ClickEvent event) {
		if (event.getButton() == loginView.getRegisterButton()) {
			((SmileApplication) AppData.getApplication()).gotoRegister();
		}
	}

	@Override
	public void layoutClick(LayoutClickEvent event) {
		if (event.getSource() == loginView.getUserGuide()) {
			//download pdf link
			loginView
					.getWindow()
					.executeJavaScript(
							"window.open('http://www.smileglobal.net/smileUserGuide.pdf', '_blank', 'fullscreen=yes');");
		}
	}
}
