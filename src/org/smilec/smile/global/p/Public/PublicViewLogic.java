package org.smilec.smile.global.p.Public;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;
import org.smilec.smile.global.hibernate.logic.QuestionDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.ui.CreateQuestionContainer;
import org.smilec.smile.global.ui.QuestionInteractionLogic;
import org.smilec.smile.global.ui.QuestionList;
import org.smilec.smile.global.ui.QuestionListManager;
import org.smilec.smile.global.utils.Utils;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.VerticalLayout;

public class PublicViewLogic implements LayoutClickListener, SelectedTabChangeListener, Serializable, CreateQuestionContainer, QuestionListManager {
	//component vars
	private PublicView publicView;
	private VerticalLayout mainLayout;
	private QuestionList questionDisplay;

	public static final int ORDER_CODE_DATE_DESC = 0;
	public static final int ORDER_CODE_DATE_ASC = 1;
	public static final int ORDER_CODE_RATING_DESC = 2;
	public static final int ORDER_CODE_POPULARITY_DESC = 3;
	public static final int ORDER_CODE_DIFFICULTY_ASC = 4;
	public static final int ORDER_CODE_DIFFICULTY_DESC = 5;

	private String searchQuestionTabText = AppData.getMessage("SearchQuestions");
	private String createQuestionTabText = AppData.getMessage("CreateQuestion");
	private String editQuestionTabText = AppData.getMessage("EditQuestion");

	//Question storage	//DAO vars
	//More questions are loaded than are displayed
	private List<Question> questionList; //stores questions currently seen
	private int maxPages;
	private int questionsPerPage = 10; //can later be set by user
	//Query Fragment Vars
	private String query = "";
	private int pageNum = 1;
	private int orderCode = ORDER_CODE_DATE_DESC;
	private User userLookup = null; //set to current user that is being searched
	//Query vars
	private int querySize;

	//option vars
	public boolean showAnswered = true; //if true, shows questions that have been answered
	public boolean showOwn = true; //show questions that you created
	public boolean showCorrect = true; //show whether you got question correct
	private boolean myquestions = false;

	public PublicViewLogic(PublicView publicView) {
		this.publicView = publicView;
		//init
		//publicView.setImmediate(true);
		this.mainLayout = publicView.getMainLayout();
		this.questionDisplay = publicView.getQuestionDisplay();
		questionDisplay.setQLManager(this, true);

		publicView.getTabSheet().addListener(this);

		//int order combo box
		ComboBox cb = publicView.getOrderComboBox();
		cb.removeAllItems();
		List<Order> orderItems = new ArrayList<Order>();
		//Must be in order of 0 incrementing by 1
		orderItems.add(new Order(ORDER_CODE_DATE_DESC, AppData.getMessage("Newest")));
		orderItems.add(new Order(ORDER_CODE_DATE_ASC, AppData.getMessage("Oldest")));
		orderItems.add(new Order(ORDER_CODE_RATING_DESC, AppData.getMessage("TopRated")));
		orderItems.add(new Order(ORDER_CODE_POPULARITY_DESC, AppData.getMessage("MostPopular")));
		orderItems.add(new Order(ORDER_CODE_DIFFICULTY_ASC, AppData.getMessage("Easiest")));
		orderItems.add(new Order(ORDER_CODE_DIFFICULTY_DESC, AppData.getMessage("Hardest")));
		BeanItemContainer<Order> beans = new BeanItemContainer<Order>(Order.class, orderItems);

		cb.setContainerDataSource(beans);
		cb.setItemCaptionPropertyId("description");
		cb.setNullSelectionAllowed(false);
		cb.setTextInputAllowed(false);
		cb.setImmediate(true);
		//Receive callbacks functions for when edited Question is cancelled or submitted from
		//CreateQuestionBox
		publicView.getCreateQuestionBox().getLogic().setContainer(this);
		//listeners
		cb.addListener(new ComboBox.ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				int newOrderCode = ((Order) event.getProperty().getValue()).id;
				if (newOrderCode != orderCode) {
					//if just changed orderCode, go back to page 1
					pageNum = 1;
				}
				orderCode = newOrderCode;
				setOrder(orderCode);
			}
		});
		publicView.getSearchButton().addListener(this);
	}

	public class Order implements Serializable {
		private int id;
		private String description;

		public Order(int id, String description) {
			this.id = id;
			this.description = description;
		}

		public int getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

		public boolean equals(Order o) {
			if (o.id == this.id)
				return true;
			return false;
		}
	}

	private void setOrderCodeCB(int code) {
		//go to order code
		ComboBox cb = publicView.getOrderComboBox();
		Iterator it = cb.getItemIds().iterator();
		Order o = (Order) it.next();
		for (int i = 0; i < orderCode; i++) {
			o = (Order) it.next();
		}
		cb.setValue(o);
	}

	@Override
	public void selectedTabChange(SelectedTabChangeEvent e) {
		//update height
		//updateHeight();
		//clear messages
		AppData.getApplication().clearNotifications();
		//reload questions if returning to answerTab
		if (publicView.getTabSheet().getSelectedTab().equals(publicView.getSearchQuestions_tab())) {
			reloadPage();
		}
	}

	//the view tab contains a vertical questionsCssLayout that has questions added to it,
	//with each question displayed in a questionDisplay object

	//called by SmileApplication
	public void myquestions(String s, int pageNum, int order) {
		this.pageNum = pageNum;
		this.query = s;
		this.orderCode = order;
		myquestions = true;
		query(query);
	}

	//called by SmileApplication
	public void query(String s, int pageNum, int order) {
		this.pageNum = pageNum;
		this.query = s;
		this.orderCode = order;
		myquestions = false;
		query(query);
	}

	//called by SmileApplication
	public void query(String s, int pageNum) {
		this.pageNum = pageNum;
		this.query = s;
		myquestions = false;
		query(query);
	}

	//called by SmileApplication
	private void query(String s) {
		this.query = s;
		loadQuestions();
	}

	private void loadQuestions() {
		//called when questionBuffer runs out of questions
		User user = AppData.getApplication().getUser();
		//check if user query (as in to show a user's questions)
		String userSearch = null;

		userLookup = null;
		publicView.getTabSheet().getTab(publicView.getSearchQuestions_tab())
				.setCaption(searchQuestionTabText);
		publicView.getTabSheet().getTab(publicView.getCreateQuestionBox()).setVisible(true);
		publicView.getHomeLink().setVisible(false);

		if (myquestions) {
			publicView.getTabSheet().getTab(publicView.getSearchQuestions_tab())
					.setCaption(AppData.getMessage("MyQuestions"));
			publicView.getTabSheet().getTab(publicView.getCreateQuestionBox()).setVisible(false);
			publicView.getHomeLink().setVisible(true);
			userSearch = user.getUsername();
		} else if (query.contains(":")) {
			String[] parts = query.split(":");
			if (parts.length == 2) {
				if (parts[0].equals("user"))
					;
				userSearch = parts[1];
			}
		}

		int start;
		try { //catch sql errors from searches with strange characters
				//TODO: block sql injection attacks
			if (userSearch != null) {
				//set user search as null if user doesn't exist
				userLookup = UserDAO.getUserByUserName(userSearch);
				if (userLookup != null) {
					if (myquestions)
						querySize = QuestionDAO.countMyQuestions(query, userLookup.getId());
					else
						querySize = QuestionDAO.countUserQuestions(userLookup.getId());
				} else {
					userSearch = null;
				}
			}
			if (userSearch == null) {
				querySize = QuestionDAO.countQuestions(query, user, showOwn, showAnswered);
			}
			maxPages = ((querySize - 1) / questionsPerPage) + 1;
			start = (pageNum - 1) * questionsPerPage;
			ComboBox cb = publicView.getOrderComboBox();
			if (userSearch == null) {
				questionList =
						QuestionDAO.getQuestions(query, start, questionsPerPage, user, showOwn,
								showAnswered, orderCode);
			} else {
				if (myquestions) {
					questionList =
							QuestionDAO.getMyQuestions(query, user.getId(), userLookup.getId(), start,
									questionsPerPage, orderCode);
				} else {
					questionList =
							QuestionDAO.getUserQuestions(user.getId(), userLookup.getId(), start,
									questionsPerPage, orderCode);
				}
			}
		} catch (Exception e) {
			if (pageNum != 1) {
				//TODO: fix loading pageNum too large
				//if no questions and not on first page, try loading query from page 1
				pageNum = 1;
				gotoPage(pageNum);
			} else {
				//if no questions and on first page, show no results message
				//no results found
				noResultsFound();
			}
			return;
		}
		if (questionList == null || questionList.size() == 0) {
			if (pageNum != 1) {
				//TODO: fix loading pageNum too large
				//if no questions and not on first page, try loading query from page 1
				pageNum = 1;
				gotoPage(pageNum);
			} else {
				//if no questions and on first page, show no results message
				//no results found
				noResultsFound();
			}
		} else {
			//render questions
			boolean exam = false; //only have exams on group
			questionDisplay.setQuestionSet(questionList, pageNum, maxPages, showCorrect, true, exam);
			setResultsText(start);
			setOrderCodeCB(orderCode);
		}
		//set searchField text to query
		publicView.getSearchField().setValue(query);
		//set pagination
	}

	public void selectQuestion(int qid) {
		questionDisplay.selectQuestion(qid);
	}

	private void noResultsFound() {
		if (query.length() == 0) {
			if (myquestions) {
				questionDisplay.noResultsFound(AppData.getMessage("noQuestions1")); //
			} else {
				questionDisplay.noResultsFound(AppData.getMessage("noPublicQs")); //
			}
		} else {
			questionDisplay.noResultsFound(AppData.getFormattedMsg("noResults", query)); //
		}
	}

	private void reloadPage() {
		//refresh question list
		loadQuestions();
		//try to reselect last selected question if still on page and question tab was open
		questionDisplay.reselectQuestion();
	}

	private void setResultsText(int start) {
		//set results text
		int end = start + questionList.size();
		/* need to uncomment if resultsText is commented out
		SmileApplication smile = (SmileApplication) AppData.getApplication();
		if (userLookup == null) {
			querySize = QuestionDAO.countQuestions(query, smile.getUser(), showOwn, showAnswered);
		} else {
			querySize = QuestionDAO.countUserQuestions(userLookup.getId());
		}
		*/
		publicView.getResultsLabel().setValue(
				AppData.getFormattedMsg("resultsPagination",
						new Object[] { (start + 1), end, querySize })); //
	}

	private void setQuery(String query) {
		this.query = query;
		if (myquestions == true)
			AppData.getApplication().gotoMyQuestions(query, pageNum, orderCode);
		else
			AppData.getApplication().gotoPublic(query, pageNum, orderCode);
	}

	private void setOrder(int orderCode) { //called when ComboBox value changes
		this.orderCode = orderCode;
		if (myquestions == true)
			AppData.getApplication().gotoMyQuestions(query, pageNum, orderCode);
		else
			AppData.getApplication().gotoPublic(query, pageNum, orderCode);
	}

	@Override
	public void layoutClick(LayoutClickEvent event) {
		Component c = event.getComponent().getParent();
		//System.out.println("layoutClick " + event.getComponent());
		if (c instanceof AbsoluteLayout) {
			//if search button clicked
			String newQuery = (String) publicView.getSearchField().getValue();
			if (query == newQuery) {
				reloadPage();
			} else {
				try {
					setQuery(URLEncoder.encode(newQuery, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//Methods inherited from QuestionListManager interface
	@Override
	public UserQuestionInteraction submitQuestion(Question question, QuestionAnswer qa, int rating,
			String comment, Integer secComplete) {
		//System.out.println("Question submit attempted");
		User user = AppData.getApplication().getUser();
		//create ui interaction
		UserQuestionInteraction ui = new UserQuestionInteraction();
		ui.setUser(user);
		ui.setQuestion(question);
		ui.setUserAnswer(qa);
		ui.setCreatedOn(Utils.getTime());
		if (rating > 0) {
			ui.setRating(rating * 2); //rating is based on 10 to allow for future possibility of half star ratings
		}
		if (secComplete != null) {
			ui.setSecComplete(secComplete);
		}
		UserDAO.saveUserQuestionIneraction(ui);
		QuestionInteractionLogic.updateQuestionInteractionStats(ui);

		return ui;
	}

	@Override
	public void gotoPage(int pageNum) {
		this.pageNum = pageNum;
		if (myquestions == true)
			AppData.getApplication().gotoMyQuestions(query, pageNum, orderCode);
		else
			AppData.getApplication().gotoPublic(query, pageNum, orderCode);
	}

	@Override
	public void editQuestion(Question q) {
		if (myquestions) {
			//publicView.getTabSheet().getTab(publicView.getSearchQuestions_tab()).setVisible(false);
			publicView.getTabSheet().getTab(publicView.getCreateQuestionBox()).setVisible(true);
		}
		publicView.getCreateQuestionBox().getLogic().editQuestion(q);
		publicView.getTabSheet().getTab(publicView.getCreateQuestionBox())
				.setCaption(editQuestionTabText);
		publicView.getTabSheet().setSelectedTab(publicView.getCreateQuestionBox());
	}

	@Override
	public void deleteQuestion(Question q) throws Exception {
		QuestionDAO.deleteQuestionComplete(q);
		reloadPage();
	}

	//Methods inherited from CreateQuestionContainer interface
	@Override
	public void questionCreated() {

	}

	@Override
	public void questionEdited() {
		if (myquestions) {
			//publicView.getTabSheet().getTab(publicView.getSearchQuestions_tab()).setVisible(true);
			publicView.getTabSheet().getTab(publicView.getCreateQuestionBox()).setVisible(false);
		}
		publicView.getTabSheet().getTab(publicView.getCreateQuestionBox())
				.setCaption(createQuestionTabText);
		publicView.getTabSheet().setSelectedTab(publicView.getSearchQuestions_tab());
	}

	@Override
	public void questionEditCancel() {
		if (myquestions) {
			//publicView.getTabSheet().getTab(publicView.getSearchQuestions_tab()).setVisible(true);
			publicView.getTabSheet().getTab(publicView.getCreateQuestionBox()).setVisible(false);
		}
		publicView.getTabSheet().getTab(publicView.getCreateQuestionBox())
				.setCaption(createQuestionTabText);
		publicView.getTabSheet().setSelectedTab(publicView.getSearchQuestions_tab());
	}

	@Override
	public boolean getMyQuestions() {
		//returns true if on myquestions page
		return myquestions;
	}
}
