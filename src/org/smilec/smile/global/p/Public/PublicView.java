package org.smilec.smile.global.p.Public;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.ui.CreateQuestionBox;
import org.smilec.smile.global.ui.QuestionList;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class PublicView extends CustomComponent {

	@AutoGenerated
	private VerticalLayout mainLayout;

	@AutoGenerated
	private TabSheet tabSheet;

	@AutoGenerated
	private CreateQuestionBox createQuestionBox;

	@AutoGenerated
	private VerticalLayout searchQuestions_tab;

	@AutoGenerated
	private QuestionList questionDisplay;

	@AutoGenerated
	private AbsoluteLayout searchBar;

	@AutoGenerated
	private Label label_2;

	@AutoGenerated
	private Label resultsLabel;

	@AutoGenerated
	private AbsoluteLayout searchButton;

	@AutoGenerated
	private ComboBox orderComboBox;

	@AutoGenerated
	private Embedded searchIcon;

	@AutoGenerated
	private TextField searchField;

	@AutoGenerated
	private Embedded searchBar_right;

	@AutoGenerated
	private Embedded searchBar_middle;

	@AutoGenerated
	private Embedded searchBar_left;

	@AutoGenerated
	private Label homeLink;

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	/**
	 * The constructor should first build the main layout, set the
	 * composition root and then do any custom initialization.
	 *
	 * The constructor will not be automatically regenerated by the
	 * visual editor.
	 */

	private PublicViewLogic logic;

	//pagination vars
	private Label rightSpacer = null;
	private Button rightButton = null;
	private Button leftButton = null;
	private Label leftSpacer = null;

	public PublicView() {
		//init
		mainLayout = buildMainLayout();
		localize();
		setCompositionRoot(mainLayout);
		//set immediate to ensure loads latest ui
		//absoluteLayout_2.setImmediate(true);
		tabSheet.setImmediate(true);
		setImmediate(true);
		//logic
		logic = new PublicViewLogic(this);
	}

	private void localize() {
		homeLink.setValue(AppData.getMessage("myQuestions")); //
		tabSheet.addTab(searchQuestions_tab, AppData.getMessage("SearchQuestions"), null); //SearchQuestions
		tabSheet.addTab(createQuestionBox, AppData.getMessage("CreateQuestion"), null); //CreateQuestion
		searchField.setInputPrompt(AppData.getMessage("SearchQuestions"));
		searchButton.setDescription(AppData.getMessage("Search"));
		label_2.setValue(AppData.getMessage("OrderBy"));
	}

	public Label getHomeLink() {
		return homeLink;
	}

	public VerticalLayout getMainLayout() {
		return mainLayout;
	}

	public TabSheet getTabSheet() {
		return tabSheet;
	}

	public CreateQuestionBox getCreateQuestionBox() {
		return createQuestionBox;
	}

	public VerticalLayout getSearchQuestions_tab() {
		return searchQuestions_tab;
	}

	public AbsoluteLayout getSearchButton() {
		return searchButton;
	}

	public ComboBox getOrderComboBox() {
		return orderComboBox;
	}

	public QuestionList getQuestionDisplay() {
		return questionDisplay;
	}

	public Embedded getSearchIcon() {
		return searchIcon;
	}

	public TextField getSearchField() {
		return searchField;
	}

	public Label getResultsLabel() {
		return resultsLabel;
	}

	public PublicViewLogic getLogic() {
		return logic;
	}

	public Button getLeftButton() {
		return leftButton;
	}

	public Button getRightButton() {
		return rightButton;
	}

	public Label getRightSpacer() {
		return rightSpacer;
	}

	public void setRightSpacer(Label rightSpacer) {
		this.rightSpacer = rightSpacer;
	}

	public Label getLeftSpacer() {
		return leftSpacer;
	}

	public void setLeftSpacer(Label leftSpacer) {
		this.leftSpacer = leftSpacer;
	}

	public void setRightButton(Button rightButton) {
		this.rightButton = rightButton;
	}

	public void setLeftButton(Button leftButton) {
		this.leftButton = leftButton;
	}

	@AutoGenerated
	private VerticalLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new VerticalLayout();
		mainLayout.setImmediate(true);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("-1px");
		mainLayout.setMargin(false);

		// top-level component properties
		setWidth("100.0%");
		setHeight("-1px");

		// homeLink
		homeLink = new Label();
		homeLink.setImmediate(false);
		homeLink.setWidth("-1px");
		homeLink.setHeight("-1px");
		homeLink.setValue("<h3><p style='padding-left:5px;'><a href=\"#home\">Home</a> > My Questions</p></h3>"); //myQuestions
		homeLink.setContentMode(3);
		mainLayout.addComponent(homeLink);

		// tabSheet
		tabSheet = buildTabSheet();
		mainLayout.addComponent(tabSheet);

		return mainLayout;
	}

	@AutoGenerated
	private TabSheet buildTabSheet() {
		// common part: create layout
		tabSheet = new TabSheet();
		tabSheet.setImmediate(true);
		tabSheet.setWidth("100.0%");
		tabSheet.setHeight("-1px");

		// searchQuestions_tab
		searchQuestions_tab = buildSearchQuestions_tab();
		tabSheet.addTab(searchQuestions_tab, "Search Questions", null); //SearchQuestions

		// createQuestionBox
		createQuestionBox = new CreateQuestionBox();
		createQuestionBox.setImmediate(true);
		createQuestionBox.setWidth("100.0%");
		createQuestionBox.setHeight("100.0%");
		tabSheet.addTab(createQuestionBox, "Create Question", null); //CreateQuestion

		return tabSheet;
	}

	@AutoGenerated
	private VerticalLayout buildSearchQuestions_tab() {
		// common part: create layout
		searchQuestions_tab = new VerticalLayout();
		searchQuestions_tab.setImmediate(false);
		searchQuestions_tab.setWidth("100.0%");
		searchQuestions_tab.setHeight("-1px");
		searchQuestions_tab.setMargin(false);

		// searchBar
		searchBar = buildSearchBar();
		searchQuestions_tab.addComponent(searchBar);

		// questionDisplay
		questionDisplay = new QuestionList();
		questionDisplay.setImmediate(false);
		questionDisplay.setWidth("100.0%");
		questionDisplay.setHeight("-1px");
		searchQuestions_tab.addComponent(questionDisplay);

		return searchQuestions_tab;
	}

	@AutoGenerated
	private AbsoluteLayout buildSearchBar() {
		// common part: create layout
		searchBar = new AbsoluteLayout();
		searchBar.setImmediate(false);
		searchBar.setWidth("100.0%");
		searchBar.setHeight("37px");
		searchBar.setMargin(false);

		// searchBar_left
		searchBar_left = new Embedded();
		searchBar_left.setImmediate(false);
		searchBar_left.setWidth("9px");
		searchBar_left.setHeight("37px");
		searchBar_left.setSource(new ThemeResource("assets/images/searchBar_left.png"));
		searchBar_left.setType(1);
		searchBar_left.setMimeType("image/png");
		searchBar.addComponent(searchBar_left, "top:0.0px;left:0.0px;");

		// searchBar_middle
		searchBar_middle = new Embedded();
		searchBar_middle.setImmediate(false);
		searchBar_middle.setWidth("100.0%");
		searchBar_middle.setHeight("37px");
		searchBar_middle.setSource(new ThemeResource("assets/images/searchBar_middle.png"));
		searchBar_middle.setType(1);
		searchBar_middle.setMimeType("image/png");
		searchBar.addComponent(searchBar_middle, "top:0.0px;right:7.0px;left:9.0px;");

		// searchBar_right
		searchBar_right = new Embedded();
		searchBar_right.setImmediate(false);
		searchBar_right.setWidth("7px");
		searchBar_right.setHeight("37px");
		searchBar_right.setSource(new ThemeResource("assets/images/searchBar_right.png"));
		searchBar_right.setType(1);
		searchBar_right.setMimeType("image/png");
		searchBar.addComponent(searchBar_right, "top:0.0px;right:0.0px;");

		// searchField
		searchField = new TextField();
		searchField.setStyleName("publicSearchField");
		searchField.setImmediate(false);
		searchField.setWidth("166px");
		searchField.setHeight("-1px");
		searchField.setInputPrompt("Search Questions");
		searchField.setSecret(false);
		searchBar.addComponent(searchField, "top:5.0px;left:41.0px;");

		// searchIcon
		searchIcon = new Embedded();
		searchIcon.setStyleName("searchIcon");
		searchIcon.setImmediate(false);
		searchIcon.setWidth("14px");
		searchIcon.setHeight("15px");
		searchIcon.setSource(new ThemeResource("icons/search.png"));
		searchIcon.setType(1);
		searchIcon.setMimeType("image/png");
		searchBar.addComponent(searchIcon, "top:10.0px;left:19.0px;");

		// orderComboBox
		orderComboBox = new ComboBox();
		orderComboBox.setStyleName("orderComboBox");
		orderComboBox.setImmediate(false);
		orderComboBox.setWidth("138px");
		orderComboBox.setHeight("-1px");
		searchBar.addComponent(orderComboBox, "top:6.0px;left:319.0px;");

		// searchButton
		searchButton = new AbsoluteLayout();
		searchButton.setStyleName("public_searchButton");
		searchButton.setImmediate(false);
		searchButton.setDescription("Search");
		searchButton.setWidth("35px");
		searchButton.setHeight("35px");
		searchButton.setMargin(false);
		searchBar.addComponent(searchButton, "top:2.0px;left:210.0px;");

		// resultsLabel
		resultsLabel = new Label();
		resultsLabel.setImmediate(false);
		resultsLabel.setWidth("-1px");
		resultsLabel.setHeight("-1px");
		resultsLabel.setValue(" ");
		searchBar.addComponent(resultsLabel, "top:7.0px;right:6.0px;");

		// label_2
		label_2 = new Label();
		label_2.setImmediate(false);
		label_2.setWidth("72px");
		label_2.setHeight("-1px");
		label_2.setValue("Order By:");
		searchBar.addComponent(label_2, "top:9.0px;left:258.0px;");

		return searchBar;
	}

}
