package org.smilec.smile.global.utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.vaadin.ui.Embedded;

public class ImageUtils {
	public static BufferedImage loadImage(String ref) {
		BufferedImage bimg = null;
		try {

			bimg = ImageIO.read(new File(ref));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bimg;
	}

	public static void setImage(BufferedImage img, int targetWidth, int targetHeight, Embedded image) {
		int sourceWidth = img.getWidth();
		int sourceHeight = img.getHeight();
		int requiredWidth, requiredHeight;
		float targetRatio = (float) targetWidth / (float) targetHeight;
		float sourceRatio = (float) sourceWidth / (float) sourceHeight;
		/*System.out.println("sourceWidth: "+sourceWidth);		  
		System.out.println("sourceHeight: "+sourceHeight);
		System.out.println("targetWidth: "+targetWidth);		  
		System.out.println("targetHeight: "+targetHeight);
		System.out.println("targetRatio: "+targetRatio);		  
		System.out.println("sourceRatio: "+sourceRatio);*/

		if (sourceRatio >= targetRatio) { // source is wider than target in proportion
			requiredWidth = targetWidth;
			requiredHeight = (int) (requiredWidth / sourceRatio);
		} else { // source is higher than target in proportion
			requiredHeight = targetHeight;
			requiredWidth = (int) (requiredHeight * sourceRatio);
		}
		//System.out.println("requiredWidth: "+requiredWidth);		  
		//System.out.println("requiredHeight: "+requiredHeight);
		image.setWidth(requiredWidth, com.vaadin.terminal.Sizeable.UNITS_PIXELS);
		image.setHeight(requiredHeight, com.vaadin.terminal.Sizeable.UNITS_PIXELS);
	}

	public static BufferedImage resize(BufferedImage img, int targetWidth, int targetHeight) {
		int sourceWidth = img.getWidth();
		int sourceHeight = img.getHeight();
		int requiredWidth, requiredHeight;
		float targetRatio = (float) targetWidth / (float) targetHeight;
		float sourceRatio = (float) sourceWidth / (float) sourceHeight;
		/*System.out.println("sourceWidth: "+sourceWidth);		  
		System.out.println("sourceHeight: "+sourceHeight);
		System.out.println("targetWidth: "+targetWidth);		  
		System.out.println("targetHeight: "+targetHeight);
		System.out.println("targetRatio: "+targetRatio);		  
		System.out.println("sourceRatio: "+sourceRatio);*/

		if (sourceRatio >= targetRatio) { // source is wider than target in proportion
			requiredWidth = targetWidth;
			requiredHeight = (int) (requiredWidth / sourceRatio);
		} else { // source is higher than target in proportion
			requiredHeight = targetHeight;
			requiredWidth = (int) (requiredHeight * sourceRatio);
		}
		//System.out.println("requiredWidth: "+requiredWidth);		  
		//System.out.println("requiredHeight: "+requiredHeight);

		BufferedImage dimg = new BufferedImage(requiredWidth, requiredHeight, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, requiredWidth, requiredHeight, 0, 0, sourceWidth, sourceHeight, null);
		g.dispose();
		return dimg;
	}

	/** 
	 * Saves a BufferedImage to the given file, pathname must not have any 
	 * periods "." in it except for the one before the format, i.e. C:/images/fooimage.png 
	 * @param img 
	 * @param saveFile 
	 */
	public static void saveImage(BufferedImage img, String ref) {
		try {
			// String format = (ref.endsWith(".png")) ? "png" : "jpg";  
			String type = ref.toLowerCase();
			String format = "jpg";

			if (type.endsWith(".png"))
				format = "png";
			else if (type.endsWith(".gif"))
				format = "gif";

			ImageIO.write(img, format, new File(ref));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
