package org.smilec.smile.global.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Utils {
	public static int greaterOfTwo(int one, int two) {
		if (one >= two)
			return one;
		else
			return two;
	}

	public static double greaterOfTwo(double one, double two) {
		if (one >= two)
			return one;
		else
			return two;
	}

	public static float greaterOfTwo(float one, float two) {
		if (one >= two)
			return one;
		else
			return two;
	}

	public static Date getTime() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}

	public static int random(int min, int max) {
		return (int) Math.floor((max - min + 1) * Math.random()) + min;
	}

	public static ArrayList listToArrayList(List l) {
		Iterator iterator = l.iterator();
		ArrayList arrayList = new ArrayList();
		while (iterator.hasNext()) {
			arrayList.add(iterator.next());
		}
		return arrayList;
	}
}
