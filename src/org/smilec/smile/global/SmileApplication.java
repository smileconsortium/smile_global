package org.smilec.smile.global;

import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.smilec.smile.global.hibernate.Evite;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.PasswordChangeRequest;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserStats;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.HibernateUtil;
import org.smilec.smile.global.hibernate.logic.MessageDAO;
import org.smilec.smile.global.hibernate.logic.QuizDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.p.Groups.CreateGroupView;
import org.smilec.smile.global.p.Groups.CreateQuizView;
import org.smilec.smile.global.p.Groups.GroupsView;
import org.smilec.smile.global.p.Groups.MemberGroupView;
import org.smilec.smile.global.p.Groups.OrganizeGroupView;
import org.smilec.smile.global.p.Groups.QuizView;
import org.smilec.smile.global.p.Home.HomeView;
import org.smilec.smile.global.p.LoginRegister.ForgotUsername;
import org.smilec.smile.global.p.LoginRegister.LoginView;
import org.smilec.smile.global.p.LoginRegister.PasswordChange;
import org.smilec.smile.global.p.LoginRegister.RegisterView;
import org.smilec.smile.global.p.Message.MessageView;
import org.smilec.smile.global.p.Profile.ProfileView;
import org.smilec.smile.global.p.Public.PublicView;
import org.smilec.smile.global.p.Public.PublicViewLogic;
import org.smilec.smile.global.p.TopBar.TopBar;
import org.smilec.smile.global.p.TopBar.UserBox;
import org.smilec.smile.global.vars.ResourcePaths;
import org.vaadin.notifique.Notifique;
import org.vaadin.notifique.Notifique.ClickListener;
import org.vaadin.notifique.Notifique.HideListener;
import org.vaadin.notifique.Notifique.Message;
import org.vaadin.overlay.CustomOverlay;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.Application;
import com.vaadin.Application.UserChangeListener;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.ParameterHandler;
import com.vaadin.terminal.Resource;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.UriFragmentUtility;
import com.vaadin.ui.UriFragmentUtility.FragmentChangedEvent;
import com.vaadin.ui.UriFragmentUtility.FragmentChangedListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SmileApplication extends Application implements UserChangeListener, ClickListener, HideListener, RefreshListener {
	public static final String NOTIFICATION_ERROR = Notifique.Styles.ERROR;
	public static final String NOTIFICATION_INFO = Notifique.Styles.INFO;
	public static final String NOTIFICATION_WARNING = Notifique.Styles.WARNING;
	public static final String NOTIFICATION_MESSAGE = Notifique.Styles.MESSAGE;
	public static final String NOTIFICATION_SUCCESS = Notifique.Styles.SUCCESS;

	private static final long refreshRate = 1000 * 10; //10-sec refresh rate
	private static final long mailCheckRate = 1000 * 60; //1 min mail rate
	private static final int messageShowTime = 1000 * 3; //show messages for 3 seconds
	private ArrayList<RefreshListener> refreshListeners = new ArrayList<RefreshListener>(); //maintain list of refresh listeners
	private long lastRefresh;

	private Window mainWindow;
	private VerticalLayout kingLayout;
	private CustomOverlay ol;
	private TopBar topBar;
	private Refresher refresher;
	private CustomComponent currentView;
	public String previousSessionFrag = null; //if fails to load a page because logged out, 
	//will store URL and redirect to there after login
	private UserBox userBox;
	private UrifuHistory urifu;
	private int eid = 0;
	private String token;
	private int cid = 0;
	private String uuid;
	private Notifique stack;
	//language
	public static String languageBundleName = "resources/MessagesBundle";
	//media paths
	public String base_url, media_url, question_url, user_url, mediaUser_url, baseUser_url,
			baseMediaUser_url;

	public static String feedbackURL = "https://gse.qualtrics.com/SE/?SID=SV_3DUKnDwEyVtqvKl";

	private class UrifuHistory extends UriFragmentUtility {
		private String prevFragment;

		public UrifuHistory() {
			super();
		}

		@Override
		public void setFragment(String fragment) {
			String prevFrag = getFragment();

			if (prevFrag != null && prevFrag.trim().length() > 0) {
				this.prevFragment = prevFrag;
			}
			super.setFragment(fragment);
		}

		public String getPrevFragment() {
			return prevFragment;
		}
	}

	@Override
	public void init() {
		//System.out.println("SmileApplication initialized: " + this);
		setTheme("smilewebtheme");
		//init hibernate
		HibernateUtil.init();

		// Create the application data instance
		AppData sessionData = new AppData(this);
		sessionData.loadProperties();

		// Register it as a listener in the application context
		getContext().addTransactionListener(sessionData);

		// Initialize the session-global data
		AppData.initLocale(getLocale(), languageBundleName);

		mainWindow = new MainWindow(this);
		mainWindow.setImmediate(true); //causes events from the client to be immediately sent to server
										//https://vaadin.com/forum/-/message_boards/view_message/117328
		mainWindow.setName("mainWindow");
		mainWindow.setScrollable(false);
		mainWindow.setHeight("-1");
		addWindow(mainWindow);
		setMainWindow(mainWindow);

		/*mainWindow.addListener(new Window.CloseListener() {
			@Override
			public void windowClose(CloseEvent e) {
				//System.out.println("MainWindow Close Listener: Closing");
			}
		});*/

		//listen for UserChange events
		this.addListener(this);

		//set URI fragment utility
		urifu = new UrifuHistory();

		//listen for Urifu Changes
		urifu.addListener(new FragmentChangedListener() {
			@Override
			public void fragmentChanged(FragmentChangedEvent source) {
				String fragment = source.getUriFragmentUtility().getFragment();
				setContent(fragment);
			}
		});

		//set content
		kingLayout = buildKingLayout();

		kingLayout.setImmediate(true);
		kingLayout.addComponent(urifu);

		//add refresher listener
		lastRefresh = System.currentTimeMillis();
		refresher = topBar.getRefresher();
		refresher.addListener(this);
		setRefreshRate(refreshRate);

		//add notifique window
		stack = new Notifique(true);
		stack.setClickListener(this);
		stack.setHideListener(this);
		stack.setWidth("80%");
		mainWindow.setContent(kingLayout);
		ol = new CustomOverlay(stack, mainWindow);
		ol.setWidth("100%");
		kingLayout.addComponent(ol);

		ParameterHandler parameterHandler = new ParameterHandler() {
			@Override
			public void handleParameters(Map parameters) {
				// Get and store the passed HTTP parameter.
				if (parameters.containsKey("evite") && parameters.containsKey("t")) {
					setEid(Integer.parseInt(((String[]) parameters.get("evite"))[0]),
							((String[]) parameters.get("t"))[0]);
				} else if (parameters.containsKey("cid") && parameters.containsKey("w")) {
					setCid(Integer.parseInt(((String[]) parameters.get("cid"))[0]),
							((String[]) parameters.get("w"))[0]);
				}
			}
		};

		mainWindow.addParameterHandler(parameterHandler);

		//set url's
		base_url = getContext().getBaseDirectory().toString();
		media_url = "";//getURL().toString();
		//media_url = media_url.substring(0, media_url.lastIndexOf('/'));
		media_url = "/uploads/";//media_url.substring(0, media_url.lastIndexOf('/')) + "/uploads/";
		question_url = base_url.substring(0, base_url.lastIndexOf('/')) + "/uploads/questions/";
		baseUser_url = base_url.substring(0, base_url.lastIndexOf('/')) + "/uploads/users/";
		baseMediaUser_url = media_url + "/users/";
		//mediaUser and user url's are set when logged in
		mediaUser_url = "";
		user_url = "";

		gotoLogin();
		setTopBarLoggedOut();

		/*
		
		Group group = GroupDAO.getGroupByName("b");
		Quiz quiz = QuizDAO.getQuizByName(group, "toto");
		QuizView qv = new QuizView(quiz, group);
		setContent(qv);
		//load page
		int page = 1;
		int tabCode = 0; //0: create, 1: answer
		int orderCode = PublicViewLogic.ORDER_CODE_DATE_ASC;
		qv.getLogic().loadPage(page, tabCode, orderCode);
		 */
	}

	@Override
	public void refresh(Refresher source) {
		//other Views like QuizView can listen to refresher and start higher refresh rates
		//only execute smile commands after checking that sufficient time has passed;
		Long current = System.currentTimeMillis();
		if (current - lastRefresh > mailCheckRate) {
			lastRefresh = current;
			mailRefresh();
		}
	}

	//called on every refresh that is longer than refreshRate
	//checks unread mail and evites
	private void mailRefresh() {
		if (isLoggedIn()) {
			//check for new messages and evites
			int numM = MessageDAO.countUnreadMsg(getUser());
			setMessageNum(numM);
		}
	}

	public void setMessageNum(int count) {
		if (getMessageNum() != count) {
			int oldNum = getMessageNum();
			topBar.getLogic().setMessageCount(count);
			if (count > oldNum) {
				if (getCurrentView() instanceof MessageView
						&& ((MessageView) getCurrentView()).getState() == MessageView.State.inbox) {
					//if on inbox when new message comes in, refresh mailbox
					((MessageView) getCurrentView()).gotoInbox();
				} else {
					//otherwise show notification
					String title = AppData.getMessage("NewMessage");
					String msg;
					if (count > 1) {
						msg = AppData.getFormattedMsg("messageCount_plural", new Object[] { count });
					} else {
						msg = AppData.getFormattedMsg("messageCount_singular", new Object[] { count });
					}
					showNotification(title, msg, NOTIFICATION_INFO);
				}
			}
		}
	}

	public int getMessageNum() {
		return topBar.getLogic().getMessageCount();
	}

	public void addRefreshListener(RefreshListener listener) {
		refresher.addListener(listener);
		refreshListeners.add(listener);
	}

	public void setRefreshRate(long millis) {
		//check if time
		refresher.setRefreshInterval(millis);
	}

	public void setEid(Integer eid, String token) {
		//System.out.println("setEid eid: " + eid + " t: " + token);
		this.eid = eid;
		if (token != null)
			this.token = token;
		getMainWindow().open(new ExternalResource(getHostname()));
		processEvite();
	}

	public void setCid(Integer cid, String token) {
		//System.out.println("setCid cid: " + cid + " t: " + token);
		this.cid = cid;
		uuid = token;
		getMainWindow().open(new ExternalResource(getHostname()));
		processPCR();
	}

	public void processPCR() {
		//System.out.println("processPCR id: " + cid);

		if (cid > 0) {
			PasswordChangeRequest r = UserDAO.getPCR(cid, uuid);

			if (r != null) {
				Date createdOn = r.getCreatedOn();
				Date today = new Date();
				boolean validPCR = true;
				long diff = today.getTime() - createdOn.getTime();
				//long diffSeconds = diff / 1000;         
				//long diffMinutes = diff / (60 * 1000);         
				long diffHours = diff / (60 * 60 * 1000);

				if (diffHours < 24) // one day
				{
					Integer uid = r.getUser_id();
					User user = UserDAO.getUserById(uid);

					if (user != null) {
						setUrifu("passwordchange");
						setContent(new PasswordChange(user, cid));
					} else
						validPCR = false;
				} else {
					validPCR = false;
				}

				if (validPCR == false) {
					UserDAO.deletePCR(cid);
					cid = 0;
					uuid = "";
					showNotification(AppData.getMessage("Sorry"), AppData.getMessage("invalidLink"),
							NOTIFICATION_ERROR);
					//this.getMainWindow().open(new ExternalResource( this.getURL() ) );
				}
			} else {
				//System.out.println("could not find the cid: " + cid);
				showNotification(AppData.getMessage("Sorry"), AppData.getMessage("invalidLink"),
						NOTIFICATION_ERROR);
				//this.getMainWindow().open(new ExternalResource( this.getURL() ) );
			}
		}
	}

	public void processEvite() {
		//System.out.println("processEvite eid: " + eid);
		if (eid > 0) {
			Evite e = GroupDAO.getEvite(eid, token);

			if (e != null) {
				Integer uid = e.getUser_id();
				Integer gid = e.getGroup_id();
				if (uid == 0) {
					gotoRegister();
				} else {
					User user = UserDAO.getUserById(uid);

					if (user != null) {
						if (!GroupDAO.userExist(gid, uid)) {
							GroupDAO.addUserToGroupById(gid, uid);
							Group g = GroupDAO.getGroupById(gid);

							if (GroupDAO.userExist(gid, uid)) {
								GroupDAO.deleteEvite(eid);
								eid = 0;
								token = "";
								String msg =
										AppData.getFormattedMsg("enrollSuccess",
												new Object[] { user.getUsername(), g.getName() });
								showNotification(AppData.getMessage("EmailInvite"), msg,
										NOTIFICATION_INFO);
							}
						}
					}
				}
			} else {
				System.out.println("could not find the eid: " + eid);
			}
		}
	}

	private void processEviteAfterRegistration() {
		//System.out.println("processEviteAfterRegistration eid: " + eid);
		if (eid > 0) {
			Evite e = GroupDAO.getEvite(eid, token);

			if (e != null) {
				Integer uid = e.getUser_id();
				Integer gid = e.getGroup_id();
				if (uid == 0) {
					User user = getUser();

					if (user != null) {
						//System.out.println("processEviteAfterRegistration new user id: " + user.getId()
						//		+ " gid: " + gid);

						if (!GroupDAO.userExist(gid, user.getId())) {
							GroupDAO.addUserToGroupById(gid, user.getId());
							Group g = GroupDAO.getGroupById(gid);
							if (GroupDAO.userExist(gid, user.getId())) {
								GroupDAO.deleteEvite(eid);
								eid = 0;
								token = "";
								Object[] args = { user.getUsername(), g.getName() };
								String msg = AppData.getFormattedMsg("enrollSuccess", args);
								showNotification(AppData.getMessage("EmailInvite"), msg,
										NOTIFICATION_INFO);
							}
						}
					} else
						System.out.println("processEviteAfterRegistration: no user info");
				}
			} else
				System.out.println("could not find the eid: " + eid);
		}
	}

	private VerticalLayout buildKingLayout() {
		// common part: create layout
		kingLayout = new VerticalLayout();
		kingLayout.setImmediate(true);
		kingLayout.setWidth("100%");
		kingLayout.setHeight("-1");
		kingLayout.setMargin(false);

		// topBar
		initTopBar();

		return kingLayout;
	}

	private void initTopBar() {
		if (topBar != null) {
			kingLayout.removeComponent(topBar);
		}
		topBar = new TopBar();
		topBar.setImmediate(false);
		topBar.setWidth("100.0%");
		topBar.setHeight("60px");
		//first remove userBox if exists
		setTopBarLoggedOut();
		if (isLoggedIn()) {
			setTopBarLoggedIn();
		}
		int index = 0;
		if (getCurrentView() != null)
			index = kingLayout.getComponentIndex(getCurrentView());
		kingLayout.addComponent(topBar, index);
	}

	public void setContent(CustomComponent c) {
		//remove old refresh listeners
		for (int i = refreshListeners.size() - 1; i >= 0; i--) {
			if (!c.equals(refreshListeners.get(i))) {
				refresher.removeListener(refreshListeners.remove(i));
			}
		}
		if (refreshListeners.size() < 1) {
			//if no other refresh listeners, set to refreshRate
			if (refreshRate != refresher.getRefreshInterval())
				setRefreshRate(refreshRate);
		}
		//remove old view
		if (getCurrentView() != null) {
			kingLayout.removeComponent(getCurrentView());
		}
		//set current view to new component
		setCurrentView(c);
		getCurrentView().addStyleName("currentView");
		kingLayout.addComponent(getCurrentView());
		clearNotifications();
		//topBar.requestRepaintAll();
	}

	private void setContent(String fragment) {
		String[] frags = fragment.split("\\?");
		String hash = frags[0];
		HashMap<String, String> hashMap = new HashMap<String, String>();
		if (frags.length > 1) {
			String params = frags[1];
			String[] pairs = params.split("&");
			for (int i = 0; i < pairs.length; i++) {
				String[] pair = pairs[i].split("=");
				String key = pair[0];
				String value = "";
				if (pair.length > 1) {
					value = pair[1];
				}
				hashMap.put(key, value);
			}
		}

		//System.out.println("SmileApplication.setContent(String fragment) called. Hash: " + hash);
		if (hash.equals("home")) {
			if (loginWall(hash)) {
				setContent(new HomeView(null));
				topBar.getLogic().selectButton(topBar.getHomeButton());
			}
		} else if (hash.equals("public")) {
			if (loginWall(hash)) {
				//only create new public view if not already on publicview
				PublicView pv;
				boolean reloadView = false;
				if (urifu.getPrevFragment().startsWith("public")
						&& getCurrentView() instanceof PublicView) {
					pv = (PublicView) getCurrentView();
					reloadView = true;
				} else {
					pv = new PublicView();
				}
				String query = "";
				int page = 1;
				int order = PublicViewLogic.ORDER_CODE_DATE_DESC;
				int qid = -1;
				if (hashMap.containsKey("query")) {
					try {
						query = URLDecoder.decode(hashMap.get("query"), "UTF-8");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (hashMap.containsKey("page")) {
					page = Integer.parseInt(hashMap.get("page"));
				}
				if (hashMap.containsKey("order")) {
					order = Integer.parseInt(hashMap.get("order"));
				}
				if (hashMap.containsKey("qid")) {
					qid = Integer.parseInt(hashMap.get("qid"));
				}

				//load appropriate query and page num
				pv.getLogic().query(query, page, order);
				if (qid > 0) {
					pv.getLogic().selectQuestion(qid);
				}
				if (!reloadView) {
					setContent(pv);
				}
				topBar.getLogic().selectButton(topBar.getPublicButton());
			}
		} else if (hash.equals("myquestions")) {
			if (loginWall(hash)) {
				//only create new public view if not already on publicview
				PublicView pv;
				boolean reloadView = false;
				if (urifu.getPrevFragment().startsWith("myquestions")
						&& getCurrentView() instanceof PublicView) {
					pv = (PublicView) getCurrentView();
					reloadView = true;
				} else {
					pv = new PublicView();
				}
				String query = "";
				int page = 1;
				int order = PublicViewLogic.ORDER_CODE_DATE_DESC;
				if (hashMap.containsKey("query")) {
					try {
						query = URLDecoder.decode(hashMap.get("query"), "UTF-8");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (hashMap.containsKey("page")) {
					page = Integer.parseInt(hashMap.get("page"));
				}
				if (hashMap.containsKey("order")) {
					order = Integer.parseInt(hashMap.get("order"));
				}
				//load appropriate query and page num
				pv.getLogic().myquestions(query, page, order);
				if (!reloadView) {
					setContent(pv);
				}
				topBar.getLogic().selectButton(topBar.getHomeButton());
			}
		} else if (hash.equals("messages")) {
			if (loginWall(hash)) {
				Group group = null;
				User user = null;
				if (hashMap.containsKey("group")) {
					int groupId = Integer.parseInt(hashMap.get("group"));
					group = GroupDAO.getGroupById(groupId);
				}
				if (hashMap.containsKey("user")) {
					int userId = Integer.parseInt(hashMap.get("user"));
					user = UserDAO.getUserById(userId);
				}
				MessageView mv = new MessageView();
				setContent(mv);
				topBar.getLogic().selectButton(topBar.getMessagesButton());
				if (group != null) {
					mv.composeToGroup(group);
				} else if (user != null) {
					mv.composeToUser(user);
				}
			}
		} else if (hash.equals("groups")) {
			if (loginWall(hash)) {
				setContent(new GroupsView());
				topBar.getLogic().selectButton(topBar.getGroupsButton());
			}
		} else if (hash.equals("groups/createNew")) {
			if (loginWall(hash)) {
				setContent(new CreateGroupView());
				topBar.getLogic().selectButton(topBar.getGroupsButton());
			}
		} else if (hash.startsWith("groups/createQuiz/")) {
			if (loginWall(hash)) {
				//extract group id
				int groupId = -1;
				try {
					groupId = Integer.decode(hash.substring(18));
				} catch (Exception e) {
					System.out.println(e);
				}

				//check if group exists
				Group group = GroupDAO.getGroupById(groupId);
				if (group != null) {
					List<Integer> organizers = GroupDAO.getGroupOrganizers(group);
					//check if logged in as specific group's organizer 
					if (group.isOwner(getUser()) || organizers.contains(getUser().getId())) {
						setContent(new CreateQuizView(group));
					} else {
						//TODO: check if a group member
						setContent(new GroupsView());
					}
				} else {
					//TODO: check if a group member
					setContent(new GroupsView());
				}
				topBar.getLogic().selectButton(topBar.getGroupsButton());
			}
		} else if (hash.startsWith("groups/")) {
			//if starts with groups, but isn't createGroup, etc, then go to specific group's page
			if (loginWall(hash)) {
				String groupName = hash.substring(7);
				//check if just a group or if also contains a quiz name
				String[] groupQuiz = groupName.split("/");
				//check if group exists
				int groupId = -1;
				try {
					groupId = Integer.decode(groupQuiz[0]);
				} catch (Exception e) {
					System.out.println(e);
				}
				Group group = GroupDAO.getGroupById(groupId);
				Quiz quiz = null;
				if (group != null) {
					//check if there is a quiz
					if (groupQuiz.length > 1) {
						int quizId = -1;
						try {
							quizId = Integer.decode(groupQuiz[1]);
						} catch (Exception e) {
							System.out.println(e);
						}
						quiz = QuizDAO.getQuizById(quizId);
					}
					//check if a group organizer or member to load QuizView
					User user = getUser();
					if (GroupDAO.userInGroup(group.getId(), user) || (group.isOwner(user))) {
						if (quiz != null) {
							//System.out.println("setting content to QuizView");
							int page = 1;
							int tabCode = 0; //0: create, 1: answer
							int orderCode = PublicViewLogic.ORDER_CODE_DATE_ASC;
							if (hashMap.containsKey("page")) {
								page = Integer.parseInt(hashMap.get("page"));
							}
							if (hashMap.containsKey("tab")) {
								tabCode = Integer.parseInt(hashMap.get("tab"));
							}
							if (hashMap.containsKey("order")) {
								orderCode = Integer.parseInt(hashMap.get("order"));
							}
							QuizView qv;
							if (getCurrentView() instanceof QuizView) {
								qv = (QuizView) getCurrentView();
							} else {
								qv = new QuizView(quiz, group);
								setContent(qv);
							}
							//load page
							qv.getLogic().loadPage(page, tabCode, orderCode);
						} else {
							//check if organizer
							List<Integer> organizers = GroupDAO.getGroupOrganizers(group);
							if (group.isOwner(user) || organizers.contains(user.getId())) {
								setContent(new OrganizeGroupView(group));
							} else {
								setContent(new MemberGroupView(group));
							}
						}
					} else {
						//goto groups page if not organizer or member
						setContent(new GroupsView());
					}
				} else {
					setContent(new GroupsView());
				}
				topBar.getLogic().selectButton(topBar.getGroupsButton());
			}
		} else if (hash.startsWith("profile")) {
			if (loginWall(hash)) {
				setContent(new ProfileView());
				topBar.getLogic().selectButton(topBar.getProfileButton());
			}
		} else if (hash.startsWith("userProfile")) {
			if (loginWall(hash)) {
				if (hashMap.containsKey("user")) {
					int userId = Integer.decode(hashMap.get("user"));
					if (userId == getUser().getId()) {
						setContent(new HomeView(null));
					} else {
						setContent(new HomeView(userId));
					}
					topBar.getLogic().deselectButtons();
				}
			}
		} else if (hash.equals("login")) {
			if (isLoggedIn()) {
				topBar.getLogic().deselectButtons();
			}
			setContent(new LoginView());
		} else if (hash.equals("register")) {
			if (isLoggedIn()) {
				gotoHome();
			} else {
				setContent(new RegisterView());
			}
		} else if (hash.equals("login/forgotusername")) {
			setContent(new ForgotUsername());
		} else if (hash.equals("redirect")) {
			if (getCurrentView() != null) {
				String prevFrag = urifu.getPrevFragment();
				if (prevFrag != null && prevFrag.trim().length() > 0 && prevFrag != "reload"
						&& prevFrag != "redirect") {
					urifu.setFragment("reload?page=" + urifu.getPrevFragment());
				} else {
					gotoHome();
				}
			}
		} else if (hash.startsWith("reload")) {
			String page = null;
			if (hashMap.containsKey("page")) {
				page = hashMap.get("page");
			}
			if (page != null && page.trim().length() > 0) {
				initTopBar();
				urifu.setFragment(page);
			} else {
				initTopBar();
				gotoHome();
			}
		}
	}

	public void setTopBarLoggedOut() {
		topBar.getLogic().setLoggedOut();
		if (userBox != null) { //remove user box if logged out and userbox is not null
			topBar.getRightVL().removeComponent(getUserBox());
			setUserBox(null);
		}
	}

	public void setTopBarLoggedIn() {
		topBar.getLogic().setLoggedIn();
		if (userBox == null) { //add userbox if logged in has not been added
			setUserBox(new UserBox(this, getUser().getUsername()));
			topBar.getRightVL().addComponent(getUserBox(), 0);
		}
		//check for new messages
		mailRefresh();
	}

	public boolean loginWall(String hash) {
		//redirects to login page if not logged in, otherwise adds userbox
		if (!isLoggedIn()) {
			if (hash != null && hash.length() > 0) {
				previousSessionFrag = hash;
			}
			gotoLogin();
			authenticationError();
			return false;
		} else {
			return true;
		}
	}

	public Boolean isLoggedIn() {
		return getUser() != null;
	}

	public void authenticationError() {
		//showNotification("Oops.", "You must login or create a new account to access this page.",
		//		NOTIFICATION_WARNING);
	}

	//Navigation functions
	public void gotoProfile() {
		//set Urifu
		urifu.setFragment("profile");
	}

	public void gotoPublic() {
		gotoPublic("", 1, PublicViewLogic.ORDER_CODE_DATE_DESC);
	}

	public void gotoPublic(String query, int page) {
		gotoPublic(query, page, PublicViewLogic.ORDER_CODE_DATE_DESC);
	}

	public void gotoPublic(String query, int page, int order) {
		//set Urifu
		urifu.setFragment("public?query=" + query + "&page=" + page + "&order=" + order);
	}

	public void gotoPublic(String query, int page, int order, int questionId) {
		//set Urifu
		urifu.setFragment("public?query=" + query + "&page=" + page + "&order=" + order + "&qid="
				+ questionId);
	}

	public void gotoMyQuestions(String query, int page, int order) {
		//set Urifu
		urifu.setFragment("myquestions?query=" + query + "&page=" + page + "&order=" + order);
	}

	public void gotoMyQuestions() {
		//set Urifu
		urifu.setFragment("myquestions");
	}

	public void gotoGroups() {
		//set Urifu
		urifu.setFragment("groups");
	}

	public void gotoCreateGroups() {
		//set Urifu
		urifu.setFragment("groups/createNew");
	}

	public void gotoGroupsOrganizerPage(Group group) {
		//set Urifu
		urifu.setFragment("groups/" + group.getId());
	}

	public void gotoGroupsMemberPage(Group group) {
		//set Urifu
		urifu.setFragment("groups/" + group.getId());
	}

	public void gotoCreateQuiz(Group group) {
		//set Urifu
		urifu.setFragment("groups/createQuiz/" + group.getId());
	}

	public void gotoQuiz(Group group, Quiz quiz) {
		//set Urifu
		urifu.setFragment("groups/" + group.getId() + "/" + quiz.getId());
	}

	public void gotoQuiz(Group group, Quiz quiz, int pageNum) {
		//set Urifu
		urifu.setFragment("groups/" + group.getId() + "/" + quiz.getId() + "?page=" + pageNum);
	}

	public void gotoQuiz(Group group, Quiz quiz, int pageNum, int tabCode, int orderCode) {
		//set Urifu
		String tab = "";
		urifu.setFragment("groups/" + group.getId() + "/" + quiz.getId() + "?page=" + pageNum + "&tab="
				+ tabCode + "&order=" + orderCode);
	}

	public void gotoHome() {
		//set Urifu
		urifu.setFragment("home");
	}

	public void gotoLogin() {
		//set Urifu
		urifu.setFragment("login");
	}

	public void gotoRegister() {
		//set Urifu
		urifu.setFragment("register");
	}

	public void gotoMessages() {
		//set Urifu
		urifu.setFragment("messages");
	}

	public void gotoMessages(Group group) {
		//set Urifu
		urifu.setFragment("messages?group=" + group.getId());
	}

	public void gotoMessages(User user) {
		//set Urifu
		urifu.setFragment("messages?user=" + user.getId());
	}

	public void setUrifu(String s) {
		urifu.setFragment(s);
	}

	//Called automatically by framework when Application instance closes,
	//usually by session timeout. 
	@Override
	public void close() {
		//anything to do before application closes
		super.close();
	}

	//Getters and Setters

	/*@Override
	public MainWindow getMainWindow() {
		return mainWindow;
	}
	public void setMainWindow(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}*/

	@Override
	public void applicationUserChanged(UserChangeEvent event) {
		User user = (User) event.getNewUser();
		if (user == null) {
			//update user stats
			if (event.getPreviousUser() != null) {
				updateLogOutStats((User) event.getPreviousUser());
			}
			//if logging out, remove user box and go to login page
			setTopBarLoggedOut();

			if (getCurrentView() instanceof LoginView) {
				//if already on login view, force reload login view to reload registerPanel
				setContent("login");
			}
			gotoLogin();
			mediaUser_url = "";
			user_url = "";
		} else {
			//if logging in or switching users, go to home page 
			mediaUser_url = baseMediaUser_url + getUser().getId() + "/";
			user_url = baseUser_url + getUser().getId() + "/";
			setTopBarLoggedIn();
			//set login time
			UserStats stats = getUser().getUserStats();
			stats.setNumSessions(stats.getNumSessions() + 1);
			stats.setLastAccessed(new Date());
			UserDAO.updateUserStats(stats);
			//If logging in after having hit a loginWall from a different page, continue to that page
			if (previousSessionFrag != null) {
				urifu.setFragment(previousSessionFrag);
				previousSessionFrag = null;
			} else {
				gotoHome();
			}
			if (eid > 0)
				processEviteAfterRegistration();
		}
	}

	private void updateLogOutStats(User user) {
		UserStats stats = user.getUserStats();
		Date lastAccessed = stats.getLastAccessed();
		Date now = new Date();
		int elapsedSecs = (int) (now.getTime() - lastAccessed.getTime()) / 1000;
		stats.setCumulativeSecs(stats.getCumulativeSecs() + elapsedSecs);
		stats.setAvgSessionSecs(((stats.getAvgSessionSecs() * stats.getNumSessions()) + elapsedSecs)
				/ (stats.getNumSessions() + 1));
		UserDAO.updateUserStats(stats);
	}

	public UserBox getUserBox() {
		return userBox;
	}

	public void setUserBox(UserBox userBox) {
		this.userBox = userBox;
	}

	@Override
	public User getUser() {
		return (User) super.getUser();
	}

	public CustomComponent getCurrentView() {
		return currentView;
	}

	public void setCurrentView(CustomComponent currentView) {
		this.currentView = currentView;
	}

	public UriFragmentUtility getUrifu() {
		return urifu;
	}

	//TEMP TODO: hard-coded hostname since forwarding problems are not resolved
	public String getHostname() {
		URL url = getURL();
		String urlString = url.toString();
		urlString = urlString.replace(":8080/SmileWeb", "/SmileWeb");
		// TODO Auto-generated method stub
		return urlString;
	}

	public void showNotification(String title, String message) {
		/*Notification n = new Notification(title, message, Notification.TYPE_TRAY_NOTIFICATION, true);
		n.setDelayMsec(time);
		n.setStyleName("custom");
		n.setPosition(Notification.POSITION_TOP_LEFT);
		*/
		showNotification(title, message, NOTIFICATION_INFO);
	}

	public void showNotification(String title, String message, String style) {
		String msg = "";
		if (title.length() > 0) {
			msg += "<strong>" + title + "</strong>";
		}
		if (message.length() > 0) {
			msg += "<br><br>" + message + "";
		}
		Resource r = null;
		if (style.equals(NOTIFICATION_SUCCESS)) {
			r = ResourcePaths.greenCheck;
		} else if (style.equals(NOTIFICATION_ERROR)) {
			r = ResourcePaths.redError;
		}
		Message m = stack.add(r, msg, style);
		System.out.println(stack);
		System.out.println(kingLayout.getComponentIndex(ol));
		Timer timer = new Timer();
		timer.schedule(new MessageHider(m), messageShowTime);
	}

	private class MessageHider extends TimerTask {
		Message m;

		@Override
		public void run() {
			m.hide();
		}

		MessageHider(Message m) {
			super();
			this.m = m;
		}
	}

	@Override
	public void messageHide(Message message) {
		// TODO Auto-generated method stub

	}

	public void clearNotifications() {
		stack.clear();
	}

	@Override
	public void messageClicked(Message message) {
		message.hide();
	}

	public void loadFeedback() {
		ExternalResource url = new ExternalResource(feedbackURL);
		mainWindow.open(url, "_blank", false);
	}

	public void changeLocale(Locale locale) {
		AppData.initLocale(locale, languageBundleName);
		//redraw current view
		urifu.setFragment("redirect");
	}
}
