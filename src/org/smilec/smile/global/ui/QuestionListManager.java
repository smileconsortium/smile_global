package org.smilec.smile.global.ui;

import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;

public interface QuestionListManager {
	public UserQuestionInteraction submitQuestion(Question question, QuestionAnswer qa, int rating,
			String comment, Integer secComplete);

	public void gotoPage(int pageNumber);

	public void editQuestion(Question q);

	public void deleteQuestion(Question q) throws Exception;

	public boolean getMyQuestions();
}
