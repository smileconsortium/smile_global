package org.smilec.smile.global.ui;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;
import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.terminal.Sizeable;

public class PieChart {

	private static final long serialVersionUID = 1L;

	JFreeChart chart;
	JFreeChartWrapper fchart;

	boolean showPercentage = false;

	public PieChart(String chartTitle, ArrayList<String> legend, ArrayList<Integer> percent,
			Integer width, Integer height, boolean showPercentage) {

		this.showPercentage = showPercentage;
		PieDataset dataset = createDataset(legend, percent);

		chart = createChart(dataset, chartTitle);

		/*final StandardChartTheme chartTheme =
				(StandardChartTheme) org.jfree.chart.StandardChartTheme.createJFreeTheme();
		final Font font = new Font("Arial", Font.PLAIN, 14);
		final Font largeFont = new Font("Arial", Font.PLAIN, 16);
		final Font extraLargeFont = new Font("Arial", Font.PLAIN, 18);
		chartTheme.setExtraLargeFont(extraLargeFont);
		chartTheme.setLargeFont(largeFont);
		chartTheme.setRegularFont(font);
		chartTheme.setSmallFont(font);
		chartTheme.setPlotBackgroundPaint(Color.white);
		chartTheme.setPlotOutlinePaint(Color.white);
		chartTheme.apply(chart);*/

		fchart = new JFreeChartWrapper(chart);

		if (width > 0 && height > 0) {
			fchart.setHeight(height, Sizeable.UNITS_PIXELS);
			fchart.setWidth(width, Sizeable.UNITS_PIXELS);
		}

	}

	/**
	 * Creates a sample dataset 
	 */

	private PieDataset createDataset(ArrayList<String> legend, ArrayList<Integer> percent) {
		DefaultPieDataset result = new DefaultPieDataset();
		for (int i = 0, len = legend.size(); i < len; i++) {
			result.setValue(legend.get(i), percent.get(i));
		}
		return result;
	}

	public JFreeChartWrapper getChart() {
		return fchart;
	}

	/**
	 * Creates a chart
	 */

	private JFreeChart createChart(PieDataset dataset, String title) {

		chart = ChartFactory.createPieChart(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		plot.setBackgroundPaint(Color.white);
		plot.setOutlinePaint(null);
		plot.setInteriorGap(0.03);
		plot.setMaximumLabelWidth(0.2);
		//plot.setLabelLinkStyle(PieLabelLinkStyle.CUBIC_CURVE);
		if (showPercentage) {
			plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}", NumberFormat
					.getNumberInstance(), NumberFormat.getPercentInstance()));
		} else {
			plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{1}", NumberFormat
					.getNumberInstance(), NumberFormat.getPercentInstance()));
		}
		//plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}={2}")); 
		//plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} = {1} ({2})")); 
		chart.getLegend().setFrame(BlockBorder.NONE);

		return chart;
	}
}
