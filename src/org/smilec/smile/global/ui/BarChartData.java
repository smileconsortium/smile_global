package org.smilec.smile.global.ui;

import java.util.ArrayList;

public class BarChartData {
	public String title;
	public String xlabel;
	public String ylabel;
	public ArrayList<String> legend;
	public ArrayList<String> name;
	public ArrayList<ArrayList<Number>> data;
	public int width;
	public int height;

	public BarChartData(String title, String xlabel, String ylabel, ArrayList<String> legend,
			ArrayList<String> name, ArrayList<ArrayList<Number>> data, int width, int height) {
		this.title = title;
		this.xlabel = xlabel;
		this.ylabel = ylabel;
		this.legend = legend;
		this.name = name;
		this.data = data;
		this.width = width;
		this.height = height;
	}

	public BarChartData() {
		legend = new ArrayList<String>();
		name = new ArrayList<String>();
		data = new ArrayList<ArrayList<Number>>();
	}
}
