package org.smilec.smile.global.ui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.Question.ContentTypeEnum;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.logic.QuestionDAO;
import org.smilec.smile.global.hibernate.logic.QuizDAO;
import org.smilec.smile.global.media.MediaManager;
import org.smilec.smile.global.p.Groups.QuizViewLogic;
import org.smilec.smile.global.p.Public.PublicViewLogic;
import org.smilec.smile.global.vars.Css;
import org.smilec.smile.global.vars.Size;
import org.smilec.smile.global.vars.SystemVars;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Video;

public class CreateQuestionLogic implements ClickListener, Property.ValueChangeListener, Upload.SucceededListener, Upload.FailedListener, Upload.ProgressListener, Upload.Receiver {
	private static final String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	private SmileApplication smile;
	private PublicViewLogic pv_logic;
	private QuizViewLogic qv_logic;
	private CreateQuestionBox box;
	private AnswerField lastToggled = null;

	private String UPLOAD_TEXT, YOUTUBE_TEXT, NO_MEDIA_TEXT;

	private boolean editMode = false;
	private Question editQuestion; //question to be edited
	private List<QuestionAnswer> loadedAnswers;

	File file;
	private String fileName;
	Quiz quiz;
	CreateQuestionContainer container = null;
	private ContentTypeEnum contentChoice = null;

	private ArrayList<AnswerField> answerFields;

	public CreateQuestionLogic(CreateQuestionBox box, Quiz quiz) {
		this.box = box;
		this.quiz = quiz;
		smile = AppData.getApplication();
		//init
		box.getCreateAnswerButton().setEnabled(true);

		UPLOAD_TEXT = AppData.getMessage("uploadText");
		YOUTUBE_TEXT = AppData.getMessage("youtubeText");
		NO_MEDIA_TEXT = AppData.getMessage("noMedia");

		//set up mediaType options group
		OptionGroup mediaSelect = box.getMediaType();
		mediaSelect.addItem(UPLOAD_TEXT);
		mediaSelect.addItem(YOUTUBE_TEXT);
		mediaSelect.addItem(NO_MEDIA_TEXT);
		mediaSelect.setNullSelectionAllowed(false); // user can not 'unselect'       
		mediaSelect.setImmediate(true); // send the change to the server at once
		mediaSelect.addListener(this); // react when the user selects something
		mediaSelect.select(NO_MEDIA_TEXT); // select this by default
		//create listeners
		answerFields = new ArrayList<AnswerField>();
		box.getCreateAnswerButton().addListener((ClickListener) this);
		box.getResetButton().addListener((ClickListener) this);
		box.getSubmitButton().addListener((ClickListener) this);
		box.getMediaEmbedButton().addListener((ClickListener) this);

		// Listen for events regarding the success of upload.
		box.getMediaUpload().addListener((Upload.SucceededListener) this);
		box.getMediaUpload().addListener((Upload.FailedListener) this);
		box.getMediaUpload().addListener((Upload.ProgressListener) this);
		box.getMediaUpload().setReceiver(this);

		resetFields();
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	public void setContainer(CreateQuestionContainer container) {
		this.container = container;
		//works like a listener, if container is set, then is called when question is
		//created or edited
	}

	private AnswerField addAnswerField(int num) {
		if (answerFields.size() < SystemVars.maxAnswers) {
			AnswerField answerField = new AnswerField(num);
			answerFields.add(answerField);
			//add above reset button
			int index = box.getCreateQuestionLayout().getComponentIndex(box.getMediaDivider());
			box.getCreateQuestionLayout().addComponent(answerField.horizontalLayout, index);
			//listeners
			answerField.toggleButton.addListener((ClickListener) this);
			answerField.deleteButton.addListener((ClickListener) this);
			//enable all delete buttons
			if (answerFields.size() > SystemVars.minAnswers) {
				for (int i = 0, l = answerFields.size(); i < l; i++) {
					answerFields.get(i).deleteButton.setEnabled(true);
				}
			}
			if (answerFields.size() == SystemVars.maxAnswers) {
				box.getCreateAnswerButton().setEnabled(false);
			}
			//update toggle letters
			updateToggleLetters();
			return answerField;
		} else {
			//show maximum answers
			AppData.getApplication().showNotification(AppData.getMessage("Oops"),
					AppData.getFormattedMsg("maxAnswers", SystemVars.maxAnswers), //
					SmileApplication.NOTIFICATION_ERROR);
			return null;
		}
	}

	private void removeAnswerField(AnswerField af, Boolean override) {
		if (answerFields.size() > SystemVars.minAnswers || override) {
			answerFields.remove(af);
			af.toggleButton.removeListener((ClickListener) this);
			af.deleteButton.removeListener((ClickListener) this);
			box.getCreateQuestionLayout().removeComponent(af.horizontalLayout);
			af = null;
			//disable all remove buttons once below certain amount of questions
			if (answerFields.size() == SystemVars.minAnswers) {
				for (int i = 0, l = answerFields.size(); i < l; i++) {
					answerFields.get(i).deleteButton.setEnabled(false);
				}
			}
			if (answerFields.size() < SystemVars.maxAnswers) {
				box.getCreateAnswerButton().setEnabled(true);
			}
			//update toggle letters
			updateToggleLetters();
			//if no incorrect answer, then set all answer choices to false
			int numToggled = 0;
			int size = answerFields.size();
			for (int i = 0, l = size; i < l; i++) {
				if (answerFields.get(i).isCorrect)
					numToggled++;
			}
			if (numToggled >= size) {
				for (int i = 0, l = size; i < l; i++) {
					if (answerFields.get(i).isCorrect)
						answerFields.get(i).toggle();
				}
			}
		} else {
			//show minimum answers
			AppData.getApplication().showNotification(AppData.getMessage("Oops"), //minAnswers
					AppData.getFormattedMsg("minAnswers", SystemVars.minAnswers), //
					SmileApplication.NOTIFICATION_WARNING);
		}
	}

	private void updateToggleLetters() {
		for (int i = 0, l = answerFields.size(); i < l; i++) {
			Button b = answerFields.get(i).toggleButton;
			b.setCaption(getLetter(i));
			b.requestRepaint();
		}
	}

	private String getLetter(int i) {
		return letters.substring(i, i + 1);
	}

	public class AnswerField implements Serializable {
		public HorizontalLayout horizontalLayout;
		public Button toggleButton;
		public TextField answerField;
		public Button deleteButton;
		public int answerNum;
		public boolean isCorrect = false;

		public AnswerField(int answerNumber) {
			this.answerNum = answerNumber;
			String answerCaption = getLetter(answerNumber);
			build(answerCaption);
			//store reference to answerField class instance in data on toggleButton and deleteButton
			toggleButton.setData(this);
			deleteButton.setData(this);
			answerField.setMaxLength(Size.maxCharsQuestionAnswerText);
		}

		public boolean toggle() {
			if (isCorrect) {
				toggleButton.removeStyleName(Css.defaultRed);
				toggleButton.requestRepaint();
				isCorrect = false;
			} else {
				toggleButton.addStyleName(Css.defaultRed);
				toggleButton.requestRepaint();
				isCorrect = true;
			}
			return isCorrect;
		}

		public void build(String caption) {
			//layout
			horizontalLayout = new HorizontalLayout();
			horizontalLayout.setImmediate(false);
			horizontalLayout.setWidth("100.0%");
			horizontalLayout.setHeight("-1px");
			horizontalLayout.setMargin(false);

			//Toggle Button
			toggleButton = new Button();
			toggleButton.setStyleName("leftButton");
			toggleButton.setCaption(caption);
			toggleButton.setImmediate(true);
			toggleButton.setWidth("33px");
			toggleButton.setHeight("35px");
			horizontalLayout.addComponent(toggleButton);
			horizontalLayout.setComponentAlignment(toggleButton, new Alignment(6));

			// answerA
			answerField = new TextField();
			answerField.setStyleName("textCenter");
			answerField.setImmediate(false);
			answerField.setWidth("100.0%");
			answerField.setHeight("35px");
			answerField.setTabIndex(2);
			answerField.setSecret(false);
			answerField.setInputPrompt(AppData.getMessage("enterAnswer")); //
			horizontalLayout.addComponent(answerField);
			horizontalLayout.setExpandRatio(answerField, 1.0f);

			// aDeleteButton
			deleteButton = new Button();
			deleteButton.setStyleName("rightButton");
			deleteButton.setCaption("-");
			deleteButton.setImmediate(true);
			deleteButton.setWidth("35px");
			deleteButton.setHeight("-1px");
			horizontalLayout.addComponent(deleteButton);
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == box.getCreateAnswerButton()) {
			addAnswerField(answerFields.size());
		} else if (event.getButton() == box.getResetButton()) {
			if (container != null) {
				if (editMode) {
					container.questionEditCancel();
				}
			}
			resetFields();
		} else if (event.getButton() == box.getSubmitButton()) {
			submitQuestion();
		} else if (event.getButton() == box.getMediaEmbedButton()) {
			//embed youtube link
			String youtube = box.getMediaEmbedTF().getValue().toString();
			if (youtube != null && youtube.trim().length() > 0) {
				//youtube = youtube.replace("watch?v=", "v/");
				int idx1 = youtube.indexOf("v=");
				String str = youtube.substring(idx1);
				int idx2 = str.indexOf("&");
				if (idx2 != -1) {
					str = str.substring(2, idx2);
				} else {
					str = str.substring(2);
				}

				youtube = "http://www.youtube.com/v/" + str;

				Embedded e = new Embedded(null, new ExternalResource(youtube));
				e.setAlternateText(AppData.getMessage("youtubeAltText")); //
				e.setMimeType("application/x-shockwave-flash");
				e.setParameter("allowFullScreen", "true");
				e.setWidth("320px");
				e.setHeight("265px");
				Panel imagePanel = box.getMediaPanel();
				imagePanel.removeAllComponents();
				imagePanel.addComponent(e);
				imagePanel.setCaption(AppData.getFormattedMsg("YoutubeVideo", youtube));

				//set content type for saving question
				contentChoice = ContentTypeEnum.document;
			}
		} else {
			//if either a toggle button or a delete button
			AnswerField af = (AnswerField) event.getButton().getData();
			if (event.getButton() == af.deleteButton) {
				removeAnswerField(af, false);
			} else if (event.getButton() == af.toggleButton) {
				toggleAnswerField(af, false);
			}
		}
	}

	private void toggleAnswerField(AnswerField af, boolean override) {
		af.toggle();
		if (!override) {
			//don't allow all answers to be marked as correct
			int numToggled = 0;
			int size = answerFields.size();
			for (int i = 0, l = size; i < l; i++) {
				if (answerFields.get(i).isCorrect)
					numToggled++;
			}
			if (numToggled >= size) {
				lastToggled.toggle();
			}
		}
		lastToggled = af;
	}

	public void viewQuestion(Question q) {
		editMode = true;
		editQuestion = q;

		box.getCreatePanel().setCaption(AppData.getMessage("ViewQuestion"));
		//change reset and submit buttons to save changes and cancel
		box.getResetButton().setCaption(AppData.getMessage("Done"));
		box.getSubmitButton().setVisible(false);
		loadQuestion(q);
		/*
		box.getQuestionField().setEnabled(false);
		box.getCreateAnswerButton().setEnabled(false);
		for (int i = 0, l = answerFields.size(); i < l; i++) {
			answerFields.get(i).answerField.setEnabled(false);
			answerFields.get(i).deleteButton.setEnabled(false);
			answerFields.get(i).toggleButton.setEnabled(false);
		}
		box.getTagsTF().setEnabled(false);
		box.getMediaType().setEnabled(false);
		box.getMediaUpload().setEnabled(false);
		box.getMediaEmbedTF().setEnabled(false);
		*/
	}

	public void editQuestion(Question q) {
		editMode = true;
		editQuestion = q;

		box.getCreatePanel().setCaption(AppData.getMessage("EditQuestion"));
		//change reset and submit buttons to save changes and cancel
		box.getResetButton().setCaption(AppData.getMessage("CancelChanges"));
		box.getSubmitButton().setCaption(AppData.getMessage("SaveChanges"));
		loadQuestion(q);
	}

	private void loadQuestion(Question q) {
		//cancel upload
		box.getMediaUpload().interruptUpload();

		lastToggled = null;
		box.getQuestionField().setValue(q.getQuestionText());
		//remove answerfields
		while (answerFields.size() > 0) {
			removeAnswerField(answerFields.get(0), true);
		}
		//add answerfields
		//store loaded answers so when updating question, don't delete answers unnecessarily 
		loadedAnswers = q.getQuestionAnswerList();
		Iterator<QuestionAnswer> it = loadedAnswers.iterator();
		int i = 0;
		while (it.hasNext()) {
			QuestionAnswer qa = it.next();
			AnswerField af = addAnswerField(i);
			af.answerField.setValue(qa.getAnswerText());
			if (qa.getIsCorrect()) {
				toggleAnswerField(af, true);
			}
			i++;
		}
		//load media
		fileName = q.getMedia_url();
		ContentTypeEnum type = q.getContentType();
		if (type == ContentTypeEnum.document) {
			//if youtube, select embed youtubelink
			box.getMediaType().select(YOUTUBE_TEXT);
			box.getMediaUpload().setVisible(false);
			box.getMediaEmbedHL().setVisible(true);
			box.getMediaEmbedTF().setValue(q.getMedia_url());
		} else if ((type == ContentTypeEnum.audio || type == ContentTypeEnum.video || type == ContentTypeEnum.image)
				&& fileName.length() > 0) {
			box.getMediaType().select(UPLOAD_TEXT);
			box.getMediaUpload().setVisible(true);
			box.getMediaEmbedHL().setVisible(false);
		} else {
			box.getMediaType().select(NO_MEDIA_TEXT);
			box.getMediaUpload().setVisible(false);
			box.getMediaEmbedHL().setVisible(false);
		}

		//load media
		MediaManager mm = new MediaManager();
		mm.setCreateMedia(box.getMediaPanel(), q);

		//load tags
		box.getTagsTF().setValue(q.getTagText());
	}

	private void resetFields() {
		box.getQuestionField().setValue("");
		box.getCreatePanel().setCaption(AppData.getMessage("CreateQuestion"));
		box.getSubmitButton().setCaption(AppData.getMessage("Submit"));
		if (!box.getSubmitButton().isVisible()) {
			box.getSubmitButton().setVisible(true);
			/*
			box.getQuestionField().setEnabled(true);
			box.getCreateAnswerButton().setEnabled(true);
			box.getTagsTF().setEnabled(true);
			box.getMediaType().setEnabled(true);
			box.getMediaUpload().setEnabled(true);
			box.getMediaEmbedTF().setEnabled(true);
			*/
		}
		box.getResetButton().setCaption(AppData.getMessage("Reset"));
		editMode = false;
		editQuestion = null;
		contentChoice = null;
		lastToggled = null;
		box.getMediaPanel().removeAllComponents();
		box.getMediaPanel().setCaption("");
		box.getMediaUpload().setVisible(false);
		box.getMediaEmbedHL().setVisible(false);
		box.getMediaType().select(NO_MEDIA_TEXT); // select this by default
		box.getMediaEmbedTF().setValue("");
		box.getTagsTF().setValue("");
		//cancel upload
		box.getMediaUpload().interruptUpload();
		//remove all answerFields
		while (answerFields.size() > 0) {
			removeAnswerField(answerFields.get(0), true);
		}
		//start with 4 answerFields
		for (int i = 0; i < SystemVars.defaultAnswers; i++) {
			addAnswerField(i);
		}
		box.getMainLayout().requestRepaintAll();
		fileName = "";
	}

	private void submitQuestion() {
		String questionText = (String) box.getQuestionField().getValue();
		//make sure question is not null

		if (questionText.equals("") || questionText == null) {
			AppData.getApplication().showNotification(AppData.getMessage("Oops"),
					AppData.getMessage("enterQuestion"), //
					SmileApplication.NOTIFICATION_WARNING);
		} else {
			//make sure question Answers are not null

			boolean nullAnswers = false;
			boolean correctAnswer = false;

			for (int i = 0; i < answerFields.size(); i++) {
				AnswerField af = answerFields.get(i);
				String answer = (String) af.answerField.getValue();
				if (answer.equals("") || answer == null) {
					nullAnswers = true;
				}
				if (af.isCorrect) {
					correctAnswer = true;
				}
			}
			if (nullAnswers == true) {
				//if answer text is null
				AppData.getApplication().showNotification(AppData.getMessage("Oops"),
						AppData.getMessage("allAnswers"), //
						SmileApplication.NOTIFICATION_WARNING);
			} else if (correctAnswer == false) {
				// if no correct answer
				AppData.getApplication().showNotification(AppData.getMessage("Oops"),
						AppData.getMessage("answerCorrect"), //
						SmileApplication.NOTIFICATION_WARNING);
			} else if (verifyMediaURL()) {

				//Question submit successfully

				Question q;
				if (editMode) {
					q = editQuestion;
				} else {
					q = new Question();
				}

				// tags
				String tagsText = (String) box.getTagsTF().getValue();

				if (tagsText == null)
					tagsText = "";

				tagsText = tagsText.trim();
				q.setTagText(tagsText);
				////

				q.setQuestionText(questionText);
				Calendar cal = Calendar.getInstance();
				q.setCreatedOn(cal.getTime());
				if (quiz != null) {
					//TODO: save two questions for quiz if public, one set to public, one set to private,
					//to allow for unique set of UserQuestionInteractions
					q.setIsPublic(0);
				} else {
					q.setIsPublic(1);
				}
				if (!editMode) {
					q.setUserId(((User) box.getApplication().getUser()).getId());
					//create question in sql database and get its new id
					QuestionDAO.createQuestion(q);
					//add to quiz if creating question in quiz
					if (quiz != null) {
						QuizDAO.addQuestionToQuiz(quiz.getId(), q.getId());
					}
				}
				//save media url, called after question is created so question id will be set
				MediaManager mm = new MediaManager();
				if (contentChoice == ContentTypeEnum.document) {
					String youtube = box.getMediaEmbedTF().getValue().toString();
					mm.saveEmbeddedQuestionMedia(q, youtube);
				} else if (contentChoice != null) {
					mm.saveUploadedQuestionMedia(q, file, fileName);
				} else {
					mm.saveNoQuestionMedia(q);
				}
				//update Question
				QuestionDAO.updateQuestion(q);
				//if edit Mode, remove deleted question answers and update others
				if (editMode) {
					//cycle through answer fields, create,update, or delete questionAnswer objects
					Integer numAnswers = answerFields.size();
					for (int i = 0; i < numAnswers; i++) {
						AnswerField af = answerFields.get(i);
						QuestionAnswer answer;
						if (loadedAnswers.size() > i) {
							//if answer exists in same position, update it
							answer = loadedAnswers.get(i);
							answer.setAnswerText(((String) af.answerField.getValue()));
							answer.setQuestion(q);
							answer.setIsCorrect(af.isCorrect == true ? 1 : 0);
							QuestionDAO.updateQuestionAnswer(answer);
						} else {
							//if added an additional answer, create a new answer
							answer = new QuestionAnswer();
							answer.setAnswerText(((String) af.answerField.getValue()));
							answer.setQuestion(q);
							answer.setNumResponses(0);
							answer.setIsCorrect(af.isCorrect == true ? 1 : 0);
							QuestionDAO.createQuestionAnswer(answer);
						}
					}
					if (loadedAnswers.size() > numAnswers) {
						//delete answers
						for (int i = loadedAnswers.size() - numAnswers; i > 0; i--) {
							QuestionAnswer answer = loadedAnswers.get(loadedAnswers.size() - i);
							QuestionDAO.deleteQuestionAnswer(answer);
						}
					}
				} else {

					//create QuestionAnswer obj's
					Integer numAnswers = answerFields.size();
					for (int i = 0; i < numAnswers; i++) {
						AnswerField af = answerFields.get(i);
						QuestionAnswer answer = new QuestionAnswer();
						answer.setAnswerText(((String) af.answerField.getValue()));
						answer.setQuestion(q);
						answer.setNumResponses(0);
						answer.setIsCorrect(af.isCorrect == true ? 1 : 0);
						QuestionDAO.createQuestionAnswer(answer);
					}
				}
				//Display confirmation
				String title = AppData.getMessage("Success");
				String message;
				if (editMode) {
					message = AppData.getMessage("QuestionUpdated"); //
				} else {
					message = AppData.getMessage("questionPosted"); //
				}
				AppData.getApplication().showNotification(title, message,
						SmileApplication.NOTIFICATION_SUCCESS);
				//notify container
				if (container != null) {
					if (editMode) {
						container.questionEdited();
					} else {
						container.questionCreated();
					}
				}
				//reset Question
				resetFields();
			}
		}
	}

	private boolean verifyMediaURL() {
		OptionGroup mediaSelect = box.getMediaType();
		String media_option = (String) mediaSelect.getValue();

		if (media_option.equals(UPLOAD_TEXT)) {
			String name = fileName;
			if (name == null || name.length() == 0) {
				AppData.getApplication().showNotification(AppData.getMessage("Oops"),
						AppData.getMessage("uploadFirst"), //
						SmileApplication.NOTIFICATION_WARNING);
				return false;
			} else
				return true;
		} else if (media_option.equals(YOUTUBE_TEXT)) {
			String youtube = box.getMediaEmbedTF().getValue().toString();
			if (youtube == null || youtube.trim().length() == 0) {
				AppData.getApplication().showNotification(AppData.getMessage("Oops"),
						AppData.getMessage("validYoutube"), //
						SmileApplication.NOTIFICATION_WARNING);
				return false;
			} else
				return true;
		} else if (media_option.equals(NO_MEDIA_TEXT)) {
			return true;
		}

		return true;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		//media Type options group
		String selected = (String) box.getMediaType().getValue();
		if (selected == UPLOAD_TEXT) {
			box.getMediaUpload().setVisible(true);
			box.getMediaEmbedHL().setVisible(false);
			box.getMediaPanel().setVisible(true);
		} else if (selected == YOUTUBE_TEXT) {
			box.getMediaUpload().setVisible(false);
			box.getMediaEmbedHL().setVisible(true);
			box.getMediaPanel().setVisible(true);
		} else if (selected == NO_MEDIA_TEXT) {
			box.getMediaUpload().setVisible(false);
			box.getMediaEmbedHL().setVisible(false);
			box.getMediaPanel().setVisible(false);
		}
	}

	@Override
	public void updateProgress(long readBytes, long contentLength) {

	}

	// Callback method to begin receiving the upload.
	@Override
	public OutputStream receiveUpload(String filename, String MIMEType) {
		FileOutputStream fos = null; // Output stream to write to
		MediaManager mm = new MediaManager();
		String user_url = mm.getUser_url();
		File dir = new File(user_url);
		if (!dir.exists())
			dir.mkdirs();
		file = new File(user_url + filename);
		try {
			// Open the file for writing.
			fos = new FileOutputStream(file);
		} catch (final java.io.FileNotFoundException e) {
			// Error while opening the file. Not reported here.
			e.printStackTrace();
			return null;
		}

		return fos; // Return the output stream to write to
	}

	// This is called if the upload is finished.
	@Override
	public void uploadSucceeded(Upload.SucceededEvent event) {
		MediaManager mm = new MediaManager();
		String user_url = mm.getUser_url();
		String media_url = mm.getMedia_url();
		String tmp = mm.getTmp();
		// Log the upload on screen.
		/*verticalLayout_1.addComponent(new Label("File " + event.getFilename()
		        + " of type '" + event.getMIMEType()
		        + "' uploaded."));*/
		Panel mediaPanel = box.getMediaPanel();
		try {
			String name = file.getCanonicalPath().toLowerCase();
			String ext = name.substring(name.lastIndexOf('.'));

			if (name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".jpeg")
					|| name.endsWith(".gif")) {
				// Display the uploaded file in the image panel.	            
				mediaPanel.setCaption(event.getFilename());
				File newfile = new File(user_url + tmp + ext);
				file.renameTo(newfile);
				file = newfile;
				BufferedImage img1 = loadImage(file.toString());
				//img1 = resize(img1, 100, 100);	 
				//File tmpFile = new File(user_url+tmpImage+ext);
				//saveImage(img1, tmpFile.toString());

				mediaPanel.removeAllComponents();
				final FileResource imageResource = new FileResource(file, smile);
				Embedded image = new Embedded("", imageResource);
				image.setType(Embedded.TYPE_IMAGE);
				MediaManager.setImageSize(img1, 100, 100, image);
				mediaPanel.addComponent(image);
				mediaPanel.setCaption(AppData.getFormattedMsg("UploadedImage", event.getFilename()));
				//set content type for saving question
				contentChoice = ContentTypeEnum.image;
				fileName = event.getFilename();
			} else if (name.endsWith(".mp3")) {
				// Display the uploaded file in the image panel.	            
				mediaPanel.setCaption(event.getFilename());
				File newfile = new File(user_url + tmp + ext);
				file.renameTo(newfile);
				file = newfile;

				mediaPanel.removeAllComponents();
				final Audio a = new Audio("audio");
				//a.setSources( new ExternalResource( "http://jonatan.virtuallypreinstalled.com/media/audio.mp3"  ) );
				//a.setSources( new FileResource(file, smile) );
				a.setSources(new ExternalResource(media_url + "users/" + smile.getUser().getId() + "/"
						+ tmp + ext));
				a.setAltText(AppData.getMessage("audioAltText"));
				mediaPanel.addComponent(a);
				mediaPanel.setCaption(AppData.getFormattedMsg("UploadedAudio", event.getFilename()));
				//set content type for saving question
				contentChoice = ContentTypeEnum.audio;
				fileName = event.getFilename();
			} else if (name.endsWith(".mp4") || name.endsWith(".mov")) {
				// Display the uploaded file in the image panel.	            
				mediaPanel.setCaption(event.getFilename());
				File newfile = new File(user_url + tmp + ext);
				file.renameTo(newfile);
				file = newfile;
				mediaPanel.removeAllComponents();
				final Video v = new Video("video");
				//v.setSources( new ExternalResource( "http://localhost:8080/uploads/users/12/uploadedMedia.mp4"  ));
				//v.setSources( new FileResource(file, smile) );
				v.setSources(new ExternalResource(media_url + "users/" + smile.getUser().getId() + "/"
						+ tmp + ext));
				v.setWidth("540px");
				v.setHeight("300px");
				v.setAltText(AppData.getMessage("videoAltText"));
				mediaPanel.addComponent(v);
				mediaPanel.setCaption(AppData.getFormattedMsg("uploadVideo", event.getFilename()));
				//set content type for saving question
				contentChoice = ContentTypeEnum.video;
				fileName = event.getFilename();
			} else {
				/*smile.getMainWindow().showNotification(
						"Please select a media file type png/jpg/gif/mp3/mp4. You chose: " + name);*/
				String msg =
						AppData.getFormattedMsg("allowedExt", name.substring(name.lastIndexOf(".")));
				AppData.getApplication().showNotification(AppData.getMessage("Oops"), msg,
						SmileApplication.NOTIFICATION_ERROR);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// This is called if the upload fails.
	@Override
	public void uploadFailed(Upload.FailedEvent event) {
		// Log the failure on screen.
		/*verticalLayout_1.addComponent(new Label("Uploading "
		        + event.getFilename() + " of type '"
		        + event.getMIMEType() + "' failed."));*/
		String msg =
				AppData.getFormattedMsg("uploadFailed1",
						new Object[] { event.getFilename(), event.getMIMEType() }); //
		AppData.getApplication().showNotification("", msg, SmileApplication.NOTIFICATION_ERROR);
	}

	public BufferedImage loadImage(String ref) {
		BufferedImage bimg = null;
		try {

			bimg = ImageIO.read(new File(ref));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bimg;
	}

	public void setPv_logic(PublicViewLogic pv_logic) {
		this.pv_logic = pv_logic;
	}

	public void setQV_logic(QuizViewLogic qv_logic) {
		this.qv_logic = qv_logic;
	}
}
