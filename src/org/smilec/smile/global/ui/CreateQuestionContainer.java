package org.smilec.smile.global.ui;

public interface CreateQuestionContainer {
	public void questionCreated();

	public void questionEdited();

	void questionEditCancel();

}
