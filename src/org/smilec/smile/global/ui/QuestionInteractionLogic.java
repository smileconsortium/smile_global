package org.smilec.smile.global.ui;

import java.io.Serializable;

import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;
import org.smilec.smile.global.hibernate.logic.HibernateDAO;
import org.smilec.smile.global.hibernate.logic.QuestionDAO;

public class QuestionInteractionLogic implements Serializable {
	public static void updateQuestionInteractionStats(UserQuestionInteraction ui) {
		//load question and answer objects fresh to ensure latest changes are reflected
		Question q = QuestionDAO.getQuestionById(ui.getQuestion().getId());
		QuestionAnswer qa = QuestionDAO.getQuestionAnswerById(ui.getUserAnswer().getId());
		//update question
		Integer rating = ui.getRating();
		int secondsComplete = ui.getSecComplete();
		int num = q.getUniqueAnswers();
		int numRatings = q.getUniqueRatings();
		float avgCorrect = q.getAvgCorrect();
		float avgRating = q.getAvgRating();
		float avgSecComplete = q.getAvgSecComplete();
		int isCorrect = qa.getIsCorrect() ? 1 : 0;

		q.setAvgCorrect(((avgCorrect * num) + isCorrect) / (num + 1));
		if (rating != null) {
			q.setAvgRating(((avgRating * numRatings) + rating) / (numRatings + 1));
			q.setUniqueRatings(numRatings + 1);
		}
		q.setAvgSecComplete(((avgSecComplete * num) + secondsComplete) / (num + 1));
		q.setUniqueAnswers(num + 1);
		//update answer
		qa.incrementNumResponses();
		//store updated Question and QuestionAnswer objects to database
		HibernateDAO.update(q);
		HibernateDAO.update(qa);
	}
}
