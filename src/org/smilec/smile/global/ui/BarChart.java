package org.smilec.smile.global.ui;

import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.terminal.Sizeable;

public class BarChart {
	JFreeChartWrapper fchart;
	JFreeChart chart;
	BarChartData bcData;

	PlotOrientation orientation = PlotOrientation.HORIZONTAL;
	boolean integerTicks = true;
	boolean includeLegend = true;

	public BarChart(BarChartData bcData) {
		buildChart(bcData);
	}

	public BarChart(BarChartData bcData, boolean isHorizontal) {
		if (isHorizontal) {
			orientation = PlotOrientation.HORIZONTAL;
		} else {
			orientation = PlotOrientation.VERTICAL;
		}
		buildChart(bcData);
	}

	public BarChart(BarChartData bcData, boolean isHorizontal, boolean integerTicks) {
		if (isHorizontal) {
			orientation = PlotOrientation.HORIZONTAL;
		} else {
			orientation = PlotOrientation.VERTICAL;
		}
		this.integerTicks = integerTicks;
		buildChart(bcData);
	}

	public BarChart(BarChartData bcData, boolean isHorizontal, boolean integerTicks,
			boolean includeLegend) {
		if (isHorizontal) {
			orientation = PlotOrientation.HORIZONTAL;
		} else {
			orientation = PlotOrientation.VERTICAL;
		}
		this.integerTicks = integerTicks;
		this.includeLegend = includeLegend;
		buildChart(bcData);
	}

	public void buildChart(BarChartData bcData) {
		this.bcData = bcData;
		CategoryDataset dataset = createDataset(bcData);
		chart = createChart(dataset);

		/*final StandardChartTheme chartTheme =
				(StandardChartTheme) org.jfree.chart.StandardChartTheme.createJFreeTheme();

		try {
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		Font font;
		try {
			font = loadFont();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			font = new Font("SansSerif", Font.PLAIN, 14);
		}
		final Font largeFont = new Font("SansSerif", Font.PLAIN, 16);
		final Font extraLargeFont = new Font("iso8859-11", Font.PLAIN, 18);
		chartTheme.setExtraLargeFont(font.deriveFont(18f));
		chartTheme.setLargeFont(font);
		chartTheme.setRegularFont(font);
		chartTheme.setSmallFont(font);
		chartTheme.apply(chart);*/

		fchart = new JFreeChartWrapper(chart);
		if (bcData.width > 0 && bcData.height > 0) {
			fchart.setHeight(bcData.height, Sizeable.UNITS_PIXELS);
			fchart.setWidth(bcData.width, Sizeable.UNITS_PIXELS);
		}
		fchart = new JFreeChartWrapper(chart);
		if (bcData.width > 0 && bcData.height > 0) {
			fchart.setHeight(bcData.height, Sizeable.UNITS_PIXELS);
			fchart.setWidth(bcData.width, Sizeable.UNITS_PIXELS);
		}
	}

	public JFreeChartWrapper getChart() {
		return fchart;
	}

	private CategoryDataset createDataset(BarChartData bcData) {
		// Prepare the data set

		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		ArrayList<String> legend = bcData.legend;
		ArrayList<String> name = bcData.name;

		for (int i = 0, len = bcData.legend.size(); i < len; i++) {
			ArrayList<Number> list = bcData.data.get(i);

			for (int j = 0, size = bcData.name.size(); j < size; j++) {
				dataset.addValue(list.get(j), legend.get(i), name.get(j));
			}

		}

		return dataset;
	}

	private JFreeChart createChart(final CategoryDataset dataset) {

		JFreeChart chart = ChartFactory.createBarChart(bcData.title, // chart title
				bcData.ylabel, // domain axis label
				bcData.xlabel, // range axis label
				dataset, // data
				orientation, // orientation
				includeLegend, // include legend
				true, false);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...

		// set the background color for the chart...
		// chart.setBackgroundPaint(Color.lightGray);

		// get a reference to the plot for further customisation...
		final CategoryPlot plot = chart.getCategoryPlot();
		plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);

		if (integerTicks) {
			// change the auto tick unit selection to integer units only...
			final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
			//rangeAxis.setRange(0.0, 100.0);
			rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		}
		// OPTIONAL CUSTOMISATION COMPLETED.

		return chart;

	}
}
