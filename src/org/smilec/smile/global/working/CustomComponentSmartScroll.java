package org.smilec.smile.global.working;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Layout;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.js.JSLib;

public class CustomComponentSmartScroll extends CustomComponent {
	private int minPanelHeight = -1;
	private AbsoluteLayout mainLayout;

	public CustomComponentSmartScroll(int minHeight) {
		this.minPanelHeight = minHeight;
		setMinPanelHeight(minHeight);
	}

	public int getMinPanelHeight() {
		return minPanelHeight;
	}

	public void setMinPanelHeight(int height) {
		minPanelHeight = height;
		//call resize function
		if (minPanelHeight > 0) {
			AppData.getApplication().getMainWindow()
					.executeJavaScript(JSLib.setMinContentHeight(height));
		}
	}

	public void setMinPanelHeight(int height, String jQueryHandle, int additionalHeight) {
		minPanelHeight = height;
		//call resize function
		AppData.getApplication().getMainWindow()
				.executeJavaScript(JSLib.setMinContentHeight(height, jQueryHandle, additionalHeight));
		if (minPanelHeight > 0) {
		}
	}

	public void setMinPanelHeight() {
		//call resize function
		if (minPanelHeight > 0) {
			AppData.getApplication().getMainWindow()
					.executeJavaScript(JSLib.setMinContentHeight(minPanelHeight));
		}
	}

	public Layout getMainLayout() {
		return mainLayout;
	}
}
