package org.smilec.smile.global.working;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.vaadin.alump.fancylayouts.FancyCssLayout;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Component;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.ui.QuestionDisplay;

public class QuestionListFancyCss extends FancyCssLayout implements LayoutClickListener {

	@AutoGenerated
	private FancyCssLayout mainLayout;

	public static final int ORDER_CODE_DATE_DESC = 0;
	public static final int ORDER_CODE_DATE_ASC = 1;
	public static final int ORDER_CODE_RATING_DESC = 2;
	public static final int ORDER_CODE_POPULARITY_DESC = 3;
	public static final int ORDER_CODE_DIFFICULTY_ASC = 4;
	public static final int ORDER_CODE_DIFFICULTY_DESC = 5;

	//Question storage	//DAO vars
	//More questions are loaded than are displayed
	private List<Question> questionBuffer; //stores questions loaded per query
	private List<Question> questions; //stores questions currently seen
	private int maxPages;
	private int questionsPerPage = 10; //can later be set by user
	//Query Fragment Vars
	private String query = "";
	private int pageNum = 1;
	private int orderCode = ORDER_CODE_DATE_DESC;
	//Query vars
	private int querySize;

	//option vars
	public boolean showAnswered = true; //if true, shows questions that have been answered
	public boolean showOwn = true; //show questions that you created
	public boolean showCorrect = true; //show whether you got question correct

	//Strings for rating
	private static Map<Integer, String> starValueCaptions = new HashMap<Integer, String>(5);

	static {
		starValueCaptions.put(1, "Poor");
		starValueCaptions.put(2, "Needs Work");
		starValueCaptions.put(3, "OK");
		starValueCaptions.put(4, "Good");
		starValueCaptions.put(5, "Excellent");
	}

	/**
	 * The constructor should first build the main layout, set the
	 * composition root and then do any custom initialization.
	 *
	 * The constructor will not be automatically regenerated by the
	 * visual editor.
	 */
	public QuestionListFancyCss() {

	}

	public void drawQuestionSet(List<Question> questions) {
		removeAllComponents();
		int numQuestions = questionsPerPage;
		try {
			for (int i = 0; i < numQuestions; i++) {
				Question question = questions.get(i);
				addQuestionDisplay(question);
			}
		} catch (Throwable e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

	private void addQuestionDisplay(Question q) {
		QuestionDisplay qd = new QuestionDisplay(q, showCorrect);
		this.addComponent(qd);
		//set listener
		qd.getMainLayout().addListener((LayoutClickListener) this);
	}

	private void setPageNumber(int pageNum) {
		this.pageNum = pageNum;
		((SmileApplication) AppData.getApplication()).gotoPublic(query, pageNum, orderCode);
	}

	private void noResultsFound() {
		//HorizontalLayout hl = publicView.getHorizontalLayout();
		//hl.removeComponent(questionsCssLayout);

		//Label l = new Label();
		//l.setWidth("250px");
		//l.setValue("Sorry, no results found for '" + query + "'.");

		//hl.addComponent(l);
		//hl.setComponentAlignment(l, new Alignment(48));
		//hl.requestRepaintAll();
	}

	@Override
	public void layoutClick(LayoutClickEvent event) {
		Component c = event.getComponent().getParent();
		if (c instanceof QuestionDisplay) {
			//if question clicked
			QuestionDisplay qd = (QuestionDisplay) c;
			//selectQuestion(qd);
		} else if (c instanceof AbsoluteLayout) {
			//if search button clicked
			//setQuery((String) publicView.getSearchField().getValue());
		}
	}
}
