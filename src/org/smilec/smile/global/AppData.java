package org.smilec.smile.global;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import com.vaadin.Application;
import com.vaadin.service.ApplicationContext.TransactionListener;

/** Holds data for one user session. */
public class AppData implements TransactionListener, Serializable {
	private ResourceBundle bundle;
	private Locale locale; // Current locale
	private String userData; // Trivial data model for the user
	private Properties conf;
	private Application app; // For distinguishing between apps

	private static ThreadLocal<AppData> instance = new ThreadLocal<AppData>();

	public AppData(Application app) {
		this.app = app;

		// It's usable from now on in the current request
		instance.set(this);
	}

	@Override
	public void transactionStart(Application application, Object transactionData) {
		// Set this data instance of this application
		// as the one active in the current thread. 
		if (this.app == application)
			instance.set(this);
	}

	@Override
	public void transactionEnd(Application application, Object transactionData) {
		// Clear the reference to avoid potential problems
		if (this.app == application)
			instance.set(null);
	}

	public static void initLocale(Locale locale, String bundleName) {
		instance.get().locale = locale;
		instance.get().bundle = ResourceBundle.getBundle(bundleName, locale);
		System.out.println("Locale initialized: " + locale.toString());
	}

	public static Locale getLocale() {
		return instance.get().locale;
	}

	public static String getMessage(String msgId) {
		try {
			return instance.get().bundle.getString(msgId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return "";
		}
	}

	public static String getFormattedMsg(String patternId, Object[] args) {
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(getLocale());
		formatter.applyPattern(instance.get().bundle.getString(patternId));
		return formatter.format(args);
	}

	public static String getFormattedMsg(String patternId, String media_url) {
		return getFormattedMsg(patternId, new Object[] { media_url });
	}

	public static String getFormattedMsg(String patternId, Integer number) {
		return getFormattedMsg(patternId, new Object[] { number });
	}

	public static String getUserData() {
		return instance.get().userData;
	}

	public static void setUserData(String userData) {
		instance.get().userData = userData;
	}

	public static SmileApplication getApplication() {
		return (SmileApplication) instance.get().app;
	}

	public static Properties getProperties() {
		return instance.get().conf;
	}

	public void loadProperties() {
		conf = new Properties();

		try {
			URL url = getClass().getClassLoader().getResource("resources/conf.properties");
			FileInputStream fis = new FileInputStream(url.getPath());
			conf.load(fis);
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}