package org.smilec.smile.global.hibernate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

//TODO: UniqueConstraint doesn't seem to be working
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "question_id", "user_id" }) })
public class UserQuestionInteraction implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
	//formerly LAZY, but Hibernate error
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@ManyToOne(fetch = FetchType.EAGER)
	//formerly LAZY, but Hibernate error
	@JoinColumn(name = "question_id", nullable = false)
	private Question question;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userAnswer_id", nullable = false)
	private QuestionAnswer userAnswer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdOn")
	private Date createdOn;

	@Column(name = "rating")
	private Integer rating;

	@Column(name = "secComplete")
	private Integer secComplete;

	public QuestionAnswer getUserAnswer() {
		return userAnswer;
	}

	public void setUserAnswer(QuestionAnswer userAnswer) {
		this.userAnswer = userAnswer;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Integer getSecComplete() {
		return secComplete;
	}

	public void setSecComplete(Integer secComplete) {
		this.secComplete = secComplete;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}