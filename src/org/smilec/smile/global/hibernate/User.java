package org.smilec.smile.global.hibernate;

import java.io.File;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;

import com.vaadin.terminal.FileResource;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.ThemeResource;

@Entity
@Table(name = "User")
public class User implements Serializable {
	private static final String defaultAvatarURL = "avatars/avatarDefault.png"; //for theme resource

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private byte[] password;

	@Column(name = "email")
	private String email;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "phone")
	private String phone = "";

	@Column(name = "photo_url")
	private String photo_url = "";

	@OneToOne(cascade = CascadeType.ALL)
	private UserStats userStats;

	@OneToOne(cascade = CascadeType.ALL)
	private Address address;

	@Column(name = "userType")
	private int userType = 1;

	@Column(name = "profilePublic")
	private int profilePublic = 1;

	public Resource getAvatar() {
		try {
			String url = getPhoto_url();
			if (url == null || url.equals("")) {
				url = defaultAvatarURL;
			}
			final Resource r;
			if (url.startsWith("avatar")) {
				r = new ThemeResource(url);
			} else {

				File file =
						new File(((SmileApplication) AppData.getApplication()).baseUser_url + id + "/"
								+ url);
				if (file.exists()) {
					r = new FileResource(file, AppData.getApplication());
				} else {
					r = new ThemeResource(defaultAvatarURL);
				}
			}
			return r;

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			Resource r = new ThemeResource(defaultAvatarURL);
			return r;
		}
	}

	public Resource getAvatarForceReload() {
		try {
			String url = getPhoto_url();
			if (url == null || url.equals("")) {
				url = defaultAvatarURL;
			}
			Resource r;
			if (url.startsWith("avatar")) {
				r = new ThemeResource(url);
			} else {
				File file =
						new File(((SmileApplication) AppData.getApplication()).baseUser_url + id + "/"
								+ url);
				if (file.exists()) {
					r = new FileResource(file, AppData.getApplication());
				} else {
					r = new ThemeResource(defaultAvatarURL);
				}
			}
			return r;

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			Resource r = new ThemeResource(defaultAvatarURL);
			return r;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getPassword() {
		return password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	public UserStats getUserStats() {
		return userStats;
	}

	public void setUserStats(UserStats userStats) {
		this.userStats = userStats;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public Boolean equals(User user) {
		return user.getId().equals(id);
	}

	public int getProfilePublic() {
		return profilePublic;
	}

	public void setProfilePublic(int profilePublic) {
		this.profilePublic = profilePublic;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof User) {
			return ((User) o).getId().equals(id);
		}
		return false;
	}
}