package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MsgRecipient")
public class MsgRecipient implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "mr_id")
	private Integer mr_id;

	@Column(name = "msg_id")
	private Integer msg_id;

	@Column(name = "recipient_id")
	private Integer recipient_id;

	@Column(name = "recipientDeleted")
	private Integer recipientDeleted = 0;

	@Column(name = "isRead")
	private Integer isRead = 0;

	public Integer getId() {
		return mr_id;
	}

	public void setId(Integer id) {
		this.mr_id = id;
	}

	public Integer getRecipient_id() {
		return recipient_id;
	}

	public void setRecipient_id(Integer recipient_id) {
		this.recipient_id = recipient_id;
	}

	public Integer getMsg_id() {
		return msg_id;
	}

	public void setMsg_id(Integer msg_id) {
		this.msg_id = msg_id;
	}

	public Integer getRecipientDeleted() {
		return recipientDeleted;
	}

	public void setRecipientDeleted(Integer recipientDeleted) {
		this.recipientDeleted = recipientDeleted;
	}

	public boolean isRead() {
		if (isRead.equals(1)) {
			return true;
		}
		return false;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public void setIsRead(boolean isRead) {
		if (isRead == true) {
			this.isRead = 1;
		} else {
			this.isRead = 0;
		}
	}
}