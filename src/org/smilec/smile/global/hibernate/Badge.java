package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Badge")
public class Badge implements Serializable {
	private Integer id;
	private String title;
	private String image;
	private Integer questionCreated;
	private Integer questionAnswered;
	private Integer sessions;
	private Integer comments;

	@Column(name = "qMedia")
	private Integer qMedia;

	@Column(name = "createdTag")
	private String createdTag;

	@Column(name = "groupCreated")
	private Integer groupCreated;

	@Column(name = "correctAnswer")
	private Integer correctAnswer;

	@Column(name = "avgRatingQ")
	private float avgRatingQ;

	public Badge() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "questionCreated")
	public Integer getQuestionCreated() {
		return questionCreated;
	}

	public void setQuestionCreated(Integer questionCreated) {
		this.questionCreated = questionCreated;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "questionAnswered")
	public Integer getQuestionAnswered() {
		return questionAnswered;
	}

	public void setQuestionAnswered(Integer questionAnswered) {
		this.questionAnswered = questionAnswered;
	}

	@Column(name = "image")
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Column(name = "sessions")
	public Integer getSessions() {
		return sessions;
	}

	public void setSessions(Integer sessions) {
		this.sessions = sessions;
	}

	@Column(name = "comments")
	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public Integer getqMedia() {
		return qMedia;
	}

	public void setqMedia(Integer qMedia) {
		this.qMedia = qMedia;
	}

	public String getCreatedTag() {
		return createdTag;
	}

	public void setCreatedTag(String createdTag) {
		this.createdTag = createdTag;
	}

	public Integer getGroupCreated() {
		return groupCreated;
	}

	public void setGroupCreated(Integer groupCreated) {
		this.groupCreated = groupCreated;
	}

	public Integer getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(Integer correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public float getAvgRatingQ() {
		return avgRatingQ;
	}

	public void setAvgRatingQ(float avgRatingQ) {
		this.avgRatingQ = avgRatingQ;
	}
}