package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class QuestionAnswer implements Serializable, Comparable<QuestionAnswer> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id", nullable = false)
	private Question question;

	@Column(name = "answerText")
	private String answerText;

	@Column(name = "isCorrect")
	private Integer isCorrect; //treated as a boolean, 1 or 0

	@Column(name = "numResponses")
	private Integer numResponses;

	public boolean equals(QuestionAnswer qa) {
		if (qa.id.equals(this.id)) {
			return true;
		} else {
			return false;
		}
	}

	public void incrementNumResponses() {
		if (numResponses == null) {
			numResponses = 0;
		}
		numResponses++;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public Integer getNumResponses() {
		return numResponses;
	}

	public void setNumResponses(Integer numResponses) {
		this.numResponses = numResponses;
	}

	public boolean getIsCorrect() {
		return isCorrect.equals(1);
	}

	public void setIsCorrect(Integer isCorrect) {
		this.isCorrect = isCorrect;
	}

	@Override
	public String toString() {
		return answerText;
	}

	@Override
	public int compareTo(QuestionAnswer qa) {
		if (qa.getId() > id) {
			return -1;
		} else if (qa.getId() < id) {
			return 1;
		} else {
			return 0;
		}
	}
}