package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QuizQuestion")
public class QuizQuestion implements Serializable {
	@Id
	@Column(name = "quiz_id")
	private Integer quiz_id;

	@Id
	@Column(name = "question_id")
	private Integer question_id;

	public Integer getQuiz_id() {
		return quiz_id;
	}

	public void setQuiz_id(Integer quiz_id) {
		this.quiz_id = quiz_id;
	}

	public Integer getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(Integer question_id) {
		this.question_id = question_id;
	}

}