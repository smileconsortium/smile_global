package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GroupOrganizer")
public class GroupOrganizer implements Serializable {
	@Id
	@Column(name = "user_id")
	private Integer user_id;

	@Id
	@Column(name = "group_id")
	private Integer group_id;

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}

}