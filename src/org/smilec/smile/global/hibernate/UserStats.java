package org.smilec.smile.global.hibernate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "UserStats")
public class UserStats implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdOn")
	private Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastAccessed")
	private Date lastAccessed;

	@Column(name = "cumulativeSecs")
	private Integer cumulativeSecs;

	@Column(name = "numSessions")
	private Integer numSessions;

	@Column(name = "avgSessionSecs")
	private Integer avgSessionSecs;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastAccessed() {
		return lastAccessed;
	}

	public void setLastAccessed(Date lastAccessed) {
		this.lastAccessed = lastAccessed;
	}

	public Integer getCumulativeSecs() {
		return cumulativeSecs;
	}

	public void setCumulativeSecs(Integer cumulativeSecs) {
		this.cumulativeSecs = cumulativeSecs;
	}

	public Integer getNumSessions() {
		return numSessions;
	}

	public void setNumSessions(Integer numSessions) {
		this.numSessions = numSessions;
	}

	public Integer getAvgSessionSecs() {
		return avgSessionSecs;
	}

	public void setAvgSessionSecs(Integer avgSessionSecs) {
		this.avgSessionSecs = avgSessionSecs;
	}
}