package org.smilec.smile.global.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "Message")
public class Message implements Serializable {
	@Transient
	private MsgRecipient msgRecipient;

	@Transient
	private String senderUsername = null;

	@Transient
	private ArrayList<String> recipientUsernames = null;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "sender_id")
	private Integer sender_id;

	@Column(name = "subject")
	private String subject;

	@Column(name = "message")
	private String message;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sentOn")
	private Date sentOn;

	@Column(name = "replyTo")
	private Integer replyTo = 0;

	//0 = message, 1 = evite
	@Column(name = "messageType")
	private Integer messageType = 0;

	//right now used to hold evite id
	@Column(name = "metaData")
	private String metaData;

	@Column(name = "senderDeleted")
	private Integer senderedDeleted = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSender_id() {
		return sender_id;
	}

	public void setSender_id(Integer sender_id) {
		this.sender_id = sender_id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getSentOn() {
		return sentOn;
	}

	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}

	public Integer getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(Integer replyTo) {
		this.replyTo = replyTo;
	}

	public Integer getMessageType() {
		return messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public String getSenderUsername() {
		return senderUsername;
	}

	public void setSenderUsername(String username) {
		this.senderUsername = username;
	}

	public ArrayList<String> getRecipientUsernames() {
		return recipientUsernames;
	}

	public void setRecipientUsernames(ArrayList<String> objects) {
		this.recipientUsernames = objects;
	}

	public boolean equals(Message m) {
		return m.id == id;
	}

	public Integer getSenderedDeleted() {
		return senderedDeleted;
	}

	public void setSenderedDeleted(Integer senderedDeleted) {
		this.senderedDeleted = senderedDeleted;
	}

	public void setMsgRecipient(MsgRecipient mr) {
		this.msgRecipient = mr;
	}

	public MsgRecipient getMsgRecipient() {
		return msgRecipient;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}
}