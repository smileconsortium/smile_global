package org.smilec.smile.global.hibernate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.smilec.smile.global.hibernate.logic.QuizDAO;

@Entity
@Table(name = "Quiz")
public class Quiz implements Serializable {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "creator_id")
	private Integer creator_id;

	@Column(name = "group_id")
	private Integer group_id;

	@Column(name = "name")
	private String name;

	@Column(name = "isPublic")
	private Integer isPublic;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	//ORDINAL: enum int is stored, STRING: enum value is stored
	private StatusEnum status;

	@Column(name = "startTime")
	private Date startTime;

	@Column(name = "createMinutes")
	private Date createMinutes;

	@Column(name = "solveMinutes")
	private Date solveMinutes;

	@Column(name = "showFeedback")
	private Integer showFeedback;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdOn")
	private Date createdOn;

	public enum StatusEnum {
		//if these enum values are changed, the smileglobal sql table, Quiz column must also be updated
		create, solve, show_result, closed, async, syncTimed;
	}

	public String getOption(String option) {
		return QuizDAO.getQuizOption(id, option);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCreator_id() {
		return creator_id;
	}

	public void setCreator_id(Integer creator_id) {
		this.creator_id = creator_id;
	}

	public Integer getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getCreateMinutes() {
		return createMinutes;
	}

	public void setCreateMinutes(Date createMinutes) {
		this.createMinutes = createMinutes;
	}

	public Date getSolveMinutes() {
		return solveMinutes;
	}

	public void setSolveMinutes(Date solveMinutes) {
		this.solveMinutes = solveMinutes;
	}

	public Integer getShowFeedback() {
		return showFeedback;
	}

	public void setShowFeedback(Integer showFeedback) {
		this.showFeedback = showFeedback;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}