package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QuestionTag")
public class QuestionTag implements Serializable {
	
	private Integer tag_id;	
	private Integer question_id;

	@Id
	@Column(name = "tag_id")
	public Integer getTag_id() {
		return tag_id;
	}

	public void setTag_id(Integer tag_id) {
		this.tag_id = tag_id;
	}

	@Id
	@Column(name = "question_id")
	public Integer getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(Integer question_id) {
		this.question_id = question_id;
	}
}
