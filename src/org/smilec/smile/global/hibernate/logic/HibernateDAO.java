package org.smilec.smile.global.hibernate.logic;

import org.hibernate.Session;
import org.hibernate.Transaction;
//TODO: Test using openSession instead of getCurrentSession.
//TODO: getCurrentSession() uses Tomcat threads for managing sessions,
//test how this works with concurrent users

public class HibernateDAO {
	public static boolean update(Object o) { //returns true if no errors
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			session.update(o);
			trns.commit();
			return true;
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (trns != null) {
				trns.rollback();
			}
			return false;
		}
	}
}
