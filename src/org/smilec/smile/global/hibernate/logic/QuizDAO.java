package org.smilec.smile.global.hibernate.logic;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.QuizOption;
import org.smilec.smile.global.hibernate.QuizQuestion;

public class QuizDAO {

	public static void createQuiz(Quiz quiz, QuizOption[] options) {
		insertQuiz(quiz);
		//after inserting quiz, get quiz id
		int quiz_id = quiz.getId();
		for (int i = 0, l = options.length; i < l; i++) {
			QuizOption exam_option = options[i];
			exam_option.setQuiz_id(quiz_id);
			QuizDAO.insertQuizOption(exam_option);
		}
	}

	private static void insertQuiz(Quiz quiz) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		quiz.setCreatedOn(new Date());
		session.save(quiz);

		trns.commit();
	}

	private static void insertQuizOption(QuizOption quizOption) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.save(quizOption);

		trns.commit();
	}

	public static void updateQuiz(Quiz quiz) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.merge(quiz);

		trns.commit();
	}

	public static boolean nameExists(Group group, String name) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Quiz where group_id = :groupId and name = :name ");
		query.setParameter("groupId", group.getId());
		query.setParameter("name", name);

		List<Quiz> list = query.list();
		trns.commit();

		if (list != null && list.size() > 0) {
			r = true;
		}

		return r;
	}

	public static Quiz getQuizByName(Group group, String name) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Quiz where group_id = :groupId and name = :name ");
		query.setParameter("groupId", group.getId());
		query.setParameter("name", name);

		List<Quiz> list = query.list();
		trns.commit();

		Quiz q = null;

		if (list.size() > 0) {
			q = list.get(0);
		}

		return q;
	}

	public static Quiz getQuizById(int quizId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Quiz where id = :quiz_id");
		query.setParameter("quiz_id", quizId);

		List<Quiz> list = query.list();
		trns.commit();

		Quiz q = null;

		if (list.size() > 0) {
			q = list.get(0);
		}

		return q;
	}

	//select Quiz.* from Quiz, QuizQuestion where QuizQuestion.question_id = 815 and Quiz.id = QuizQuestion.quiz_id;

	public static Quiz getQuizByQuestion(Question q) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		SQLQuery query =
				session.createSQLQuery(
						"select Quiz.* from Quiz, QuizQuestion where Quiz.id = QuizQuestion.quiz_id and QuizQuestion.question_id = "
								+ q.getId()).addEntity(Quiz.class);
		List<Quiz> list = query.list();
		trns.commit();

		Quiz quiz = null;

		if (list.size() > 0) {
			quiz = list.get(0);
		}

		return quiz;
	}

	public static boolean sessionNameExist(int uid, String name) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Quiz where creator_id = :uid and name = :name ");
		query.setParameter("uid", uid);
		query.setParameter("name", name);

		List<Quiz> list = query.list();
		trns.commit();

		if (list != null && list.size() > 0) {
			r = true;
		}

		return r;
	}

	public static Question getQuestion(int qid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery(" FROM Question WHERE id=:id ");
		query.setParameter("id", qid);
		List<Question> list = query.list();

		trns.commit();

		Question q = null;

		if (list.size() > 0) {
			q = list.get(0);
		}

		return q;
	}

	public static void insertQuestion(Question q) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.save(q);

		trns.commit();
	}

	public static void updateQuestion(Question q) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.merge(q);

		trns.commit();
	}

	public static List<QuizOption> getQuizOptions(int quizId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery(" FROM QuizOption WHERE quiz_id=:quizId ");
		query.setParameter("quizId", quizId);
		List<QuizOption> list = query.list();

		trns.commit();

		return list;
	}

	public static String getQuizOption(Integer quizId, String option) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createQuery(" FROM QuizOption WHERE quiz_id=:quizId and quiz_option=:quizOption");
		query.setParameter("quizId", quizId);
		query.setParameter("quizOption", option);
		List<QuizOption> list = query.list();

		trns.commit();

		if (list.size() > 0) {
			return list.get(0).getQuiz_value();
		} else {
			return null;
		}
	}

	public static List<Question> getQuizQuestions(int qid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createSQLQuery(
						" SELECT Question.* FROM Question, QuizQuestion WHERE QuizQuestion.quiz_id="
								+ qid + " AND Question.id=QuizQuestion.question_id ").addEntity(
						Question.class);
		List<Question> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Integer> getQuizQuestionIds(int qid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery(" SELECT Question.id as qid FROM Question, QuizQuestion WHERE QuizQuestion.quiz_id="
						+ qid + " AND Question.id=QuizQuestion.question_id ");
		query.addScalar("qid", IntegerType.INSTANCE);
		List<Integer> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Question> getQuizQuestionsCreated(int qid, int userId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createSQLQuery(
						" SELECT Question.* FROM Question, QuizQuestion WHERE QuizQuestion.quiz_id="
								+ qid
								+ " AND Question.id=QuizQuestion.question_id and Question.user_id = "
								+ userId).addEntity(Question.class);
		List<Question> list = query.list();

		trns.commit();

		return list;
	}

	public static void addQuestionToQuiz(int quiz_id, int question_id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		QuizQuestion q = new QuizQuestion();
		q.setQuiz_id(quiz_id);
		q.setQuestion_id(question_id);
		session.save(q);

		trns.commit();
	}

	public static void deleteQuestionFromSession(int quiz_id, int question_id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createQuery("delete QuizQuestion where quiz_id = :quiz_id and question_id = :question_id ");
		query.setParameter("quiz_id", quiz_id);
		query.setParameter("question_id", question_id);
		query.executeUpdate();

		trns.commit();
	}

	public static void deleteQuestionFromQuizQuestion(int question_id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete QuizQuestion where question_id = :question_id ");
		query.setParameter("question_id", question_id);
		query.executeUpdate();

		trns.commit();
	}

	public static List<Quiz> getSessionsForUser(int uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Quiz where creator_id = :uid ");
		query.setParameter("uid", uid);
		List<Quiz> list = query.list();

		trns.commit();

		return list;
	}

	public static boolean questionExist(int quizId, String question) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query;

		query =
				session.createSQLQuery(
						" SELECT Question.* FROM Question, QuizQuestion WHERE QuizQuestion.quiz_id="
								+ quizId
								+ " AND Question.id=QuizQuestion.question_id AND MATCH (Question.questionText) AGAINST ('"
								+ question + "')").addEntity(Question.class);
		//query = session.createSQLQuery(" SELECT Question.* FROM Question, QuizQuestion WHERE QuizQuestion.quiz_id="+qid+" AND Question.id=QuizQuestion.question_id AND Question.questionText='"+question+"'").addEntity(Question.class);	    

		List<Question> list = query.list();

		trns.commit();

		if (list != null && list.size() > 0) {
			r = true;
		}

		return r;
	}

	public static Quiz getSession(int id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery(" FROM Quiz WHERE id=:id ");
		query.setParameter("id", id);
		List<Quiz> list = query.list();

		trns.commit();

		Quiz q = null;

		if (list.size() > 0) {
			q = list.get(0);
		}

		return q;
	}

	public static void deleteSessionFromGroup(int id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete Quiz where id = :id ");
		query.setParameter("id", id);
		query.executeUpdate();

		trns.commit();
	}

	public static void deleteQuizQuestion(int quiz_id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete QuizQuestion where quiz_id = :quiz_id ");
		query.setParameter("quiz_id", quiz_id);
		query.executeUpdate();

		trns.commit();
	}

	public static Integer getNumQuizQuestionsAnswered(int quizId, int userId) {
		//returns number of questions answered in quiz
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		SQLQuery query =
				session.createSQLQuery("select count(*) as answeredQ from Question, QuizQuestion, "
						+ "UserQuestionInteraction where Question.id = QuizQuestion.question_id "
						+ "and QuizQuestion.quiz_id = " + quizId + " and Question.id = "
						+ "UserQuestionInteraction.question_id and "
						+ "UserQuestionInteraction.user_id = " + userId);
		query.addScalar("answeredQ", Hibernate.INTEGER);
		List<Integer> list = query.list();
		trns.commit();
		if (list == null || list.size() == 0) {
			//if no Questoins match query
			return null;
		}
		return list.get(0);
	}

	public static Integer getNumQuizQuestionsNotOwn(int quizId, int userId) {
		//returns an array of two integers, number of unanswered questions and number of answered questions
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		SQLQuery query =
				session.createSQLQuery("select count(*) as unansweredQ from Question,"
						+ " QuizQuestion where Question.id = QuizQuestion.question_id and "
						+ "QuizQuestion.quiz_id = " + quizId + " and Question.user_id != " + userId);
		query.addScalar("unansweredQ", Hibernate.INTEGER);
		List<Integer> list = query.list();
		trns.commit();
		if (list == null || list.size() == 0) {
			//if no Questoins match query
			return null;
		}
		return list.get(0);
	}

	public static Integer getNumQuizQuestions(int quizId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		SQLQuery query =
				session.createSQLQuery("select count(*) as qq from Question,"
						+ " QuizQuestion where Question.id = QuizQuestion.question_id and "
						+ "QuizQuestion.quiz_id = " + quizId);
		query.addScalar("qq", Hibernate.INTEGER);
		List<Integer> list = query.list();
		trns.commit();
		if (list == null || list.size() == 0) {
			//if no Questoins match query
			return null;
		}
		return list.get(0);
	}

	public static int getNumQuizQuestionsPosted(int quizId, int userId) {
		//select count(Question.id) from Question, QuizQuestion, User, Quiz where User.id = 3001 and Quiz.id = QuizQuestion.quiz_id and QuizQuestion.question_id = Question.id and Quiz.id = 3033 and User.id = Question.user_id;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		String queryString =
				"select count(Question.id) as questionsPosted from Question, QuizQuestion, User, Quiz where User.id = "
						+ userId
						+ " and Quiz.id = QuizQuestion.quiz_id and QuizQuestion.question_id = Question.id and Quiz.id = "
						+ quizId + " and User.id = Question.user_id;";
		SQLQuery query = session.createSQLQuery(queryString);
		query.addScalar("questionsPosted", Hibernate.INTEGER);
		List<Integer> list = query.list();
		trns.commit();
		if (list == null || list.size() == 0) {
			//if no Questions match query
			return 0;
		}
		return list.get(0);
	}

	public static Integer countQuizUserInteractions(Quiz quiz) {
		//select UserQuestionInteraction.* from UserQuestionInteraction, Question, QuizQuestion where UserQuestionInteraction.question_id = Question.id and Question.id = QuizQuestion.question_id and QuizQuestion.quiz_id = 3033;

		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery("select count(UserQuestionInteraction.id) as num from UserQuestionInteraction, Question, QuizQuestion where UserQuestionInteraction.question_id = Question.id and Question.id = QuizQuestion.question_id and QuizQuestion.quiz_id = "
						+ quiz.getId());
		query.addScalar("num", Hibernate.INTEGER);
		List<Integer> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list.get(0);
	}

	public static List<Object[]> getQuizAnswerAverages(int quizId) {
		//select UserQuestionInteraction.*, count(UserQuestionInteraction.id), avg(UserQuestionInteraction.secComplete), avg(rating), sum(QuestionAnswer.isCorrect) from QuizQuestion, UserQuestionInteraction left join QuestionAnswer on QuestionAnswer.id = UserQuestionInteraction.userAnswer_id where QuizQuestion.quiz_id = 3036 and QuizQuestion.question_id = UserQuestionInteraction.question_id group by UserQuestionInteraction.user_id;
		//returns Object[user_id, numAnswered questions, num Questions answered correctly, avgSecComplete, avgRatingGave

		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		String qq =
				"select UserQuestionInteraction.user_id as userId, count(UserQuestionInteraction.id) as numAnswered,"
						+ " sum(QuestionAnswer.isCorrect) as numCorrect, avg(UserQuestionInteraction.secComplete) as avgSecComplete,"
						+ " avg(rating) as avgRatingGave "
						+ "from QuizQuestion, UserQuestionInteraction "
						+ "left join QuestionAnswer on QuestionAnswer.id = "
						+ "UserQuestionInteraction.userAnswer_id where QuizQuestion.quiz_id = " + quizId
						+ " and QuizQuestion.question_id = " + "UserQuestionInteraction.question_id "
						+ "group by UserQuestionInteraction.user_id;";

		SQLQuery query = session.createSQLQuery(qq);
		query.addScalar("userId", IntegerType.INSTANCE);
		query.addScalar("numAnswered", IntegerType.INSTANCE);
		query.addScalar("numCorrect", IntegerType.INSTANCE);
		query.addScalar("avgSecComplete", FloatType.INSTANCE);
		query.addScalar("avgRatingGave", FloatType.INSTANCE);

		List<Object[]> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list;
	}

	public static List<Object[]> getQuizPostingAverages(int quizId) {
		//returns Object[user_id, numPosted questions, avg rating gotten for questions, % other got questions correct,
		//avg time for others to answer your questions

		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		String qq =
				"select user_id, count(id) as num, avg(avgRating) as rating, avg(avgCorrect) "
						+ "as correct, avg(avgSecComplete) as complete from Question, QuizQuestion "
						+ "where Question.id = QuizQuestion.question_id and QuizQuestion.quiz_id = "
						+ quizId + " group by user_id;";

		SQLQuery query = session.createSQLQuery(qq);
		query.addScalar("user_id", IntegerType.INSTANCE);
		query.addScalar("num", IntegerType.INSTANCE);
		query.addScalar("rating", FloatType.INSTANCE);
		query.addScalar("correct", FloatType.INSTANCE);
		query.addScalar("complete", FloatType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list;
	}

	public static void deleteQuizComplete(int quizId) throws Exception {
		//delete Questions (delete QuestionAnswers, UserQuestionInteractions, and comments)
		//delete QuizQuestion
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			//get list of question id's then delete from appropriate tables
			SQLQuery query =
					session.createSQLQuery(
							"SELECT Question.* FROM Question, QuizQuestion WHERE QuizQuestion.quiz_id = "
									+ quizId + " AND Question.id = QuizQuestion.question_id;")
							.addEntity(Question.class);
			Query query2;
			List<Question> list = query.list();
			Iterator<Question> i = list.iterator();
			String qString = "";
			while (i.hasNext()) {
				Question q = i.next();
				qString += q.getId();
				if (i.hasNext()) {
					qString += ", ";
				}
			}
			if (list.size() > 0) {
				//delete comments
				query =
						session.createSQLQuery("delete from QuestionComment where question_id in ("
								+ qString + ");");
				query.executeUpdate();
				//delete userQuestionInteraction
				query =
						session.createSQLQuery("delete from UserQuestionInteraction where question_id in ("
								+ qString + ");");
				query.executeUpdate();
				//delete questionAnswer
				query =
						session.createSQLQuery("delete from QuestionAnswer where question_id in ("
								+ qString + ");");
				query.executeUpdate();
				//delete questionTag
				query =
						session.createSQLQuery("delete from QuestionTag where question_id in ("
								+ qString + ");");
				query.executeUpdate();
			}
			//delete QuizQuestion
			query = session.createSQLQuery("delete from QuizQuestion where quiz_id = " + quizId + ";");
			query.executeUpdate();
			//delete Quiz
			query = session.createSQLQuery("delete from Quiz where id = " + quizId + ";");
			query.executeUpdate();
			//delete Quiz options
			query2 = session.createQuery("delete QuizOption where quiz_id = :qid ");
			query2.setParameter("qid", quizId);
			query2.executeUpdate();
			trns.commit();
			//delete question (and media)
			i = list.iterator();
			while (i.hasNext()) {
				Question q = i.next();
				QuestionDAO.deleteQuestion(q);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			try {
				if (trns != null) {
					trns.rollback();
				}
			} catch (Exception ee) {
				System.out.println(ee.getMessage());
			}
			throw new Exception();
		}
	}

	public static void deleteGroup(Group group) throws Exception {
		//cycle through and delete every session
		//then delete from userGroup, and groupOrganizer tables
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = null;
		try {
			//get list of question id's then delete from appropriate tables
			trns = session.beginTransaction();
			SQLQuery query =
					session.createSQLQuery("SELECT Quiz.id as qid FROM Quiz where group_id = "
							+ group.getId() + ";");
			query.addScalar("qid", IntegerType.INSTANCE);
			List<Integer> list = query.list();
			trns.commit();
			Iterator<Integer> i = list.iterator();
			while (i.hasNext()) {
				Integer qid = i.next();
				deleteQuizComplete(qid);
			}
			//start new transaction
			session = factory.getCurrentSession();
			trns = session.beginTransaction();
			//delete UserGroup
			query =
					session.createSQLQuery("delete from UserGroup where group_id = " + group.getId()
							+ ";");
			query.executeUpdate();
			//delete from Group Organizer
			query =
					session.createSQLQuery("delete from GroupOrganizer where group_id = "
							+ group.getId() + ";");
			query.executeUpdate();
			//delete Group
			query = session.createSQLQuery("delete from GroupTable where id = " + group.getId() + ";");
			query.executeUpdate();
			trns.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			try {
				if (trns != null) {
					trns.rollback();
				}
			} catch (Exception ee) {
				System.out.println(ee.getMessage());
			}
			throw new Exception();
		}
	}
}
