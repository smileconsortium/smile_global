package org.smilec.smile.global.hibernate.logic;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.smilec.smile.global.hibernate.Evite;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.GroupOrganizer;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserGroup;

public class GroupDAO {

	public static void insertGroup(Group group) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		group.setCreatedOn(new Date());
		session.save(group);

		trns.commit();
	}

	public static List<org.smilec.smile.global.hibernate.Group> getAllGroups() {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Group");
		List<Group> list = query.list();

		trns.commit();

		return list;
	}

	public static void addGroupOrganizer(GroupOrganizer organizer) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		session.save(organizer);

		trns.commit();

	}

	public static void deleteGroupOrganizer(Integer gid, Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createQuery("delete GroupOrganizer where group_id = :gid and user_id = :uid ");
		query.setParameter("gid", gid);
		query.setParameter("uid", uid);
		query.executeUpdate();

		trns.commit();
	}

	public static List<Integer> getGroupOrganizers(Group group) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery(" select user_id from GroupOrganizer where group_id = :gid ");
		query.setParameter("gid", group.getId());
		List<Integer> list = query.list();

		trns.commit();

		return list;
	}

	public static void updateGroup(Group group) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.merge(group);

		trns.commit();
	}

	public static boolean nameExists(int gid, int oid, String name) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Group where organizer_id = :oid and name = :name ");
		query.setParameter("oid", oid);
		query.setParameter("name", name);

		List<Group> list = query.list();
		trns.commit();
		Group group;

		if (list != null && list.size() > 0) {
			group = list.get(0);
			if (group.getId() != gid)
				r = true;
		}

		return r;
	}

	public static boolean nameExists(int oid, String name) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Group where organizer_id = :oid and name = :name ");
		query.setParameter("oid", oid);
		query.setParameter("name", name);

		List<Group> list = query.list();
		trns.commit();

		if (list != null && list.size() > 0) {
			r = true;
		}

		return r;
	}

	public static boolean nameExists(String groupName) {
		boolean r = false;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Group where name = :name ");
		query.setParameter("name", groupName);

		List<Group> list = query.list();
		trns.commit();

		if (list != null && list.size() > 0) {
			r = true;
		}

		return r;
	}

	public static Group getGroupById(Integer gid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Group where id=:gid ");
		query.setParameter("gid", gid);
		List<Group> list = query.list();

		trns.commit();

		Group group = null;

		if (list != null && list.size() > 0) {
			group = list.get(0);
		}

		return group;
	}

	public static Group getGroupByName(String groupName) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Group where name = :name ");
		query.setParameter("name", groupName);

		List<Group> list = query.list();
		trns.commit();

		if (list != null && list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public static String getGroupName(int gid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("select name from Group where id = :id ");
		query.setParameter("id", gid);

		List<String> list = query.list();
		trns.commit();

		if (list != null && list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public static int removeAllUsers(int gid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete UserGroup where group_id=" + gid);
		int result = query.executeUpdate(); // result returns the number of the deleted rows in UserGroup 

		trns.commit();
		//System.out.println("deleteUsers : " + result);
		return result;
	}

	public static List<Group> getGroups(User user) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createSQLQuery(
						"select GroupTable.* from GroupTable, UserGroup, User where UserGroup.user_id = "
								+ user.getId()
								+ " and GroupTable.id = UserGroup.group_id and UserGroup.user_id = User.id;")
						.addEntity(Group.class);
		List<Group> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list;
	}

	public static List<Group> getGroupsWhereOrganizer(int uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery(" from Group where organizer_id = :uid ");
		query.setParameter("uid", uid);
		List<Group> list = query.list();

		trns.commit();

		return list;
	}

	public static Evite getEvite(int eid, String token) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Evite where id = :eid AND token = :token");
		query.setParameter("eid", eid);
		query.setParameter("token", token);

		List<Evite> list = query.list();
		trns.commit();

		Evite e = null;

		if (list.size() > 0) {
			e = list.get(0);
		}

		return e;
	}

	public static Evite inviteUserToGroup(Integer gid, Integer uid, String token) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Evite e = new Evite();
		e.setGroup_id(gid);
		e.setUser_id(uid);
		e.setInvitedOn(new Date());
		e.setToken(token);
		session.save(e);

		trns.commit();

		return e;
	}

	public static void deleteEvite(int eid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete Evite where id = :eid ");
		query.setParameter("eid", eid);
		query.executeUpdate();

		trns.commit();
	}

	public static void addUserToGroupById(Integer gid, Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		UserGroup ug = new UserGroup();
		ug.setGroup_id(gid);
		ug.setUser_id(uid);
		session.save(ug);

		trns.commit();
	}

	public static void addUserToGroup(Group group, User user) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		UserGroup ug = new UserGroup();
		ug.setGroup_id(group.getId());
		ug.setUser_id(user.getId());
		session.save(ug);

		trns.commit();
	}

	public static void deleteUserFromGroup(int gid, int uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete UserGroup where user_id = :uid and group_id = :gid ");
		query.setParameter("uid", uid);
		query.setParameter("gid", gid);
		query.executeUpdate();

		trns.commit();
	}

	public static void deleteGroupsFromUserGroup(int gid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete UserGroup where group_id = :gid ");
		query.setParameter("gid", gid);
		query.executeUpdate();

		trns.commit();
	}

	public static void deleteGroup(int gid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete Group where id = :gid ");
		query.setParameter("gid", gid);
		query.executeUpdate();

		trns.commit();
	}

	public static boolean userExist(int gid, int uid) {
		boolean r = true;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from UserGroup where user_id = :uid and group_id = :gid ");
		query.setParameter("uid", uid);
		query.setParameter("gid", gid);

		List<User> list = query.list();
		trns.commit();

		if (list.size() > 0) {
			r = true;
		} else
			r = false;

		return r;
	}

	public static boolean userInGroup(Integer gid, User user) {
		boolean r = true;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from UserGroup where user_id = :uid and group_id = :gid ");
		query.setParameter("uid", user.getId());
		query.setParameter("gid", gid);

		List list = query.list();
		trns.commit();

		if (list.size() > 0) {
			r = true;
		} else
			r = false;

		return r;
	}

	public static List<User> getGroupUsers(Group group) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createSQLQuery(
						" SELECT User.* FROM User, GroupTable, UserGroup WHERE GroupTable.id="
								+ group.getId()
								+ " AND UserGroup.group_id=GroupTable.id AND User.id=UserGroup.user_id ")
						.addEntity(User.class);
		List<User> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list;
	}

	public static List<Quiz> getQuizzes(Group group) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery(" from Quiz where group_id = :gid ");
		query.setParameter("gid", group.getId());
		List<Quiz> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list;
	}

	public static Integer countQuizzes(Group group) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery("select count(*) as num from Quiz where group_id = "
						+ group.getId());
		query.addScalar("num", Hibernate.INTEGER);
		List<Integer> list = query.list();

		trns.commit();
		//System.out.println("getUserGroup : " + list.size());
		return list.get(0);
	}

	public static List<Object[]> searchGroupsByName(String gName) {
		//returns [Group, oragnizer username (String)]
		//select * from GroupTable where GroupTable.name LIKE '%a%';
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery(
						"select GroupTable.*, User.username from GroupTable, User where GroupTable.organizer_id = User.id and GroupTable.name LIKE '%"
								+ gName + "%'").addEntity("Group", Group.class);
		query.addScalar("username", org.hibernate.type.StringType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Object[]> searchGroupsByOrganizer(String uName) {
		//returns [Group, oragnizer username (String)]
		//select GroupTable.* from GroupTable, User where GroupTable.organizer_id = User.id and User.username LIKE '%a%';
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery(
						"select GroupTable.*, User.username from GroupTable, User where GroupTable.organizer_id = User.id and User.username LIKE '%"
								+ uName + "%'").addEntity("Group", Group.class);
		query.addScalar("username", org.hibernate.type.StringType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Object[]> searchNewGroupsByName(String gName, int userId) {
		//returns [Group, oragnizer username (String)]
		//select * from GroupTable where GroupTable.id not in (select UserGroup.group_id from UserGroup where UserGroup.user_id = 3001) and GroupTable.name LIKE '%%';
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery(
						"select GroupTable.*, User.username from GroupTable, User where GroupTable.id not in (select UserGroup.group_id from UserGroup where UserGroup.user_id = "
								+ userId
								+ ") and GroupTable.organizer_id = User.id and GroupTable.name LIKE '%"
								+ gName + "%'").addEntity("Group", Group.class);
		query.addScalar("username", org.hibernate.type.StringType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Object[]> searchNewGroupsByOrganizer(String uName, int userId) {
		//returns [Group, oragnizer username (String)]
		//select GroupTable.* from GroupTable, User where GroupTable.organizer_id = User.id and GroupTable.id not in (select UserGroup.group_id from UserGroup where UserGroup.user_id = 3001) and User.username LIKE '%%'
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery(
						"select GroupTable.*, User.username from GroupTable, User where GroupTable.organizer_id = User.id and GroupTable.id not in (select UserGroup.group_id from UserGroup where UserGroup.user_id = "
								+ userId + ") and User.username LIKE '%" + uName + "%'").addEntity(
						"Group", Group.class);
		query.addScalar("username", org.hibernate.type.StringType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();

		return list;
	}
}
