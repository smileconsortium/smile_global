package org.smilec.smile.global.hibernate.logic;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import org.smilec.smile.global.hibernate.Message;
import org.smilec.smile.global.hibernate.MsgRecipient;
import org.smilec.smile.global.hibernate.User;

public class MessageDAO {
	public static List<Object[]> getInbox(User user) {
		//returns {Message, MsgRecipient, sender username}
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		List<Object[]> list = new ArrayList<Object[]>();
		try {
			SQLQuery query =
					session.createSQLQuery(
							"select Message.*, MsgRecipient.*, User.username from"
									+ " MsgRecipient, Message left join User on "
									+ "Message.sender_id = User.id where Message.id = "
									+ "MsgRecipient.msg_id and MsgRecipient.recipient_id = "
									+ user.getId()
									+ " and MsgRecipient.recipientDeleted = 0 group by Message.id order by sentOn desc;")
							.addEntity(Message.class).addEntity(MsgRecipient.class)
							.addScalar("username", StringType.INSTANCE);
			list = query.list();
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static int countUnreadMsg(User user) {
		//returns {Message, sender username}
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		int count = 0;
		try {
			SQLQuery query =
					session.createSQLQuery(
							"select count(mr_id) as count from MsgRecipient where recipient_id = "
									+ user.getId() + " and recipientDeleted = 0 and isRead = 0;")
							.addScalar("count", IntegerType.INSTANCE);
			List<Integer> list = query.list();
			if (list.size() > 0) {
				count = list.get(0);
			}
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return count;
	}

	public static void saveMessage(Message m) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = null;
		try {
			trns = session.beginTransaction();
			session.save(m);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
			throw e;
		}
	}

	public static void updateMessage(Message m) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.merge(m);

		trns.commit();
	}

	public static void updateMsgRecipient(MsgRecipient mr) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.merge(mr);

		trns.commit();
	}

	public static void saveMsgRecipient(MsgRecipient m) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = null;
		try {
			trns = session.beginTransaction();
			session.save(m);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
			throw e;
		}
	}

	public static List<Object[]> getOutboxMsgRecipients(User user) {
		//returns {Message, sender username}
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		List<Object[]> list = new ArrayList<Object[]>();
		try {
			SQLQuery query =
					session.createSQLQuery("select Message.*, User.username"
							+ " from Message, MsgRecipient left join User on "
							+ "MsgRecipient.recipient_id = User.id "
							+ "where Message.id = MsgRecipient.msg_id "
							+ "and Message.sender_id = "
							+ user.getId()
							+ " and Message.senderDeleted = 0 and Message.messageType != 1 order by sentOn desc;");
			query.addEntity(Message.class).addScalar("username", StringType.INSTANCE);
			list = query.list();
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static void deleteOutboxMessages(ArrayList<Integer> selected) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		String messages = "(";
		for (int i = 0, l = selected.size(); i < l; i++) {
			messages += selected.get(i) + ",";
		}
		messages = messages.substring(0, messages.length() - 1) + ")";
		SQLQuery query =
				session.createSQLQuery("update Message set senderDeleted = 1 where Message.id in "
						+ messages + ";");
		query.executeUpdate();
		trns.commit();
	}

	public static void deleteInboxMessages(ArrayList<Integer> selected, int userId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		String messages = "(";
		for (int i = 0, l = selected.size(); i < l; i++) {
			messages += selected.get(i) + ",";
		}
		messages = messages.substring(0, messages.length() - 1) + ")";
		SQLQuery query =
				session.createSQLQuery("update MsgRecipient set recipientDeleted = 1 where MsgRecipient.recipient_id = "
						+ userId + " and MsgRecipient.msg_id in " + messages + ";");
		query.executeUpdate();
		trns.commit();
	}

	public static void markAsRead(Integer msgId, Integer userId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		SQLQuery query =
				session.createSQLQuery("update MsgRecipient set isRead = 1 where msg_id = " + msgId
						+ " and recipient_id = " + userId + ";");
		query.executeUpdate();
		trns.commit();
	}
}
