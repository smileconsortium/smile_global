package org.smilec.smile.global.hibernate.logic;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.type.IntegerType;
import org.smilec.smile.global.AppData;
import org.smilec.smile.global.SmileApplication;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;
import org.smilec.smile.global.p.Public.PublicViewLogic;

public class QuestionDAO {
	public static List<Question> getQuestions(String fulltext, int start, int range, User user,
			boolean showOwn, boolean showAnswered, int orderCode) {
		return getQuestions(fulltext, start, range, user, showOwn, showAnswered, orderCode, null);
	}

	public static List<Question> getQuestions(String fulltext, int start, int range, User user,
			boolean showOwn, boolean showAnswered, int orderCode, Quiz quiz) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		List<Question> questionList;
		try {
			String q =
					buildQuestionQuery(fulltext, start, range, user, showOwn, showAnswered, false,
							orderCode, quiz);
			//if showing answered, then the questionQuery will return a Question object and an
			//integer indicating if the given user has answered a question
			if (showAnswered && user != null) {
				Query query =
						session.createSQLQuery(q).addEntity("Question", Question.class)
								.addEntity("UserQuestionInteraction", UserQuestionInteraction.class);
				List<Object[]> list = query.list();
				trns.commit();

				//generate list of just questions, with answers set
				questionList = new ArrayList<Question>();
				Iterator<Object[]> i = list.iterator();
				while (i.hasNext()) {
					Object[] o = i.next();
					Question quest = (Question) o[0];
					UserQuestionInteraction uqi = (UserQuestionInteraction) o[1];
					quest.setCurrentUserInteraction(uqi);
					questionList.add(quest);
				}
			} else {
				//query just returns a question list
				Query query = session.createSQLQuery(q).addEntity("Question", Question.class);
				questionList = query.list();
				trns.commit();

				if (questionList.size() == 0) {
					//if no Questions match query
					return new ArrayList<Question>();
				}
			}
			return questionList;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return new ArrayList<Question>();
		}
	}

	public static Integer countQuestions(String fulltext, User user, boolean showOwn,
			boolean showAnswered) {
		return countQuestions(fulltext, user, showOwn, showAnswered, null);
	}

	public static Integer countQuestions(String fulltext, User user, boolean showOwn,
			boolean showAnswered, Quiz quiz) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			String q = buildQuestionQuery(fulltext, 0, 0, user, showOwn, showAnswered, true, null, quiz);
			SQLQuery query = session.createSQLQuery(q);
			query.addScalar("num", IntegerType.INSTANCE);
			// you might need to use org.hibernate.type.StandardBasicTypes.INTEGER / STRING 
			// for Hibernate v3.6+, 
			// see https://hibernate.onjira.com/browse/HHH-5138
			List<Integer> list = query.list();
			trns.commit();
			return list.get(0);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return null;
		}
	}

	private static String buildQuestionQuery(String fulltext, int start, int range, User user,
			boolean showOwn, boolean showAnswered, boolean count, Integer orderCode) {
		return buildQuestionQuery(fulltext, start, range, user, showOwn, showAnswered, count, orderCode,
				null);
	}

	public static String buildQuestionQuery(String fulltext, int start, int range, User user,
			boolean showOwn, boolean showAnswered, boolean count, Integer orderCode, Quiz quiz) {
		if (user == null) {
			//if user is null, just get all questions
			showOwn = true;
			showAnswered = true;
		}
		String q;
		//either count or select
		if (count == true) {
			q = "select count(*) as num from Question";
			if (quiz != null) {
				q +=
						", QuizQuestion where Question.id = QuizQuestion.question_id and QuizQuestion.quiz_id = "
								+ quiz.getId() + " ";
			}
		} else {
			if (showAnswered && user != null) { //if showing answered questions, need to also request
				//userQuestionInteraction for current user
				/*q =
						"select *, id in (select question_id from UserQuestionInteraction "
								+ "where UserQuestionInteraction.user_id =" + user.getId()
								+ ") as answered from Question";*/
				q = "select Question.*, UserQuestionInteraction.* from Question ";

				//if Quiz is not null, limit results to those Questions in Quiz
				if (quiz != null) {
					q +=
							"inner join QuizQuestion on Question.id = QuizQuestion.question_id and QuizQuestion.quiz_id = "
									+ quiz.getId() + " ";
				}
				q +=
						"left join UserQuestionInteraction on Question.id = "
								+ "UserQuestionInteraction.question_id and UserQuestionInteraction.user_id = "
								+ user.getId();
			} else {
				q = "select * from Question";
			}
		}
		//add where statements
		List<String> whereStatements = new ArrayList<String>();
		//only show public questions unless qiuz if not null
		if (quiz == null) {
			whereStatements.add(" Question.isPublic = " + 1);
		}
		if (!showAnswered && user != null) {
			whereStatements.add(" id not in (select question_id from UserQuestionInteraction"
					+ " and UserQuestionInteraction.user_id = " + user.getId() + ")");
		}
		if (!showOwn && user != null) {
			whereStatements.add(" Question.user_id != " + user.getId());
		}
		if (fulltext != "") {
			whereStatements.add(" MATCH(questionText, tagText) AGAINST(\"" + fulltext + "\")");
		}
		for (int i = 0, l = whereStatements.size(); i < l; i++) {
			if (i == 0) {
				q += " WHERE";
			} else {
				q += " AND";
			}
			q += whereStatements.get(i);
		}
		//add group statement in case there are multiple user question interactions (shouldn't be)
		if (count == false) {
			q += " group by Question.id";
		}
		//add order statement
		if (orderCode != null) {
			switch (orderCode) {
			case PublicViewLogic.ORDER_CODE_DATE_ASC:
				q += " ORDER BY Question.createdOn";
				break;
			case PublicViewLogic.ORDER_CODE_DATE_DESC:
				q += " ORDER BY Question.createdOn desc";
				break;
			case PublicViewLogic.ORDER_CODE_DIFFICULTY_ASC:
				q += " ORDER BY avgCorrect desc";
				break;
			case PublicViewLogic.ORDER_CODE_DIFFICULTY_DESC:
				q +=
						" ORDER BY case when avgCorrect is null then 1 else 0 end, avgCorrect, uniqueAnswers desc";
				break;
			case PublicViewLogic.ORDER_CODE_RATING_DESC:
				q += " ORDER BY avgRating desc";
				break;
			case PublicViewLogic.ORDER_CODE_POPULARITY_DESC:
				q += " ORDER BY uniqueAnswers desc";
				break;
			}
		}
		//add limit statement
		if (start > -1 && range > 0) {
			q = q + " limit " + start + "," + range;
		}
		q += ";";
		return q;
	}

	public static void updateQuestion(Question q) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		try {
			session.merge(q);
			trns.commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		}
	}

	public static void updateQuestionAnswer(QuestionAnswer q) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		try {
			session.merge(q);
			trns.commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		}
	}

	public static ArrayList<Object[]> getQuestionComments(Question question, int startFrom, int num) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		ArrayList<Object[]> arrayList;
		try {
			/*String q =
					"select User.username, UserQuestionInteraction.commentText "
							+ "from UserQuestionInteraction left join User on "
							+ "UserQuestionInteraction.user_id = User.id where "
							+ "UserQuestionInteraction.commentText != '' and "
							+ "UserQuestionInteraction.question_id = " + question.getId() + ";";*/
			String q =
					"select User.username, QuestionComment.commentText, QuestionComment.id "
							+ "from QuestionComment left join User on "
							+ "QuestionComment.user_id = User.id where "
							+ "QuestionComment.commentText != '' and "
							+ "QuestionComment.question_id = " + question.getId()
							+ " ORDER BY createdOn DESC LIMIT " + startFrom + "," + num + ";";
			SQLQuery query = session.createSQLQuery(q);
			query.addScalar("username", Hibernate.STRING);
			query.addScalar("commentText", Hibernate.STRING);
			query.addScalar("id", Hibernate.INTEGER);
			arrayList = new ArrayList<Object[]>();
			for (Object rows : query.list()) {
				Object[] row = (Object[]) rows;
				String username = (String) row[0];
				String commentText = (String) row[1];
				Integer id = (Integer) row[2];
				Object[] comments = { username, commentText, id };
				arrayList.add(comments);
			}
			trns.commit();
			return arrayList;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return new ArrayList<Object[]>();
		}
	}

	public static void updateQuestionComment(Integer id, String commentText) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		String sql = "UPDATE QuestionComment SET commentText='" + commentText + "' WHERE id=" + id;
		Query query = session.createSQLQuery(sql);
		query.executeUpdate();

		trns.commit();
	}

	public static void deleteQuestionComment(Integer id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete QuestionComment where id = :id ");
		query.setParameter("id", id);
		query.executeUpdate();

		trns.commit();
	}

	public static Question getQuestionById(int qid) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Question where id = :qid");
		query.setParameter("qid", qid);
		List<Question> list = query.list();
		trns.commit();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public static QuestionAnswer getQuestionAnswerById(Integer id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from QuestionAnswer where id = :id");
		query.setParameter("id", id);
		List<QuestionAnswer> list = query.list();
		trns.commit();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public static Question getQuestionAndInteractionById(int qid, int userId) {
		//returns question object with currentUserQuestionInteraction set
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		String qq =
				"select Question.*, UserQuestionInteraction.* from Question left join UserQuestionInteraction on Question.id = "
						+ "UserQuestionInteraction.question_id and UserQuestionInteraction.user_id = "
						+ userId + " where Question.id = " + qid + ";";
		Query query =
				session.createSQLQuery(qq).addEntity("Question", Question.class)
						.addEntity("UserQuestionInteraction", UserQuestionInteraction.class);
		List<Object[]> list = query.list();
		trns.commit();

		//generate list of just questions, with answers set
		Question q;
		Iterator<Object[]> i = list.iterator();
		if (i.hasNext()) {
			Object[] o = i.next();
			Question quest = (Question) o[0];
			UserQuestionInteraction uqi = (UserQuestionInteraction) o[1];
			quest.setCurrentUserInteraction(uqi);
			q = quest;
			return q;
		} else {
			return null;
		}
	}

	public static List<QuestionAnswer> getQuestionAnswers(Question question) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		List<QuestionAnswer> list = new LinkedList<QuestionAnswer>();
		try {
			Query query = session.createQuery("from QuestionAnswer where question_id = :question_id");
			query.setParameter("question_id", question.getId());
			list = query.list();
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static List<UserQuestionInteraction> getUserQuestionInteractions(int questionId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		List<UserQuestionInteraction> list = null;
		try {
			Query query =
					session.createQuery("from UserQuestionInteraction where question_id = :question_id");
			query.setParameter("question_id", questionId);
			list = query.list();
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static Question createQuestion(Question q) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = null;
		try {
			trns = session.beginTransaction();
			session.save(q);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//only necessary if .openSession() is used instead of getCurrentSession()
			//session.flush();
			//session.close();
		}
		return q;
	}

	public static QuestionAnswer createQuestionAnswer(QuestionAnswer qa) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			session.save(qa);
			trns.commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//only necessary if .openSession() is used instead of getCurrentSession()
			//session.flush();
			//session.close();
		}
		return qa;
	}

	public static void deleteQuestion(Question q) {
		if (q != null && q.getContentType() != Question.ContentTypeEnum.document
				&& q.getMedia_url() != null && q.getMedia_url().length() > 0) {
			try {
				String question_url = ((SmileApplication) AppData.getApplication()).question_url;
				String media_url = q.getMedia_url();
				String ext = media_url.substring(media_url.lastIndexOf('.'));
				File media_file = new File(question_url + q.getId() + ext);
				//AppData.getMainWindow().showNotification("deleteQuestion:"+question_url+question_id+ext);

				if (media_file.delete()) {
					//System.out.println(media_file.getName() + " is deleted!");
				} else {
					//System.out.println("Delete operation is failed.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete Question where id = :id ");
		query.setParameter("id", q.getId());
		query.executeUpdate();

		trns.commit();
	}

	public static void deleteQuestionComplete(Question q) throws Exception {
		if (q != null && q.getContentType() != Question.ContentTypeEnum.document
				&& q.getMedia_url() != null && q.getMedia_url().length() > 0) {
			try {
				String question_url = ((SmileApplication) AppData.getApplication()).question_url;
				String media_url = q.getMedia_url();
				String ext = media_url.substring(media_url.lastIndexOf('.'));
				File media_file = new File(question_url + q.getId() + ext);
				//AppData.getMainWindow().showNotification("deleteQuestion:"+question_url+question_id+ext);

				if (media_file.delete()) {
					//System.out.println(media_file.getName() + " is deleted!");
				} else {
					//System.out.println("Delete operation is failed.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}

		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			int qid = q.getId();

			Query query = session.createQuery("delete Question where id = :id ");
			query.setParameter("id", qid);
			query.executeUpdate();

			query = session.createQuery("delete QuestionAnswer where question_id = :id ");
			query.setParameter("id", qid);
			query.executeUpdate();

			query = session.createQuery("delete UserQuestionInteraction where question_id = :id ");
			query.setParameter("id", qid);
			query.executeUpdate();

			query = session.createQuery("delete QuizQuestion where question_id = :id ");
			query.setParameter("id", qid);
			query.executeUpdate();

			query = session.createQuery("delete QuestionTag where question_id = :id ");
			query.setParameter("id", qid);
			query.executeUpdate();

			query = session.createQuery("delete QuestionComment where question_id = :id ");
			query.setParameter("id", qid);
			query.executeUpdate();

			trns.commit();
		} catch (Exception e) {
			try {
				trns.rollback();
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				e.printStackTrace();
				throw e;
			}
		}
	}

	public static void deleteQuestionMedia(Question q) {
		if (q != null && q.getContentType() != Question.ContentTypeEnum.document
				&& q.getMedia_url().length() > 0) {
			try {
				String question_url = ((SmileApplication) AppData.getApplication()).question_url;
				String media_url = q.getMedia_url();
				String ext = media_url.substring(media_url.lastIndexOf('.'));
				File media_file = new File(question_url + q.getId() + ext);
				//AppData.getMainWindow().showNotification("deleteQuestion:"+question_url+question_id+ext);

				if (media_file.delete()) {
					//System.out.println(media_file.getName() + " is deleted!");
				} else {
					//System.out.println("Delete operation is failed.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void deleteQuestionAnswer(QuestionAnswer qa) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete QuestionAnswer where id = :id ");
		query.setParameter("id", qa.getId());
		query.executeUpdate();

		query = session.createQuery("delete UserQuestionInteraction where userAnswer_id = :id ");
		query.setParameter("id", qa.getId());
		query.executeUpdate();

		trns.commit();
	}

	public void deleteUserQuestionInteraction(int question_id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query =
				session.createQuery("delete UserQuestionInteraction where question_id = :question_id ");
		query.setParameter("question_id", question_id);
		query.executeUpdate();

		trns.commit();
	}

	public static int countUserQuestions(int userSearchId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		String q =
				"select count(Question.id) as num from Question where user_id = " + userSearchId
						+ " and isPublic = 1";
		SQLQuery query = session.createSQLQuery(q);
		query.addScalar("num", IntegerType.INSTANCE);
		List<Integer> list = query.list();
		trns.commit();
		return list.get(0);
	}

	public static List<Question> getUserQuestions(int userId, int userSearchId, int start,
			int questionBufferSize, int orderCode) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		String qq =
				"select Question.*, UserQuestionInteraction.* from Question left join UserQuestionInteraction on Question.id = UserQuestionInteraction.question_id and UserQuestionInteraction.user_id = "
						+ userId + " WHERE Question.isPublic = 1 and Question.user_id = " + userSearchId;
		//add group statement in case there are multiple user question interactions (shouldn't be)
		qq += " group by Question.id";
		//add order statement
		switch (orderCode) {
		case PublicViewLogic.ORDER_CODE_DATE_ASC:
			qq += " ORDER BY Question.createdOn";
			break;
		case PublicViewLogic.ORDER_CODE_DATE_DESC:
			qq += " ORDER BY Question.createdOn desc";
			break;
		case PublicViewLogic.ORDER_CODE_DIFFICULTY_ASC:
			qq += " ORDER BY avgCorrect desc";
			break;
		case PublicViewLogic.ORDER_CODE_DIFFICULTY_DESC:
			qq +=
					" ORDER BY case when avgCorrect is null then 1 else 0 end, avgCorrect, uniqueAnswers desc";
			break;
		case PublicViewLogic.ORDER_CODE_RATING_DESC:
			qq += " ORDER BY avgRating desc";
			break;
		case PublicViewLogic.ORDER_CODE_POPULARITY_DESC:
			qq += " ORDER BY uniqueAnswers desc";
			break;
		}

		qq += " limit " + start + "," + questionBufferSize;

		Query query =
				session.createSQLQuery(qq).addEntity("Question", Question.class)
						.addEntity("UserQuestionInteraction", UserQuestionInteraction.class);
		List<Object[]> list = query.list();
		trns.commit();

		//generate list of just questions, with answers set
		ArrayList<Question> questionList = new ArrayList<Question>();
		Iterator<Object[]> i = list.iterator();
		while (i.hasNext()) {
			Object[] o = i.next();
			Question quest = (Question) o[0];
			UserQuestionInteraction uqi = (UserQuestionInteraction) o[1];
			quest.setCurrentUserInteraction(uqi);
			questionList.add(quest);
		}

		return questionList;
	}

	public static int countMyQuestions(String fulltext, int userSearchId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		String whereStatements = "";
		String q = "select count(Question.id) as num from Question where user_id = " + userSearchId;

		if (fulltext != "") {
			whereStatements = " MATCH(questionText, tagText) AGAINST(\"" + fulltext + "\")";
			q += " AND " + whereStatements;
		}

		SQLQuery query = session.createSQLQuery(q);
		query.addScalar("num", IntegerType.INSTANCE);
		List<Integer> list = query.list();
		trns.commit();
		return list.get(0);
	}

	public static List<Question> getMyQuestions(String fulltext, int userId, int userSearchId,
			int start, int questionBufferSize, int orderCode) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		String whereStatements = "";

		String qq =
				"select Question.*, UserQuestionInteraction.* from Question left join UserQuestionInteraction on Question.id = UserQuestionInteraction.question_id and UserQuestionInteraction.user_id = "
						+ userId + " WHERE Question.user_id = " + userSearchId;

		if (fulltext != "") {
			whereStatements = " MATCH(questionText, tagText) AGAINST(\"" + fulltext + "\")";
			qq += " AND " + whereStatements;
		}
		//add group statement in case there are multiple user question interactions (shouldn't be)
		qq += " group by Question.id";
		//add order statement
		switch (orderCode) {
		case PublicViewLogic.ORDER_CODE_DATE_ASC:
			qq += " ORDER BY Question.createdOn";
			break;
		case PublicViewLogic.ORDER_CODE_DATE_DESC:
			qq += " ORDER BY Question.createdOn desc";
			break;
		case PublicViewLogic.ORDER_CODE_DIFFICULTY_ASC:
			qq += " ORDER BY avgCorrect desc";
			break;
		case PublicViewLogic.ORDER_CODE_DIFFICULTY_DESC:
			qq +=
					" ORDER BY case when avgCorrect is null then 1 else 0 end, avgCorrect, uniqueAnswers desc";
			break;
		case PublicViewLogic.ORDER_CODE_RATING_DESC:
			qq += " ORDER BY avgRating desc";
			break;
		case PublicViewLogic.ORDER_CODE_POPULARITY_DESC:
			qq += " ORDER BY uniqueAnswers desc";
			break;
		}

		qq += " limit " + start + "," + questionBufferSize;

		Query query =
				session.createSQLQuery(qq).addEntity("Question", Question.class)
						.addEntity("UserQuestionInteraction", UserQuestionInteraction.class);
		List<Object[]> list = query.list();
		trns.commit();

		//generate list of just questions, with answers set
		ArrayList<Question> questionList = new ArrayList<Question>();
		Iterator<Object[]> i = list.iterator();
		while (i.hasNext()) {
			Object[] o = i.next();
			Question quest = (Question) o[0];
			UserQuestionInteraction uqi = (UserQuestionInteraction) o[1];
			quest.setCurrentUserInteraction(uqi);
			questionList.add(quest);
		}

		return questionList;
	}
}
