package org.smilec.smile.global.hibernate.logic;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtil {

	private static SessionFactory sessionFactory = null;

	/*SessionFactory is created per application, not per client, as per 
	Hibernate API: 
		SessionFactory is an expensive-to-create, threadsafe object,
		intended to be shared by all application threads. It is created once,
	 	usually on application startup, from a Configuration instance. */
	//Session per Client is currently used
	//TODO: Set hibernate.current_session_context_class to SmileApplication instances?
	//TODO: Check if session per question model is better
	public static void init() {
		if (sessionFactory == null) {
			try {
				sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
				System.out.println("HibernateUtil: SessionFactory created.");
			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed. " + ex);
				throw new ExceptionInInitializerError(ex);
			}
		}
		//System.out.println("HibernateUtil: SessionFactory exists.");
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory != null) {
			return sessionFactory;
		} else {
			init();
			return sessionFactory;
		}
	}

	public static void closeSessionFactory() {
		if (sessionFactory != null) {
			try {
				sessionFactory.close();
			} catch (Exception e) {
				sessionFactory = null;
				System.out.println(e);
			}
		}
		sessionFactory = null;
	}
}
