package org.smilec.smile.global.hibernate.logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import org.smilec.smile.global.hibernate.Address;
import org.smilec.smile.global.hibernate.Badge;
import org.smilec.smile.global.hibernate.Country;
import org.smilec.smile.global.hibernate.Evite;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.PasswordChangeRequest;
import org.smilec.smile.global.hibernate.QuestionComment;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;
import org.smilec.smile.global.hibernate.UserStats;

public class UserDAO {
	public final static int NO_USERNAME_ERROR = 37;
	public final static int USERNAME_FOUND = 42;
	private static final String key = "21232f297a57a5a743894a0e4a801fc3"; //used for MySQL AES encryption

	public static UserStats generateFreshUserStats() {
		UserStats userStats = new UserStats();
		Calendar cal = Calendar.getInstance();
		userStats.setCreatedOn(cal.getTime());
		userStats.setLastAccessed(cal.getTime());
		userStats.setNumSessions(0);
		userStats.setCumulativeSecs(0);
		userStats.setAvgSessionSecs(0);
		return userStats;
	}

	public static Address generateFreshAddress() {
		Address address = new Address();
		return address;
	}

	public static Integer checkUsername(String username) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			Query query = session.createQuery("from User where username = :username");
			query.setParameter("username", username);
			List<User> list = query.list();
			trns.commit();
			if (list.size() == 0) {
				//if no Users match query
				return UserDAO.NO_USERNAME_ERROR;
			}
			return UserDAO.USERNAME_FOUND;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return null;
		}
	}

	public static List<User> getUserByEmail(String email) {
		List<User> list = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();

		try {
			Query query = session.createQuery("from User where email = :email");
			query.setParameter("email", email);
			list = query.list();

			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static User getUserById(int id) {
		User user = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			Query query = session.createQuery("from User where id = :id");
			query.setParameter("id", id);
			List<User> list = query.list();
			if (list.size() > 0) {
				user = list.get(0);
			}
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return user;
	}

	public static String getUserName(int userId) {
		String username = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			Query query = session.createQuery("select username from User where id = :id");
			query.setParameter("id", userId);
			List<String> list = query.list();
			if (list.size() > 0) {
				username = list.get(0);
			}
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return username;
	}

	public static List<String> getUserNames(ArrayList<Integer> senderIds) {
		List<String> usernames = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		String ss = senderIds.toString();
		ss = ss.substring(1, ss.length() - 1);
		try {
			SQLQuery query =
					session.createSQLQuery("select username from User where id in (" + ss + ");");
			query.addScalar("username", StringType.INSTANCE);
			List<String> list = query.list();
			usernames = list;
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		return usernames;
	}

	public static User getUserByUserName(String username) {
		User user = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			Query query = session.createQuery("from User where username = :username");
			query.setParameter("username", username);
			List<User> list = query.list();
			if (list.size() > 0) {
				user = list.get(0);
			}
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return user;
	}

	public static User createUser(User user, String password) {
		//create new userStats and address foreign rows
		UserStats userStats = UserDAO.createUserStats(UserDAO.generateFreshUserStats());
		Address address = UserDAO.createAddress(UserDAO.generateFreshAddress());
		user.setUserStats(userStats);
		user.setAddress(address);
		//then create rest of user
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();

		int userType = 1; // 1: user, 0: admin. Admin users can only be created from admin panel
		String sql =
				"insert into User (username, password, email, firstName, lastName, phone, photo_url, userStats_id, address_id, userType)"
						+ " values('"
						+ user.getUsername()
						+ "',AES_ENCRYPT('"
						+ password
						+ "','"
						+ key
						+ "'),'"
						+ user.getEmail()
						+ "','"
						+ user.getFirstName()
						+ "','"
						+ user.getLastName()
						+ "','"
						+ user.getPhone()
						+ "','"
						+ user.getPhoto_url()
						+ "',"
						+ userStats.getId() + "," + user.getAddress().getId() + "," + userType + ")";
		try {
			Query query = session.createSQLQuery(sql);
			query.executeUpdate();
			trns.commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		}
		return user;
	}

	public static void updateUser(User user) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		session.merge(user.getAddress());

		String sql =
				"UPDATE User SET email='" + user.getEmail() + "', firstName='" + user.getFirstName()
						+ "'," + " lastName='" + user.getLastName() + "'," + " phone='"
						+ user.getPhone() + "'," + " photo_url='" + user.getPhoto_url() + "',"
						+ " profilePublic = " + user.getProfilePublic() + " WHERE id=" + user.getId();
		Query query = session.createSQLQuery(sql);
		query.executeUpdate();

		//session.merge(user);		
		trns.commit();
	}

	public static UserStats createUserStats(UserStats userStats) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			session.save(userStats);
			trns.commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//only necessary if .openSession() is used instead of getCurrentSession()
			//session.flush();
			//session.close();
		}
		return userStats;
	}

	public static void updateUserStats(UserStats stats) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();

		session.merge(stats);

		trns.commit();
	}

	public static Address createAddress(Address address) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			session.save(address);
			trns.commit();
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//only necessary if .openSession() is used instead of getCurrentSession()
			//session.flush();
			//session.close();
		}
		return address;
	}

	public static User login(String username, String password) {
		User user = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			Query query =
					session.createQuery("from User where username = :username and password=AES_ENCRYPT(:password, :key)");
			query.setParameter("username", username);
			query.setParameter("password", password);
			query.setParameter("key", key);
			List<User> list = query.list();
			if (list.size() > 0) {
				user = list.get(0);
			}
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return user;
	}

	public static void setPassword(User user, String password) {
		//sets and updates password
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			Query query =
					session.createQuery("update User set password=AES_ENCRYPT(:password, :key) where id = :user_id");
			query.setParameter("user_id", user.getId());
			query.setParameter("password", password);
			query.setParameter("key", key);
			query.executeUpdate();
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public static String getPassword(User user) {
		return getPassword(user.getId());
	}

	public static String getPassword(int id) {
		String user = null;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query =
				session.createSQLQuery(
						"select AES_DECRYPT(password,'" + key + "') as password from User where id="
								+ id).addScalar("password", StringType.INSTANCE);

		List<String> list = query.list();
		trns.commit();

		if (list.size() > 0) {
			user = list.get(0);
			//System.out.println(user);
		}

		return user;
	}

	//saves a user question interaction
	public static void saveUserQuestionIneraction(UserQuestionInteraction ui) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			session.save(ui);
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (trns != null) {
				trns.rollback();
			}
		}
	}

	public static void saveQuestionComment(QuestionComment qc) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			session.save(qc);
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (trns != null) {
				trns.rollback();
			}
		}
	}

	public static List<Country> getCountries() {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Country");
		List<Country> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Badge> getBadges() {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Badge");
		List<Badge> list = query.list();

		trns.commit();

		return list;
	}

	public static List<Evite> getEvites(int userId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Evite where user_id = :userId");
		query.setParameter("userId", userId);
		List<Evite> list = query.list();

		trns.commit();

		return list;
	}

	public static int countEvites(int userId) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		int count = 0;
		SQLQuery query =
				session.createSQLQuery(
						"select count(id) as count from Evite where user_id = " + userId + ";")
						.addScalar("count", IntegerType.INSTANCE);
		List<Integer> list = query.list();
		if (list.size() > 0)
			count = list.get(0);
		trns.commit();

		return count;
	}

	public static List<Object[]> getEviteInfo(int userId) {
		//returns [Group, inviteId, organizer username]
		//select Evite.*, GroupTable.*, User.username from Evite, GroupTable, User where Evite.group_id = GroupTable.id and Evite.user_id = User.id and Evite.user_id = 3001;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		SQLQuery query =
				session.createSQLQuery(
						"select Evite.id as eId, GroupTable.*, User.username "
								+ "from Evite, GroupTable, User where Evite.group_id = "
								+ "GroupTable.id and GroupTable.organizer_id = User.id and "
								+ "Evite.user_id = " + userId).addEntity("GroupTable", Group.class);
		query.addScalar("eId", org.hibernate.type.IntegerType.INSTANCE);
		query.addScalar("username", org.hibernate.type.StringType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();

		return list;
	}

	public static PasswordChangeRequest sendPCR(Integer uid, String token) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		PasswordChangeRequest r = new PasswordChangeRequest();
		r.setUser_id(uid);
		r.setCreatedOn(new Date());
		r.setToken(token);
		session.save(r);

		trns.commit();

		return r;
	}

	public static PasswordChangeRequest getPCR(int id, String token) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession(); // openSession()
		Transaction trns = session.beginTransaction();
		Query query =
				session.createQuery("from PasswordChangeRequest where id = :id AND token = :token");
		query.setParameter("id", id);
		query.setParameter("token", token);

		List<PasswordChangeRequest> list = query.list();
		trns.commit();

		PasswordChangeRequest r = null;

		if (list.size() > 0) {
			r = list.get(0);
		}

		return r;
	}

	public static void deletePCR(int id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("delete PasswordChangeRequest where id = :id ");
		query.setParameter("id", id);
		query.executeUpdate();

		trns.commit();
	}

	public static String getCountryName(Integer id) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		Query query = session.createQuery("from Country where id=:id");
		query.setParameter("id", id);
		List<Country> list = query.list();
		trns.commit();
		Country c;
		String name = "";

		if (list.size() > 0) {
			c = list.get(0);
			name = c.getName();
		}

		return name;
	}

	public static Object[] questionCreated(Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		// int num = ( (Long) session.createQuery("select count(*) from Question where user_id="+uid).iterate().next() ).intValue();
		SQLQuery query =
				session.createSQLQuery("select count(id) as num, avg(avgRating) as rating, avg(avgCorrect) as correct, avg(avgSecComplete) as complete, sum(isPublic) as numPublic from Question where user_id="
						+ uid);
		query.addScalar("num", IntegerType.INSTANCE);
		query.addScalar("rating", FloatType.INSTANCE);
		query.addScalar("correct", FloatType.INSTANCE);
		query.addScalar("complete", FloatType.INSTANCE);
		query.addScalar("numPublic", IntegerType.INSTANCE);
		List<Object[]> list = query.list();

		trns.commit();

		return list.get(0);
	}

	public static Object[] questionAnswered(Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		//int num = ( (Long) session.createQuery("select count(*) from UserQuestionInteraction where user_id="+uid).iterate().next() ).intValue();

		SQLQuery query =
				session.createSQLQuery("select count(user_id) as num, avg(rating) as rating, avg(SecComplete) as complete from UserQuestionInteraction where user_id="
						+ uid);
		query.addScalar("num", Hibernate.INTEGER);
		query.addScalar("rating", Hibernate.FLOAT);
		query.addScalar("complete", Hibernate.FLOAT);
		List<Object[]> list = query.list();

		trns.commit();

		return list.get(0);
	}

	public static Integer questionAnsweredCorrectly(Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		//int num = ( (Long) session.createQuery("select count(*) from UserQuestionInteraction where user_id="+uid).iterate().next() ).intValue();

		SQLQuery query =
				session.createSQLQuery("select count(user_id) as num from QuestionAnswer, UserQuestionInteraction where userAnswer_id=QuestionAnswer.id and QuestionAnswer.isCorrect=1 and user_id="
						+ uid);
		query.addScalar("num", Hibernate.INTEGER);
		List<Integer> list = query.list();

		trns.commit();

		return list.get(0);
	}

	public static Integer getNumComments(Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();
		SQLQuery query =
				session.createSQLQuery("select count(id) as num from QuestionComment where user_id = "
						+ uid);
		query.addScalar("num", Hibernate.INTEGER);
		List<Integer> list = query.list();

		trns.commit();

		return list.get(0);
	}

	public static Integer getNumSessions(Integer uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		String sql =
				"select Quiz.id as id, SUM(Question.id) as num from Question, QuizQuestion, Quiz where Question.id=QuizQuestion.question_id"
						+ " and QuizQuestion.quiz_id=Quiz.id and Question.user_id="
						+ uid
						+ " GROUP BY Quiz.id";
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("id", Hibernate.INTEGER);
		query.addScalar("num", Hibernate.INTEGER);
		List<Integer> list = query.list();

		trns.commit();

		return list.size();
	}

	public static List<String> getUserTagText(int uid, String tagText) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		String sql =
				"select tagText from Question where user_id=" + uid + " and MATCH(tagText) AGAINST('"
						+ tagText + "');";
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("tagText", StringType.INSTANCE);
		List<String> list = query.list();

		trns.commit();

		return list;
	}

	public static Integer countQuestionsWithMedia(int uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		String sql =
				"select count(id) as num from Question where user_id=" + uid
						+ " and media_url is not null and media_url != '';";
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("num", IntegerType.INSTANCE);
		List<Integer> list = query.list();

		trns.commit();
		Integer num = 0;
		if (list.size() > 0) {
			num = list.get(0);
		}
		return num;
	}

	public static Integer countGroupsCreated(int uid) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction trns = session.beginTransaction();

		String sql = "select count(id) as num from GroupTable where organizer_id=" + uid + ";";
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("num", IntegerType.INSTANCE);
		List<Integer> list = query.list();

		trns.commit();
		Integer num = 0;
		if (list.size() > 0) {
			num = list.get(0);
		}
		return num;
	}
}
