package org.smilec.smile.global.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QuizOption")
public class QuizOption implements Serializable {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "quiz_id")
	private Integer quiz_id;

	@Column(name = "quiz_option")
	private String quiz_option;

	@Column(name = "quiz_value")
	private String quiz_value;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuiz_id() {
		return quiz_id;
	}

	public void setQuiz_id(Integer quiz_id) {
		this.quiz_id = quiz_id;
	}

	public String getQuiz_option() {
		return quiz_option;
	}

	public void setQuiz_option(String quiz_option) {
		this.quiz_option = quiz_option;
	}

	public String getQuiz_value() {
		return quiz_value;
	}

	public void setQuiz_value(String quiz_value) {
		this.quiz_value = quiz_value;
	}
}