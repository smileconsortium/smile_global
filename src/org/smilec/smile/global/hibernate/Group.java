package org.smilec.smile.global.hibernate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "GroupTable")
public class Group implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "organization_id")
	private Integer organization_id;
	@Column(name = "name")
	private String name;
	@Column(name = "passcode")
	private String passcode;
	@Column(name = "organizer_id")
	private Integer organizer_id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdOn")
	private Date createdOn;
	
	public boolean isOwner(User user) {
		if (user.getId().equals(organizer_id)) {
			return true;
		}
		return false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrganization_id() {
		return organization_id;
	}

	public void setOrganization_id(Integer organization_id) {
		this.organization_id = organization_id;
	}

	public String getName() {
		return name;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public String getPasscode() {
		return passcode;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getOrganizer_id() {
		return organizer_id;
	}

	public void setOrganizer_id(Integer organizer_id) {
		this.organizer_id = organizer_id;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
