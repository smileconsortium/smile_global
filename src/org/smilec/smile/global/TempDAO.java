package org.smilec.smile.global;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.smilec.smile.global.hibernate.Group;
import org.smilec.smile.global.hibernate.Question;
import org.smilec.smile.global.hibernate.QuestionAnswer;
import org.smilec.smile.global.hibernate.Quiz;
import org.smilec.smile.global.hibernate.User;
import org.smilec.smile.global.hibernate.UserQuestionInteraction;
import org.smilec.smile.global.hibernate.logic.GroupDAO;
import org.smilec.smile.global.hibernate.logic.HibernateUtil;
import org.smilec.smile.global.hibernate.logic.QuestionDAO;
import org.smilec.smile.global.hibernate.logic.QuizDAO;
import org.smilec.smile.global.hibernate.logic.UserDAO;
import org.smilec.smile.global.ui.QuestionInteractionLogic;
import org.smilec.smile.global.utils.Utils;

//For Development Testing 

public class TempDAO {

	public static void main(String[] args) throws Exception {
		//localization tests

		String s = getFormattedMsg("passcodeLengthPrompt", 45);
		System.out.println(s);

		//HibernateUtil.init();
		//generateDummyUserStats(3000);
		//generateDummyQuestions(6000);
		//updateAvatars(3000);
		//generateDummyInteractions(90000);

		/*User user = UserDAO.getUserById(3001);
		List<Group> myGroups = GroupDAO.getGroups(user);
		Iterator<Group> i = myGroups.iterator();
		while (i.hasNext()) {
			Group g = i.next();
			System.out.println(g.getName());
			System.out.println(g.getId());
			System.out.println(g.isOrganizer(user));
			System.out.println(GroupDAO.countQuizzes(g));
			List<Quiz> quizzes = GroupDAO.getQuizzes(g);
			Iterator<Quiz> ii = quizzes.iterator();
			while (ii.hasNext()) {
				Quiz quiz = ii.next();
				System.out.println(quiz.getName());
			}
		}
		*/
		//create i quiz questions

		//int quizId = 3037;
		/*for (int i = 0; i < 50; i++) {
			//int userId = Utils.random(2970, 3006); //Group 2
			int userId = Utils.random(2970, 3000);
			if (userId != 3001 && userId != 3002) {
				createQuizQuestion(userId, quizId);
			}
		}*/
		//create l interactions
		//for (int l = 0; l < 300; l++) {
		//createQuizQuestionInteraction(quizId);
		//}

		//GroupResults.getStudentReport(3037);

		//add users to group 
		/*
		Group g = GroupDAO.getGroupById(7);
		for (int i = 2970; i < 3000; i++) {
			GroupDAO.addUserToGroup(g, UserDAO.getUserById(i));
		}*/
		/*
		Question q = QuestionDAO.getQuestionById(815);
		Quiz quiz = QuizDAO.getQuizByQuestion(q);
		*/

		//Simulate a whole sync session
		/*
		Group g = GroupDAO.getGroupById(1);

		Quiz quiz = new Quiz();
		quiz.setCreator_id(1);
		quiz.setGroup_id(1);
		quiz.setIsPublic(0);
		quiz.setName("TestSession - " + Math.random());
		quiz.setShowFeedback(0);
		quiz.setStatus(StatusEnum.closed);
		QuizOption[] options = new QuizOption[1];
		QuizOption exam_option = new QuizOption();
		exam_option.setQuiz_option("exam");
		exam_option.setQuiz_value("0");
		options[0] = exam_option;
		QuizDAO.createQuiz(quiz, options);

		//generate questions
		String[] users =
				{ "noahh", "mcook", "rumbi", "Shemiah", "vnyamunda", "Shemiah", "Pride", "Annah1",
						"mcook", "jhakim", "jonathan", "miriro", "Pride", "noah", "noah" };
		String[] questionText =
				{ "Where are lymphocytes created?", "In what city does Noah Freedman live?",
						"What is immunoglobin?", "What is Shemiah's surname?",
						"Which one is a web browser?", "What are we having for lunch?",
						"What are the functions of B lymphocyte surface proteins?",
						"What is immumoglobin?",
						"What school within Stanford University does Noah work for?",
						"The following characteristics of lymphocytes are correct ",
						"Which of the following is true about membrane proteins on the B lymphocytes? ",
						"What is nongoncocal nurethrisis?",
						"How many teeth  should  a six year old have?",
						"How did the session go on a scale of 1-4 \n1 = poor\n4 = excellent",
						"Which Airline is pictured?" };
		String[] answerA =
				{ "Bone marrow", "Stanford, CA", "a cell", "Hakim", "Android", "Sadza", "antibodies",
						"Antibiotic", "Medical School", "they are produced in the thyroid gland",
						"They are all the same on different B lymphocytes", "Dunno", "12", "1",
						"South African Airlines" };
		String[] answerB =
				{ "White blood cells", "San Francisco, CA", "a white blood cell", "Mugonda",
						"Google Chrome", "Nandos", "antibiotics", "Bone marrow",
						"School of Engineering", "they are produced in lymph nodes", "2, ",
						"Ask a medical expert", "20", "2", "Air Zimbabwe" };
		String[] answerC =
				{ "The inner chamber of the heart", "Palo Alto, CA", "a red blood cell", "Mashaah",
						"Windows 7", "Nothing", "nutrients", "White blood cells", "School of Business",
						"are never all of one type",
						"They differ by a small portion on each B lymphocyte", "", "32", "3",
						"US Airways" };
		String[] answerD =
				{ "They spontaneously appear in the blood", "Cupertino, CA", "other", "Nyaude",
						"Ubuntu", "Water", "detectors", "Red blood cel", "Graduate School of Education",
						"they carry oxygen and carbon dioxide ", "They are are called Immunoglobulins",
						"", "24", "4", "Cathay Pacific" };
		String[] answerE =
				{ "", "", "", "", "", "", "", "", "", "",
						"They are not on the membrane but in the nucleus", "", "", "", "" };
		String[] answerF = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
		int[][] correct =
				{ { 1 }, { 3 }, { 4 }, { 4 }, { 2 }, { 2 }, { 1 }, { 1 }, { 4 }, { 2 }, { 3 }, { 2 },
						{ 4 }, { 1, 2, 4 }, { 2 } };

		//Create Questions
		for (int i = 0, l = questionText.length; i < l; i++) {
			int uid = UserDAO.getUserByUserName(users[i]).getId();
			createQuizQuestion(uid, quiz.getId(), questionText[i], correct[i], answerA[i], answerB[i],
					answerC[i], answerD[i], answerE[i], answerF[i]);
		}

		//after creating all questions, answer all questions
		int counter = 1;
		while (counter > 99) {
			//createQuizQuestionInteraction(quiz.getId());
			//get user
			Group group = GroupDAO.getGroupById(quiz.getGroup_id());
			List<User> users1 = GroupDAO.getGroupUsers(group);
			User user = users1.get(Utils.random(0, users1.size() - 1));
			//create ui interaction
			UserQuestionInteraction ui = new UserQuestionInteraction();
			ui.setUser(user);
			//get question
			List<Question> questions = QuizDAO.getQuizQuestions(quiz.getId());
			Question question = questions.get(Utils.random(0, questions.size() - 1));
			//check that haven't already answered question
			Question qCheck = QuestionDAO.getQuestionAndInteractionById(question.getId(), user.getId());
			if (qCheck.getCurrentUserInteraction() != null) {
				//already answered
				return;
			}
			ui.setQuestion(question);
			ArrayList<QuestionAnswer> answers = question.getQuestionAnswerList();
			QuestionAnswer qa = answers.get(Utils.random(0, answers.size() - 1));
			ui.setUserAnswer(qa);
			ui.setCreatedOn(Utils.getTime());
			//random rating and time to complete
			int rating = Utils.random(2, 10);
			Integer secComplete = Utils.random(1, 90);
			ui.setSecComplete(secComplete);
			ui.setRating(rating);
			UserDAO.saveUserQuestionIneraction(ui);
			QuestionInteractionLogic.updateQuestionInteractionStats(ui);
			counter--;
		}
		/*
		User user = ((SmileApplication) AppData.getApplication()).getUser();
		//create ui interaction
		UserQuestionInteraction ui = new UserQuestionInteraction();
		ui.setUser(user);
		ui.setQuestion(question);
		ui.setUserAnswer(qa);
		ui.setCreatedOn(Utils.getTime());
		if (rating > 0) {
			ui.setRating(rating * 2); //rating is based on 10 to allow for future possibility of half star ratings
		}
		if (secComplete != null) {
			ui.setSecComplete(secComplete);
		}
		UserDAO.saveUserQuestionIneraction(ui);
		QuestionInteractionLogic.updateQuestionInteractionStats(ui);
		*/
	}

	public static Locale getLocale() {
		return new Locale("en", "US");
	}

	public static ResourceBundle getBundle() {
		return ResourceBundle.getBundle("resources/MessagesBundle");
	}

	public static String getFormattedMsg(String patternId, Object[] args) {
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(getLocale());
		formatter.applyPattern(getBundle().getString(patternId));
		return formatter.format(args);
	}

	public static String getFormattedMsg(String patternId, String media_url) {
		return getFormattedMsg(patternId, new Object[] { media_url });
	}

	public static String getFormattedMsg(String patternId, Integer number) {
		return getFormattedMsg(patternId, new Object[] { number });
	}

	public static void createQuizQuestion(int userId, int quizId, String questionText, int[] correct,
			String answerA, String answerB, String answerC, String answerD, String answerE,
			String answerF) {
		//Question submit successfully
		Question q = new Question();
		q.setUserId(userId);
		q.setQuestionText(questionText);
		Calendar cal = Calendar.getInstance();
		q.setCreatedOn(cal.getTime());
		q.setUniqueAnswers(0);
		q.setTagText("");
		QuestionDAO.createQuestion(q);
		//add to quiz if creating question in quiz
		QuizDAO.addQuestionToQuiz(quizId, q.getId());
		//create question in sql database and get its new id
		//create QuestionAnswer obj's
		ArrayList<String> answers = new ArrayList<String>();
		if (answerA != null && answerA.length() > 0)
			answers.add(answerA);
		if (answerB != null && answerB.length() > 0)
			answers.add(answerB);
		if (answerC != null && answerC.length() > 0)
			answers.add(answerC);
		if (answerD != null && answerD.length() > 0)
			answers.add(answerD);
		if (answerE != null && answerE.length() > 0)
			answers.add(answerE);
		if (answerF != null && answerF.length() > 0)
			answers.add(answerF);
		for (int i = 0; i < answers.size(); i++) {
			QuestionAnswer answer = new QuestionAnswer();
			answer.setAnswerText(answers.get(i));
			int _correct = 0;
			for (int j = 0, m = correct.length; j < m; j++) {
				if (correct[j] == i)
					_correct = 1;
			}
			answer.setIsCorrect(_correct);
			answer.setQuestion(q);
			answer.setNumResponses(0);
			QuestionDAO.createQuestionAnswer(answer);
		}
	}

	public static void createQuizQuestionInteraction(int quizId) {
		Quiz quiz = QuizDAO.getQuizById(quizId);
		Group group = GroupDAO.getGroupById(quiz.getGroup_id());
		List<User> users = GroupDAO.getGroupUsers(group);
		User user = users.get(Utils.random(0, users.size() - 1));
		if (group.getOrganizer_id().equals(user.getId())) {
			//organizer can't answer question
			return;
		}
		//Quiz
		List<Question> questions = QuizDAO.getQuizQuestions(quiz.getId());
		Question question = questions.get(Utils.random(0, questions.size() - 1));
		//check that haven't already answered question
		Question qCheck = QuestionDAO.getQuestionAndInteractionById(question.getId(), user.getId());
		if (qCheck.getCurrentUserInteraction() != null) {
			//already answered
			return;
		}
		//create ui interaction
		UserQuestionInteraction ui = new UserQuestionInteraction();
		ui.setUser(user);
		ui.setQuestion(question);
		ArrayList<QuestionAnswer> answers = question.getQuestionAnswerList();
		ui.setUserAnswer(answers.get(Utils.random(0, answers.size() - 1)));
		ui.setCreatedOn(Utils.getTime());
		ui.setRating(Utils.random(2, 10));
		//fake time to complete
		int secComplete = Utils.random(1, 90);
		ui.setSecComplete(secComplete);
		UserDAO.saveUserQuestionIneraction(ui);
		QuestionInteractionLogic.updateQuestionInteractionStats(ui);
	}

	public static void updateAvatars(int totalUsers) {
		for (int i = 1; i <= totalUsers; i++) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			Transaction trns = session.beginTransaction();
			try {
				//add new userStats to all users
				int rand = Utils.random(0, 18);
				String avatarString = "avatars/avatar" + rand + ".jpg";
				int userId = i;
				Query query =
						session.createSQLQuery("update user set photo_url = '" + avatarString
								+ "' where user.id = " + userId + ";");
				query.executeUpdate();
				//commit changes
				trns.commit();
			} catch (RuntimeException e) {
				e.printStackTrace();
				if (trns != null) {
					trns.rollback();
				}
			}
		}
	}

	public static void generateDummyUserStats(int howMany) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		try {
			//add new userStats to all users
			for (int i = 0; i < howMany; i++) {
				session.save(UserDAO.generateFreshUserStats());
			}
			//commit changes
			trns.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (trns != null) {
				trns.rollback();
			}
		}
	}

	public static void generateDummyInteractions(int howMany) {
		for (int i = 0; i < howMany; i++) {
			//get random user and random question
			User user = getRandomUser();
			Question question = getRandomQuestion();
			//create ui interaction
			UserQuestionInteraction ui = new UserQuestionInteraction();
			ui.setUser(user);
			ui.setQuestion(question);
			Object[] answers = question.getQuestionAnswerList().toArray();
			ui.setUserAnswer((QuestionAnswer) answers[Utils.random(0, answers.length - 1)]);
			ui.setCreatedOn(Utils.getTime());
			ui.setRating(Utils.random(1, 10));
			//fake time to complete
			int secComplete = Utils.random(1, 90);
			ui.setSecComplete(secComplete);

			UserDAO.saveUserQuestionIneraction(ui);
			QuestionInteractionLogic.updateQuestionInteractionStats(ui);
		}
	}

	public static void generateDummyQuestions(int howMany) {
		//Math questions
		double maxNumber = 100;
		double minNumber = 0;
		double minAnswers = 2;
		double maxAnswers = 6;
		String[] operations = { "x", "+", "-", "���" };

		for (int i = 0; i < howMany; i++) {
			//generate question and answers
			String qText = "";
			int int1 = (int) ((maxNumber - minNumber + 1) * Math.random() + minNumber);
			int int2 = (int) ((maxNumber - minNumber + 1) * Math.random() + minNumber);
			int operation = (int) (Math.random() * (operations.length - 1));
			int correctAnswer = 0;
			switch (operation) {
			case 0:
				correctAnswer = int1 * int2;
				qText = int1 + " x " + int2 + " = ";
				break;
			case 1:
				correctAnswer = int1 + int2;
				qText = int1 + " + " + int2 + " = ";
				break;
			case 2:
				correctAnswer = int1 - int2;
				qText = int1 + " - " + int2 + " = ";
				break;
			case 3:
				correctAnswer = int1 / int2;
				qText = int1 + " ��� " + int2 + " = ";
				break;
			}
			Question q = new Question();
			User user = getRandomUser();
			q.setUserId(user.getId());
			q.setQuestionText(qText);
			q.setUniqueAnswers(0);
			Calendar cal = Calendar.getInstance();
			q.setCreatedOn(cal.getTime());
			q.setUniqueAnswers(0);
			q.setTagText("");
			//q.setIsPublic(0);
			QuestionDAO.createQuestion(q);
			int numAnswers =
					(int) (Math.floor(Math.random() * (maxAnswers - minAnswers + 1)) + minAnswers);
			ArrayList<QuestionAnswer> answers = new ArrayList<QuestionAnswer>();
			//generate incorrect answers
			for (int j = 0; j < numAnswers; j++) {
				QuestionAnswer answer = new QuestionAnswer();
				answer.setQuestion(q);
				answer.setAnswerText(Integer.toString((int) ((maxNumber - minNumber + 1) * Math.random() + minNumber)));
				answer.setNumResponses(0);
				answer.setIsCorrect(0);
				answers.add(answer);
			}
			//generate correct answer
			QuestionAnswer correct = new QuestionAnswer();
			correct.setQuestion(q);
			correct.setAnswerText(correctAnswer + "! ");
			correct.setNumResponses(0);
			correct.setIsCorrect(1);
			answers.add(correct);
			//save answers in random order
			Collections.shuffle(answers);
			for (int j = 1; j < answers.size(); j++) {
				QuestionDAO.createQuestionAnswer(answers.get(j));
			}
		}
	}

	public static Object[] generateDummyQuestion(int userId) {
		//returns Question, ArrayList<QuestionAnswer>
		//Math questions
		double maxNumber = 100;
		double minNumber = 0;
		double minAnswers = 2;
		double maxAnswers = 6;
		String[] operations = { "x", "+", "-", "���" };

		//generate question and answers
		String qText = "";
		int int1 = (int) ((maxNumber - minNumber + 1) * Math.random() + minNumber);
		int int2 = (int) ((maxNumber - minNumber + 1) * Math.random() + minNumber);
		int operation = (int) (Math.random() * (operations.length - 1));
		int correctAnswer = 0;
		switch (operation) {
		case 0:
			correctAnswer = int1 * int2;
			qText = int1 + " x " + int2 + " = ";
			break;
		case 1:
			correctAnswer = int1 + int2;
			qText = int1 + " + " + int2 + " = ";
			break;
		case 2:
			correctAnswer = int1 - int2;
			qText = int1 + " - " + int2 + " = ";
			break;
		case 3:
			correctAnswer = int1 / int2;
			qText = int1 + " ��� " + int2 + " = ";
			break;
		}
		Question q = new Question();
		q.setUserId(userId);
		q.setQuestionText(qText);
		q.setUniqueAnswers(0);
		Calendar cal = Calendar.getInstance();
		q.setCreatedOn(cal.getTime());
		q.setUniqueAnswers(0);
		q.setTagText("");
		q.setIsPublic(0);
		int numAnswers = (int) (Math.floor(Math.random() * (maxAnswers - minAnswers + 1)) + minAnswers);
		ArrayList<QuestionAnswer> answers = new ArrayList<QuestionAnswer>();
		//generate incorrect answers
		for (int j = 0; j < numAnswers; j++) {
			QuestionAnswer answer = new QuestionAnswer();
			answer.setQuestion(q);
			answer.setAnswerText(Integer.toString((int) ((maxNumber - minNumber + 1) * Math.random() + minNumber)));
			answer.setNumResponses(0);
			answer.setIsCorrect(0);
			answers.add(answer);
		}
		//generate correct answer
		QuestionAnswer correct = new QuestionAnswer();
		correct.setQuestion(q);
		correct.setAnswerText(correctAnswer + "! ");
		correct.setNumResponses(0);
		correct.setIsCorrect(1);
		answers.add(correct);
		//save answers in random order
		Collections.shuffle(answers);
		return new Object[] { q, answers };
	}

	public static User getRandomUser() {
		User user = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from User order by rand()").setMaxResults(1);
		List<User> list = query.list();
		trns.commit();
		if (list.size() > 0) {
			user = list.get(0);
		}
		return user;
	}

	public static Question getRandomQuestion() {
		Question question = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction trns = session.beginTransaction();
		Query query = session.createQuery("from Question order by rand()").setMaxResults(1);
		List<Question> list = query.list();
		trns.commit();
		if (list.size() > 0) {
			question = list.get(0);
		}
		return question;
	}
}
