package org.smilec.smile.global.js;

import java.io.Serializable;

public class JSLib implements Serializable {
	//Calls function in resize.js to set the minimum height that the Vaadin window can be shrunk to
	public static String setMinContentHeight(int height) {
		return "setMinimumContentHeight(" + height + ");";
	}

	//Calls function in resize.js to set the minimum height that the Vaadin window can be shrunk to
	//height is greater of minimum height or height of object refrenced by jquery handle
	public static String setMinContentHeight(int height, String jQueryHandle, int additionalHeight) {
		return "setMinimumContentHeightObj(" + height + ", '" + jQueryHandle + "', " + additionalHeight
				+ ");";
	}
	//Calls function in interaction.js to give a button mouseover, mouseout, and mousedown functions
}
